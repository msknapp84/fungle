package fungle.common.core.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public final class DateGuesser {
	private DateGuesser() {}
	
	public static Calendar guessCalendar(String time) {
		Calendar c = Calendar.getInstance();
		if (time.matches("\\d+")) {
			Long t = Long.parseLong(time);
			c.setTimeInMillis(t);
			return c;
		} else if (time.matches("\\d{4}\\D?\\d{2}\\D?\\d{2}")) {
			String t = time.replaceAll("[\\-/:]","");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			try {
				Date d = sdf.parse(t);
				c.setTime(d);
				return c;
			} catch (ParseException e) {
			}
		}else if (time.matches("\\d{4}\\D?\\d{2}\\D?\\d{2}\\s*\\d{2}\\D?\\d{2}\\D?\\d{2}")) {
			String t = time.replaceAll("[\\-/:\\s]","");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			try {
				Date d = sdf.parse(t);
				c.setTime(d);
				return c;
			} catch (ParseException e) {
			}
		}
		return null;
	}

	public static Date guessDate(String string) {
		Calendar c = guessCalendar(string);
		if (c!=null) {
			return c.getTime();
		}
		return null;
	}

}
