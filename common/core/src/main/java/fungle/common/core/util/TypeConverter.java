package fungle.common.core.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public final class TypeConverter {
	private TypeConverter(){
		
	}
	
	public static <T> T convert(Object o,Class<?> clazz) {
		T t = null;
		if (o!=null) {
			if (clazz.isAssignableFrom(o.getClass())) {
				return (T)o;
			}
			if (clazz.isPrimitive()) {
				return (T) toPrimitive(o,clazz);
			}
			t = (T) tryPrimitiveWrapper(o, clazz);
		}
		if (t==null) {
			t=tryConstructor(o, clazz);
		}
		if (o!=null && t==null) {
			t=tryTemporal(o, clazz);
		}
		if (t==null) {
			t=tryStaticMethod(o, clazz, "valueOf");
		}
		if (t==null) {
			t=tryStaticMethod(o, clazz, "parse");
		}
		if (t==null) {
			t=tryStaticMethod(o, clazz, "newInstance");
		}
		if (t==null) {
			t=tryStaticMethod(o, clazz, "getInstance");
		}
		if (t==null) {
			t=tryStringConstructor(o, clazz);
		}
		if (t==null) {
			t=tryStaticMethod(o.toString(), clazz, "fromString");
		}
		if (t==null) {
			t=tryNoArgConstructor(o, clazz);
		}
		return t;
	}
	
	private static <T> T tryTemporal(Object o,Class<?> clazz) {
		if (Date.class.equals(clazz)) {
			if (Long.class.isAssignableFrom(o.getClass())) {
				Date d = new Date((long)o);
				return (T) d;
			} else if (Calendar.class.isAssignableFrom(o.getClass())) {
				return (T) ((Calendar)o).getTime();
			} else {
				return (T) DateGuesser.guessDate(o.toString());
			}
		} else if (Calendar.class.equals(clazz)) {
			if (Long.class.isAssignableFrom(o.getClass())) {
				Calendar c = Calendar.getInstance();
				c.setTimeInMillis((long)o);
				return (T) c;
			} else if (Calendar.class.isAssignableFrom(o.getClass())) {
				Calendar c = Calendar.getInstance();
				c.setTime((Date)o);
				return (T) c;
			} else {
				return (T) DateGuesser.guessCalendar(o.toString());
			}
		}
		return null;
	}

	private static <T> T tryStaticMethod(Object o, Class<?> clazz,String name) {
		T t = null;
		try {
			Method m = clazz.getDeclaredMethod(name, o.getClass());
			t = (T) m.invoke(null, o);
		} catch (Exception e) {
		}
		return t;
	}

	private static <T> T tryConstructor(Object o, Class<?> clazz) {
		T t = null;
		try {
			Constructor constructor = clazz.getConstructor(o.getClass());
			t = (T) constructor.newInstance(o);
		} catch (Exception e) {
		}
		return t;
	}

	private static <T> T tryStringConstructor(Object o, Class<?> clazz) {
		T t = null;
		try {
			Constructor constructor = clazz.getConstructor(String.class);
			t = (T) constructor.newInstance(o.toString());
		} catch (Exception e) {
		}
		return t;
	}

	private static <T> T tryNoArgConstructor(Object o, Class<?> clazz) {
		T t = null;
		try {
			t = (T) clazz.newInstance();
		} catch (Exception e) {
		}
		return t;
	}

	private static Object toPrimitive(Object o, Class<?> clazz) {
		if (boolean.class.equals(clazz)) {
			return Boolean.valueOf(o.toString());
		} else if (int.class.equals(clazz)) {
			return Integer.valueOf(o.toString());
		} else if (long.class.equals(clazz)) {
			return Long.valueOf(o.toString());
		} else if (short.class.equals(clazz)) {
			return Short.valueOf(o.toString());
		} else if (double.class.equals(clazz)) {
			return Double.valueOf(o.toString());
		} else if (float.class.equals(clazz)) {
			return Float.valueOf(o.toString());
		} else if (char.class.equals(clazz)) {
			return Character.valueOf(o.toString().charAt(0));
		} else if (byte.class.equals(clazz)) {
			return Byte.valueOf(o.toString());
		}
		return null;
	}

	private static Object tryPrimitiveWrapper(Object o, Class<?> clazz) {
		if (Boolean.class.equals(clazz)) {
			return Boolean.valueOf(o.toString());
		} else if (Integer.class.equals(clazz)) {
			return Integer.valueOf(o.toString());
		} else if (Long.class.equals(clazz)) {
			return Long.valueOf(o.toString());
		} else if (Short.class.equals(clazz)) {
			return Short.valueOf(o.toString());
		} else if (Double.class.equals(clazz)) {
			return Double.valueOf(o.toString());
		} else if (Float.class.equals(clazz)) {
			return Float.valueOf(o.toString());
		} else if (Character.class.equals(clazz)) {
			return Character.valueOf(o.toString().charAt(0));
		} else if (Byte.class.equals(clazz)) {
			return Byte.valueOf(o.toString());
		}
		return null;
	}

	public static Class<?> getPrimitive(Class<? extends Object> class1) {
		if (Boolean.class.equals(class1)) {
			return boolean.class;
		} else if (Byte.class.equals(class1)) {
			return byte.class;
		} else if (Double.class.equals(class1)) {
			return double.class;
		} else if (Float.class.equals(class1)) {
			return float.class;
		} else if (Character.class.equals(class1)) {
			return char.class;
		} else if (Long.class.equals(class1)) {
			return long.class;
		} else if (Integer.class.equals(class1)) {
			return int.class;
		} else if (Short.class.equals(class1)) {
			return short.class;
		}
		return null;
	}

	public static boolean isPrimitiveWrapper(Class<? extends Object> class1) {
		return (Boolean.class.equals(class1) ||
				Byte.class.equals(class1) ||
				Double.class.equals(class1) ||
				Float.class.equals(class1) ||
				Character.class.equals(class1) ||
				Long.class.equals(class1) ||
				Integer.class.equals(class1) ||
				Short.class.equals(class1));
	}
}
