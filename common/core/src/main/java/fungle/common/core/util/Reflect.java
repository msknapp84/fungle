package fungle.common.core.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Reflect {
	private static final Logger logger = LoggerFactory.getLogger(Reflect.class);
	
	private Reflect(){
		
	}

	public static void verifyHasField(Class<?> clazz,String fieldName) {
		Field f=getField(clazz, fieldName);
		if (f == null) {
			logger.trace("The class "+clazz+" was found to not have "+fieldName);
			throw new IllegalArgumentException("The field name "+fieldName+" on class "+clazz.getSimpleName()+" does not exist.");
		}
	}
	
	public static boolean fieldExistsLike(Class<?> clazz,String fieldName,Class<?> fieldType) {
		Field f=getField(clazz, fieldName);
		return f!=null && fieldType.equals(f.getType());
	}
	
	public static Field getField(Class<?> clazz,String fieldName){
		if (fieldName.contains(".")) {
			// allow paths to values delimited by periods.
			int i = fieldName.indexOf(".");
			String subObjectFieldName = fieldName.substring(0,i);
			String remainingFieldNamePath = fieldName.substring(i+1);
			Field tmp = getField(clazz, subObjectFieldName);
			if (tmp==null) {
				return null;
			}
			Class<?> subClass = tmp.getType();
			return getField(subClass,remainingFieldNamePath);
		}
		Field f = null;
		try {
			f = clazz.getDeclaredField(fieldName);
		} catch (Exception e) {
			
		}
		if (f == null) {
			Class<?> parent = clazz.getSuperclass();
			if (parent!=null) {
				f = getField(parent,fieldName);
			}
		}
		if (f==null && logger.isDebugEnabled()) {
			logger.debug("Reflect never found the field named "+fieldName+" on the class "+clazz.getName());
		}
		return f;
	}
	
	public static Collection<?> collect(Collection<?> objects,Field field) {
		return collect(objects,field.getName(),field.getType());
	}
	
	public static <T> Collection<T> collect(Collection<?> objects,String fieldName,Class<T> clazz) {
		List<T> results = new ArrayList<>();
		if (CollectionUtils.isEmpty(objects)) {
			return results;
		}
		for (Object o : objects) {
			T t = getFieldValue(fieldName, o, clazz);
			if (t!=null) {
				results.add(t);
			}
		}
		return results;
	}
	
	public static <T> T getFieldValue(String fieldName,Object instance,Class<T> returnType) {
		Object o = instance;
		String[] fieldParts = fieldName.split("\\.");
		for (String field : fieldParts) {
			o = getFieldValue_singleField(field, o, Object.class);
			if (o == null) {
				return null;
			}
		}
		return TypeConverter.convert(o, returnType);
	}
	
	private static <T> T getFieldValue_singleField(String fieldName,Object instance,Class<T> returnType) {
		if (instance==null) {
			return null;
		}
		Field field;
		try {
			field = getField(instance.getClass(), fieldName);
			if (field == null) {
				return null;
			}
			boolean acc = field.isAccessible();
			Object o;
			try {
				if (!acc) {
					field.setAccessible(true);
				}
				o = field.get(instance);
				
			} finally {
				if (!acc) {
					field.setAccessible(false);
				}
			}
			if (o == null) {
				return null;
			}
			return TypeConverter.convert(o, returnType);
		} catch (Exception e) {
		}
		return null;
	}
	
	public static void setFieldValue(String fieldName,Object instance,Object value) {
		Object o = instance;
		String[] fieldParts = fieldName.split("\\.");
		for (int i = 0;i<fieldParts.length-1;i++) {
			String field = fieldParts[i];
			o = getFieldValue_singleField(field, o, Object.class);
			if (o == null) {
				return;
			}
		}
		setFieldValue_singleField(fieldParts[fieldParts.length-1],o, value);
	}
	
	public static void setFieldValue_singleField(String fieldName,Object instance,Object value) {
		if (instance==null) {
			return;
		}
		if (value==null) {
			// TODO log this.
		}
		Field field;
		try {
			field =getField(instance.getClass(), fieldName);
			if (field == null) {
				return;
			}
			boolean acc = field.isAccessible();
			try {
				if (!acc) {
					field.setAccessible(true);
				}
				field.set(instance,value);
			} finally {
				if (!acc) {
					field.setAccessible(false);
				}
			}
		} catch (Exception e) {
			// I don't care.
			System.out.println("Failed to set a value.");
		}
	}

	public static Class<?> getFieldType(String fieldName, Class<?> class1) {
		Field f = getField(class1, fieldName);
		return f==null?null:f.getType();
	}
}