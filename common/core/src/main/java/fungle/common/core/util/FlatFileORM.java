package fungle.common.core.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;

public class FlatFileORM {
	private Map<Class<?>,Map<String,? extends Object>> references = new HashMap<>();
	
	public static class FlatFileORMBuilder {
		private Map<Class<?>,Map<String,? extends Object>> references = new HashMap<>();
		
		public <T extends Object> FlatFileORMBuilder addMapping(Class<T> clazz,Map<String,T> map) {
			this.references.put(clazz, map);
			return this;
		}
		
		public FlatFileORM build() {
			return new FlatFileORM(references);
		}
	}
	
	public static FlatFileORMBuilder builder() {
		return new FlatFileORMBuilder();
	}
	
	public static FlatFileORM newInstanceWithoutReferences() {
		return new FlatFileORM(Collections.EMPTY_MAP);
	}
	
	private FlatFileORM(Map<Class<?>,Map<String,? extends Object>> references) {
		this.references = references;
	}

	public <T> Map<String, T> loadFromClasspath(String classpath,
			Class<T> type) {
		if (!classpath.startsWith("/")) {
			classpath = "/" + classpath;
		}
		return load(FlatFileORM.class.getResourceAsStream(classpath), type);
	}

	public <T> Map<String, T> loadFromFilePath(String filepath,
			Class<T> type) {
		return loadFromFile(new File(filepath), type);
	}

	public <T> Map<String, T> loadFromFile(File file, Class<T> type) {
		try {
			return load(new FileInputStream(file), type);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return Collections.EMPTY_MAP;
	}

	public <T> Map<String, T> load(InputStream in, Class<T> type) {
		Map<String, T> map = new HashMap<>();
		try {
			String text = IOUtils.toString(in);
			String[] objects = text.split("====\n");
			for (String object : objects) {
				String[] lines = object.split("\n");
				T instance = type.newInstance();
				boolean idFound = false;
				for (String line : lines) {
					if (line == null || line.startsWith("#") || line.isEmpty()) {
						continue;
					}
					try {
						int i = line.indexOf("=");
						String key = line.substring(0, i).trim();
						String value = line.substring(i + 1, line.length())
								.trim();
						if ("_id".equals(key)) {
							map.put(value, instance);
							idFound = true;
						} else {
							Class<?> fieldType = Reflect
									.getFieldType(key, type);
							if (fieldType != null) {
								// check cross references.
								Object v = null;
								if (references.containsKey(fieldType)) {
									Map<String,?> mp = references.get(fieldType);
									v = mp.get(value);
									
								} 
								if (v==null) {
									v = TypeConverter.convert(value,
											fieldType);
								}
								if (v!=null) {
									Reflect.setFieldValue(key, instance, v);
								} else {
									System.out.println("Failed to get an object for key "+key+", value "+value);
								}
							}
						}
					} catch (Exception e) {
						// do nothing, we are making our best effort to build
						// things but may fail on just some fields.
					}
				}
				if (!idFound) {
					map.put(instance.toString(), instance);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(in);
		}
		return map;
	}

}
