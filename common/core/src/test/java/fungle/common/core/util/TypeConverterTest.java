package fungle.common.core.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

public class TypeConverterTest {

	
	@Test
	public void convert() throws ParseException {
//		Assert.assertNull(TypeConverter.convert(null, String.class));
		Assert.assertEquals("ajf",TypeConverter.convert("ajf", String.class));
		Assert.assertEquals(new Integer(5),TypeConverter.convert("5", Integer.class));
		Assert.assertTrue(TypeConverter.convert("true", Boolean.class));
		Assert.assertEquals("ajve",TypeConverter.<Foo>convert("ajve", Foo.class).x);
		Assert.assertEquals("34",TypeConverter.<Foo>convert(new Integer(34), Foo.class).x);
		Assert.assertEquals("loiju",TypeConverter.<Bar>convert("loiju", Bar.class).x);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date d = sdf.parse("2010-02-15");
		Assert.assertEquals(d,TypeConverter.<Date>convert("2010-02-15", Date.class));
		Assert.assertEquals(d,TypeConverter.<Date>convert("2010/02/15", Date.class));
		Assert.assertEquals(d,TypeConverter.<Date>convert("2010:02:15", Date.class));
		
		Assert.assertNotNull(TypeConverter.convert("foo", Zop.class));
		
		sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		d = sdf.parse("2010-02-15 10:18:04");
		Assert.assertEquals(d,TypeConverter.<Date>convert("2010-02-15 10:18:04", Date.class));
	}
	
	@Test
	public void getPrimitive() {
		Assert.assertEquals(boolean.class, TypeConverter.getPrimitive(Boolean.class));
		Assert.assertEquals(double.class, TypeConverter.getPrimitive(Double.class));
		Assert.assertEquals(byte.class, TypeConverter.getPrimitive(Byte.class));
		Assert.assertEquals(char.class, TypeConverter.getPrimitive(Character.class));
	}
	
	@Test
	public void isPrimitiveWrapper() {
		Assert.assertTrue(TypeConverter.isPrimitiveWrapper(Boolean.class));
		Assert.assertTrue(TypeConverter.isPrimitiveWrapper(Double.class));
		Assert.assertTrue(TypeConverter.isPrimitiveWrapper(Integer.class));
		Assert.assertFalse(TypeConverter.isPrimitiveWrapper(boolean.class));
		Assert.assertFalse(TypeConverter.isPrimitiveWrapper(double.class));
		Assert.assertFalse(TypeConverter.isPrimitiveWrapper(String.class));
		Assert.assertFalse(TypeConverter.isPrimitiveWrapper(short.class));
		Assert.assertFalse(TypeConverter.isPrimitiveWrapper(Calendar.class));
		Assert.assertFalse(TypeConverter.isPrimitiveWrapper(Date.class));
	}
	
	private static class Foo {
		String x;
		public Foo(String x) {
			this.x = x;
		}
		public String getX() {return x;}
	}
	private static class Bar {
		String x;
		private Bar(){}
		public static Bar valueOf(String y) {
			Bar b = new Bar();
			b.x=y;
			return b;
		}
	}
	
	private static class Zop {
		public Zop(){}
	}
}
