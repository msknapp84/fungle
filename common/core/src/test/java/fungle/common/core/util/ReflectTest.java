package fungle.common.core.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;


public class ReflectTest {

	@Test
	public void verifyHasField() {
		Reflect.verifyHasField(P.class, "v");
		Reflect.verifyHasField(C.class, "v");
		Reflect.verifyHasField(C.class, "x");
		Reflect.verifyHasField(C.class, "y.z");
	}
	
	@Test (expected=IllegalArgumentException.class)
	public void verifyHasField2() {
		Reflect.verifyHasField(P.class, "x");
	}
	
	@Test
	public void fieldExistsLike() {
		Assert.assertTrue(Reflect.fieldExistsLike(P.class, "v", String.class));
		Assert.assertFalse(Reflect.fieldExistsLike(P.class, "w", String.class));
		Assert.assertTrue(Reflect.fieldExistsLike(C.class, "x", String.class));
		Assert.assertTrue(Reflect.fieldExistsLike(C.class, "y", Y.class));
		
		// when searching the C class, also check its parent.
		Assert.assertTrue(Reflect.fieldExistsLike(C.class, "v", String.class));
		Assert.assertFalse(Reflect.fieldExistsLike(C.class, "z", String.class));
		Assert.assertTrue(Reflect.fieldExistsLike(C.class, "y.z", String.class));
		Assert.assertFalse(Reflect.fieldExistsLike(C.class, "v.z", String.class));
	}
	
	@Test
	public void collect() {
		List<C> cs = Arrays.asList(new C("foo"),new C("bar"),new C("zop"));
		Collection<String> found = (Collection) Reflect.collect(cs, "x", String.class);
		Assert.assertEquals(3,found.size());
		Assert.assertTrue(found.contains("foo"));
		Assert.assertTrue(found.contains("bar"));
		Assert.assertTrue(found.contains("zop"));
		
		cs = Arrays.asList(C.f("abc"),C.f("zyx"),C.f("lwijre"),C.f("qwejlr"));
		found = (Collection) Reflect.collect(cs, "y.z", String.class);
		Assert.assertEquals(4,found.size());
		Assert.assertTrue(found.contains("abc"));
		Assert.assertTrue(found.contains("zyx"));
		Assert.assertTrue(found.contains("lwijre"));
		Assert.assertTrue(found.contains("qwejlr"));
		
		found = (Collection) Reflect.collect(cs, "y.a", String.class);
		Assert.assertTrue(found.isEmpty());
	}
	
	@Test
	public void getFieldValue() {
		C c = new C("v");
		Assert.assertEquals("v",Reflect.getFieldValue("x", c, String.class));
		c = C.f("o");
		Assert.assertEquals("o",Reflect.getFieldValue("y.z", c, String.class));
		c = new C();
		c.setV("e");
		Assert.assertEquals("e",Reflect.getFieldValue("v", c, String.class));
	}
	
	@Test
	public void	setFieldValue() {
		C c = new C();
		Reflect.setFieldValue("x", c, "u");
		Assert.assertEquals("u",c.x);
		Reflect.setFieldValue("v", c, "t");
		Assert.assertEquals("t",c.getV());

		c.y=new Y();
		Reflect.setFieldValue("y.z", c, "d");
		Assert.assertEquals("d",c.y.z);
	}
	
	@Test
	public void getFieldType() {
		Assert.assertEquals(String.class,Reflect.getFieldType("x", C.class));
		Assert.assertEquals(Y.class,Reflect.getFieldType("y", C.class));
		Assert.assertEquals(String.class,Reflect.getFieldType("y.z", C.class));
		Assert.assertEquals(String.class,Reflect.getFieldType("v", C.class));
		Assert.assertNull(Reflect.getFieldType("b", C.class));
	}
	
	private static class P {
		private String v;
		public void setV(String v) {this.v = v;}
		public Object getV() {
			return v;
		}
	}
	private static class C extends P{
		private String x;
		private Y y;
		public C(){}
		public C(String x){this.x = x;}
		public static C f(String z){Y y = new Y();y.z=z;C c = new C();c.y=y;return c;}
	}
	private static class Y {
		private String z;
	}
}