package fungle.common.hdao.util;

import org.junit.Assert;
import org.junit.Test;

public class HBaseByteConverterUtilTest {
	
	@Test
	public void invert() {
		byte[] bs = new byte[]{(byte)0,(byte)1};
		byte[] inverted = HBaseByteConverter.invert(bs);
		byte[] expected = new byte[]{Byte.MAX_VALUE,Byte.MAX_VALUE-1};
		Assert.assertArrayEquals(expected,inverted);
		byte[] recovered = HBaseByteConverter.invert(inverted);
		Assert.assertArrayEquals(bs,recovered);
	}
	
	@Test
	public void merge() {
		byte[] a = new byte[]{(byte)3,(byte)24,(byte)-12};
		byte[] b = new byte[]{(byte)-84};
		byte[] c = new byte[]{(byte)120,(byte)18,(byte)98};
		byte[] m = HBaseByteConverter.merge(a,b,c);
		byte[] e = new byte[]{(byte)3,(byte)24,(byte)-12,(byte)-84,(byte)120,(byte)18,(byte)98};
		Assert.assertArrayEquals(e,m);
		
		Assert.assertTrue(HBaseByteConverter.merge(new byte[0]).length<1);
	}
	
	@Test
	public void convert() {
		String x = "fubar";
		byte[] b = HBaseByteConverter.toBytes(x);
		Assert.assertEquals(x,HBaseByteConverter.toObject(b, String.class));
		
		Boolean t = true;
		Boolean f = false;
		boolean tt = true;
		boolean ff = false;
		Assert.assertTrue(HBaseByteConverter.toObject(HBaseByteConverter.toBytes(t), Boolean.class));
		Assert.assertTrue(HBaseByteConverter.toObject(HBaseByteConverter.toBytes(tt), Boolean.class));
		Assert.assertFalse(HBaseByteConverter.toObject(HBaseByteConverter.toBytes(f), Boolean.class));
		Assert.assertFalse(HBaseByteConverter.toObject(HBaseByteConverter.toBytes(ff), Boolean.class));
	}
	
	@Test
	public void convert2() {
		byte[] b = HBaseByteConverter.toBytes(new Integer(8));
		Assert.assertEquals(new Integer(8), HBaseByteConverter.<Integer>toObject(b, Integer.class));
		b = HBaseByteConverter.toBytes(new Short((short) 2895));
		Assert.assertEquals((short) 2895, (short)HBaseByteConverter.<Short>toObject(b, Short.class));
		b = HBaseByteConverter.toBytes(new Double(6134.6283));
		Double d = HBaseByteConverter.<Double>toObject(b, Double.class);
		Assert.assertTrue(Math.abs(6134.6283-d)<0.0001);
	}
	
}
