package fungle.common.hdao.model;

import org.junit.Assert;
import org.junit.Test;

public class SimpleIDGeneratorTest {

	
	@Test
	public void test() {
		SimpleIDGenerator g = SimpleIDGenerator.getInstance(String.class);
		SimpleIDGenerator g2 = SimpleIDGenerator.getInstance(String.class);
		Assert.assertTrue(g==g2);
	}
	
	@Test
	public void test2() {
		SimpleIDGenerator g = SimpleIDGenerator.getInstance(String.class);
		SimpleIDGenerator g2 = SimpleIDGenerator.getInstance(Integer.class);
		Assert.assertFalse(g.equals(g2));
		
		String h1 = g.nextHexString();
		String h2 = g2.nextHexString();
		String h3 = g.nextHexString();
		String h4 = g2.nextHexString();
		Assert.assertNotSame(h1,h2);
		Assert.assertNotSame(h1,h3);
		Assert.assertNotSame(h1,h4);
		Assert.assertNotSame(h2,h3);
		Assert.assertNotSame(h2,h4);
		Assert.assertNotSame(h3,h4);
	}
}
