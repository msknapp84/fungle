package fungle.common.hdao.model;

public interface ByteIdentifiable {
	ByteId getByteId();
	void setByteId(ByteId id);
	void setByteId(byte[] id);
}