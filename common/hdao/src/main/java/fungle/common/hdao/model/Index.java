package fungle.common.hdao.model;

import java.util.List;

import org.apache.hadoop.hbase.client.HTable;

public interface Index<T extends ByteIdentifiable> {
	ByteRange getKey(T entity);
	boolean isUnique();
	HTable getReferencedTable();
	String getReferenceTableName();
	String getIndexTableName();
	List<IndexField> getFieldsInOrder();
	HTable getIndexTable();
}