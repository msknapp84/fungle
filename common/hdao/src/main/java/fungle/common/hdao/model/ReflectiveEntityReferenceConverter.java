package fungle.common.hdao.model;

import fungle.common.core.util.Reflect;

public class ReflectiveEntityReferenceConverter<F extends ByteIdentifiable,T extends ByteIdentifiable> implements EntityReferenceConverter<F,T> {
	
	private Class<F> localClass;
	private Class<T> foreignClass;
	private String fromFieldName,toFieldName;
	
	public ReflectiveEntityReferenceConverter(){
	}
	
	public ReflectiveEntityReferenceConverter(Class<F> localClass,final String fromFieldName,Class<T> foreignClass,final String toFieldName) {
		this.fromFieldName = fromFieldName;
		this.toFieldName = toFieldName;
		this.localClass = localClass;
		this.foreignClass = foreignClass;
	}

	@Override
	public T getForeignEntity(F f) {
		return (T) Reflect.getFieldValue(fromFieldName, f, localClass);
	}

//	@Override
//	public F getLocalEntity(T t) {
//		return (F) Reflect.getFieldValue(toFieldName, t, foreignClass);
//	}

	@Override
	public void setForeignEntity(F f, ByteIdentifiable t) {
		Reflect.setFieldValue(fromFieldName, f, t);
	}

	public String getFromFieldName() {
		return fromFieldName;
	}

	public void setFromFieldName(String fromFieldName) {
		this.fromFieldName = fromFieldName;
	}

	public String getToFieldName() {
		return toFieldName;
	}

	public void setToFieldName(String toFieldName) {
		this.toFieldName = toFieldName;
	}

	@Override
	public T newForeignInstance() {
		try {
			return foreignClass.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public F newLocalInstance() {
		try {
			return localClass.newInstance();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
