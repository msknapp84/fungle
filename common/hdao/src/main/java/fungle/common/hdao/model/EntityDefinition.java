package fungle.common.hdao.model;

import java.util.Set;

public interface EntityDefinition<F extends ByteIdentifiable> extends ByteIdDefinition<F> {
	/**
	 * Gets all references from a foreign entity to this entity,
	 * meaning that the foreign entity 'knows' about the reference
	 * while the local entity could be oblivious of the other.
	 * @return
	 */
	Set<EntityReference<?,F>> getIncomingReferences();
	
	/**
	 * Gets all references that this entity has to other entities.
	 * Meaning there should be a field on this entity whose type 
	 * is of the foreign entity, or a collection of them.
	 * @return
	 */
	Set<EntityReference<F,?>> getOutgoingReferences();
	
	/**
	 * Returns true if there are any unique constraints on this entity.
	 * @return
	 */
	boolean hasUniqueConstraints();
	
	/**
	 * Returns an array of objects that must be unique amongst the whole
	 * table of entities.  The objects returned do not need to be unique from 
	 * each other, just from the values found in other entities.  The method
	 * should always return the same size of array, including null values 
	 * if necessary.  An index in the array must correspond to the same 
	 * field all the time.
	 * @param entity
	 * @return
	 */
	Object[] getUniqueValues(F entity);
	
	/**
	 * Returns true if all fields that must not be null are not null.
	 * @param entity
	 * @return
	 */
	boolean verifyNotNullConstraints(F entity);
	
	
	<X extends ByteIdentifiable> X getForeignEntity(F entity,EntityReference<F,X> reference);
}