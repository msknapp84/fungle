package fungle.common.hdao.model;

import java.util.List;

import org.apache.commons.collections4.BidiMap;
import org.apache.hadoop.hbase.client.HTable;

import fungle.common.hdao.ByteIdentifiableDAO;

public interface DAODefinition<T extends ByteIdentifiable> {
	EntityDefinition<T> getEntityDefinition();
	List<Index<T>> getIndexes();
	BidiMap<String, Column> getMapping();
	IDGenerator getByteIdGenerator();
	Column getNonExistentColumn();
	HTable getTable();
	<X extends ByteIdentifiable> ByteIdentifiableDAO<X> getOutgoingDAO(EntityReference<T,X> reference);
	<X extends ByteIdentifiable> ByteIdentifiableDAO<X> getIncomingDAO(EntityReference<X,T> reference);
	Index<T> getIndexForReference(EntityReference<T,?> reference);
	Index<T> getIndexByFieldName(String ... fieldNames);
}