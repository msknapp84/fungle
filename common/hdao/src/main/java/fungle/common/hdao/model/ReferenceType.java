package fungle.common.hdao.model;

public enum ReferenceType {
	ONETOMANY,
	MANYTOONE,
	MANYTOMANY,
	ONETOONE;

	public ReferenceType opposite() {
		if (this == ONETOMANY) {
			return MANYTOONE;
		} else if (this == MANYTOONE) {
			return ONETOMANY;
		}
		return this;
	}
}
