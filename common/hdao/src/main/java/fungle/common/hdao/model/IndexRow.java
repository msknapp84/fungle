package fungle.common.hdao.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class IndexRow implements ByteIdentifiable {
	private ByteId byteId;

	private List<ByteId> refersToIds = new ArrayList<>();
	
	public IndexRow(ByteId indexKey) {
		this.byteId = indexKey;
	}
	public IndexRow(ByteId indexKey,ByteId... references) {
		this.byteId = indexKey;
		for (ByteId bs : references) {
			this.refersToIds.add(bs);
		}
	}
	
	public IndexRow() {
	}

	@Override
	public ByteId getByteId() {
		return byteId;
	}

	@Override
	public void setByteId(ByteId id) {
		this.byteId = id;
	}
	
	public void addReference(ByteId ref) {
		this.refersToIds.add(ref);
	}
	
	public void addReferences(Collection<ByteId> refs) {
		this.refersToIds.addAll(refs);
	}
	
	public void setSingleReference(ByteId ref) {
		this.refersToIds.clear();
		addReference(ref);
	}

	public ByteId getSingleReference() {
		return refersToIds.get(0);
	}

	public List<ByteId> getReferences() {
		return refersToIds;
	}
	@Override
	public void setByteId(byte[] id) {
		this.byteId = new ByteId(id);
	}

	public void addReference(byte[] ref) {
		this.refersToIds.add(new ByteId(ref));
	}
	
	public void setSingleReference(byte[] ref) {
		this.setSingleReference(new ByteId(ref));
	}

}