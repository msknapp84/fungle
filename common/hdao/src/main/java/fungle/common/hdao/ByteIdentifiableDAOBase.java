package fungle.common.hdao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.Cascade;
import fungle.common.hdao.model.OnError;

public abstract class ByteIdentifiableDAOBase<T extends ByteIdentifiable> implements ByteIdentifiableDAO<T> {
	private static final Logger logger = LoggerFactory.getLogger(ByteIdentifiableDAOBase.class);
	
	@Override
	public T get(ByteId id,Fetch fetch) {
		Map<ByteId,T> found = get(Collections.singleton(id),fetch);
		return found.get(id);
	}
	
	@Override
	public void save(T entity,Cascade cascade,OnError onError) {
		save(Collections.singleton(entity),cascade,onError);
	}

	@Override
	public void delete(Collection<T> entities,Cascade cascade,OnError onError) {
		List<ByteId> ids = new ArrayList<>();
		entities.forEach(entity -> ids.add(entity.getByteId()));
		deleteById(ids,cascade,onError);
	}

	@Override
	public void delete(T entity,Cascade cascade,OnError onError) {
		deleteById(entity.getByteId(),cascade,onError);
	}

	@Override
	public void deleteById(ByteId id,Cascade cascade,OnError onError) {
		deleteById(Collections.singleton(id),cascade,onError);
	}
	
	@Override
	public boolean hasId(ByteId id) {
		return hasIds(Collections.singleton(id)).get(id);
	}

	public abstract void deleteByIdMap(Map<ByteId,T> ids, Cascade cascade,
			OnError onError);

	/**
	 * From the given collection of ids, this returns the set that is NOT
	 * persistent in hbase.
	 * 
	 * @param ids
	 * @return
	 */
	public Set<ByteId> getNonPersistent(Collection<ByteId> ids) {
		return getPersistent(ids, false);
	}

	/**
	 * From the given collection of ids, this returns the set that is persistent
	 * in hbase.
	 * 
	 * @param ids
	 * @return
	 */
	public Set<ByteId> getPersistent(Collection<ByteId> ids) {
		return getPersistent(ids, true);
	}

	/**
	 * From the given collection of ids, this returns the set whose persistence
	 * is the same as the second arg. Meaning that if the second arg is true,
	 * this returns the subset of ids that are already present in the hbase
	 * table, and vice versa.
	 * 
	 * @param ids
	 * @param matchTo
	 * @return
	 */
	public Set<ByteId> getPersistent(Collection<ByteId> ids, boolean matchTo) {
		Set<ByteId> res = new HashSet<ByteId>();
		Map<ByteId, Boolean> has = hasIds(ids);
		for (ByteId id : has.keySet()) {
			if (matchTo == has.get(id)) {
				res.add(id);
			}
		}
		return res;
	}

	protected void verifyIdsInCollection(Collection<ByteId> ids) {
		if (ids == null) {
			throw new IllegalArgumentException("ids collection is null.");
		}
		for (ByteId id : ids) {
			if (id == null || id.length() < 1) {
				throw new IllegalArgumentException("id is null or empty.");
			}
		}
	}

	protected void verifyIds(Collection<T> entities) {
		if (entities == null) {
			throw new IllegalArgumentException("Entities must not be null.");
		}
		for (T entity : entities) {
			if (entity == null) {
				throw new IllegalArgumentException(
						"Must not have a null entity in the collection when verifying they have ids.");
			}
			if (entity.getByteId() == null || entity.getByteId().length() < 1) {
				throw new IllegalArgumentException(
						"One of the entities is missing an id.");
			}
		}
	}

	protected Collection<ByteId> getByteIds(Collection<T> entities) {
		if (entities == null) {
			throw new IllegalArgumentException("Entities must not be null.");
		}
		List<ByteId> ids = new ArrayList<>();
		for (T entity : entities) {
			if (entity == null) {
				if (logger.isDebugEnabled()) {
					logger.debug("In getByteIds, an entity was null.  It's being skipped.");
				}
				continue;
			}
			if (entity.getByteId() != null && entity.getByteId().length() > 0) {
				ids.add(entity.getByteId());
			}
		}
		return ids;
	}
	
	protected Map<ByteId,T> convertToMap(Collection<T> entities) {
		Map<ByteId,T> converted = new HashMap<ByteId,T> ();
		entities.forEach(entity -> converted.put(entity.getByteId(), entity));
		return converted;
	}

	/**
	 * For the collection of ids, this returns a map from each id to a boolean
	 * that is true if the id is present in the hbase table.
	 */
	public abstract Map<ByteId, Boolean> hasIds(Collection<ByteId> ids);
}