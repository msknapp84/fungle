package fungle.common.hdao.util;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections4.BidiMap;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fungle.common.core.util.Reflect;
import fungle.common.hdao.ByteConverter;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.Column;

public final class HBaseEntityUtil {
	private static final Logger logger = LoggerFactory
			.getLogger(HBaseEntityUtil.class);

	private HBaseEntityUtil() {
	}

	public static <T extends ByteIdentifiable> T mergeInto(T instance,
			Result result, Class<T> clazz, BidiMap<String, Column> mapping) {
		if (result == null || result.getRow() == null) {
			return null;
		}
		try {
			if (instance == null) {
				instance = clazz.newInstance();
			}
			instance.setByteId(result.getRow());
			for (String fieldName : mapping.keySet()) {
				try {
					Column column = mapping.get(fieldName);
					byte[] value = result.getValue(column.getColumnFamily(),
							column.getColumnQualifier());
					if (value != null && value.length > 0) {
						Object o = HBaseByteConverter.toObject(
								value,
								Reflect.getFieldType(fieldName,
										instance.getClass()));
						Reflect.setFieldValue(fieldName, instance, o);
					}
				} catch (Exception e) {
					logger.error(
							"Had an exception while trying to set a value for the field named {}, {}",
							fieldName, e.getMessage());
				}
			}
		} catch (Exception e) {
			logger.error(
					"Had an exception while trying to merge a result into an instance, {}",
					e.getMessage());
		}
		return instance;

	}

	public static <T extends ByteIdentifiable> T getEntity(Result result,
			Class<T> clazz, BidiMap<String, Column> mapping) {
		return mergeInto(null, result, clazz, mapping);
	}

	public static <T extends ByteIdentifiable> Put makePut(T entity,
			BidiMap<String, Column> mapping) {
		Put put = new Put(entity.getByteId().getByteId());
		// Class<?> clazz = entity.getClass();
		for (String fieldName : mapping.keySet()) {
			try {
				// Field field = clazz.getDeclaredField(fieldName);
				Column column = mapping.get(fieldName);
				Object value = Reflect.getFieldValue(fieldName, entity,
						Object.class);
				if (value != null) {
					// it might legitimately be null, don't throw an
					// exception if it is null
					byte[] bytes = null;
					if (column.isForeignReference()) {
						bytes = getBytesAsForeignReference(value);
					} else {
						bytes = HBaseByteConverter.toBytes(value);
					}
					if (bytes != null && ("".equals(value) || bytes.length > 0)) {
						put.add(column.getColumnFamily(),
								column.getColumnQualifier(), bytes);
					} else {
						logger.warn(
								"For the {} entity, a value yielded a null/empty byte array for the field named {} with value {}",
								entity.getClass().getSimpleName(), fieldName,
								value.toString());
					}
				} else if (logger.isDebugEnabled()) {
					Field f = Reflect.getField(entity.getClass(), fieldName);
					if (f == null) {
						logger.warn(
								"The {} entity does not have a field by the name of {}, trying to get its value could not possibly have worked.",
								entity.getClass().getSimpleName(), fieldName);
					}
				}
			} catch (SecurityException e) {
				logger.error(
						"Had a security exception when trying to reflectively get the value for the field named {}",
						fieldName);
			} catch (IllegalArgumentException e) {
				logger.error(
						"Had an illegal argument exception when trying to reflectively get the value for the field named {}",
						fieldName);
			}
		}
		return put;
	}

	private static byte[] getBytesAsForeignReference(Object value) {
		if (value==null) {
			return null;
		}
		if (!(value instanceof ByteIdentifiable)) {
			throw new IllegalStateException("A value was expected to be a sub type of "
					+ "byte identifiable but was actually "+value.getClass().getName());
		}
		ByteIdentifiable o = (ByteIdentifiable)value;
		if (o.getByteId()!=null) {
			return o.getByteId().getByteId();
		} else {
			logger.warn("An instance of byte identifiable was missing its id"
					+ " as we were trying to make a put operation, it can't be saved");
		}
		return null;
	}
}