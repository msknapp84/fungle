package fungle.common.hdao;

import org.apache.commons.collections4.BidiMap;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;

import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.Column;
import fungle.common.hdao.util.HBaseEntityUtil;

public class ReflectiveResultConverter<T extends ByteIdentifiable> implements ResultConverter<T> {

	private Class<T> clazz;
	
	public ReflectiveResultConverter(Class<T> clazz) {
		this.clazz= clazz;
	}
	
	@Override
	public T toEntity(Result result,BidiMap<String, Column> mapping) {
		return HBaseEntityUtil.getEntity(result,clazz,mapping);
	}

	@Override
	public Put toPut(T entity,BidiMap<String, Column> mapping) {
		return HBaseEntityUtil.makePut(entity,mapping);
	}

	@Override
	public T mergeInto(T entity, Result result, BidiMap<String, Column> mapping) {
		return HBaseEntityUtil.mergeInto(entity,result,clazz,mapping);
	}
}