package fungle.common.hdao;

public interface ByteConverter<T> {
	T fromBytes(byte[] bytes);
	byte[] toBytes(T entity);
}
