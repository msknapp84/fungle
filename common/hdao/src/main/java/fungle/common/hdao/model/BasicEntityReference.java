package fungle.common.hdao.model;

import org.apache.commons.lang.Validate;

public class BasicEntityReference<F extends ByteIdentifiable, T extends ByteIdentifiable>
		implements EntityReference<F, T> {
	private final boolean required;
	private final Class<F> from;
	private final Class<T> to;
	private final ReferenceType type;
	private final EntityReferenceConverter<F, T> converter;

	private BasicEntityReference(BasicEntityReferenceBuilder<F, T> b) {
		this.required = b.required;
		this.type = b.type;
		this.converter = b.converter;
		this.to = b.to;
		this.from = b.from;
		validate();
	}

	private void validate() {
		Validate.notNull(this.type,"Must set the reference type");
		Validate.notNull(this.from,"Must know who the reference is from.");
		Validate.notNull(this.to,"Must know who the reference is to.");
		Validate.notNull(this.converter,"The reference lacks a converter.");
	}

	public static <F extends ByteIdentifiable, T extends ByteIdentifiable> BasicEntityReferenceBuilder<F, T> builder() {
		return new BasicEntityReferenceBuilder<F, T>();
	}

	public String toString() {
		return String.format("%s reference from %s to %s", type.toString(),
				from.getSimpleName(), to
						.getSimpleName());
	}

	public boolean isRequired() {
		return required;
	}

	public ReferenceType getType() {
		return type;
	}

	public Class<F> getFromClass() {
		return from;
	}

	public Class<T> getToClass() {
		return to;
	}

	@Override
	public T getForeignEntity(F f) {
		return f==null?null:converter.getForeignEntity(f);
	}

	@Override
	public void setForeignEntity(F f, ByteIdentifiable t) {
		if (f!=null) {
			converter.setForeignEntity(f, t);
		}
	}

	@Override
	public T newForeignInstance() {
		return converter.newForeignInstance();
	}

	@Override
	public F newLocalInstance() {
		return converter.newLocalInstance();
	}

	public static class BasicEntityReferenceBuilder<F extends ByteIdentifiable, T extends ByteIdentifiable> {
		private boolean required = true;
		private Class<F> from;
		private Class<T> to;
		private ReferenceType type = ReferenceType.ONETOMANY;
		private EntityReferenceConverter<F, T> converter;

		public BasicEntityReferenceBuilder<F, T> converter(
				EntityReferenceConverter<F, T> converter) {
			this.converter = converter;
			return this;
		}

		public BasicEntityReferenceBuilder<F, T> fromFieldName(
				String fromFieldName) {
			if (converter == null) {
				converter = new ReflectiveEntityReferenceConverter<>();
			}
			((ReflectiveEntityReferenceConverter)converter).setFromFieldName(fromFieldName);
			return this;
		}

		public BasicEntityReferenceBuilder<F, T> toFieldName(
				String toFieldName) {
			if (converter == null) {
				converter = new ReflectiveEntityReferenceConverter<>();
			}
			((ReflectiveEntityReferenceConverter)converter).setFromFieldName(toFieldName);
			return this;
		}
		
		public BasicEntityReferenceBuilder<F, T> manyToOne() {
			this.type = ReferenceType.MANYTOONE;
			return this;
		}

		public BasicEntityReferenceBuilder<F, T> manyToMany() {
			this.type = ReferenceType.MANYTOMANY;
			return this;
		}

		public BasicEntityReferenceBuilder<F, T> oneToOne() {
			this.type = ReferenceType.ONETOONE;
			return this;
		}

		public BasicEntityReferenceBuilder<F, T> oneToMany() {
			this.type = ReferenceType.ONETOMANY;
			return this;
		}

		public BasicEntityReferenceBuilder<F, T> required() {
			this.required = true;
			return this;
		}

		public BasicEntityReferenceBuilder<F, T> notRequired() {
			this.required = false;
			return this;
		}

		public BasicEntityReferenceBuilder<F, T> to(Class<T> to) {
			this.to = to;
			return this;
		}

		public BasicEntityReferenceBuilder<F, T> from(Class<F> from) {
			this.from = from;
			return this;
		}

		public BasicEntityReference<F, T> build() {
			return new BasicEntityReference<>(this);
		}
	}
}