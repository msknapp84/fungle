package fungle.common.hdao.model;

public enum ReferenceDirection {
	THIS_REFERS_TO_THAT, THAT_REFERS_TO_THIS,BIDIRECTIONAL;

	public ReferenceDirection opposite() {
		if (this == ReferenceDirection.THAT_REFERS_TO_THIS) {
			return THIS_REFERS_TO_THAT;
		} else if (this == ReferenceDirection.THIS_REFERS_TO_THAT){
			return THAT_REFERS_TO_THIS;
		}
		return this;
	}

}
