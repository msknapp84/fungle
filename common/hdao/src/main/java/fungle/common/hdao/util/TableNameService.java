package fungle.common.hdao.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.MasterNotRunningException;
import org.apache.hadoop.hbase.TableNotFoundException;
import org.apache.hadoop.hbase.ZooKeeperConnectionException;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.util.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fungle.common.hdao.ByteIdentifiableDAO;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.DAODefinition;
import fungle.common.hdao.model.EntityDefinition;

public class TableNameService {
	private static final Logger logger = LoggerFactory.getLogger(TableNameService.class);
	private static final String DEFAULT_FAMILY="f";
	private static final String DELIMITER = ".";
	private final String schemaName;
	private final Map<Class<? extends ByteIdentifiable>,TableNamesForEntity> names = new HashMap<>();
	private final Configuration conf;
	private HBaseAdmin admin;
	private final boolean dropTablesFirst;
	
	public TableNameService(final String schemaName,final Configuration conf,boolean dropTablesFirst) {
		this.schemaName = schemaName;
		this.conf = conf;
		if (dropTablesFirst && "prod".equals(schemaName)) {
			throw new IllegalArgumentException("Can't specify drop tables first for prod schema.");
		}
		this.dropTablesFirst = dropTablesFirst;
	}
	
	public TableNamesForEntity get(Class<? extends ByteIdentifiable> clazz,String tableFamily,String indexFamily) {
		TableNamesForEntity n = names.get(clazz);
		if (n==null) {
			n = new TableNamesForEntity(clazz,tableFamily,indexFamily);
			names.put(clazz, n);
			logger.info("Added a table name service for "+clazz.getSimpleName());
		}
		return n;
	}
	
	public TableNamesForEntity get(Class<? extends ByteIdentifiable> clazz) {
		return get(clazz,DEFAULT_FAMILY,DEFAULT_FAMILY);
	}
	
	public TableNamesForEntity get(ByteIdentifiableDAO<?> dao) {
		return get(dao.getEntityClass(),DEFAULT_FAMILY,DEFAULT_FAMILY);
	}
	
	public TableNamesForEntity get(ByteIdentifiableDAO<?> dao,String tableFamily,String indexFamily) {
		return get(dao.getEntityClass(),tableFamily,indexFamily);
	}
	
	public TableNamesForEntity get(EntityDefinition<?> def) {
		return get(def.getEntityType(),DEFAULT_FAMILY,DEFAULT_FAMILY);
	}
	
	public TableNamesForEntity get(DAODefinition<?> def) {
		return get(def.getEntityDefinition().getEntityType(),DEFAULT_FAMILY,DEFAULT_FAMILY);
	}
	
	public String getSchemaName() {
		return schemaName;
	}
	
	public Configuration getConfiguration() {
		return conf;
	}
	
	private HBaseAdmin getAdmin() {
		if (this.admin == null) {
			try {
				this.admin = new HBaseAdmin(getConfiguration());
			} catch (MasterNotRunningException e) {
				logger.error(e.getMessage(),e);
			} catch (ZooKeeperConnectionException e) {
				logger.error(e.getMessage(),e);
			}
		}
		return this.admin;
	}
	
	public class TableNamesForEntity {
		private final Class<? extends ByteIdentifiable> clazz;
		private final String requiredTableFamily,requiredIndexFamily;
		
		public TableNamesForEntity(final Class<? extends ByteIdentifiable> clazz,String tableFamily,String indexFamily) {
			this.clazz=clazz;
			this.requiredIndexFamily=indexFamily;
			this.requiredTableFamily = tableFamily;
		}
		
		public final Class<? extends ByteIdentifiable> getType() {
			return clazz;
		}
		
		public String getTableName() {
			return schemaName+DELIMITER+clazz.getSimpleName().toLowerCase();
		}
		
		public String getIndexTableName(String fieldName) {
			return getTableName()+DELIMITER+fieldName+DELIMITER+"index";
		}
		
		private HTable getTable(String name) {
			HTable table = null;
			try {
				table = new HTable(conf,name);
			} catch (Exception e) {
				String fn = requiredIndexFamily;
				if (name.toLowerCase().endsWith(clazz.getSimpleName().toLowerCase())) {
					fn = requiredTableFamily;
				}
				table = ensureTable(name, fn);
				logger.error(e.getMessage(),e);
			}
			return table;
		}
		
		public HTable ensureTable(String tableName,String... familyNames) {
			try {
				if (!getAdmin().isTableAvailable(tableName)) {
					createTable(tableName, familyNames);
				} else {
					if (dropTablesFirst) {
						// must drop and re-create the table.
						dropTable(tableName);
						createTable(tableName, familyNames);
					} else {
						addMissingColumns(tableName, familyNames);
					}
				}
				if (logger.isDebugEnabled()) {
					logger.debug("Successfully ensured that a table exists. "+tableName);
				}
				return new HTable(conf,tableName);
			} catch (IOException e) {
				logger.error("Failed to ensure that a table exists, "+tableName,e);
			}
			return null;
		}

		private void dropTable(String tableName) throws TableNotFoundException, IOException {
//			HTableDescriptor d = getAdmin().getTableDescriptor(Bytes.toBytes(tableName));
			logger.info("Dropping table "+tableName);
			getAdmin().disableTable(tableName);
			getAdmin().deleteTable(tableName);
		}

		private void addMissingColumns(String tableName, String... familyNames)
				throws TableNotFoundException, IOException {
			// make sure it has the required column.
			logger.info("Adding missing columns (if any) to table "+tableName);
			HTableDescriptor desc = getAdmin().getTableDescriptor(Bytes.toBytes(tableName));
			for (String familyName : familyNames) {
				if (!desc.hasFamily(Bytes.toBytes(familyName))) {
//							desc.addFamily(new HColumnDescriptor(familyName));
					getAdmin().addColumn(Bytes.toBytes(tableName),new HColumnDescriptor(familyName));
				}
			}
		}

		private void createTable(String tableName, String... familyNames)
				throws IOException {
			// must create the table;
			logger.info("creating the table "+tableName);
			HTableDescriptor desc = new HTableDescriptor(tableName);
			for (String familyName : familyNames) {
				desc.addFamily(new HColumnDescriptor(familyName));
			}
			getAdmin().createTable(desc);
		}
		
		public HTable getTable() {
			return getTable(getTableName());
		}
		
		public HTable ensureTable() {
			return ensureTable(getTableName(),requiredTableFamily);
		}
		
		public HTable getIndexTable(String fieldName) {
			return getTable(getIndexTableName(fieldName));
		}
		
		public HTable ensureIndexTable(String fieldName) {
			return ensureTable(getIndexTableName(fieldName),requiredIndexFamily);
		}
	}

//	public void clearAllTables() {
//		if ("prod".equals(schemaName)) {
//			throw new IllegalArgumentException("Cannot drop production tables");
//		}
//		if (StringUtils.isEmpty(schemaName)) {
//			throw new IllegalArgumentException("Cannot drop tables when the schema name is empty.");
//		}
//		try {
//			if (logger.isInfoEnabled()) {
//				logger.info("Clearing all tables in the "+schemaName+" schema.");
//			}
//			for (HTableDescriptor td : getAdmin().listTables(schemaName+".*")) {
//				HTable t = new HTable(getConfiguration(), td.getName());
//				if (t!=null) {
//					if (logger.isDebugEnabled()) {
//						logger.debug("Deleting all rows from "+Bytes.toString(t.getTableName()));
//					}
//					Delete delete = new Delete();
//					delete.deleteFamily(Bytes.toBytes("f"));
//					t.delete(delete);
//				}
//			}
//		} catch (IOException e) {
//			logger.error(e.getMessage(),e);
//		}
//	}
}