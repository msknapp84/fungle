package fungle.common.hdao;

import org.apache.commons.collections4.BidiMap;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;

import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.Column;

public interface ResultConverter<T extends ByteIdentifiable> {
	T toEntity(Result result,BidiMap<String, Column> mapping);
	T mergeInto(T entity,Result result,BidiMap<String, Column> mapping);
	Put toPut(T entity,BidiMap<String, Column> mapping);
}
