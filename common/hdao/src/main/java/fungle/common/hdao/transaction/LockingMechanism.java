package fungle.common.hdao.transaction;

import java.util.Set;
import java.util.concurrent.TimeoutException;

import org.apache.hadoop.hbase.client.HTable;

import fungle.common.hdao.model.ByteId;

public interface LockingMechanism {
	void acquireSharedLock(HTable table,Set<ByteId> rows) throws InterruptedException, TimeoutException;
	void acquireExclusiveLock(HTable table,Set<ByteId> rows) throws InterruptedException, TimeoutException;
	void releaseLocks(HTable table,Set<ByteId> rows);
}