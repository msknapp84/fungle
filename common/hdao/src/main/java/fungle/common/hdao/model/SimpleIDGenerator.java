package fungle.common.hdao.model;

import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.google.common.hash.HashFunction;

public class SimpleIDGenerator implements IDGenerator {
	private static final String frmt = "yyyyMMdd HH:mm:ss.sss";
	private String salt;
	private int number;

	private static HashFunction md5 = com.google.common.hash.Hashing.md5();

	private static Map<Object, SimpleIDGenerator> generators = new ConcurrentHashMap<>(
			16);

	public static SimpleIDGenerator getInstance(Object o) {
		synchronized (generators) {
			if (generators.containsKey(o)) {
				return generators.get(o);
			}
			Calendar c = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat(frmt);
			String date = sdf.format(c.getTime());
			SimpleIDGenerator g = new SimpleIDGenerator(o.toString() + date);
			generators.put(o, g);
			return g;
		}
	}

	private SimpleIDGenerator(String salt) {
		this.salt = salt;
	}

	public synchronized ByteId nextBytes() {
		return new ByteId(md5.hashString(salt + number++, Charset.defaultCharset())
				.asBytes());
	}

	public synchronized String nextHexString() {
		return md5.hashString(salt + number++, Charset.defaultCharset())
				.toString();
	}
}