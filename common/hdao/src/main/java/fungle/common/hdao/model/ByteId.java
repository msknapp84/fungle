package fungle.common.hdao.model;

import java.util.Arrays;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.util.Bytes.ByteArrayComparator;

@XmlJavaTypeAdapter(ByteIdAdapter.class)
public class ByteId implements Comparable<ByteId> {
	private static final ByteArrayComparator comparator = new ByteArrayComparator();
	private final byte[] byteId;

	public ByteId(byte[] byteId) {
		if (byteId==null || byteId.length<1) {
			 throw new IllegalArgumentException("bytes cannot be null/empty.");
		}
		this.byteId = byteId;
	}

	public byte[] getByteId() {
		return Arrays.copyOf(this.byteId, this.byteId.length);
	}

	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof ByteId)) {
			return false;
		}
		ByteId that = (ByteId) o;
		return new EqualsBuilder().append(this.byteId, that.byteId).isEquals();
	}
	
	public int hashCode() {
		return new HashCodeBuilder().append(this.byteId).toHashCode();
	}
	
	public int length() {
		return byteId.length;
	}
	
	public String toString() {
		return Bytes.toString(byteId);
	}

	public int compareTo(ByteId other) {
		if (other==null) {
			return -1;
		}
		return comparator.compare(byteId, other.getByteId());
	}
}