package fungle.common.hdao;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.commons.lang.Validate;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.util.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.HashMultimap;

import fungle.common.core.util.Reflect;
//import fungle.common.core.util.Reflect;
import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.ByteRange;
import fungle.common.hdao.model.Cascade;
import fungle.common.hdao.model.Column;
import fungle.common.hdao.model.DAODefinition;
import fungle.common.hdao.model.EntityReference;
import fungle.common.hdao.model.Index;
import fungle.common.hdao.model.IndexRow;
import fungle.common.hdao.model.IndexRowConverter;
import fungle.common.hdao.model.IndexRowDefinition;
import fungle.common.hdao.model.OnError;
import fungle.common.hdao.model.ReferenceType;
import fungle.common.hdao.transaction.Transaction;
import fungle.common.hdao.transaction.TransactionOp;
import fungle.common.hdao.transaction.TransactionOp.TransactionOpFactory;
import fungle.common.hdao.util.CascadeUtils;
import fungle.common.hdao.util.FetchUtils;
import fungle.common.hdao.util.HBaseDAOHelp;

public abstract class HBaseDAOBase<T extends ByteIdentifiable> extends
		ByteIdentifiableDAOBase<T> {
	private static final Logger logger = LoggerFactory
			.getLogger(HBaseDAOBase.class);

	protected DAODefinition<T> daoDefinition;

	public HBaseDAOBase() {
	}

	protected void validate() {
		Validate.notNull(daoDefinition, "The DAO Definition is not set");
		for (Field field : daoDefinition.getEntityDefinition().getEntityType()
				.getDeclaredFields()) {
			String fieldName = field.getName();
			if (!"byteId".equals(fieldName)
					&& !daoDefinition.getMapping().containsKey(fieldName)) {
				logger.warn(
						"The mapping for entity {} does not cover the field named {}",
						daoDefinition.getEntityDefinition().getEntityType()
								.getSimpleName(), fieldName);
			}
		}
		for (String fieldName : daoDefinition.getMapping().keySet()) {
			// verify that these field names exist.
			Reflect.verifyHasField(daoDefinition.getEntityDefinition()
					.getEntityType(), fieldName);
		}
	}

	public Map<ByteId, T> get(Collection<ByteId> ids, Fetch fetch) {
		Transaction transaction = new Transaction(OnError.CONTINUE);
		Map<ByteId, T> res = get(ids, fetch, transaction);
		transaction.execute();
		return res;
	}

	@Override
	public Map<ByteId, T> get(Collection<ByteId> ids, Fetch fetch,
			Transaction transaction) {
		Map<ByteId, T> entities = getLazily(ids, transaction);
		// the last argument tells it to flush the transaction if
		// it decides to fetch any entities.
		get_referenced_entities(entities, fetch, transaction, true);
		return entities;
	}

	public Map<ByteId, T> getAll(Fetch fetch, Transaction transaction,
			Object serverFilter, Predicate<T> clientFilter) {
		Map<ByteId, T> entities = new HashMap<>();
		Scan scan = new Scan();
		if (serverFilter != null) {
			if (serverFilter instanceof Filter) {
				scan.setFilter((Filter) serverFilter);
			} else {
				throw new IllegalArgumentException(String.format(
						"The filter type %s is not supported.", serverFilter
								.getClass().getSimpleName()));
			}
		}
		Map<ByteId,Scan> scans = new HashMap<>();
		ByteId defaultId = new ByteId(Bytes.toBytes("a"));
		scans.put(defaultId, scan);
		TransactionOp<T> operation = startFactory().clientFilter(clientFilter).build(entities, scans);
		transaction.addOperation(operation);
		
		// the last argument tells it to flush the transaction if
		// it decides to fetch any entities.
		get_referenced_entities(entities, fetch, transaction, true);
		return entities;
	}

	private Map<ByteId, T> getLazily(Collection<ByteId> ids,
			Transaction transaction) {
		Map<ByteId, T> entities = new HashMap<>();
		if (ids == null || ids.isEmpty()) {
			return entities;
		}
		ids.forEach(id -> entities.put(id, daoDefinition.getEntityDefinition()
				.newInstance(id)));
		Map<ByteId, Get> gets = new HashMap<>();
		ids.forEach(id -> gets.put(id, new Get(id.getByteId())));
		if (!entities.isEmpty() && !gets.isEmpty()) {
			transaction.addOperation(startFactory().build(entities, gets));
		} else {
			logger.debug(
					"While trying to lazily get {} entities, there were none to get.",
					daoDefinition.getEntityDefinition().getEntityType()
							.getSimpleName());
		}
		return entities;
	}

	private void get_referenced_entities(Map<ByteId, T> entities, Fetch fetch,
			Transaction transaction) {
		get_referenced_entities(entities, fetch, transaction, false);
	}

	private void get_referenced_entities(Map<ByteId, T> entities, Fetch fetch,
			Transaction transaction, boolean mightNeedFlush) {
		if (logger.isDebugEnabled()) {
			logger.debug("Getting referenced entities from "
					+ getEntityClass().getSimpleName());
		}
		boolean hasFlushed = false;
		for (EntityReference<T, ? extends ByteIdentifiable> reference : daoDefinition
				.getEntityDefinition().getOutgoingReferences()) {
			if (!fetch.shouldFetch(reference)) {
				continue;
			}
			Fetch subFetch = fetch.getSubFetch(reference);
			if (logger.isDebugEnabled()) {
				logger.debug(
						"Executing a cascaded get for the {} {} reference from entity {} to entity {}",
						reference.isRequired() ? "required" : "optional",
						reference.getType().toString(), daoDefinition
								.getEntityDefinition().getEntityType()
								.getSimpleName(), reference.getToClass());
			}
			if (mightNeedFlush && !hasFlushed) {
				if (logger.isDebugEnabled()) {
					logger.debug(
							"While trying to eagerly fetch entities for the reference "
									+ "from {} to {}, we are flushing the transaction to get current entities.",
							daoDefinition.getEntityDefinition().getEntityType()
									.getSimpleName(), reference.getToClass()
									.getSimpleName());
				}
				transaction.flush();
				hasFlushed = true;
			}
			Map<ByteId, ?> foreignEntities = getForeignEntities(subFetch,
					entities, reference, transaction);
			if (logger.isDebugEnabled()) {
				logger.debug("Returned from getting foreign entities, relinking them to local entities.");
			}
			relinkToForeignEntities(entities, reference, foreignEntities);
		}
	}

	private void relinkToForeignEntities(Map<ByteId, T> entities,
			EntityReference<T, ? extends ByteIdentifiable> reference,
			Map<ByteId, ?> foreignEntities) {
		for (T entity : entities.values()) {
			ByteIdentifiable entityPlaceHolder = reference
					.getForeignEntity(entity);
			if (entityPlaceHolder != null
					&& entityPlaceHolder.getByteId() != null) {
				ByteId foreignId = entityPlaceHolder.getByteId();
				ByteIdentifiable actualForeignEntity = (ByteIdentifiable) foreignEntities
						.get(foreignId);
				reference.setForeignEntity(entity, actualForeignEntity);
			}
		}
	}

	private Map<ByteId, ?> getForeignEntities(Fetch fetch,
			Map<ByteId, T> entities, EntityReference<T, ?> reference,
			Transaction transaction) {
		if (entities == null || entities.isEmpty()) {
			logger.warn("Asked to get foreign entities but the map of entities was null/empty.");
			return Collections.EMPTY_MAP;
		}
		if (reference == null) {
			logger.warn("Asked to get foreign entities but the reference object was null/empty.");
			return Collections.EMPTY_MAP;
		}
		List<ByteId> foreignIds = HBaseDAOHelp.<T> getForeignIdList(entities,
				reference);
		if (foreignIds == null || foreignIds.isEmpty()) {
			if (logger.isDebugEnabled()) {
				logger.debug("When trying to get the foreign id list, a null/empty result was returned.");
			}
			return Collections.EMPTY_MAP;
		}
		ByteIdentifiableDAO<?> outgoingDAO = daoDefinition
				.getOutgoingDAO(reference);
		if (outgoingDAO == null) {
			logger.warn(
					"The DAO for {} returned no outgoing DAO for the reference to {}",
					daoDefinition.getEntityDefinition().getEntityType()
							.getSimpleName(), reference.getToClass()
							.getSimpleName());
		}
		if (logger.isDebugEnabled()) {
			logger.debug(
					"The DAO for {} is using {} to get {} foreign entities.",
					daoDefinition.getEntityDefinition().getEntityType()
							.getSimpleName(), outgoingDAO.getClass()
							.getSimpleName(), foreignIds.size());
		}
		Map<ByteId, ?> foreignEntities = outgoingDAO.get(foreignIds, fetch,
				transaction);
		return foreignEntities;
	}

	@Override
	public void save(Collection<T> entities, Cascade cascade,
			Transaction transaction) {
		if (logger.isDebugEnabled()) {
			logger.debug("Saving a collection of entities of type "
					+ getEntityClass().getSimpleName());
		}
		HBaseDAOHelp.ensureIds(daoDefinition.getByteIdGenerator(), entities);
		save_cascade(entities, cascade, transaction);
		for (Index<T> index : daoDefinition.getIndexes()) {
			updateIndex(index, convertToMap(entities), transaction);
		}
		Map<ByteId, Put> puts = new HashMap<>();
		entities.forEach(entity -> puts.put(
				entity.getByteId(),
				daoDefinition.getEntityDefinition().getConverter()
						.toPut(entity, daoDefinition.getMapping())));
		Map<ByteId, T> entityMapping = convertToMap(entities);
		transaction.addOperation(startFactory().build(entityMapping, puts));
	}

	@Override
	public void save(final Collection<T> entities, final Cascade cascade,
			final OnError onError) {
		Transaction transaction = new Transaction(onError);
		save(entities, cascade, transaction);
		transaction.execute();
	}

	private TransactionOpFactory<T> startFactory() {
		return TransactionOp.getFactory(daoDefinition.getEntityDefinition())
				.converter(daoDefinition.getEntityDefinition().getConverter())
				.mapping(daoDefinition.getMapping())
				.table(daoDefinition.getTable());
	}

	private void save_cascade(Collection<T> entities, Cascade cascade,
			Transaction transaction) {
		if (CollectionUtils.isEmpty(entities)) {
			if (logger.isDebugEnabled()) {
				logger.debug("Not cascading a save on entities because it is null/empty.");
			}
			return;
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Executing cascaded saves for "
					+ getEntityClass().getSimpleName());
		}
		for (EntityReference<T, ?> reference : daoDefinition
				.getEntityDefinition().getOutgoingReferences()) {
			if (reference == null) {
				logger.warn(
						"The DAO for {} returned a collection of outgoing references with a null value.",
						daoDefinition.getEntityDefinition().getEntityType()
								.getSimpleName());
				continue;
			}
			if (!cascade.shouldCascade(reference)) {
				if (logger.isDebugEnabled()) {
					logger.debug(
							"Decided not to cascade a save by entity {} to its dependency {}",
							daoDefinition.getEntityDefinition().getEntityType()
									.getSimpleName(), reference.getToClass()
									.getSimpleName());
				}
				continue;
			}
			Cascade subCascade = cascade.subCascade(reference);
			if (subCascade == null) {
				logger.info("A cascade object returned null for a sub cascade, that is not allowed.  Using 'never' instead");
				subCascade = CascadeUtils.never();
			}
			ByteIdentifiableDAO foreignDAO = daoDefinition
					.getOutgoingDAO(reference);
			if (foreignDAO != null) {
				List foreignEntities = HBaseDAOHelp.getForeignEntityList(
						entities, reference, true);
				if (logger.isDebugEnabled()) {
					logger.debug(
							"For the reference from {} to {}, found {} entities to save through a cascade",
							daoDefinition.getEntityDefinition().getEntityType()
									.getSimpleName(), reference.getToClass()
									.getSimpleName(), foreignEntities.size());
				}
				if (CollectionUtils.isNotEmpty(foreignEntities)) {
					verifyIds(foreignEntities);
					foreignDAO.save(foreignEntities, subCascade, transaction);
				}
				if (logger.isDebugEnabled()) {
					logger.debug(
							"Returned from the cascaded save for the reference from {} to {}",
							daoDefinition.getEntityDefinition().getEntityType()
									.getSimpleName(), reference.getToClass()
									.getSimpleName());
				}
			} else {
				logger.warn(
						"The dao for {} returned a null object from 'getOutgoingDAO' for the reference to {}",
						daoDefinition.getEntityDefinition().getEntityType()
								.getSimpleName(), reference.getToClass()
								.getSimpleName());
			}
		}
	}

	private void updateIndex(Index<T> index, Map<ByteId, T> entities,
			Transaction transaction) {
		if (logger.isTraceEnabled()) {
			logger.trace("Updating the index for " + index);
		}
		Map<ByteId, Put> puts = new HashMap<>();
		Map<ByteId, IndexRow> indexEntities = new DualHashBidiMap<>();
		IndexRowConverter converter = new IndexRowConverter(index.isUnique());
		com.google.common.collect.Multimap<ByteId, IndexRow> indexRows = HashMultimap
				.<ByteId, IndexRow> create();
		int countGood = 0;
		for (T entity : entities.values()) {
			if (entity == null) {
				if (logger.isDebugEnabled()) {
					logger.debug("Encountered a null entity in a collection while trying to update an index.");
				}
				continue;
			}
			try {
				ByteRange indexKeys = index.getKey(entity);
				if (indexKeys == null) {
					if (logger.isTraceEnabled()) {
						logger.trace(
								"A {} entity produced no key for the index: {}\n\t{}",
								entity.getClass().getSimpleName(),
								index.toString(), entity.toString());
					}
					// this is possible, sometimes entities don't satisfy the
					// index criteria.
					// TODO check if it's required. decide how to handle
					// missing required fields.
					continue;
				}
				countGood++;
				// ranges of index keys should only be used in searches, since
				// we are
				// doing a put right now we don't have to worry about it.
				IndexRow indexRow = new IndexRow(indexKeys.getFrom(),
						entity.getByteId());
				indexRows.put(indexKeys.getFrom(), indexRow);
			} catch (NullPointerException e) {
				logger.error(e.getMessage(), e);
				// just continue.
			}
		}
		if (logger.isDebugEnabled() && indexRows.isEmpty()) {
			logger.debug("Produced no index rows for index " + index);
		} else if (logger.isTraceEnabled()) {
			logger.trace("Produced " + countGood + " rows out of "
					+ entities.size());
		}
		for (ByteId indexKey : indexRows.keySet()) {
			Collection<IndexRow> rows = indexRows.get(indexKey);
			if (logger.isTraceEnabled() && rows.size() > 1) {
				logger.trace("Merging a multi value index key");
			}
			IndexRow merged = new IndexRow(indexKey);
			for (IndexRow ir : rows) {
				merged.addReferences(ir.getReferences());
			}
			Put put = converter.toPut(merged, null);
			puts.put(indexKey, put);
			indexEntities.put(indexKey, merged);
		}
		if (!puts.isEmpty()) {
			TransactionOp operation = indexFactory(index, converter).build(
					indexEntities, puts);
			transaction.addOperation(operation);
		} else if (logger.isDebugEnabled()) {
			logger.debug("Produced no put operations for the update on index "
					+ index);
		}
	}

	private TransactionOpFactory<IndexRow> indexFactory(Index<T> index,
			IndexRowConverter converter) {
		return TransactionOp
				.getFactory(IndexRowDefinition.get(index.isUnique()))
				.converter(converter).table(index.getIndexTable())
				.mapping(null);
	}

	private void cleanIndex(Index<T> index, Map<ByteId, T> entities,
			Transaction transaction) {
		if (logger.isTraceEnabled()) {
			logger.trace("Cleaning the index for " + index);
		}
		if (logger.isDebugEnabled() && entities.isEmpty()) {
			logger.debug("Asked to clean the index but given an empty map of entities.");
		}
		Map<ByteId, Delete> deletes = new HashMap<>();
		Map<ByteId, IndexRow> indexEntitiesToDelete = new HashMap<>();
		IndexRowConverter converter = new IndexRowConverter(index.isUnique());
		for (T entity : entities.values()) {
			if (entity == null) {
				if (logger.isDebugEnabled()) {
					logger.debug("Given a null entity to clean index from");
				}
				continue;
			}
			ByteRange indexKey = index.getKey(entity);
			if (indexKey == null) {
				if (logger.isDebugEnabled()) {
					logger.debug(
							"While trying to clean the {} index {}, an entity produced no key {}",
							daoDefinition.getEntityDefinition().getEntityType()
									.getSimpleName(), index.toString(),
							entity.toString());
				}
				continue;
			}
			// when doing a clean operation, ranges of keys should not be
			// possible,
			// so we don't worry about scan operations and what not. This is
			// only
			// reached when deleting specific instances, ranges of keys only
			// happen
			// when a search is done on the prefix of an index.
			IndexRow indexRow = new IndexRow(indexKey.getFrom());
			// TODO this is bad logic, if the index has multiple values
			// then you may just need to do a put operation with a null value
			// instead of deleting the entire key. This could result in losing
			// references to entities. This must be fixed.
			Delete delete = new Delete(indexKey.from());
			if (!index.isUnique()) {
				delete.deleteColumn(IndexRowConverter.COLUMN_FAMILY, entity
						.getByteId().getByteId());
			}
			deletes.put(indexKey.getFrom(), delete);
			indexEntitiesToDelete.put(indexKey.getFrom(), indexRow);
		}
		if (!deletes.isEmpty() && !indexEntitiesToDelete.isEmpty()) {
			TransactionOp operation = indexFactory(index, converter).build(
					indexEntitiesToDelete, deletes);
			transaction.addOperation(operation);
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug(
						"While trying to clean an index, no delete statements "
								+ "were generated, the entities must not have generated any index keys: {}",
						index.toString());
			}
		}
	}

	@Override
	public void deleteById(Collection<ByteId> ids, Cascade cascade,
			OnError onError) {
		if (logger.isTraceEnabled()) {
			logger.trace("Running deleteById method, cascade=" + cascade);
		}
		Map<ByteId, T> idMap = new HashMap<>();
		ids.forEach(id -> idMap.put(id, daoDefinition.getEntityDefinition()
				.newInstance(id)));
		Transaction transaction = new Transaction(OnError.CONTINUE);
		deleteByIdMap(idMap, cascade, transaction);
		transaction.execute();
	}

	@Override
	public void deleteByIdMap(Map<ByteId, T> ids, Cascade cascade,
			OnError onError) {
		Transaction transaction = new Transaction(OnError.CONTINUE);
		deleteByIdMap(ids, cascade, transaction);
		transaction.execute();
	}

	@Override
	public void deleteByIdMap(Map<ByteId, T> idMap, Cascade cascade,
			Transaction transaction) {
		if (logger.isTraceEnabled()) {
			logger.trace("Running deleteByIdMap method, cascade=" + cascade);
		}
		verifyIdsInCollection(idMap.keySet());

		Map<ByteId, T> entities = null;
		for (EntityReference reference : daoDefinition.getEntityDefinition()
				.getIncomingReferences()) {
			if (reference == null) {
				logger.warn(
						"The DAO for {} returned a collection of incoming references with a null value.",
						daoDefinition.getEntityDefinition().getEntityType()
								.getSimpleName());
				continue;
			}
			if (!reference.isRequired()) {
				// the entity that refers to this can allow a null value.
				continue;
			}
			if (!cascade.shouldCascade(reference)) {
				continue;
			}
			// if it's anything to many, do not cascade deletion
			// if it's anything to one, proceed with deletion cascading.
			if (reference.getType() == ReferenceType.MANYTOMANY
					|| reference.getType() == ReferenceType.ONETOMANY) {
				continue;
			}
			Cascade subCascade = cascade.subCascade(reference);
			if (logger.isTraceEnabled()) {
				logger.trace("cascading a delete by id map to reference "
						+ reference);
			}
			daoDefinition.getIncomingDAO(reference).deleteWhatRefersTo(
					idMap.keySet(), subCascade, transaction, reference);
			if (logger.isTraceEnabled()) {
				logger.trace("Finished a cascaded delete of reference "
						+ reference);
			}
		}
		// delete index entries for it too.
		for (Index<T> index : daoDefinition.getIndexes()) {
			// unfortunately there is no way to clear an index without
			// the
			// real objects.
			if (entities == null) {
				entities = getLazily(idMap.keySet(), transaction);
				transaction.flush();
			}
			cleanIndex(index, entities, transaction);
		}

		Map<ByteId, Delete> deletes = new HashMap<>();
		idMap.keySet().forEach(
				id -> deletes.put(id, new Delete(id.getByteId())));
		if (!entities.isEmpty() && !deletes.isEmpty()) {
			TransactionOp operation = startFactory().build(entities, deletes);
			transaction.addOperation(operation);
		} else {
			logger.debug("There were no entities to delete by idmap for {}",
					daoDefinition.getEntityDefinition().getEntityType()
							.getSimpleName());
		}
	}

	@Override
	public void deleteWhatRefersTo(Collection<ByteId> ids, Cascade cascade,
			Transaction transaction, EntityReference<T, ?> reference) {
		if (logger.isDebugEnabled()) {
			logger.debug(
					"The DAO for {} will delete what refers to {}, using {} id(s).",
					daoDefinition.getEntityDefinition().getEntityType()
							.getSimpleName(), reference.getToClass()
							.getSimpleName(), ids.size());
		}
		Index<T> index = daoDefinition.getIndexForReference(reference);
		if (index == null) {
			logger.warn(
					"The DAO for {} returned a null index for the reference to {}",
					daoDefinition.getEntityDefinition().getEntityType()
							.getSimpleName(), reference.getToClass()
							.getSimpleName());
			return;
		}
		Map<ByteId, Get> gets = new HashMap<>();
		Map<ByteId, IndexRow> indexRows = new HashMap<>();
		for (ByteId id : ids) {
			Get get = new Get(id.getByteId());
			gets.put(id, get);
			indexRows.put(id, new IndexRow(id));
		}
		IndexRowConverter converter = new IndexRowConverter(index.isUnique());
		TransactionOp operation = indexFactory(index, converter).build(
				indexRows, gets);
		transaction.addOperation(operation);
		// must get the index rows, then get the entities, then delete the
		// entities.
		transaction.flush();
		HBaseDAOHelp.removeNullValues(indexRows);

		Map<ByteId, T> entities = getUsingIndex_step2(indexRows,
				FetchUtils.never(), transaction);
		if (logger.isDebugEnabled()) {
			logger.debug(
					"Using the {} index on the {} entity, we found {} entities that will be deleted.",
					index.getIndexTableName(), daoDefinition
							.getEntityDefinition().getEntityType()
							.getSimpleName(), reference.getToClass()
							.getSimpleName(), entities.size());
		}
		deleteByIdMap(entities, cascade, transaction);
	}

	@Override
	public Map<ByteId, Boolean> hasIds(Collection<ByteId> ids) {
		Transaction transaction = new Transaction(OnError.CONTINUE);
		Map<ByteId, Boolean> results = hasIds(ids, transaction);
		transaction.execute();
		return results;
	}

	public Map<ByteId, Boolean> hasIds(Collection<ByteId> ids,
			Transaction transaction) {
		if (logger.isTraceEnabled()) {
			logger.trace("Running hasIds method");
		}
		Map<ByteId, Boolean> res = new HashMap<>();
		Column c = daoDefinition.getNonExistentColumn();
		List<Get> gets = new ArrayList<>();
		for (ByteId id : ids) {
			Get get = new Get(id.getByteId());
			// narrow down column to get so we minimize traffic.
			get.addColumn(c.getColumnFamily(), c.getColumnQualifier());
			gets.add(get);
			res.put(id, Boolean.FALSE);
		}
		// TODO make this use the transaction instead.
		try {
			Result[] results = daoDefinition.getTable().get(gets);
			for (Result result : results) {
				if (result.getRow() != null) {
					res.put(new ByteId(result.getRow()), Boolean.TRUE);
				}
			}
		} catch (IOException e) {
			logger.error(
					"Had an IO exception while retrieving results from a table.",
					e);
		}
		return res;
	}

	public Map<ByteId, T> getUsingIndex(Index<T> index, Fetch fetch,
			Transaction transaction, Collection<T> templates) {
		if (index == null) {
			throw new IllegalArgumentException(
					"I can't get anything with a null index.");
		}
		Map<ByteId, IndexRow> indexRows = getIndexRows(index, transaction,
				templates);
		if (logger.isDebugEnabled()) {
			logger.debug("Done flushing a get for an index row");
		}
		return getUsingIndex_step2(indexRows, fetch, transaction);
	}

	public Map<ByteId, T> getUsingIndex(Index<T> index, Fetch fetch,
			Transaction transaction, T template) {
		return getUsingIndex(index, fetch, transaction,
				Collections.singleton(template));
	}

	public Map<ByteId, T> getUsingIndex(Index<T> index, Fetch fetch,
			OnError onError, T template) {
		if (index == null) {
			throw new IllegalArgumentException(
					"I can't get anything with a null index.");
		}
		Transaction transaction = new Transaction(onError);
		Map<ByteId, T> results = getUsingIndex(index, fetch, transaction,
				template);
		transaction.execute();
		HBaseDAOHelp.removeNullValues(results);
		return results;
	}

	public Map<ByteId, T> scanUsingIndex(Index<T> index, Fetch fetch,
			Transaction transaction, T from, T to) {
		if (index == null) {
			throw new IllegalArgumentException(
					"I can't get anything with a null index.");
		}
		Map<ByteId, IndexRow> indexRows = getIndexRows(index, transaction,
				from, to);
		return getUsingIndex_step2(indexRows, fetch, transaction);
	}

	@Override
	public Set<ByteId> getIdsUsingIndex(Index<T> index, OnError onError,
			T from, T to) {
		if (index == null) {
			throw new IllegalArgumentException(
					"I can't get anything with a null index.");
		}
		Transaction transaction = new Transaction(onError);
		Set<ByteId> results = getIdsUsingIndex(index, transaction, from, to);
		transaction.execute();
		results.remove(null);
		return results;
	}

	@Override
	public Set<ByteId> getIdsUsingIndex(Index<T> index,
			Transaction transaction, T from, T to) {
		Map<ByteId, IndexRow> indexRows = getIndexRows(index, transaction,
				from, to);
		Set<ByteId> results = new HashSet<>();
		for (IndexRow row : indexRows.values()) {
			if (row != null) {
				results.addAll(row.getReferences());
			}
		}
		return results;
	}

	private Map<ByteId, IndexRow> getIndexRows(Index<T> index,
			Transaction transaction, T from, T to) {
		if (index == null) {
			throw new IllegalArgumentException(
					"I can't get anything with a null index.");
		}
		if (logger.isTraceEnabled()) {
			logger.trace("Doing a scan using an index.");
		}
		ByteId fromKey = index.getKey(from).getFrom();
		ByteId toKey = index.getKey(to).getTo();
		Scan indexScan = new Scan(fromKey.getByteId(), toKey.getByteId());
		Map<ByteId, Scan> indexScans = new HashMap<>();
		indexScans.put(fromKey, indexScan);
		Map<ByteId, IndexRow> indexRows = new HashMap<>();
		IndexRowConverter irc = new IndexRowConverter(index.isUnique());
		transaction.addOperation(indexFactory(index, irc).build(indexRows,
				indexScans));
		transaction.flush();
		HBaseDAOHelp.removeNullValues(indexRows);
		return indexRows;
	}

	@Override
	public Set<ByteId> getIdsUsingIndex(Index<T> index, OnError onError,
			Collection<T> templates) {
		if (index == null) {
			throw new IllegalArgumentException(
					"I can't get anything with a null index.");
		}
		Transaction transaction = new Transaction(onError);
		Set<ByteId> results = getIdsUsingIndex(index, transaction, templates);
		transaction.execute();
		return results;
	}

	@Override
	public Map<ByteId, T> getUsingIndex(Index<T> index, Fetch fetch,
			OnError onError, Collection<T> templates) {
		if (index == null) {
			throw new IllegalArgumentException(
					"I can't get anything with a null index.");
		}
		Transaction transaction = new Transaction(onError);
		Map<ByteId, T> results = getUsingIndex(index, fetch, transaction,
				templates);
		transaction.execute();
		return results;
	}

	public Set<ByteId> getIdsUsingIndex(Index<T> index,
			Transaction transaction, Collection<T> templates) {
		if (index == null) {
			throw new IllegalArgumentException(
					"I can't get anything with a null index.");
		}
		Map<ByteId, IndexRow> indexRows = getIndexRows(index, transaction,
				templates);
		transaction.flush();
		HBaseDAOHelp.removeNullValues(indexRows);
		Set<ByteId> results = new HashSet<>();
		for (IndexRow row : indexRows.values()) {
			if (row != null) {
				results.addAll(row.getReferences());
			}
		}
		return results;
	}

	@Override
	public Set<ByteId> getIdsUsingIndex(Index<T> index, OnError onError,
			T template) {
		if (index == null) {
			throw new IllegalArgumentException(
					"I can't get anything with a null index.");
		}
		Transaction transaction = new Transaction(onError);
		Set<ByteId> results = getIdsUsingIndex(index, transaction, template);
		transaction.execute();
		results.remove(null);
		return results;
	}

	@Override
	public Set<ByteId> getIdsUsingIndex(Index<T> index,
			Transaction transaction, T template) {
		if (index == null) {
			throw new IllegalArgumentException(
					"I can't get anything with a null index.");
		}
		return getIdsUsingIndex(index, transaction,
				Collections.singleton(template));
	}

	private Map<ByteId, IndexRow> getIndexRows(Index<T> index,
			Transaction transaction, T template) {
		if (index == null) {
			throw new IllegalArgumentException(
					"I can't get anything with a null index.");
		}
		return getIndexRows(index, transaction, Collections.singleton(template));
	}

	private Map<ByteId, IndexRow> getIndexRows(Index<T> index,
			Transaction transaction, Collection<T> templates) {
		if (index == null) {
			throw new IllegalArgumentException(
					"I can't get anything with a null index.");
		}
		if (logger.isTraceEnabled()) {
			logger.trace("Doing a get using an index " + index);
		}
		Map<ByteId, IndexRow> indexRowsToGet = new HashMap<>();
		if (templates == null || templates.isEmpty()) {
			logger.debug("Given an empty set of templates to retrieve index rows for.");
			return indexRowsToGet;
		}
		Map<ByteId, Get> getActions = new HashMap<>();
		Set<ByteRange> foundRanges = new HashSet<>();
		for (T template : templates) {
			ByteRange idRange = index.getKey(template);
			if (idRange != null) {
				if (idRange.isSingleValue()) {
					Get get = new Get(idRange.from());
					getActions.put(idRange.getFrom(), get);
					indexRowsToGet.put(idRange.getFrom(),
							new IndexRow(idRange.getFrom()));
				} else {
					// we will come back to these and make scan items later.
					foundRanges.add(idRange);
				}
			}
		}
		Map<ByteId, Scan> scanActions = new HashMap<>();
		Map<ByteId, IndexRow> indexRowsToScan = new HashMap<>();
		if (!foundRanges.isEmpty()) {
			// merge them so we avoid a lot of unnecessary IO.
			foundRanges = ByteRange.merge(foundRanges);
			// now create the scans.
			for (ByteRange idRange : foundRanges) {
				Scan scan = new Scan(idRange.from(), idRange.to());
				scanActions.put(idRange.getFrom(), scan);
				indexRowsToScan.put(idRange.getFrom(),
						new IndexRow(idRange.getFrom()));
			}
		}
		if (indexRowsToGet.isEmpty() && indexRowsToScan.isEmpty()) {
			// return early, there is nothing to get.
			return indexRowsToGet;
		}
		IndexRowConverter irc = new IndexRowConverter(index.isUnique());
		if (!indexRowsToGet.isEmpty()) {
			transaction.addOperation(indexFactory(index, irc).build(
					indexRowsToGet, getActions));
		}
		if (!indexRowsToScan.isEmpty()) {
			transaction.addOperation(indexFactory(index, irc).build(
					indexRowsToScan, scanActions));
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Flushing a get for an index row");
		}
		transaction.flush();

		indexRowsToGet.putAll(indexRowsToScan);

		HBaseDAOHelp.removeNullValues(indexRowsToGet);
		return indexRowsToGet;
	}

	private Map<ByteId, T> getUsingIndex_step2(Map<ByteId, IndexRow> indexRows,
			Fetch fetch, Transaction transaction) {
		if (indexRows == null || indexRows.isEmpty()) {
			return Collections.EMPTY_MAP;
		}
		// step two:
		if (logger.isTraceEnabled()) {
			logger.trace("Retrieving values from a table using "
					+ indexRows.size() + " index rows");
		}
		List<ByteId> ids = new ArrayList<>();
		for (IndexRow indexRow : indexRows.values()) {
			if (indexRow == null) {
				continue;
			}
			// Map<ByteId, Get> gets = new HashMap<>();
			for (ByteId ref : indexRow.getReferences()) {
				ids.add(ref);
				// Get getEntity = new Get(ref.getByteId());
				// gets.put(ref, getEntity);
				// T entity =
				// daoDefinition.getEntityDefinition().newInstance(ref);
				// entities.put(ref, entity);
			}
		}
		if (logger.isDebugEnabled() && ids.isEmpty()) {
			logger.trace("Found no entities using a map of index rows.");
		}
		return get(ids, fetch, transaction);
		// the transaction needs to be executed, but that comes later.
		// return entities;
	}

	public void deleteUsingIndex(Index<T> index, Cascade cascade,
			OnError onError, T template) {
		if (index == null) {
			throw new IllegalArgumentException(
					"I can't delete anything with a null index.");
		}
		Transaction transaction = new Transaction(onError);
		deleteUsingIndex(index, cascade, transaction, template);
		transaction.execute();
	}

	public void deleteUsingIndex(Index<T> index, Cascade cascade,
			Transaction transaction, T template) {
		if (index == null) {
			throw new IllegalArgumentException(
					"I can't delete anything with a null index.");
		}
		Map<ByteId, T> entities = getUsingIndex(index, FetchUtils.never(),
				transaction, template);
		deleteByIdMap(entities, cascade, transaction);
	}

	public void deleteUsingIndexRange(Index<T> index, Cascade cascade,
			OnError onError, T from, T to) {
		if (index == null) {
			throw new IllegalArgumentException(
					"I can't delete anything with a null index.");
		}
		Transaction transaction = new Transaction(onError);
		deleteUsingIndexRange(index, cascade, transaction, from, to);
		transaction.execute();
	}

	public void deleteUsingIndexRange(Index<T> index, Cascade cascade,
			Transaction transaction, T from, T to) {
		if (index == null) {
			throw new IllegalArgumentException(
					"I can't delete anything with a null index.");
		}
		Map<ByteId, T> entities = scanUsingIndex(index, FetchUtils.never(),
				transaction, from, to);
		deleteByIdMap(entities, cascade, transaction);
	}

	public Map<ByteId, T> getAll(Fetch fetch, OnError onError,
			Object serverFilter, Predicate<T> clientFilter) {
		Transaction transaction = new Transaction(onError);
		Map<ByteId, T> all = getAll(fetch, transaction, serverFilter,
				clientFilter);
		transaction.execute();
		return all;
	}

	public DAODefinition<T> getDefinition() {
		return daoDefinition;
	}

	@Override
	public Class<T> getEntityClass() {
		return daoDefinition.getEntityDefinition().getEntityType();
	}
}