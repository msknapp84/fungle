package fungle.common.hdao.model;

import org.apache.hadoop.hbase.util.Bytes;

public class Column {
	private byte[] columnFamily;
	private byte[] columnQualifier;
	private boolean foreignReference;

	public Column() {

	}

	public Column(byte[] columnFamily, byte[] columnQualifier) {
		this(columnFamily,columnQualifier,false);
	}

	public Column(byte[] columnFamily, byte[] columnQualifier,boolean foreignReference) {
		this.columnFamily = columnFamily;
		this.columnQualifier = columnQualifier;
		this.foreignReference=foreignReference;
	}

	public static Column from(String cf, String cq) {
		return new Column(Bytes.toBytes(cf), Bytes.toBytes(cq));
	}

	public static Column from(String cf, String cq,boolean foreignReference) {
		return new Column(Bytes.toBytes(cf), Bytes.toBytes(cq),foreignReference);
	}

	public byte[] getColumnFamily() {
		return columnFamily;
	}

	public void setColumnFamily(byte[] columnFamily) {
		this.columnFamily = columnFamily;
	}

	public byte[] getColumnQualifier() {
		return columnQualifier;
	}

	public void setColumnQualifier(byte[] columnQualifier) {
		this.columnQualifier = columnQualifier;
	}

	public boolean isForeignReference() {
		return foreignReference;
	}

	public void setForeignReference(boolean foreignReference) {
		this.foreignReference = foreignReference;
	}
	
	public String toString() {
		return Bytes.toString(columnFamily)+" "+Bytes.toString(columnQualifier);
	}
}