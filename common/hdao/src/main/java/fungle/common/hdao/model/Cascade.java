package fungle.common.hdao.model;

public interface Cascade {
	boolean shouldCascade(EntityReference<?,?> reference);
	Cascade subCascade(EntityReference<?,?> reference);
}
