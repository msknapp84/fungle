package fungle.common.hdao.transaction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeoutException;

import org.apache.hadoop.hbase.client.HTable;

import fungle.common.hdao.model.ByteId;

public class SimpleLockingMechanism implements LockingMechanism {

	private final Map<HTable,Map<ByteId,Locks>> locks = new HashMap<HTable,Map<ByteId,Locks>>();
	
	@Override
	public void acquireSharedLock(HTable table, Set<ByteId> rows) throws InterruptedException, TimeoutException {
		Map<ByteId, Locks> m = getMapForTable(table);
		for (ByteId row : rows) {
			Locks ls = getLocksForRow(m, row);
			ls.addShared(Thread.currentThread());
		}
	}

	private Locks getLocksForRow(Map<ByteId, Locks> m, ByteId row) {
		Locks ls = m.get(row);
		if (ls == null) {
			synchronized (m) {
				if (m.get(row)==null) {
					ls = new Locks();
					m.put(row, ls);
				}
			}
			ls = m.get(row);
		}
		return ls;
	}

	private Map<ByteId, Locks> getMapForTable(HTable table) {
		Map<ByteId,Locks> m = locks.get(table);
		if (m==null) {
			synchronized (locks) {
				m = new HashMap<ByteId,Locks>();
				if (locks.get(table)==null) {
					locks.put(table, m);
				}
			}
			m = locks.get(table);
		}
		return m;
	}

	@Override
	public void acquireExclusiveLock(HTable table, Set<ByteId> rows) throws InterruptedException {
		Map<ByteId, Locks> m = getMapForTable(table);
		for (ByteId row : rows) {
			Locks ls = getLocksForRow(m, row);
			ls.addExclusive(Thread.currentThread());
		}
	}

	@Override
	public void releaseLocks(HTable table, Set<ByteId> rows) {
		Map<ByteId, Locks> m = getMapForTable(table);
		for (ByteId row : rows) {
			Locks ls = getLocksForRow(m, row);
			ls.releaseLocks(Thread.currentThread());
		}
	}
	
	private static final class Locks {
		private List<Thread> shared = new ArrayList<>();
		private Thread pendingExclusive;
		private Thread exclusive;
		
		public synchronized void addShared(Thread threadId) throws InterruptedException, TimeoutException {
			checkAndWait();
			shared.add(threadId);
		}
		
		public synchronized void releaseLocks(Thread currentThread) {
			shared.remove(currentThread);
			if (pendingExclusive.equals(currentThread)) {
				pendingExclusive=null;
			}
			if (exclusive.equals(currentThread)) {
				exclusive=null;
			}
		}

		private void checkAndWait() throws InterruptedException, TimeoutException {
			if (exclusive !=null) {
				exclusive.join(4000);
				if (exclusive!=null) {
					throw new TimeoutException("An exclusive lock on a row is preventing the operation from proceeding.");
				}
			}
			if (pendingExclusive!=null) {
				pendingExclusive.join(4000);
				if (pendingExclusive!=null) {
					throw new TimeoutException("An exclusive lock on a row is preventing the operation from proceeding.");
				}
			}
		}
		
		public synchronized void addExclusive(Thread threadId) throws InterruptedException {
			if (exclusive!=null) {
				if (pendingExclusive==null) {
					pendingExclusive=threadId;
				} else {
					pendingExclusive.join();
				}
				exclusive.join();
			}
			exclusive=threadId;
		}
		
	}

}
