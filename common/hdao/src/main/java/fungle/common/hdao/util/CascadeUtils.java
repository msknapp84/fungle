package fungle.common.hdao.util;

import java.util.List;

import fungle.common.hdao.model.Cascade;
import fungle.common.hdao.model.EntityReference;

public final class CascadeUtils {
	private CascadeUtils(){}
	
	public static Cascade always() {
		return new Cascade() {

			@Override
			public boolean shouldCascade(EntityReference<?,?> reference) {
				return true;
			}

			@Override
			public Cascade subCascade(EntityReference<?,?> reference) {
				return this;
			}

		};
	}
	
	public static Cascade never() {
		return new Cascade() {

			@Override
			public boolean shouldCascade(EntityReference<?,?> reference) {
				return false;
			}

			@Override
			public Cascade subCascade(EntityReference<?,?> reference) {
				return this;
			}
		};
	}
	
	public static Cascade oneLevel() {
		return new Cascade() {

			@Override
			public boolean shouldCascade(EntityReference<?,?> reference) {
				return true;
			}

			@Override
			public Cascade subCascade(EntityReference<?,?> reference) {
				return never();
			}
		};
	}
	
	public static class DefaultCascade implements Cascade {
		private final List<EntityReference<?,?>> refs;
		private final Cascade sub;

		public DefaultCascade(List<EntityReference<?,?>> refs,Cascade sub) {
			this.refs = refs;
			this.sub = sub;
		}
		
		@Override
		public boolean shouldCascade(EntityReference<?,?> reference) {
			return refs.contains(reference);
		}

		@Override
		public Cascade subCascade(EntityReference<?,?> reference) {
			return sub;
		}
		
	}
	
	public static Cascade forRefs(List<EntityReference<?,?>> refs) {
		return new DefaultCascade(refs, always());
	}
}
