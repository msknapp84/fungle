package fungle.common.hdao;

import java.util.List;

import org.apache.commons.collections4.BidiMap;
import org.apache.hadoop.hbase.client.HTable;

import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.Column;
import fungle.common.hdao.model.DAODefinition;
import fungle.common.hdao.model.EntityDefinition;
import fungle.common.hdao.model.IDGenerator;
import fungle.common.hdao.model.Index;
import fungle.common.hdao.model.SimpleIDGenerator;
import fungle.common.hdao.util.TableNameService;

public abstract class BaseDAODefinition<T extends ByteIdentifiable> implements
		DAODefinition<T> {

	protected final EntityDefinition<T> definition;
	protected final TableNameService tableNameService;
	
	protected BidiMap<String, Column> mapping;
	protected List<Index<T>> indexes;
	
	public BaseDAODefinition(final EntityDefinition<T> definition,
			TableNameService tableNameService) {
		this.definition = definition;
		this.tableNameService = tableNameService;
	}

	@Override
	public EntityDefinition<T> getEntityDefinition() {
		return definition;
	}

	@Override
	public List<Index<T>> getIndexes() {
		if (indexes == null) {
			indexes = buildIndexes();
		}
		return indexes;
	}

	@Override
	public BidiMap<String, Column> getMapping() {
		if (mapping==null) {
			mapping = buildMapping();
		}
		return mapping;
	}

	@Override
	public IDGenerator getByteIdGenerator() {
		return SimpleIDGenerator.getInstance(definition.getEntityType());
	}

	@Override
	public Column getNonExistentColumn() {
		return Column.from("vwjpoafespo", "pojvwe");
	}

	@Override
	public HTable getTable() {
		return tableNameService.get(definition.getEntityType()).getTable();
	}

	public abstract List<Index<T>> buildIndexes();
	public abstract BidiMap<String, Column> buildMapping(); 
}