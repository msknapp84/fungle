package fungle.common.hdao.model;

import org.apache.commons.collections4.BidiMap;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;

import fungle.common.hdao.ResultConverter;

public class IndexRowConverter implements ResultConverter<IndexRow> {
	public static final byte[] COLUMN_FAMILY = Bytes.toBytes("f");
	public static final byte[] COLUMN_QUALIFIER = Bytes.toBytes("q");

	private boolean unique = false;
	
	public IndexRowConverter(boolean unique) {
		this.unique = unique;
	}
	
	@Override
	public IndexRow toEntity(Result result, BidiMap<String, Column> mapping) {
		IndexRow row =new IndexRow();
		return mergeInto(row, result, mapping);
	}

	@Override
	public Put toPut(IndexRow entity, BidiMap<String, Column> mapping) {
		Put put = new Put(entity.getByteId().getByteId());
		if (unique) {
			put.add(COLUMN_FAMILY,COLUMN_QUALIFIER , entity.getSingleReference().getByteId());
		} else {
			for (ByteId ref : entity.getReferences()) {
				put.add(COLUMN_FAMILY,ref.getByteId(),Bytes.toBytes("1"));
			}
		}
		return put;
	}

	@Override
	public IndexRow mergeInto(IndexRow row, Result result,
			BidiMap<String, Column> mapping) {
		row.setByteId(result.getRow());
		for (KeyValue kv : result.list()) {
			if (unique) {
				row.setSingleReference(kv.getValue());
			} else {
				row.addReference(kv.getQualifier());
			}
		}
		return row;
	}

}
