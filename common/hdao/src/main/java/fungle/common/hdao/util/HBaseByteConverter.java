package fungle.common.hdao.util;

import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.hbase.util.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fungle.common.core.util.TypeConverter;
import fungle.common.hdao.ByteConverter;
import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.ByteIdentifiable;

public final class HBaseByteConverter {
	private static final Logger logger = LoggerFactory.getLogger(HBaseByteConverter.class);
	private static final Map<Class<?>, ByteConverter<?>> typeConverters = new HashMap<>();

	public static void addConverter(Class<?> clazz, ByteConverter<?> converter) {
		typeConverters.put(clazz, converter);
	}

	private HBaseByteConverter() {
	}

	public static <T> T toObject(byte[] value, Class<T> type) {
		if (typeConverters.containsKey(type)) {
			return (T) typeConverters.get(type).fromBytes(value);
		}
		if (ByteId.class.isAssignableFrom(type)) {
			return (T) new ByteId(value);
		}
		if (ByteIdentifiable.class.isAssignableFrom(type)) {
			ByteIdentifiable instance = null;
			try {
				instance = (ByteIdentifiable) type.newInstance();
				instance.setByteId(value);
			} catch (Exception e) {
			}
			return (T) instance;
		}
		if (Date.class.equals(type)) {
			Date d = new Date(Bytes.toLong(value));
			return (T) d;
		}
		if (Calendar.class.equals(type)) {
			Calendar c = Calendar.getInstance();
			c.setTimeInMillis(Bytes.toLong(value));
			return (T) c;
		}
		if (ByteIdentifiable.class.isAssignableFrom(type)) {
			T t;
			try {
				t = type.newInstance();
				((ByteIdentifiable) t).setByteId(value);
				return t;
			} catch (Exception e) {
			}
			return null;
		}
		Class<?> argType = type;
		if (TypeConverter.isPrimitiveWrapper(type)) {
			argType = TypeConverter.getPrimitive(type);
		}
		String toName = "to" + StringUtils.capitalize(argType.getSimpleName());
		try {
			Class<?> c = byte[].class;
			Method m = Bytes.class.getMethod(toName, new Class<?>[] { c });
			Object o = m.invoke(null, value);
			return TypeConverter.convert(o, type);
		} catch (Exception e) {
		}
		logger.warn("Found no way to transform bytes into the class "+type.getSimpleName());
		return null;
	}

	public static byte[] toBytes(Object value) {
		byte[] res = null;
		// use type converters first, it gives people an opportunity 
		// to override the default behaviour.  For some classes (like
		// address) the default behaviour is wrong.
		if (typeConverters.containsKey(value.getClass())) {
			ByteConverter<Object> bc = (ByteConverter<Object>) typeConverters
					.get(value.getClass());
			res = bc.toBytes(value);
			if (res != null) {
				return res;
			}
		}
		if (ByteIdentifiable.class.isAssignableFrom(value.getClass())) {
			ByteIdentifiable bid = (ByteIdentifiable) value;
			return bid.getByteId() == null ? null : bid.getByteId().getByteId();
		}
		if (Date.class.isAssignableFrom(value.getClass())) {
			return Bytes.toBytes(((Date) value).getTime());
		}
		if (Calendar.class.isAssignableFrom(value.getClass())) {
			return Bytes.toBytes(((Calendar) value).getTimeInMillis());
		}
		Class<?> argType = value.getClass();
		if (TypeConverter.isPrimitiveWrapper(value.getClass())) {
			argType = TypeConverter.getPrimitive(value.getClass());
		}
		try {
			Method m = Bytes.class.getMethod("toBytes",
					new Class<?>[] { argType });
			res = (byte[]) m.invoke(null, value);
		} catch (Exception e) {
		}
		if (res == null) {
			logger.warn("Found no way to transform a class into bytes "+value.getClass().getSimpleName());
		}
		return res;
	}

	public static ByteId invert(ByteId resp) {
		return new ByteId(invert(resp.getByteId()));
	}

	public static byte[] invert(byte[] res) {
		byte[] inverted = new byte[res.length];
		for (int i = 0; i < res.length; i++) {
			inverted[i] = (byte) (Byte.MAX_VALUE - res[i]);
		}
		return inverted;
	}

	public static ByteId merge(ByteId... bytes) {
		int length = 0;
		for (ByteId b : bytes) {
			length += b.length();
		}
		byte[] combined = new byte[length];
		int destPos = 0;
		for (ByteId b : bytes) {
			System.arraycopy(b.getByteId(), 0, combined, destPos, b.length());
			destPos += b.length();
		}
		return new ByteId(combined);
	}

	public static byte[] merge(byte[]... bytes) {
		if (bytes == null || bytes.length < 1) {
			return new byte[0];
		}
		int length = 0;
		for (byte[] b : bytes) {
			length += b.length;
		}
		byte[] combined = new byte[length];
		if (length < 1) {
			return combined;
		}
		if (length == 0) {
			return combined;
		}
		int destPos = 0;
		for (byte[] b : bytes) {
			System.arraycopy(b, 0, combined, destPos, b.length);
			destPos += b.length;
		}
		return combined;
	}

	public static ByteId mergeBytes(List<byte[]> bytes) {
		if (bytes == null || bytes.isEmpty()) {
			return null;
		}
		int length = 0;
		for (byte[] b : bytes) {
			if (b != null) {
				length += b.length;
			}
		}
		if (length == 0) {
			return null;
		}
		byte[] combined = new byte[length];
		int destPos = 0;
		for (byte[] b : bytes) {
			if (b != null) {
				System.arraycopy(b, 0, combined, destPos, b.length);
				destPos += b.length;
			}
		}
		return new ByteId(combined);
	}

	public static ByteId merge(List<ByteId> bytes) {
		if (bytes == null || bytes.isEmpty()) {
			return null;
		}
		int length = 0;
		for (ByteId b : bytes) {
			length += b.length();
		}
		if (length == 0) {
			return null;
		}
		byte[] combined = new byte[length];
		int destPos = 0;
		for (ByteId b : bytes) {
			System.arraycopy(b, 0, combined, destPos, b.length());
			destPos += b.length();
		}
		return new ByteId(combined);
	}
}