package fungle.common.hdao;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeoutException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.HTable;

import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.transaction.Isolation;
import fungle.common.hdao.transaction.LockingMechanism;
import fungle.common.hdao.transaction.SimpleLockingMechanism;

public class HBaseDAOSettings {
	private static final Map<Configuration,MySettings> settings = new HashMap<Configuration,MySettings>();

	public static Isolation getIsolation(Configuration configuration) {
		return getSettings(configuration).getIsolation();
	}
	
	private static MySettings getSettings(Configuration configuration) {
		MySettings ms = settings.get(configuration);
		if (ms == null) {
			ms = new MySettings();
		}
		return ms;
	}

	public static void setIsolation(Configuration configuration,Isolation isolation) {
		getSettings(configuration).setIsolation(isolation);
	}
	
	public static LockingMechanism getLockingMechanism(Configuration configuration) {
		return getSettings(configuration).getLockingMechanism();
	}
	
	public static void setLockingMechanism(Configuration configuration,LockingMechanism lockingMechanism) {
		getSettings(configuration).setLockingMechanism(lockingMechanism);
	}
	
	
	private static class MySettings {
		private Isolation isolation;
		private LockingMechanism lockingMechanism;

		private Isolation getIsolation() {
			if (isolation == null) {
				isolation = Isolation.REPEATABLE_READ;
			}
			return isolation;
		}
		
		private void setIsolation(Isolation isolation) {
			if (isolation == null) {
				isolation = Isolation.READ_UNCOMMITTED;
			}
			isolation = isolation;
		}
		
		private LockingMechanism getLockingMechanism() {
			if (lockingMechanism==null) {
				lockingMechanism=new SimpleLockingMechanism();
			}
			return lockingMechanism;
		}
		
		private void setLockingMechanism(LockingMechanism lockingMechanism) {
			if (lockingMechanism==null) {
				lockingMechanism = new LockingMechanism() {
					

					@Override
					public void acquireSharedLock(HTable table, Set<ByteId> rows)
							throws InterruptedException, TimeoutException {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void acquireExclusiveLock(HTable table,
							Set<ByteId> rows) throws InterruptedException,
							TimeoutException {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void releaseLocks(HTable table, Set<ByteId> rows) {
						// TODO Auto-generated method stub
						
					}
				};
			}
			lockingMechanism = lockingMechanism;
		}
	}
	
}
