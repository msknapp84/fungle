package fungle.common.hdao;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.Cascade;
import fungle.common.hdao.model.OnError;
import fungle.common.hdao.transaction.Transaction;

public abstract class MemDAOBase<T extends ByteIdentifiable> extends
		ByteIdentifiableDAOBase<T> {

	private Map<ByteId, T> index = new HashMap<>();

	public T get(ByteId id,boolean eager) {
		return index.get(id);
	}
	

	public Map<ByteId, T> get(Collection<ByteId> ids,boolean eager) {
		Map<ByteId, T> mp = new HashMap<>();
		ids.forEach(id -> {if (index.get(id)!=null) mp.put(id, index.get(id));});
		return mp;
	}
//
//	public Map<U, T> getByField(String fieldName, Object value,boolean eager) {
//		Map<U, T> mp = new HashMap<>();
//		boolean b = false;
//		Field f = null;
//		try {
//			f = getEntityClass().getDeclaredField(fieldName);
//			b = f.isAccessible();
//			final Field ff = f;
//			index.values().forEach(entity -> {
//				try {
//					if (ff.get(entity).equals(value))
//						mp.put(entity.getByteId(), entity);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			});
//		} catch (Exception e1) {
//			e1.printStackTrace();
//		} finally {
//			if (f!=null && !b) {
//				f.setAccessible(false);
//			}
//		}
//		return mp;
//	}
//
//	public void save(Collection<T> entities,Cascade cascade) {
//		entities.forEach(entity -> index.put(entity.getByteId(), entity));
//	}
//
//	public void save(T entity,Cascade cascade) {
//		index.put(entity.getByteId(), entity);
//	}
//
//	public void delete(Collection<T> entities,Cascade cascade) {
//		entities.forEach(entity -> index.remove(entity.getByteId()));
//	}
//
//	public void delete(T entity,Cascade cascade) {
//		index.remove(entity.getByteId());
//	}
//
//	public void deleteById(Collection<U> ids,Cascade cascade) {
//		ids.forEach(id -> index.remove(id));
//	}
//
//	public void deleteById(U id,Cascade cascade) {
//		index.remove(id);
//	}
//
//	
//
//	@Override
//	public Map<U, Boolean> hasIds(Collection<U> ids) {
//		// TODO Auto-generated method stub
//		return null;
//	}


	@Override
	public Map<ByteId, T> get(Collection<ByteId> ids, Fetch fetch,
			Transaction transaction) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(Collection<T> entities, Cascade cascade, OnError onError) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save(Collection<T> entities, Cascade cascade,
			Transaction transaction) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteById(Collection<ByteId> ids, Cascade cascade,
			OnError onError) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteByIdMap(Map<ByteId, T> idMap, Cascade cascade,
			Transaction transaction) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Class<T> getEntityClass() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteByIdMap(Map<ByteId, T> ids, Cascade cascade,
			OnError onError) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Map<ByteId, Boolean> hasIds(Collection<ByteId> ids) {
		// TODO Auto-generated method stub
		return null;
	}
}