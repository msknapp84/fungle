package fungle.common.hdao;

import fungle.common.hdao.model.EntityReference;

public interface Fetch {
	boolean shouldFetch(EntityReference<?,?> reference);
	Fetch getSubFetch(EntityReference<?,?> reference);
}