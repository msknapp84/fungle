package fungle.common.hdao.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.Validate;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.util.Bytes.ByteArrayComparator;

public class ByteRange implements Comparable<ByteRange> {
	private final ByteId from, to;

	public ByteRange(ByteId from) {
		Validate.notNull(from);
		this.from = from;
		this.to = from;
	}

	public ByteRange(ByteId from, ByteId to) {
		Validate.notNull(from);
		Validate.notNull(to);
		this.from = from;
		this.to = to;
	}

	public ByteRange(byte[] from) {
		Validate.notNull(from);
		this.from = new ByteId(from);
		this.to = this.from;
	}

	public ByteRange(byte[] from, byte[] to) {
		Validate.notNull(from);
		Validate.notNull(to);
		this.from = new ByteId(from);
		this.to = new ByteId(to);
	}

	public boolean isSingleValue() {
		return this.from.equals(this.to);
	}
	
	public ByteId getFrom() {
		return from;
	}
	
	public byte[] from() {
		return from.getByteId();
	}
	
	public byte[] to() {
		return to.getByteId();
	}

	public ByteId getTo() {
		return to;
	}
	
	public boolean matches(Scan scan) {
		return (Arrays.equals(from.getByteId(),scan.getStartRow()) 
				&& Arrays.equals(to.getByteId(),scan.getStopRow())); 
	}

	public boolean equals(ByteRange other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ByteRange)) {
			return false;
		}
		ByteRange that = (ByteRange) other;
		return this.from.equals(that.from) && this.to.equals(that.to);
	}

	public int hashCode() {
		return new HashCodeBuilder().append(from).append(to).toHashCode();
	}
	
	public int compareTo(ByteRange other) {
		return from.compareTo(other.from);
	}

	public static Set<ByteRange> merge(Set<ByteRange> foundRanges) {
		if (foundRanges==null || foundRanges.isEmpty()) {
			return Collections.EMPTY_SET;
		}
		List<ByteRange> sorted = new ArrayList<>(foundRanges);
		Collections.sort(sorted);
		byte[] start = sorted.get(0).from();
		byte[] stop = sorted.get(0).to();
		ByteArrayComparator comparator = new ByteArrayComparator();
		Set<ByteRange> merged = new HashSet<>();
		for (ByteRange idRange : sorted) {
			// if the stop from the last range is less
			if (comparator.compare(stop, idRange.from())>=-1) {
				// these can be merged into one.
				if (comparator.compare(idRange.to(), stop)>0) {
					// the new range has a later stop value
					stop = idRange.to();
				}
			} else {
				// we have identified a gap in the ranges,
				merged.add(new ByteRange(start,stop));
				start=idRange.from();
				stop = idRange.to();
			}
		}
		// don't forget to add the last one.
		merged.add(new ByteRange(start,stop));
		return merged;
	}
	
	
}
