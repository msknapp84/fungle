package fungle.common.hdao.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.Validate;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.util.Bytes;

import fungle.common.core.util.Reflect;
import fungle.common.hdao.util.HBaseByteConverter;

public class BasicIndex<T extends ByteIdentifiable> implements Index<T> {
	private final HTable indexTable;
	private final HTable referencedTable;
	private final boolean unique;
	private final List<IndexField> fields;

	private BasicIndex(final HTable indexTable, final HTable referencedTable,
			final boolean unique, final List<IndexField> fields) {
		this.indexTable = indexTable;
		this.referencedTable = referencedTable;
		this.unique = unique;
		this.fields = fields;
		validate();
	}

	private void validate() {
		Validate.notNull(this.indexTable,"Index table must not be null.");
		Validate.notNull(this.referencedTable,"Referenced table must not be null.");
		Validate.notEmpty(fields,"Fields must not be null or empty.");
	}

	@Override
	public ByteRange getKey(T entity) {
		if (entity == null) {
			return null;
		}
		List<byte[]> bs = new ArrayList<>();
		for (IndexField indexField : fields) {
			Object value = Reflect.getFieldValue(indexField.getName(), entity,
					Object.class);
			if (value == null) {
				// no reason to worry, just continue
				continue;
			}
			byte[] res = HBaseByteConverter.toBytes(value);
			if (res != null && res.length > 0) {
				if (!indexField.isAscending()) {
					// we invert it so the value is indexed in descending order.
					res = HBaseByteConverter.invert(res);
				}
				bs.add(res);
			} else if (!ByteIdentifiable.class.isAssignableFrom(value.getClass())) {
				// if it is another entity, perhaps that entity is missing an id, so I 
				// don't worry about it.
				
				// if it's something else, we really should have been able to 
				// convert it to bytes somehow.
				System.out.println("Failed to convert a value to bytes. type "+value.getClass());
				// TODO switch to a logger.
			}
		}
		ByteId id = HBaseByteConverter.mergeBytes(bs);
		return id==null?null:new ByteRange(id);
	}

	@Override
	public boolean isUnique() {
		return unique;
	}

	@Override
	public String getIndexTableName() {
		return Bytes.toString(indexTable.getTableName());
	}

	@Override
	public List<IndexField> getFieldsInOrder() {
		return fields;
	}

	@Override
	public String getReferenceTableName() {
		return Bytes.toString(referencedTable.getTableName());
	}

	public static BasicIndexBuilder builder() {
		return new BasicIndexBuilder();
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (unique) {
			sb.append("unique ");
		} else {
			sb.append("multi-key ");
		}
		sb.append("index on ");
		sb.append(this.getReferenceTableName()).append(" in table ");
		sb.append(getIndexTableName())
				.append(' ');
		fields.forEach(f -> {
			sb.append(f.getName() + (f.isAscending() ? " asc, " : " desc, "));
		});
		return sb.toString();
	}

	public static class BasicIndexBuilder {
		private HTable indexTable, referenceTable;
		private boolean unique;
		private List<IndexField> fields = new ArrayList<IndexField>();

		public BasicIndexBuilder() {

		}

		public BasicIndexBuilder indexTable(HTable indexTable) {
			this.indexTable = indexTable;
			return this;
		}

		public BasicIndexBuilder referenceTable(HTable referenceTable) {
			this.referenceTable = referenceTable;
			return this;
		}

		public BasicIndexBuilder unique(boolean unique) {
			this.unique = unique;
			return this;
		}

		public BasicIndexBuilder ascending(String fieldName) {
			this.fields.add(new IndexField(fieldName, true, true));
			return this;
		}

		public BasicIndexBuilder descending(String fieldName) {
			this.fields.add(new IndexField(fieldName, true, false));
			return this;
		}

		public BasicIndexBuilder virtual(String fieldName) {
			this.fields.add(new IndexField(fieldName, false, false));
			return this;
		}

		public <T extends ByteIdentifiable> BasicIndex<T> build() {
			return new BasicIndex<>(indexTable, referenceTable, unique, fields);
		}
	}

	@Override
	public HTable getReferencedTable() {
		return referencedTable;
	}

	@Override
	public HTable getIndexTable() {
		return indexTable;
	}
}