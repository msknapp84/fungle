package fungle.common.hdao.model;

/**
 * A reference between two entities.  The entity on the from side is always the one that
 * is responsible for 'knowing' about the foreign entity, the foreign entity may or may not 
 * have any knowledge of the entity that refers to it.
 * @author cloudera
 *
 * @param <F>
 * @param <T>
 */
public interface EntityReference<F extends ByteIdentifiable,T extends ByteIdentifiable> extends EntityReferenceConverter<F,T> {
	/**
	 * If this returns true, it means that the entity type must have 
	 * a non-null reference to a real instance of the to type.  If 
	 * it has a null value and isRequired returns true, that is a 
	 * constraint violation.  You can think of this as a not null
	 * constraint on a foreign key.
	 * @return
	 */
	boolean isRequired();
	
	/**
	 * Returns the reference type: many to one, one to many, etc.
	 * This should be from the viewpoint of the 'from' entity.
	 * @return
	 */
	ReferenceType getType();
	
	/**
	 * The from class.
	 * @return
	 */
	Class<F> getFromClass();
	
	/**
	 * The to class.
	 * @return
	 */
	Class<T> getToClass();
	
//	EntityDefinition<F> getFrom();
}