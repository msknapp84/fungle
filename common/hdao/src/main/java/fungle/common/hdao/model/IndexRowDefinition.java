package fungle.common.hdao.model;

import fungle.common.hdao.ResultConverter;

public class IndexRowDefinition implements ByteIdDefinition<IndexRow> {
	public static final IndexRowDefinition UNIQUE = new IndexRowDefinition(true);
	public static final IndexRowDefinition MULTIKEY = new IndexRowDefinition(false);
	
	public static final IndexRowDefinition get(boolean unique) {
		return unique ? UNIQUE: MULTIKEY;
	}
	
	public static final IndexRowDefinition get(Index index) {
		return index.isUnique() ? UNIQUE: MULTIKEY;
	}
	
	private final IndexRowConverter converter;
	
	private IndexRowDefinition(boolean unique) {
		this.converter = new IndexRowConverter(unique);
	}

	@Override
	public Class<IndexRow> getEntityType() {
		return IndexRow.class;
	}

	@Override
	public ByteId getByteId(IndexRow entity) {
		return entity == null ? null : entity.getByteId();
	}

	@Override
	public IndexRow newInstance(ByteId byteId) {
		return new IndexRow(byteId);
	}

	@Override
	public ResultConverter<IndexRow> getConverter() {
		return converter;
	}
}