package fungle.common.hdao.transaction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

import org.apache.commons.collections4.BidiMap;
import org.apache.commons.lang.Validate;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Operation;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Row;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.util.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fungle.common.hdao.ResultConverter;
import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.ByteIdDefinition;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.Column;
import fungle.common.hdao.model.OnError;

public class TransactionOp<T extends ByteIdentifiable> {
	private static final Logger logger = LoggerFactory
			.getLogger(TransactionOp.class);
	private static final Map<ByteIdDefinition<?>, TransactionOpFactory<?>> factoryCache = new HashMap<>();

	private final HTable table;
	private final ByteIdDefinition<T> definition;
	private final ResultConverter<T> converter;
	private final BidiMap<String, Column> mapping;
	private final boolean checkOperation;

	private final Map<ByteId, T> entities;
	private final Map<ByteId, ? extends Operation> actions;

	private TransactionOp<T> rollbackPutOperation;
	private TransactionOp<T> rollbackDeleteOperation;
	
	private Predicate<T> clientFilter;

	public String toString() {
		return String.format("For table %s, execute %d %s operation(s)",
				Bytes.toString(table.getTableName()), actions.size(),
				getOperationType().getSimpleName());
	}

	private TransactionOp(TransactionOpFactory<T> factory,
			Map<ByteId, T> entities, Map<ByteId, ? extends Operation> actions) {
		this.table = factory.table;
		this.definition = factory.definition;
		this.converter = factory.converter;
		this.mapping = factory.mapping;
		this.entities = entities;
		this.actions = actions;
		this.checkOperation = factory.checkOperation;
		this.clientFilter = factory.clientFilter;
		validate();
		if (logger.isDebugEnabled()) {
			logger.debug(String
					.format("Created a new transaction operation for table %s, entity %s, with %d %s operation(s)",
							Bytes.toString(table.getTableName()), definition
									.getEntityType().getSimpleName(), actions
									.size(), getOperationType().getSimpleName()));
		}
	}

	private void validate() {
		Validate.notNull(table, "Must specify table");
		Validate.notNull(definition, "Must specify entity type");
		Validate.notNull(converter, "Must specify converter");
		Validate.notNull(entities, "Must specify entities");
		Validate.notNull(actions, "Must specify todos");
		Validate.isTrue(actions.size() > 0,
				"Empty transaction operations are not permitted");
		Set<Class<? extends Operation>> types = new HashSet<>();
		for (Operation o : actions.values()) {
			types.add(o.getClass());
		}
		Validate.isTrue(
				types.size() == 1,
				"There can only be one type of action per operation, either "
						+ "get, put, delete, or scan.  You cannot mix them"
						+ " in a single transaction operation.  Instead, split them"
						+ "into multiple transaction operations for a single transaction.");
		Class<?> type = types.iterator().next();
		boolean isRead = Get.class.equals(type) || Scan.class.equals(type);
		if (!Scan.class.equals(type)) {
			Validate.isTrue(actions.size() == entities.size(),
					"Must have same number of entities and operations.");
		}
		for (ByteId id : entities.keySet()) {
			if (id == null) {
				throw new IllegalArgumentException(
						"Keys in the entity map cannot be null.");
			}
			if (!isRead) {
				// remaining rules only apply to read operations.
				continue;
			}
			if (entities.get(id) == null) {
				throw new IllegalArgumentException(
						"For Get/Scan operations, Keys in the entity map must map to a placeholder entity at the very least.");
			}
			if (!entities.get(id).getByteId().equals(id)) {
				throw new IllegalArgumentException(
						"Placeholder entities must have the correct id.");
			}
		}
	}

	public static <T extends ByteIdentifiable> TransactionOpFactory<T> getFactory(
			ByteIdDefinition<T> definition) {
		TransactionOpFactory<T> f = (TransactionOpFactory<T>) factoryCache
				.get(definition);
		if (f == null) {
			f = new TransactionOpFactory<>(definition);
		}
		return f;
	}

	public static <T extends ByteIdentifiable> TransactionOpFactory<T> newFactory(
			ByteIdDefinition<T> definition, boolean cache) {
		TransactionOpFactory<T> f = new TransactionOpFactory<>(definition);
		if (cache) {
			factoryCache.put(definition, f);
		}
		return f;
	}

	public static Map<ByteIdDefinition<?>, TransactionOpFactory<?>> getFactorycache() {
		return factoryCache;
	}

	public HTable getTable() {
		return table;
	}

	public ByteIdDefinition<T> getDefinition() {
		return definition;
	}

	public ResultConverter<T> getConverter() {
		return converter;
	}

	public BidiMap<String, Column> getMapping() {
		return mapping;
	}

	public Map<ByteId, T> getEntities() {
		return entities;
	}

	public Map<ByteId, ? extends Operation> getActions() {
		return actions;
	}

	public boolean isReadOnly() {
		Class<?> type = getOperationType();
		return type.equals(Get.class) || type.equals(Scan.class);
	}

	public Class<? extends Operation> getOperationType() {
		return this.actions.values().iterator().next().getClass();
	}

	public boolean isRowOperation() {
		return Row.class.isAssignableFrom(getOperationType());
	}

	public List<? extends Row> getRowOperations() {
		return new ArrayList<Row>((Collection<? extends Row>) actions.values());
	}

	public void execute(OnError onError) throws InterruptedException,
			IOException {
		if (logger.isDebugEnabled()) {
			logger.debug(String
					.format("Executing a transaction operation where on error is %s, readOnly=%s, description: %s",
							onError.toString(), String.valueOf(isReadOnly()),
							toString()));
		}
		if (onError == OnError.ATTEMPT_UNDO && !isReadOnly()) {
			makeRollbackOperation();
		}
		if (!checkOperation) {
			execute_unchecked();
		} else {
			execute_checked();
		}
	}

	private void execute_checked() {
		// checkAndPut, checkAndDelete, etc.
		Class<? extends Operation> type = getOperationType();
		if (Put.class.equals(type)) {

		} else if (Delete.class.equals(type)) {

		}
		throw new UnsupportedOperationException(
				"Checked operations are currently not supported.");
	}

	private void execute_unchecked() throws InterruptedException, IOException {
		if (isRowOperation()) {
			List<? extends Row> rowops = getRowOperations();
			if (logger.isDebugEnabled()) {
				logger.debug(String.format(
						"Executing %d %s operation(s) unchecked on table %s",
						rowops.size(), getOperationType().getSimpleName(),
						Bytes.toString(getTable().getTableName())));
			}
			// these implement row: append, delete, exec, get, increment,
			// mutation,put, rowmutations
			Object[] results = this.table.batch(rowops);
			if (isReadOnly()) {
				List<ByteId> foundIds = updateEntities(results);
				List<ByteId> missingIds = new ArrayList<>(
						this.entities.keySet());
				missingIds.removeAll(foundIds);
				if (logger.isDebugEnabled()) {
					logDebugStatsForGet(foundIds, missingIds);
				}
				// for DML statements, the ids may not be returned,
				// but for get/scan, a lack of an id means it does not
				// exist.
				for (ByteId id : missingIds) {
					if (id == null) {
						continue;
					}
					this.entities.put(id, null);
				}
			} else if (logger.isDebugEnabled()) {
				logDebugStats(results);
			}
		} else {
			// these operations do not implement row: scan, multiput,
			if (Scan.class.isAssignableFrom(getOperationType())) {
				// scans should have been merged in advance, the HBaseDAOBase
				// and ByteRange
				// classes have logic that merges them to reduce HBase network
				// IO.
				Collection<? extends Operation> ops = this.actions.values();
				for (Operation op : ops) {
					Scan scan = (Scan) op;
					if (logger.isDebugEnabled()) {
						logger.debug("Scanning from {} to {}",
								scan.getStartRow()!=null && scan.getStartRow().length==4?Bytes.toInt(scan.getStartRow()):"-infinity",
								scan.getStopRow()!=null && scan.getStopRow().length==4?Bytes.toInt(scan.getStopRow()):"infinity");
					}
					ResultScanner scanner = table.getScanner(scan);
					Result[] results = null;
					while ((results = scanner.next(100)).length > 0) {
						updateEntities(results);
					}
				}
			} else {
				throw new UnsupportedOperationException(getOperationType()
						+ " is not supported yet.");
			}
		}
	}

	private void logDebugStatsForGet(List<ByteId> foundIds,
			List<ByteId> missingIds) {
		logger.debug(String.format("Found %d entities out of %d",
				foundIds.size(), foundIds.size() + missingIds.size()));
	}

	private void logDebugStats(Object[] results) {
		int nullCount = 0;
		for (int i = 0; i < results.length; i++) {
			if (results[i] == null) {
				nullCount++;
			}
		}
		logger.debug(String.format(
				"%d of the %d put/delete requests completed successfully.",
				(results.length - nullCount), results.length));
	}

	private List<ByteId> updateEntities(Object[] results) {
		if (results == null || results.length < 1) {
			logger.warn("Asked to update entities, but the results were null/empty.");
		}
		if (logger.isTraceEnabled()) {
			logger.trace("Updating entities from a transaciton operation with "
					+ results.length + " results.");
		}
		List<ByteId> foundIds = new ArrayList<>();
		for (Object o : results) {
			if (!(o instanceof Result)) {
				if (logger.isDebugEnabled()) {
					logger.debug("One of the results was actually of type "
							+ o.getClass() + " it is being skipped.");
				}
				continue;
			}
			Result result = (Result) o;
			if (result.getRow() != null) {
				ByteId id = new ByteId(result.getRow());
				foundIds.add(id);
				// id could be null if you request something that does not
				// exist.
				T placeHolder = this.entities.get(id);
				if (placeHolder == null) {
					// this is reached for scan operations, where the entities
					// map did not
					// have an entry in the entities map.
					placeHolder = definition.newInstance(id);
					this.entities.put(id, placeHolder);
				}
				converter.mergeInto(placeHolder, result, mapping);
				if (clientFilter!=null && !clientFilter.test(placeHolder)) {
					// we don't want to keep this.
					this.entities.remove(id);
				}
				if (logger.isTraceEnabled()) {
					logger.trace("Merged results into an entity.  Entity toString: "
							+ placeHolder.toString());
				}
			} else if (logger.isTraceEnabled()) {
				logger.trace("A result came back with a null row, this should mean you requested an id that did not exist.");
			}
		}
		return foundIds;
	}

	private void makeRollbackOperation() throws IOException {
		if (isReadOnly()) {
			logger.info("Asked to make a rollback operation when this is read only...  I don't think so.");
			return;
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Making a rollback operation, description: %s"
					+ toString());
		}
		// get the old values and create put operations for them.
		// must build the new operations. First get current values.
		Map<ByteId, T> oldEntities = new HashMap<ByteId, T>();
		List<Get> gets = new ArrayList<>();
		this.entities.keySet().forEach(id -> gets.add(new Get(id.getByteId())));
		List<Result> results;
		results = Arrays.asList(this.table.get(gets));
		Map<ByteId, Put> puts = new HashMap<>();
		Map<ByteId, Delete> deletes = new HashMap<>();
		List<ByteId> foundIds = new ArrayList<ByteId>();
		List<ByteId> missingIds = new ArrayList<ByteId>(this.entities.keySet());
		for (Result result : results) {
			if (result == null) {
				logger.debug("Had a null result (this should not be possible)");
				continue;
			}
			byte[] row = result.getRow();
			if (row == null) {
				continue;// this means something does not already exist.
			}
			ByteId id = new ByteId(row);
			foundIds.add(id);
			T entity = converter.toEntity(result, mapping);
			puts.put(id, converter.toPut(entity, mapping));
			oldEntities.put(id, entity);
		}
		Map<ByteId, T> missingEntities = new HashMap<ByteId, T>();
		missingIds.removeAll(foundIds);
		for (ByteId missingId : missingIds) {
			if (missingId == null) {
				continue;
			}
			Delete delete = new Delete(missingId.getByteId());
			deletes.put(missingId, delete);
			missingEntities.put(missingId, null);
		}
		if (!puts.isEmpty()) {
			TransactionOpFactory<T> f = new TransactionOpFactory<>(this);
			this.rollbackPutOperation = f.build(oldEntities, puts);
			if (logger.isDebugEnabled()) {
				logger.debug("Finished making a rollback put operation, rollback description: "
						+ rollbackPutOperation.toString());
			}
		}
		if (!deletes.isEmpty()) {
			TransactionOpFactory<T> f = new TransactionOpFactory<>(this);
			this.rollbackDeleteOperation = f.build(missingEntities, deletes);
			if (logger.isDebugEnabled()) {
				logger.debug("Finished making a rollback delete operation, rollback description: "
						+ rollbackDeleteOperation.toString());
			}
		}
	}

	public boolean rollback() {
		if (rollbackPutOperation == null) {
			logger.warn("Asked to roll back an operation, but there is no rollback op saved.");
			return false;
		}
		boolean worked = false;
		try {
			logger.info("Executing a rollback for " + toString()
					+ ", rollback description: "
					+ rollbackPutOperation.toString());
			if (rollbackPutOperation != null) {
				rollbackPutOperation.execute(OnError.CONTINUE);
			}
			if (rollbackDeleteOperation != null) {
				rollbackDeleteOperation.execute(OnError.CONTINUE);
			}
			if (logger.isTraceEnabled()) {
				logger.trace("The rollback finished without exception, rollback description: "
						+ rollbackPutOperation.toString());
			}
			worked = true;
		} catch (InterruptedException e) {
			logger.error("While rolling back, the thread was interrupted");
		} catch (IOException e) {
			logger.error("While rolling back, we had an IO exception: ", e);
		}
		return worked;
	}

	public static final class TransactionOpFactory<T extends ByteIdentifiable> {
		private HTable table;
		private final ByteIdDefinition<T> definition;
		private ResultConverter<T> converter;
		private BidiMap<String, Column> mapping;
		private boolean checkOperation = false;
		private Predicate<T> clientFilter;

		public TransactionOpFactory(TransactionOp<T> op) {
			this.table = op.table;
			this.definition = op.definition;
			this.converter = op.converter;
			this.mapping = op.mapping;
			this.checkOperation = op.checkOperation;
			this.clientFilter = op.clientFilter;
		}

		public TransactionOpFactory(ByteIdDefinition<T> definition) {
			this.definition = definition;
		}

		public TransactionOpFactory<T> clientFilter(Predicate<T> clientFilter) {
			this.clientFilter = clientFilter;
			return this;
		}

		public TransactionOpFactory<T> mapping(BidiMap<String, Column> mapping) {
			this.mapping = mapping;
			return this;
		}

		public TransactionOpFactory<T> converter(ResultConverter<T> converter) {
			this.converter = converter;
			return this;
		}

		public TransactionOpFactory<T> table(HTable table) {
			this.table = table;
			return this;
		}

		public TransactionOpFactory<T> checkOperation(boolean checkOperation) {
			this.checkOperation = checkOperation;
			return this;
		}

		public TransactionOp<T> build(Map<ByteId, T> entities,
				Map<ByteId, ? extends Operation> actions) {
			return new TransactionOp<>(this, entities, actions);
		}
	}
}