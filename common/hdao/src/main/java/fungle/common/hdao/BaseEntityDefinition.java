package fungle.common.hdao;

import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.EntityDefinition;
import fungle.common.hdao.model.EntityReference;

public abstract class BaseEntityDefinition<T extends ByteIdentifiable> implements EntityDefinition<T> {

	private final Class<T> clazz;
	
	public BaseEntityDefinition(final Class<T> clazz) {
		this.clazz= clazz;
	}
	
	@Override
	public Class<T> getEntityType() {
		return clazz;
	}

	@Override
	public ByteId getByteId(T entity) {
		return entity.getByteId();
	}

	@Override
	public T newInstance(ByteId byteId) {
		T p = null;
		try {
			p = clazz.newInstance();
			p.setByteId(byteId);
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return p;
	}

	@Override
	public ResultConverter<T> getConverter() {
		return new ReflectiveResultConverter<>(clazz);
	}

	@Override
	public boolean hasUniqueConstraints() {
		return false;
	}

	@Override
	public Object[] getUniqueValues(T entity) {
		return new Object[0];
	}

	@Override
	public boolean verifyNotNullConstraints(T entity) {
		return false;
	}

	@Override
	public <X extends ByteIdentifiable> X getForeignEntity(T entity,
			EntityReference<T, X> reference) {
		return reference.getForeignEntity(entity);
	}
}