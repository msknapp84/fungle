package fungle.common.hdao.model;

public class IndexField {
	private final String name;
	private final boolean real;
	private final boolean ascending;
	
	public IndexField(String name,boolean real,boolean ascending) {
		this.name = name;
		this.real = real;
		this.ascending = ascending;
	}
	
	public IndexField(IndexField other) {
		this.name = other.name;
		this.real = other.real;
		this.ascending = other.ascending;
	}
	
	public String getName() {
		return name;
	}
	public boolean isReal() {
		return real;
	}
	public boolean isAscending() {
		return ascending;
	}
	public String toString() {
		return String.format("%s %s",name,String.valueOf(ascending));
	}
}