package fungle.common.hdao.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import fungle.common.core.util.Reflect;
import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.EntityReference;
import fungle.common.hdao.model.IDGenerator;
import fungle.common.hdao.model.SimpleIDGenerator;

public class HBaseDAOHelp {

	public static <F extends ByteIdentifiable,T extends ByteIdentifiable> ByteId getForeignId(EntityReference<F,T> reference, F entity) {
		T t = reference.getForeignEntity(entity);
		return t==null?null:t.getByteId();
	}

//	public static <F extends ByteIdentifiable,T extends ByteIdentifiable> T makePlaceHolder(EntityReference<F,T> reference, ByteId id) {
//		return reference.getTo().newInstance(id);
//	}
	
	public static <T> Map<ByteId,T> removeNullValues(Map<ByteId,T> original) {
		Iterator<Entry<ByteId,T>> iter = original.entrySet().iterator();
		while (iter.hasNext()) {
			Entry<ByteId,T> entry = iter.next();
			if (entry.getValue()==null){
				iter.remove();
			}
		}
		return original;
	}

	public static <T extends ByteIdentifiable> List<ByteId> getForeignIdList(Collection<T> entities,
			EntityReference<T, ?> reference) {
		List<ByteId> foreignIds = new ArrayList<>();
		for (T entity : entities) {
			if (entity==null) {
				continue;
			}
			ByteIdentifiable foreignEntity = reference.getForeignEntity(entity);
			if (foreignEntity!=null && foreignEntity.getByteId()!=null) {
				foreignIds.add(foreignEntity.getByteId());
			}
		}
		return foreignIds;
	}

//	public static <T extends ByteIdentifiable> List<ByteId> getIncomingIdList(Collection<T> entities,
//			EntityReference<?, T> reference) {
//		List<ByteId> foreignIds = new ArrayList<>();
//		for (T entity : entities) {
//			ByteIdentifiable foreignEntity = reference.getForeignEntity(entity);
//			if (foreignEntity!=null && foreignEntity.getByteId()!=null) {
//				foreignIds.add(foreignEntity.getByteId());
//			}
//		}
//		return foreignIds;
//	}

	public static <T extends ByteIdentifiable> List<ByteId> getForeignIdList(Map<ByteId, T> entities,
			EntityReference<T, ?> reference) {
		List<ByteId> foreignIds = new ArrayList<>();
		for (T entity : entities.values()) {
			if (entity==null) {
				continue;
			}
			ByteIdentifiable foreignEntity = reference.getForeignEntity(entity);
			if (foreignEntity!=null && foreignEntity.getByteId()!=null) {
				foreignIds.add(foreignEntity.getByteId());
			}
		}
		return foreignIds;
	}

	public static <F extends ByteIdentifiable,T extends ByteIdentifiable> Map<ByteId,T> getForeignIdMap(Collection<F> entities,
			EntityReference<F,T> reference,boolean generateIds) {
		Map<ByteId,T> foreignMap = new HashMap<>();
		for (F entity : entities) {
			if (entity==null) {
				continue;
			}
			T foreignEntity = reference.getForeignEntity(entity);
			if (foreignEntity!=null) {
				if (generateIds && foreignEntity.getByteId()==null) {
					ByteId id = SimpleIDGenerator.getInstance(foreignEntity.getClass()).nextBytes();
					foreignEntity.setByteId(id);
				}
				if (foreignEntity.getByteId()!=null) {
					foreignMap.put(foreignEntity.getByteId(),foreignEntity);
				}
			}
		}
		return foreignMap;
	}

	public static <F extends ByteIdentifiable,T extends ByteIdentifiable> Map<ByteId,T> getForeignIdMap(Map<ByteId, F> entities,
			EntityReference<F,T> reference,boolean generateIds) {
		Map<ByteId,T> foreignMap = new HashMap<>();
		for (F entity : entities.values()) {
			if (entity==null) {
				continue;
			}
			T foreignEntity = reference.getForeignEntity(entity);
			if (foreignEntity!=null) {
				if (generateIds && foreignEntity.getByteId()==null) {
					ByteId id = SimpleIDGenerator.getInstance(foreignEntity.getClass()).nextBytes();
					foreignEntity.setByteId(id);
				}
				if (foreignEntity.getByteId()!=null) {
					foreignMap.put(foreignEntity.getByteId(),foreignEntity);
				}
			}
		}
		return foreignMap;
	}

	public static <F extends ByteIdentifiable,T extends ByteIdentifiable> List<T> getForeignEntityList(Collection<F> entities,
			EntityReference<F,T> reference,boolean generateIds) {
		List<T> foreignEntities = new ArrayList<>();
		for (F entity : entities) {
			if (entity==null) {
				continue;
			}
			T foreignEntity = reference.getForeignEntity(entity);
			if (foreignEntity!=null) {
				if (generateIds && foreignEntity.getByteId()==null) {
					ByteId id = SimpleIDGenerator.getInstance(foreignEntity.getClass()).nextBytes();
					foreignEntity.setByteId(id);
				}
				if (foreignEntity.getByteId()!=null) {
					foreignEntities.add(foreignEntity);
				}
			}
		}
		return foreignEntities;
	}

	public static <F extends ByteIdentifiable,T extends ByteIdentifiable> List<T> getForeignEntityList(Map<ByteId, F> entities,
			EntityReference<F,T> reference,boolean generateIds) {
		List<T> foreignEntities = new ArrayList<>();
		for (F entity : entities.values()) {
			if (entity==null) {
				continue;
			}
			T foreignEntity = reference.getForeignEntity(entity);
			if (foreignEntity!=null) {
				if (generateIds && foreignEntity.getByteId()==null) {
					ByteId id = SimpleIDGenerator.getInstance(foreignEntity.getClass()).nextBytes();
					foreignEntity.setByteId(id);
				}
				if (foreignEntity.getByteId()!=null) {
					foreignEntities.add(foreignEntity);
				}
			}
		}
		return foreignEntities;
	}

	public static void ensureIds(IDGenerator generator, Collection<? extends ByteIdentifiable> entities) {
		if (entities == null) {
			throw new IllegalArgumentException("Entities must not be null.");
		}
		if (entities.isEmpty()) {
			return;
		}
		for (ByteIdentifiable entity : entities) {
			if (entity == null) {
				throw new IllegalArgumentException(
						"Must not have a null entity in the collection when verifying they have ids.");
			}
			if (entity.getByteId() == null || entity.getByteId().length() < 1) {
				ByteId id = generator.nextBytes();
				entity.setByteId(id);
			}
		}
		Class<?> c = entities.iterator().next().getClass();
		for (Field f : c.getDeclaredFields()) {
			if (!ByteIdentifiable.class.isAssignableFrom(f.getType())) {
				continue;
			}
			Collection<ByteIdentifiable> subs = new ArrayList<ByteIdentifiable>();
			for (ByteIdentifiable entity : entities) {
				ByteIdentifiable o = Reflect.getFieldValue(f.getName(), entity, ByteIdentifiable.class);
				if (o!=null) {
					subs.add(o);
				}
			}
			if (!subs.isEmpty()) {
				SimpleIDGenerator g = SimpleIDGenerator.getInstance(f.getType());
				ensureIds(g, (Collection<? extends ByteIdentifiable>) subs);
			}
		}
	}

//	public static <T> Map<ByteId, T> convertToMap(Collection<ByteId> foreignIds,Class<T> clazz) {
//		Map<ByteId, T> m = new HashMap<ByteId,T>();
//		foreignIds.forEach(id -> m.put(id, makePlaceHolder(id,clazz)));
//		return m;
//	}

//	public static <T> T makePlaceHolder(ByteId id,Class<T> clazz) {
//		T instance=null;
//		try {
//			instance = clazz.newInstance();
//		} catch (InstantiationException | IllegalAccessException e) {
//			e.printStackTrace();
//		}
//		if (Reflect.fieldExistsLike(clazz,"id",ByteId.class)){
//			Reflect.setFieldValue("id", instance, id);
//		} else if (Reflect.fieldExistsLike(clazz,"byteId",ByteId.class)) {
//			Reflect.setFieldValue("byteId", instance, id);
//		}
//		return instance;
//	}
}
