package fungle.common.hdao.model;

public interface EntityReferenceConverter<F extends ByteIdentifiable,T extends ByteIdentifiable> {
	T getForeignEntity(F f);
//	F getLocalEntity(T t);
	void setForeignEntity(F f,ByteIdentifiable t);
	T newForeignInstance();
	F newLocalInstance();
}
