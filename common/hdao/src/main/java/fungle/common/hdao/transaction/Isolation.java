package fungle.common.hdao.transaction;

public enum Isolation {
	READ_UNCOMMITTED,
	READ_COMMITTED,
	REPEATABLE_READ,
	SERIALIZED_TABLE,
	SERIALIZED_DATASTORE;
}