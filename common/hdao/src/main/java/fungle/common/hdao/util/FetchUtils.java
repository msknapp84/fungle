package fungle.common.hdao.util;

import java.util.List;

import fungle.common.hdao.Fetch;
import fungle.common.hdao.model.EntityReference;

public final class FetchUtils {
	private FetchUtils(){}
	
	public static Fetch always() {
		return new Fetch() {

			@Override
			public boolean shouldFetch(EntityReference<?,?> reference) {
				return true;
			}

			@Override
			public Fetch getSubFetch(EntityReference<?,?> reference) {
				return this;
			}
		};
	}
	
	public static Fetch never() {
		return new Fetch() {

			@Override
			public boolean shouldFetch(EntityReference<?,?> reference) {
				return false;
			}

			@Override
			public Fetch getSubFetch(EntityReference<?,?> reference) {
				return this;
			}
		};
	}
	
	public static Fetch oneLevel() {
		return new Fetch() {

			@Override
			public boolean shouldFetch(EntityReference<?,?> reference) {
				return true;
			}

			@Override
			public Fetch getSubFetch(EntityReference<?,?> reference) {
				return never();
			}
		};
	}
	
	public static class DefaultFetch implements Fetch {
		private final List<EntityReference<?,?>> refs;
		private final Fetch sub;

		public DefaultFetch(List<EntityReference<?,?>> refs,Fetch sub) {
			this.refs = refs;
			this.sub = sub;
		}
		
		@Override
		public boolean shouldFetch(EntityReference<?,?> reference) {
			return refs.contains(reference);
		}

		@Override
		public Fetch getSubFetch(EntityReference<?,?> reference) {
			return sub;
		}
		
	}
	
	public static Fetch forRefs(List<EntityReference<?,?>> refs) {
		return new DefaultFetch(refs, always());
	}
}
