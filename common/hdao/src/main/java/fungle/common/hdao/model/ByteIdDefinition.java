package fungle.common.hdao.model;

import fungle.common.hdao.ResultConverter;

public interface ByteIdDefinition<T extends ByteIdentifiable> {
	Class<T> getEntityType();
	ByteId getByteId(T entity);
	T newInstance(ByteId byteId);
	ResultConverter<T> getConverter();

}
