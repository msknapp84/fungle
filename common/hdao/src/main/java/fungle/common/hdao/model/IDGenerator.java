package fungle.common.hdao.model;

public interface IDGenerator {
	ByteId nextBytes();
	String nextHexString();
}
