package fungle.common.hdao.model;

public enum OnError {
	/**
	 * Here it will attempt to undo changes it already made.
	 * This is your best chance of keeping the table 
	 * consistent, but it does add a lot of overhead.  There
	 * is still no guarantee that any attempt to undo changes 
	 * will work.
	 */
	ATTEMPT_UNDO,
	
	/**
	 * Here it will stop making future modifications, but
	 * will not attempt to undo previous changes.  This could
	 * easily lead to table inconsistencies.
	 */
	ABORT,
	
	/**
	 * Continue making updates despite any errors, make all 
	 * updates that are possible.  This can easily lead
	 * to table inconsistencies.
	 */
	CONTINUE;
}
