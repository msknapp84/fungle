package fungle.common.hdao.model;

import java.util.Base64;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class ByteIdAdapter extends XmlAdapter<String,ByteId> {

	@Override
	public ByteId unmarshal(String v) throws Exception {
		return new ByteId(Base64.getDecoder().decode(v));
	}

	@Override
	public String marshal(ByteId v) throws Exception {
		return Base64.getEncoder().encodeToString(v.getByteId());
	}
}