//package fungle.common.hdao;
//
//import java.util.Collections;
//import java.util.List;
//
//import org.apache.commons.collections4.BidiMap;
//import org.apache.hadoop.hbase.client.HTable;
//import org.apache.hadoop.hbase.util.Bytes;
//
//import fungle.common.hdao.model.ByteIdentifiable;
//import fungle.common.hdao.model.Column;
//import fungle.common.hdao.model.IDGenerator;
//import fungle.common.hdao.model.SimpleIDGenerator;
//import fungle.common.hdao.model.Index;
//import fungle.common.hdao.model.Reference;
//import fungle.common.hdao.model.Reference.ReferenceType;
//
//public class SimpleHBaseDAO<T extends ByteIdentifiable> extends HBaseDAOBase<T> {
//
//	private final Class<T> clazz;
//	private final HTable table;
//	private final ResultConverter<T> converter;
//	private final BidiMap<String, Column> mapping;
//	private final SimpleIDGenerator generator;
//	
//	public SimpleHBaseDAO(Class<T> clazz,HTable table,BidiMap<String, Column> mapping) {
//		this.clazz=clazz;
//		this.table = table;
//		this.converter = new ReflectiveResultConverter<>(clazz);
//		this.mapping = mapping;
//		this.generator = SimpleIDGenerator.getInstance(getEntityClass());
//	}
//
//	@Override
//	public HTable getTable() {
//		return table;
//	}
//
//	@Override
//	public BidiMap<String, Column> getMapping() {
//		return mapping;
//	}
//
//	@Override
//	public Class<T> getEntityClass() {
//		return clazz;
//	}
//
//	@Override
//	public Column getNonExistentColumn() {
//		return new Column(Bytes.toBytes("jfoiweuj32r323kl89"),Bytes.toBytes("zwe3oiuj32nf309"));
//	}
//
//	@Override
//	public IDGenerator getByteIdGenerator() {
//		return generator;
//	}
//
//	@Override
//	public ResultConverter<T> getConverter() {
//		return converter;
//	}
//
//	@SuppressWarnings("unchecked")
//	@Override
//	public List<Reference<T>> getReferences(ReferenceType type) {
//		return Collections.EMPTY_LIST;
//	}
//
//	@SuppressWarnings("unchecked")
//	@Override
//	public List<Index<T>> getIndexes() {
//		return Collections.EMPTY_LIST;
//	}
//}