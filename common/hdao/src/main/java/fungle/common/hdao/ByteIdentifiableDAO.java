package fungle.common.hdao;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.Cascade;
import fungle.common.hdao.model.DAODefinition;
import fungle.common.hdao.model.EntityReference;
import fungle.common.hdao.model.Index;
import fungle.common.hdao.model.OnError;
import fungle.common.hdao.transaction.Transaction;

public interface ByteIdentifiableDAO<T extends ByteIdentifiable> {
	T get(ByteId id, Fetch fetch);

	Map<ByteId, T> get(Collection<ByteId> ids, Fetch fetch);

	Map<ByteId, T> get(Collection<ByteId> ids, Fetch fetch,
			Transaction transaction);

	void save(Collection<T> entities, Cascade cascade, OnError onError);

	void save(Collection<T> entities, Cascade cascade, Transaction transaction);

	void save(T entity, Cascade cascade, OnError onError);

	void delete(Collection<T> entities, Cascade cascade, OnError onError);

	void delete(T entity, Cascade cascade, OnError onError);

	void deleteById(Collection<ByteId> ids, Cascade cascade, OnError onError);

	void deleteById(ByteId id, Cascade cascade, OnError onError);

	void deleteByIdMap(Map<ByteId, T> idMap, Cascade cascade,
			Transaction transaction);

	void deleteWhatRefersTo(Collection<ByteId> ids, Cascade cascade,
			Transaction transaction,EntityReference<T,?> reference);

	Class<T> getEntityClass();

	boolean hasId(ByteId id);

	Map<ByteId, Boolean> hasIds(Collection<ByteId> ids);

	Map<ByteId, T> getAll(Fetch fetch, OnError onError,Object serverFilter,Predicate<T> clientFilter);

	Map<ByteId, T> getAll(Fetch fetch, Transaction transaction,Object serverFilter,Predicate<T> clientFilter);

	Map<ByteId, T> scanUsingIndex(Index<T> index, Fetch fetch, Transaction transaction,
			T from, T to);

	Map<ByteId, T> getUsingIndex(Index<T> index, Fetch fetch, OnError onError, T template);

	Map<ByteId, T> getUsingIndex(Index<T> index,  Fetch fetch, Transaction transaction,
			T template);

	void deleteUsingIndex(Index<T> index,  Cascade cascade, OnError onError,
			T template);

	void deleteUsingIndex(Index<T> index,  Cascade cascade, Transaction transaction,
			T template);

	void deleteUsingIndexRange(Index<T> index,  Cascade cascade, OnError onError,
			T from,T to);

	void deleteUsingIndexRange(Index<T> index,  Cascade cascade, Transaction transaction,
			T from,T to);

	Set<ByteId> getIdsUsingIndex(Index<T> index, OnError onError,
			T from, T to);

	Set<ByteId> getIdsUsingIndex(Index<T> index, Transaction transaction,
			T from, T to);

	Set<ByteId> getIdsUsingIndex(Index<T> index, OnError onError, T template);

	Set<ByteId> getIdsUsingIndex(Index<T> index,  Transaction transaction,
			T template);
	
	Set<ByteId> getIdsUsingIndex(Index<T> index, OnError onError,Collection<T> templates);

	Map<ByteId,T> getUsingIndex(Index<T> index, Fetch fetch,OnError onError,Collection<T> templates);
	
	DAODefinition<T> getDefinition();
}