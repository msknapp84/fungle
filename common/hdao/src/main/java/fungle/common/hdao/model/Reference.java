package fungle.common.hdao.model;

import org.apache.commons.lang.Validate;

import fungle.common.core.util.Reflect;
import fungle.common.hdao.ByteIdentifiableDAO;

public class Reference<T extends ByteIdentifiable> {
	private final ByteIdentifiableDAO<?> foreignDAO;
	private final ReferenceDirection direction;
	private final ReferenceType type;
	private final String fieldName;
	private final boolean required;
	private final Class<T> entityType;
	
	public String toString() {
		return String.format("The entity %s has a reference to the DAO for %s, type=%s, cardinality=%s",
				entityType.getSimpleName(),foreignDAO.getEntityClass().getSimpleName(),
				type.toString(),type.toString());
	}

	public Reference(ByteIdentifiableDAO<?> foreignDAO, ReferenceDirection direction,
			ReferenceType type, String fieldName, boolean required,Class<T> entityType) {
		this.foreignDAO = foreignDAO;
		this.direction = direction;
		this.fieldName = fieldName;
		this.type = type;
		this.required = required;
		this.entityType = entityType;
		validate();
	}

	private void validate() {
		Validate.notNull(foreignDAO,"Foreign DAO must not be null");
		Validate.notNull(direction,"Foreign DAO must not be null");
		Validate.notEmpty(fieldName,"fieldName must not be null/empty");
		Validate.notNull(type,"cardinality must not be null");
		Reflect.verifyHasField(entityType, fieldName);
	}

	public Reference opposite(ByteIdentifiableDAO<?> otherDAO) {
		return new Reference(otherDAO, this.direction.opposite(),
				this.type.opposite(), this.fieldName, this.required,otherDAO.getEntityClass());
	}

	public ByteIdentifiableDAO<?> getForeignDAO() {
		return foreignDAO;
	}

	public ReferenceDirection getType() {
		return direction;
	}

	public String getFieldName() {
		return fieldName;
	}

	public boolean isRequired() {
		return required;
	}

	public ReferenceType getCardinality() {
		return type;
	}

	public static ReferenceBuilder manyToOne(ByteIdentifiableDAO<?> foreignDAO,
			String fieldName) {
		return new ReferenceBuilder().direction(ReferenceDirection.THIS_REFERS_TO_THAT)
				.cardinality(ReferenceType.MANYTOONE)
				.foreignDAO(foreignDAO).fieldName(fieldName);
	}

	public static ReferenceBuilder oneToMany(ByteIdentifiableDAO<?> foreignDAO,
			String fieldName) {
		return new ReferenceBuilder().direction(ReferenceDirection.THIS_REFERS_TO_THAT)
				.cardinality(ReferenceType.ONETOMANY)
				.foreignDAO(foreignDAO).fieldName(fieldName);
	}

	public static ReferenceBuilder oneToOne(ByteIdentifiableDAO<?> foreignDAO,
			String fieldName) {
		return new ReferenceBuilder().direction(ReferenceDirection.THIS_REFERS_TO_THAT)
				.cardinality(ReferenceType.ONETOONE)
				.foreignDAO(foreignDAO).fieldName(fieldName);
	}

	public static ReferenceBuilder builder(ByteIdentifiableDAO<?> foreignDAO,
			String fieldName) {
		return new ReferenceBuilder()
				.foreignDAO(foreignDAO).fieldName(fieldName);
	}

	public static final class ReferenceBuilder {
		private ByteIdentifiableDAO<?> foreignDAO;
		private ReferenceDirection direction = ReferenceDirection.THIS_REFERS_TO_THAT;
		private ReferenceType cardinality = ReferenceType.MANYTOONE;
		private String fieldName;
		private boolean required = false;

		public ReferenceBuilder() {

		}

		public ReferenceBuilder direction(ReferenceDirection direction) {
			this.direction = direction;
			return this;
		}

		public ReferenceBuilder cardinality(ReferenceType cardinality) {
			this.cardinality = cardinality;
			return this;
		}

		public ReferenceBuilder thatToThis() {
			this.direction = ReferenceDirection.THAT_REFERS_TO_THIS;
			return this;
		}

		public ReferenceBuilder thisToThat() {
			this.direction = ReferenceDirection.THIS_REFERS_TO_THAT;
			return this;
		}

		public ReferenceBuilder bidirectional() {
			this.direction = ReferenceDirection.BIDIRECTIONAL;
			return this;
		}

		public ReferenceBuilder oneToMany() {
			this.cardinality=ReferenceType.ONETOMANY;
			return this;
		}

		public ReferenceBuilder manyToOne() {
			this.cardinality=ReferenceType.MANYTOONE;
			return this;
		}

		public ReferenceBuilder oneToOne() {
			this.cardinality=ReferenceType.ONETOONE;
			return this;
		}

		public ReferenceBuilder manyToMany() {
			this.cardinality=ReferenceType.MANYTOMANY;
			return this;
		}

		public ReferenceBuilder fieldName(String fieldName) {
			this.fieldName = fieldName;
			return this;
		}

		public ReferenceBuilder required(boolean required) {
			this.required = required;
			return this;
		}

		public ReferenceBuilder foreignDAO(ByteIdentifiableDAO<?> foreignDAO) {
			this.foreignDAO = foreignDAO;
			return this;
		}

		public ReferenceBuilder isRequired() {
			this.required=true;
			return this;
		}

		public ReferenceBuilder notRequired() {
			this.required=false;
			return this;
		}

		public <X extends ByteIdentifiable> Reference<X> build(Class<X> clazz) {
			return new Reference<X>(foreignDAO, direction, cardinality, fieldName,
					required,clazz);
		}
	}
}