package fungle.common.hdao.transaction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fungle.common.hdao.model.OnError;

public class Transaction {
	private static final Logger logger = LoggerFactory
			.getLogger(Transaction.class);

	private final OnError onError;
	private boolean executing = false, closed = false, hadError = false,
			attemptedRolledBack = false, rollBackSuccessful = false;
	private List<TransactionOp> operations = new ArrayList<>();
	private List<TransactionOp> completedOperations = new ArrayList<>();
	private Exception caught;

	public Transaction(OnError onError) {
		this.onError = onError;
	}

	public void addOperation(TransactionOp operation) {
		if (closed) {
			throw new IllegalStateException("Already closed.");
		}
		if (executing) {
			throw new IllegalStateException("Already executing.");
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Just added an operation to a transaction: "
					+ operation);
		}
		operations.add(operation);
	}

	public void execute() {
		flush();
		closed = true;
		if (logger.isDebugEnabled()) {
			logger.debug("Just closed a transaction:\n" + toString());
		}
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("===> OnError: ").append(onError.toString()).append("; ");
		sb.append("executing: ").append(executing).append("; ");
		sb.append("closed: ").append(closed).append("; ");
		sb.append("hadError: ").append(hadError).append("; ");
		sb.append("attemptedRolledBack: ").append(attemptedRolledBack)
				.append("; ");
		sb.append("rollBackSuccessful: ").append(rollBackSuccessful)
				.append("; ");
		if (caught != null) {
			sb.append("caught exception message: ").append(caught.getMessage())
					.append("; ");
		} else {
			sb.append("No exceptions encountered.").append("; ");
		}
		sb.append(operations.size()).append(" operations remaining.")
				.append("; ");
		sb.append(completedOperations.size()).append(" operations completed.")
				.append(" <===");
		return sb.toString();
	}

	public void flush() {
		if (executing) {
			throw new IllegalStateException("Already executing.");
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Flushing a transaction\n" + toString());
		}
		this.executing = true;
		try {

			// TODO acquire locks using the locking strategy.

			// TODO group the operations.
			for (TransactionOp<?> op : operations) {
				try {
					completedOperations.add(0, op);
					op.execute(onError);
				} catch (Exception e) {
					caught = e;
					logger.error("Failed to execute a transaction operation "
							+ e.getMessage(), e);
					hadError = true;
					if (onError != OnError.CONTINUE) {
						closed = true;
						break;
					}
				}
			}
			if (hadError) {
				if (onError == OnError.ATTEMPT_UNDO) {
					rollback();
				}
			} else {
				operations.removeAll(completedOperations);
			}
		} finally {
			executing = false;
		}
	}

	private void rollback() {
		logger.info("Rolling back a transaction");
		attemptedRolledBack = true;
		rollBackSuccessful = true;
		int successCount = 0;
		for (TransactionOp<?> op : completedOperations) {
			if (!op.rollback()) {
				rollBackSuccessful = false;
			} else {
				successCount++;
			}
		}
		logger.info("Finished attempt to roll back a transaction, successfully rolled back "
				+ successCount
				+ " ops of "
				+ completedOperations.size()
				+ " that were executed.");
	}

	public OnError getOnError() {
		return onError;
	}

	public boolean isExecuting() {
		return executing;
	}

	public boolean isClosed() {
		return closed;
	}

	public boolean isHadError() {
		return hadError;
	}

	public List<TransactionOp> getOperations() {
		return Collections.unmodifiableList((List<TransactionOp>) operations);
	}

	public List<TransactionOp> getCompletedOperations() {
		return Collections
				.unmodifiableList((List<TransactionOp>) completedOperations);
	}

	public Exception getException() {
		return caught;
	}

	public boolean isAttemptedRolledBack() {
		return attemptedRolledBack;
	}

	public boolean isRollBackSuccessful() {
		return rollBackSuccessful;
	}
}