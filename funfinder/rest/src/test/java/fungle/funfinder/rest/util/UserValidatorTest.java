package fungle.funfinder.rest.util;

import org.junit.Test;

import fungle.funfinder.rest.exceptions.UnacceptablePasswordException;
import fungle.funfinder.rest.validation.UserValidator;

public class UserValidatorTest {

	@Test
	public void validateEmail() {
		UserValidator.validateEmail("dude@email.com");
	}

	@Test(expected = IllegalArgumentException.class)
	public void validateEmail2() {
		UserValidator.validateEmail("dude @email.com");
	}

	@Test(expected = IllegalArgumentException.class)
	public void validateEmail3() {
		UserValidator.validateEmail("{}dude@email.com");
	}

	@Test(expected = IllegalArgumentException.class)
	public void validateEmail4() {
		UserValidator.validateEmail("dude");
	}

	@Test
	public void validateName() {
		UserValidator.validateName("Bob");
	}

	@Test
	public void validateName2() {
		UserValidator.validateName("Bob Paul");
	}

	@Test(expected = IllegalArgumentException.class)
	public void validateName3() {
		UserValidator.validateName("{Bob");
	}

	@Test(expected = IllegalArgumentException.class)
	public void validateName4() {
		UserValidator.validateName("Bob>");
	}

	@Test
	public void validateUserName() {
		UserValidator.validateUserName("Joe_-386");
	}

	@Test(expected = IllegalArgumentException.class)
	public void validateUserName2() {
		UserValidator.validateUserName("Joe{");
	}

	@Test(expected = IllegalArgumentException.class)
	public void validateUserName3() {
		UserValidator.validateUserName("Joe<");
	}

	@Test(expected = IllegalArgumentException.class)
	public void validateUserName4() {
		UserValidator.validateUserName("Joe ");
	}

	@Test(expected = UnacceptablePasswordException.class)
	public void checkPassword() {
		UserValidator.checkPassword("password");
	}

	@Test(expected = UnacceptablePasswordException.class)
	public void checkPassword2() {
		UserValidator.checkPassword("password5");
	}

	@Test(expected = UnacceptablePasswordException.class)
	public void checkPassword3() {
		UserValidator.checkPassword("Password");
	}

	@Test
	public void checkPassword4() {
		UserValidator.checkPassword("Password8");
	}

	@Test
	public void checkPassword5() {
		UserValidator.checkPassword("Pa$$w0rd89");
	}

	@Test
	public void validateAddressField() {
		UserValidator.validateAddressField("wood lawn");
	}

	@Test(expected = IllegalArgumentException.class)
	public void validateAddressField2() {
		UserValidator.validateAddressField("wood lawn {");
	}

	@Test(expected = IllegalArgumentException.class)
	public void validateAddressField3() {
		UserValidator.validateAddressField("wood lawn *");
	}

	@Test
	public void validateAddressField4() {
		UserValidator.validateAddressField("wood lawn 3");
	}
}
