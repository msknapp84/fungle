package fungle.funfinder.rest;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import fungle.funfinder.data.dao.UserDAO;
import fungle.funfinder.data.entity.User;

@Component("userDetailsService")
public class FungleUserDetailsService implements UserDetailsService {
	private static Logger logger = LoggerFactory.getLogger(FungleUserDetailsService.class);
	private static Set<String> administrators = new HashSet<>();
	
	static {
		try {
			String s = IOUtils.toString(FungleUserDetailsService.class.getResourceAsStream("/administrators.txt"));
			administrators.addAll(Arrays.asList(s.split("\n")));
		} catch (Exception e) {
			logger.error("Failed to load the administrators.txt file");
		}
	}
	
	private UserDAO userDAO;
	
	@Autowired
	public FungleUserDetailsService(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@Override
	public UserDetails loadUserByUsername(String userName)
			throws UsernameNotFoundException {
		User user = userDAO.getByUserName(userName);
		if (user==null) {
			return null;
		}
		if (user.getUserName()==null) {
			logger.error("Found a user with a null user name");
			return null;
		}
		boolean isAdmin = administrators.contains(user.getUserName());
		return new FungleUserDetails(user,isAdmin);
	}
}