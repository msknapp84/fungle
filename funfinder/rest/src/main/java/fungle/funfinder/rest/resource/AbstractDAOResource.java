package fungle.funfinder.rest.resource;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fungle.common.hdao.ByteIdentifiableDAO;
import fungle.common.hdao.Fetch;
import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.ByteIdAdapter;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.OnError;
import fungle.common.hdao.util.CascadeUtils;
import fungle.common.hdao.util.FetchUtils;

public abstract class AbstractDAOResource<T extends ByteIdentifiable> {
	private static final Logger logger = LoggerFactory.getLogger(AbstractDAOResource.class);
	
	public abstract ByteIdentifiableDAO<T> getDAO();
	
	
	public AbstractDAOResource() {
		
	}
	
//	@GET
//	@Produces(value={MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	public List<T> get(Collection<String> ids,String fetchMode) {
		List<ByteId> bids = convertToByteIds(ids);
		Fetch fetch = parseFetch(fetchMode);
		Map<ByteId,T> entities = getDAO().get(bids, fetch);
		return new ArrayList<>(entities.values());
	}

	public List<ByteId> convertToByteIds(Collection<String> ids) {
		List<ByteId> bids = new ArrayList<>();
		ByteIdAdapter adapter = new ByteIdAdapter();
		for (String id : ids) {
			if (id!=null && !id.isEmpty()){
				try {
					ByteId bid = adapter.unmarshal(id);
					bids.add(bid);
				} catch (Exception e) {
					logger.error(e.getMessage(),e);
				}
			}
		}
		return bids;
	}
	
//	@POST
	public void save(Collection<T> entities) {
		getDAO().save(entities, CascadeUtils.always(), OnError.ATTEMPT_UNDO);
	}
	
	
//
//	Map<ByteId, T> get(Collection<ByteId> ids, Fetch fetch);
//
//	Map<ByteId, T> get(Collection<ByteId> ids, Fetch fetch,
//			Transaction transaction);
//
//	void save(Collection<T> entities, Cascade cascade, OnError onError);
//
//	void save(Collection<T> entities, Cascade cascade, Transaction transaction);
//
//	void save(T entity, Cascade cascade, OnError onError);
//
//	void delete(Collection<T> entities, Cascade cascade, OnError onError);
//
//	void delete(T entity, Cascade cascade, OnError onError);
//
//	void deleteById(Collection<ByteId> ids, Cascade cascade, OnError onError);
//
//	void deleteById(ByteId id, Cascade cascade, OnError onError);
//
//	void deleteByIdMap(Map<ByteId, T> idMap, Cascade cascade,
//			Transaction transaction);
//
//	void deleteWhatRefersTo(Collection<ByteId> ids, Cascade cascade,
//			Transaction transaction,EntityReference<T,?> reference);
//
//	Class<T> getEntityClass();
//
//	boolean hasId(ByteId id);
//
//	Map<ByteId, Boolean> hasIds(Collection<ByteId> ids);
//
//	Map<ByteId, T> scanUsingIndex(Index<T> index, Fetch fetch, Transaction transaction,
//			T from, T to);
//
//	Map<ByteId, T> getUsingIndex(Index<T> index, Fetch fetch, OnError onError, T template);
//
//	Map<ByteId, T> getUsingIndex(Index<T> index,  Fetch fetch, Transaction transaction,
//			T template);
//
//	void deleteUsingIndex(Index<T> index,  Cascade cascade, OnError onError,
//			T template);
//
//	void deleteUsingIndex(Index<T> index,  Cascade cascade, Transaction transaction,
//			T template);
//
//	void deleteUsingIndexRange(Index<T> index,  Cascade cascade, OnError onError,
//			T from,T to);
//
//	void deleteUsingIndexRange(Index<T> index,  Cascade cascade, Transaction transaction,
//			T from,T to);
//
//	Set<ByteId> getIdsUsingIndex(Index<T> index, OnError onError,
//			T from, T to);
//
//	Set<ByteId> getIdsUsingIndex(Index<T> index, Transaction transaction,
//			T from, T to);
//
//	Set<ByteId> getIdsUsingIndex(Index<T> index, OnError onError, T template);
//
//	Set<ByteId> getIdsUsingIndex(Index<T> index,  Transaction transaction,
//			T template);
//	
//	Set<ByteId> getIdsUsingIndex(Index<T> index, OnError onError,Collection<T> templates);
//
//	Map<ByteId,T> getUsingIndex(Index<T> index, Fetch fetch,OnError onError,Collection<T> templates);
//	
//	DAODefinition<T> getDefinition();


	private Fetch parseFetch(String fetchMode) {
		if ("always".equals(fetchMode)) {
			return FetchUtils.always();
		} else if ("once".equals(fetchMode) || "oneLevel".equals(fetchMode)) {
			return FetchUtils.oneLevel();
		}
		return FetchUtils.never();
	}
	
	
	
}
