package fungle.funfinder.rest.resource;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Singleton;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.OnError;
import fungle.common.hdao.util.CascadeUtils;
import fungle.common.hdao.util.FetchUtils;
import fungle.funfinder.data.dao.ActivityTypeDAO;
import fungle.funfinder.data.entity.ActivityType;

@Path("activityType")
@Singleton
@Service
public class ActivityTypeResource {
	
	private ActivityTypeDAO activityTypeDAO;
	
	public ActivityTypeResource() {
		
	}

	@Autowired
	public ActivityTypeResource(ActivityTypeDAO activityTypeDAO) {
		this.activityTypeDAO=activityTypeDAO;
	}
	
	public ActivityTypeDAO getActivityTypeDAO() {
		return activityTypeDAO;
	}

	public void setActivityTypeDAO(ActivityTypeDAO activityTypeDAO) {
		this.activityTypeDAO = activityTypeDAO;
	}
	
	@GET
	@Path("/all")
	@Produces(value={MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	public List<ActivityType> getAll() {
		Map<ByteId, ActivityType> types = activityTypeDAO.getAll(FetchUtils.always(),OnError.CONTINUE,null,null);
		return new ArrayList<>(types.values());
	}
	
	@POST
	public Response post(ActivityType activityType,@Context UriInfo uriInfo) {
		// must do validation, check that name is not null/empty,
		// and outdoorsy is set.
		if (activityType==null) {
			return Response.status(400).entity("The activity type was null").build();
		}
		if (StringUtils.isBlank(activityType.getName())) {
			// it's not valid.
			return Response.status(400).entity("The name must not be null, empty, or all white space.").build();
		}
		if (!isNameValid(activityType.getName())) {
			// since it's not valid, don't include it in the response, it could be 
			// part of a XSS attack.
			return Response.status(400).entity("The name provided was not valid, it should only have letters, white space, and a hyphen.").build();
		}
		// from here on the name is trusted.
		if (activityType.getFeeling()==null) {
			return Response.status(400).entity("All parameters must be set, the 'feeling vector' was null.").build();
		}
//		ActivityType existing = activityTypeDAO.getByName(activityType.getName());
//		if (existing==null) {
			activityTypeDAO.save(activityType, CascadeUtils.always(), OnError.ATTEMPT_UNDO);
			ActivityType existing = activityTypeDAO.getByName(activityType.getName());
			if (existing !=null) {
				// success
				URI uri = uriInfo.getAbsolutePathBuilder().path(existing.getName()).build();
				
				// when using created, the uri provided will be set on 
				// the response's 'location' header.
				return Response.created(uri).build();
			} else {
				// failed to save.
				return Response.serverError().build();
			}
//		} else {
//			// already exists.
//			return Response.notModified("An activity type with that name already exists.").entity(existing).build();
//		}
	}
	
	@GET
	@Path("/{name}")
	public ActivityType getByName(@PathParam("name") String name) {
		if (!isNameValid(name)) {
			// since it's not valid, don't include it in the response, it could be 
			// part of a XSS attack.
			throw new IllegalArgumentException("The name provided is not valid.");
		}
		ActivityType existing = activityTypeDAO.getByName(name);
		return existing;
	}
	
	@DELETE
	@Path("/{name}")
	public Response deleteByName(@PathParam("name") String name) {
		if (StringUtils.isBlank(name)) {
			// it's not valid.
			return Response.status(400).entity("The name must not be null, empty, or all white space.").build();
		}
		if (!isNameValid(name)) {
			// since it's not valid, don't include it in the response, it could be 
			// part of a XSS attack.
			return Response.status(400).entity("The name provided was not valid, it should only have letters, white space, and a hyphen.").build();
		}
		ActivityType type = activityTypeDAO.getByName(name);
		if (type==null) {
			// I know the name is valid at this point,
			return Response.status(404).entity("There is no activity type by the name "+name).build();
		}
		activityTypeDAO.delete(type, CascadeUtils.always(), OnError.ATTEMPT_UNDO);
		type = activityTypeDAO.getByName(name);
		if (type==null) {
			return Response.noContent().build();
		} else {
			return Response.serverError().entity("Failed to delete the activity type "+name).build();
		}
	}
	
	/**
	 * This is used to validate the name input, specifically to filter out
	 * cross site scripting attacks.  If this returns false, assume it's a 
	 * 'dangerous' string that might be part of an attack.
	 * @param name
	 * @return
	 */
	public boolean isNameValid(String name) {
		return StringUtils.isNotBlank(name) && name.matches("[a-zA-Z\\-\\s]+");
	}
}