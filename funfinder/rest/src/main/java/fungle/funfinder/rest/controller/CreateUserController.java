package fungle.funfinder.rest.controller;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import fungle.common.core.util.DateGuesser;
import fungle.common.hdao.model.OnError;
import fungle.common.hdao.util.CascadeUtils;
import fungle.funfinder.data.dao.UserDAO;
import fungle.funfinder.data.entity.Address;
import fungle.funfinder.data.entity.User;
import fungle.funfinder.rest.exceptions.ExistingUserException;
import fungle.funfinder.rest.validation.UserValidator;

@Controller("createUser")
public class CreateUserController {

	private final UserDAO userDAO;
	
	// the ultra safe password encrypter.  We do NOT 
	// want our customers to have their passwords stolen and/or
	// guessed now do we.
	private final BCryptPasswordEncoder encoder;

	@Autowired
	public CreateUserController(UserDAO userDAO,BCryptPasswordEncoder encoder) {
		this.userDAO = userDAO;
		this.encoder = encoder;
	}

	@RequestMapping("createUser")
	public String createUser(@RequestParam("firstName") String firstName,
			@RequestParam("lastName") String lastName,
			@RequestParam("email") String email,
			@RequestParam("userName") String userName,
			@RequestParam("male") boolean male,
			@RequestParam("password") String password,
			@RequestParam("password2") String password2,
			@RequestParam("dateOfBirth") String dob,
			@RequestParam("street1") String street1,
			@RequestParam("street2") String street2,
			@RequestParam("city") String city, @RequestParam("state") String state,
			@RequestParam("zip") String zip) {
		if (!password.equals(password2)) {
			throw new IllegalArgumentException("passwords don't match.");
		}
		UserValidator.checkPassword(password);

		User existing = userDAO.getByEmail(email);
		if (existing != null) {
			throw new ExistingUserException("email", existing.getByteId()
					.toString());
		}
		existing = userDAO.getByUserName(userName);
		if (existing != null) {
			throw new ExistingUserException("user name", existing.getByteId()
					.toString());
		}

		Date dateOfBirth = DateGuesser.guessDate(dob);
		
		UserValidator.validateName(firstName);
		UserValidator.validateName(lastName);
		UserValidator.validateUserName(userName);
		UserValidator.validateEmail(email);
		Address address = makeAddress(street1, street2, city, state, zip);
		User user = new User().firstName(firstName).lastName(lastName)
				.address(address).male(male);
		user.setEmail(email);
		user.setUserName(userName);
		user.defaultId();
		user.dateOfBirth(dateOfBirth);
		// we use the SHA algorithm to encrypt passwords.
		// TODO see if we can use SHA2 instead.
		String encryptedPassword = encoder.encode(password);
		user.setEncryptedPassword(encryptedPassword);
		userDAO.save(user, CascadeUtils.always(), OnError.ATTEMPT_UNDO);

		existing = userDAO.getByEmail(email);
		if (existing!=null) {
			return "../../home";
		}
		// it failed for some reason.
		// TODO make a failure page.
		return "";
	}

	public Address makeAddress(String street1, String street2, String city,
			String state, String zip) {
		UserValidator.validateAddressField(street2);
		String regex = "(\\d+)\\s+(.*)";
		Pattern pattern = Pattern.compile(regex);
		Matcher m = pattern.matcher(street1);
		int num = 0;
		if (m.find()) {
			num = Integer.parseInt(m.group(1));
			street1 = m.group(2);
		}
		UserValidator.validateAddressField(street1);
		UserValidator.validateAddressField(city);
		UserValidator.validateAddressField(state);
		zip = zip.replace("\\s+", "");
		int nzip = 0;
		int subzip = 0;
		if (zip.contains("-")) {
			String[] ps = zip.split("-");
			zip = ps[0];
			nzip = Integer.parseInt(zip);
			subzip = Integer.parseInt(ps[1]);
		}
		nzip = Integer.parseInt(zip);
		Address address = new Address().street1(street1).street2(street2)
				.city(city).state(state).zip(nzip).subZip(subzip);
		return address;
	}
}