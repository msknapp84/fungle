package fungle.funfinder.rest.provider;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.ext.ParamConverter;

import fungle.common.core.util.DateGuesser;

public class DateProvider implements ParamConverter<Date> {
	private static final String FORMAT = "yyyy/MM/dd HH:mm:ss";
	
	@Override
	public Date fromString(String time) {
		return DateGuesser.guessDate(time);
	}

	@Override
	public String toString(Date time) {
		SimpleDateFormat sdf = new SimpleDateFormat(FORMAT);
		String v = sdf.format(time);
		return v;
	}
}