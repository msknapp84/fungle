package fungle.funfinder.rest.resource;

import javax.ws.rs.Path;

import org.springframework.stereotype.Service;

import fungle.common.hdao.ByteIdentifiableDAO;
import fungle.funfinder.data.dao.UserDAO;
import fungle.funfinder.data.entity.User;

//@Path("/user")
//@Service("userResource")
public class UserResource extends AbstractDAOResource<User> {

	private UserDAO userDAO;
	
	
	@Override
	public ByteIdentifiableDAO<User> getDAO() {
		return userDAO;
	}

}
