package fungle.funfinder.rest.exceptions;

public class UnacceptablePasswordException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7881887926334957298L;

	private final String password;
	private final String justification;
	
	public UnacceptablePasswordException(String password,String justification) {
		super("The password did not meet our quality standards.");
		this.password = password;
		this.justification = justification;
	}
}
