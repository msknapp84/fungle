package fungle.funfinder.rest.provider;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.Date;

import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverterProvider;

public class FungleParamConverterProvider implements ParamConverterProvider {

	private static final DateProvider dateProvider = new DateProvider();
	private static final CalendarProvider calendarProvider = new CalendarProvider();
	
	@Override
	public <T> ParamConverter<T> getConverter(Class<T> clazz, Type type,
			Annotation[] annotations) {
		if (clazz.equals(Calendar.class)) {
			return (ParamConverter<T>) calendarProvider;
		} else if (clazz.equals(Date.class)) {
			return (ParamConverter<T>) dateProvider;
		}
		return null;
	}

}
