package fungle.funfinder.rest.provider;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.ws.rs.ext.ParamConverter;

import fungle.common.core.util.DateGuesser;

public class CalendarProvider implements ParamConverter<Calendar> {
	private static final String FORMAT = "yyyy/MM/dd HH:mm:ss";
	
	@Override
	public Calendar fromString(String time) {
		return DateGuesser.guessCalendar(time);
	}

	@Override
	public String toString(Calendar time) {
		SimpleDateFormat sdf = new SimpleDateFormat(FORMAT);
		String v = sdf.format(time.getTime());
		return v;
	}

}
