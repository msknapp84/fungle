package fungle.funfinder.rest;

import java.util.Arrays;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import fungle.funfinder.data.entity.User;

public class FungleUserDetails implements UserDetails {
	private static final long serialVersionUID = 5945626604547076735L;

	private static final GrantedAuthority USER = new GrantedAuthority() {
		private static final long serialVersionUID = -2719452808839193340L;
		@Override
		public String getAuthority() {
			return "ROLE_USER";
		}
		public String toString() {
			return "ROLE_USER";
		}
	};
//	private static final GrantedAuthority ROLE_USER = new GrantedAuthority() {
//		private static final long serialVersionUID = -2719452808839193340L;
//		@Override
//		public String getAuthority() {
//			return "ROLE_USER";
//		}
//	};
	private static final GrantedAuthority ADMINISTRATOR = new GrantedAuthority() {
		private static final long serialVersionUID = 496573735300192151L;

		@Override
		public String getAuthority() {
			return "ROLE_ADMINISTRATOR";
		}
		public String toString() {
			return "ROLE_ADMINISTRATOR";
		}
	};
	
	private final User user;
	private final boolean isAdmin;
	
	public FungleUserDetails(User user, boolean isAdmin) {
		this.user = user;
		this.isAdmin = isAdmin;
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		if (isAdmin) {
			return Arrays.asList(USER,ADMINISTRATOR);
		} else {
			return Arrays.asList(USER);
		}
	}

	@Override
	public String getPassword() {
		return user.getEncryptedPassword();
	}

	@Override
	public String getUsername() {
		return user.getUserName();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}