package fungle.funfinder.rest.validation;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fungle.funfinder.rest.exceptions.UnacceptablePasswordException;

public final class UserValidator {
	private static final Logger logger = LoggerFactory.getLogger(UserValidator.class);
	private static Set<String> illegalUserNames = new HashSet<>();
	private static Set<String> illegalEmails = new HashSet<>();
	
	static {
		String s=null;
		try {
			s = IOUtils.toString(UserValidator.class.getResourceAsStream("/illegalUserNames.txt"));
			illegalUserNames.addAll(Arrays.asList(s.split("\n")));
		} catch (IOException e) {
			logger.error("Failed to load the illegal user names file");
		}
		try {
			s = IOUtils.toString(UserValidator.class.getResourceAsStream("/illegalEmails.txt"));
			illegalEmails.addAll(Arrays.asList(s.split("\n")));
		} catch (IOException e) {
			logger.error("Failed to load the illegal user names file");
		}
	}
	
	private UserValidator(){
		
	}

	public static void validateEmail(String email) {
		if (email == null || email.isEmpty()) {
			throw new IllegalArgumentException(
					"The email address is not acceptable.  it must "
							+ "not be null/empty.");
		}
		if (!email.matches("[\\w\\d_\\-!]+@[\\w]+\\.com")) {
			throw new IllegalArgumentException(
					"The email address is not acceptable.  it must contain no spaces, "
							+ "and the only allowed special characters are hyphen and/or an exclamation mark.");
		}
		String[] ps = email.split("@");
		for (String s : illegalEmails) {
			if (ps[0].toLowerCase().contains(s)) {
				throw new IllegalArgumentException(
						"The email address is not acceptable.  it must contain no spaces, "
								+ "and the only allowed special characters are hyphen and/or an exclamation mark.");
			}
		}
	}

	public static void validateName(String name) {
		if (name == null || name.isEmpty()) {
			throw new IllegalArgumentException(
					"The name is not acceptable.  it must "
							+ "not be null/empty.");
		}
		if (!name.matches("\\w[\\w\\s\\d]+")) {
			throw new IllegalArgumentException(
					"The name can only contain characters and spaces and numbers, and "
							+ "the first character must be a letter.");
		}
	}

	public static void validateUserName(String name) {
		if (name == null || name.isEmpty()) {
			throw new IllegalArgumentException(
					"The name is not acceptable.  it must "
							+ "not be null/empty.");
		}
		if (!name.matches("[\\w][\\w\\d_\\-]*")) {
			throw new IllegalArgumentException(
					"The user name can only contain characters numbers, a hyphen, and/or an underscore.  The first character must be a letter.");
		}
		for (String s : illegalUserNames) {
			if (name.toLowerCase().contains(s)) {
				throw new IllegalArgumentException(
						"User names cannot contain the string "+s);
			}
		}
	}

	public static void checkPassword(String password) {
		if (password == null) {
			throw new UnacceptablePasswordException("null",
					"passwords don't match.");
		}
		if (password.length() < 8 || password.length() > 16) {
			throw new UnacceptablePasswordException(password,
					"the length must be within 8 and 16 characters");
		}
		if (!password.matches(".*\\d.*")) {
			throw new UnacceptablePasswordException(password,
					"the password must contain a digit/number");
		}
		if (!password.matches(".*[a-z].*")) {
			throw new UnacceptablePasswordException(password,
					"the password must contain a lower-case letter");
		}
		if (!password.matches(".*[A-Z].*")) {
			throw new UnacceptablePasswordException(password,
					"the password must contain an upper-case letter");
		}
	}

	public static void validateAddressField(String street) {
		if (street == null || street.isEmpty()) {
			return;
		}
		if (!street.matches("[\\w\\s\\-]*")) {
			throw new IllegalArgumentException(
					"address fields may only contain characters, spaces, and hyphens.");
		}
	}
}
