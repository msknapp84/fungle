package fungle.funfinder.rest.exceptions;

public class ExistingUserException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3168195235198846198L;
	private final String basedOnField;
	private final String existingUserId;
	
	public ExistingUserException(String basedOnField,String existingUserId) {
		super("There is another user with the same "+basedOnField);
		this.basedOnField = basedOnField;
		this.existingUserId = existingUserId;
	}

	public String getBasedOnField() {
		return basedOnField;
	}

	public String getExistingUserId() {
		return existingUserId;
	}

}
