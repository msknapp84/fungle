<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="description" content="Lets you create and edit activity types for the fungle website.">
	<meta name="author" content="Michael Knapp">
	<!-- saved from url=(0014)about:internet -->
	
	<title>Create Activity Types</title>
	<!-- Redefine the base so it's easy to link things. -->
	<base href="/fungle.funfinder.web/" />
	<link rel="stylesheet" href="css/reset.css" />
	<link rel="stylesheet" href="css/fungle.css" />
	<link rel="stylesheet" href="css/activityType.css" />
	<script src="javascript/jquery-2.1.1.min.js"></script>
	<script src="javascript/activityType.js"></script>
</head>
<body>
	<header>
		<div class="headerTop screenWidth">
			<div class="headerTop fixedWidth">
				<h1>Fungle</h1>
				<h2>Find Something Fun To Do!</h2>
				<div class="ad horizontalAd">Your ad here!</div>
			</div>
		</div>
		<div class="nav screenWidth">
			<nav class="nav fixedWidth">
				<ul>
					<!-- Lets you define your profile and preferences -->
					<li><a href="">Profile</a></li><li>
						<!-- Takes you to your home page --> <a href="home.jsp">Home</a>
					</li><li>
						<!-- Lets you define new data like events, places, activities, and activity types (admin only) -->
						<a href="">Create</a>
					</li><li>
						<!-- Lets you see your schedule --> <a href="">My Schedule</a>
					</li><li>
						<!-- Lets you see events you went to in the past, and review them -->
						<a href="">My Recent Events</a>
					</li><li>
						<!-- Lets you log out --> <a href="logout">Logout</a>
					</li><li>
						<!-- Lets you search --> <a href="">Search</a>
					</li>
				</ul>
				
			</nav>
		</div>
	</header>
	<div class="screenWidth mainWrapper">
		<main class="outer fixedWidth">
			<section class="activityTypeDiv ">
				<!-- Let's display all the activity types. -->
				<img id="reloadButton" class="clickable" onclick="refreshActivityTypes()" src="icons/gif/green/32x32/reload.gif"/>
				<table id="activityTypeTable">
					<colgroup>
						<col class="controlsColumn"/>
						<col class="nameColumn"/>
						<col class="outdoorsyColumn"/>
						<col class="recreationalColumn"/>
						<col class="expensiveColumn"/>
						<col class="riskyColumn"/>
						<col class="socialColumn"/>
						<col class="timeConsumingColumn"/>
					</colgroup>
					<thead>
						<tr>
							<th>controls</th>
							<th>name<img src="icons/gif/black/16x16/arrow2_w.gif" onclick="sort(this)"/></th>
							<th>outdoorsy</th>
							<th>recreational<img src="icons/gif/black/16x16/arrow2_w.gif" onclick="sort(this)"/></th>
							<th>expensive<img src="icons/gif/black/16x16/arrow2_w.gif" onclick="sort(this)"/></th>
							<th>risky<img src="icons/gif/black/16x16/arrow2_w.gif" onclick="sort(this)"/></th>
							<th>social<img src="icons/gif/black/16x16/arrow2_w.gif" onclick="sort(this)"/></th>
							<th>time consuming<img src="icons/gif/black/16x16/arrow2_w.gif" onclick="sort(this)"/></th>
						</tr>
					</thead>
					<tbody>
						<!-- This will get populated once when the page loads so that if a search engine
						queries this page, it will see lots of useful information.  It will be updated whenever
						the user adds a new entry, via ajax.-->
					</tbody>
				</table>
			</section>
			<div class="outerFormDiv">
				<section class="formDiv">
					<form id="activityTypeForm" action="#" onsubmit="submitActivityType(event)">
						<table>
							<colgroup>
								<col style="width:50%;text-align:right;"/>
								<col style="width:50%;text-align:left"/>
							</colgroup>
							<tbody>
								<tr>
								<td>
								<label for="activityTypeName">Name *</label></td>
								<!-- throw in two spaces just to make it line up with the rest of the rows. -->
								<td>&nbsp;&nbsp;<input type="text" id="activityTypeName" name="name" 
								title="The activity type name" required autofocus pattern="[a-zA-Z\s\-]+"/></td>
								</tr>
								<tr>
									<td><label for="activityTypeOutdoorsy">Out&shy;doorsy *</label></td>
									<td>&nbsp;<input type="radio" name="outdoorsy" value="true"
											id="activityTypeOutdoorsy" required/>true<input type="radio"
											name="outdoorsy" value="false" required/>false</td>
								</tr>
								<tr>
								<td><label for="activityTypeRecreationRating">Recreation
											Rating</label></td>
								<td><input type="range" min="1" max="10" value="5"
										name="recreationRating" id="activityTypeRecreationRating" onchange="onChangeSlider()" /><span class="sliderValue" id="activityTypeRecreationRatingValue">5</span></td>
								</tr>
								<tr>
								<td><label for="activityTypeExpenseRating">Expense
											Rating</label></td>
								<td><input type="range" min="1" max="10" value="5"
										name="expenseRating" id="activityTypeExpenseRating"  onchange="onChangeSlider()"/><span class="sliderValue" id="activityTypeExpenseRatingValue">5</span></td>
								</tr>
								<tr>
								<td><label for="activityTypeSocialRating">Social
											Rating</label></td>
								<td><input type="range" min="1" max="10" value="5"
										name="socialRating" id="activityTypeSocialRating"  onchange="onChangeSlider()"/><span class="sliderValue" id="activityTypeSocialRatingValue">5</span></td>
								</tr>
								<tr>
								<td><label for="activityTypeRiskRating">Risk Rating</label></td>
								<td><input type="range" min="1" max="10" value="5" name="riskRating"
										id="activityTypeRiskRating"  onchange="onChangeSlider()"/><span class="sliderValue" id="activityTypeRiskRatingValue">5</span></td>
								</tr>
								<tr>
								<td><label for="activityTypeTimeConsumptionRating">Time
											Consumption Rating</label></td>
								<td><input type="range" min="1" max="10" value="5"
										name="timeConsumptionRating"
										id="activityTypeTimeConsumptionRating" onchange="onChangeSlider()" /><span class="sliderValue" id="activityTypeTimeConsumptionRatingValue">5</span></td>
								</tr>
								<tr>
									<td><input type="reset" value="Reset" onclick="resetSliders()"/></td><td id="submitActivityTypeCell"><input type="submit" value="Submit" id="activityTypeSubmit"/></td>
								</tr>
							</tbody>
						</table>
						
					</form>
				</section>
				<aside class="leftSidebar">
					<p>This is the left sidebar</p>
					<div class="ad skyscraperAd">Your ad here!</div>
				</aside>
			</div>
		</main>
	</div><!-- End screen width main wrapper -->
	<div class="screenWidth fatFooterWrapper">
		<segment class="fatFooter fixedWidth">
			
		</segment>
	</div>
	<div class="screenWidth footerWrapper">
		<footer class="footer fixedWidth">
			This is the footer.
		</footer>
	</div>
	<div class="scrollToTopButton">
		<img onclick="jumpToTop()" src="icons/gif/white/64x64/arrow1_n.gif" />
	</div>
</body>
</html>