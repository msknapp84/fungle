<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>New User</title>
</head>
<body>
	Let's create a new user!
	<br />
	<br />
	<form action="main/createUser" method="post">
		First Name: <input type="text" name="firstName" /><br /> Last Name: <input
			type="text" name="lastName" /><br /> Email: <input type="text"
			name="email" /><br /> User Name: <input type="text" name="userName" /><br />
		Sex:<br /> <input type="radio" name="male" value="true">male</input><br />
		<input type="radio" name="male" value="false">female</input><br />
		Password<input type="text" name="password" /><br /> Confirm Password:
		<input type="text" name="password2" /><br /> Date of Birth
		(yyyy/MM/dd): <input type="text" name="dateOfBirth" /><br /> Address:<br />
		Street Line 1: <input type="text" name="street1" /><br /> Street Line
		2: <input type="text" name="street2" /><br /> City: <input type="text"
			name="city" /><br /> State: <input type="text" name="state" /><br />
		Zip: <input type="text" name="zip" /><br /> <input type="submit"
			value="Submit" />
	</form>
</body>
</html>