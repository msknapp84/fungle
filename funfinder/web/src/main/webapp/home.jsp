<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Fungle Home</title>
</head>
<body>
	You made it!
	<br />
	<a href="logout">logout</a>

	<br />
	<security:authorize access="hasRole('ROLE_ADMINISTRATOR')">
		You are an administrator!<br />
		<a href="admin/createActivityType.jsp">Create Activity Type</a>
		<br />
	</security:authorize>
	<a href="manage/createPlace.jsp">Create Place</a>
	<br />
	<a href="manage/createActivity.jsp">Create Activity</a>
	<br />
	<a href="manage/createEvent.jsp">Create Event</a>
	<br />
</body>
</html>