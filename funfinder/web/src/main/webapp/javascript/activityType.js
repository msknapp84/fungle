var scrollButtonVisible=false;
var deletingRow=undefined;

// done when the page is ready:
$(function() {
	// I want the scroll to top button to work.
	$(window).scroll(manageScrollToTop);
	
	
	// need the navigation to work.
	
	// fill in the activity types table.
	refreshActivityTypes();
});

function  onChangeSlider(event) {
	$('#activityTypeRecreationRatingValue').text($('#activityTypeRecreationRating').val());
	$('#activityTypeSocialRatingValue').text($('#activityTypeSocialRating').val());
	$('#activityTypeTimeConsumptionRatingValue').text($('#activityTypeTimeConsumptionRating').val());
	$('#activityTypeExpenseRatingValue').text($('#activityTypeExpenseRating').val());
	$('#activityTypeRiskRatingValue').text($('#activityTypeRiskRating').val());
}

/**
 * This will get the activity type from the form data,
 * as a JSON object.
 */
function getCurrentActivityType() {
	var activityType = new Object();
	activityType.name=$('#activityTypeName').val();
	var feeling = new Object();
	feeling.outdoorsy=$('#activityTypeOutdoorsy').val();
	feeling.riskRating=$('#activityTypeRiskRating').val();
	feeling.socialRating=$('#activityTypeSocialRating').val();
	feeling.recreationRating=$('#activityTypeRecreationRating').val();
	feeling.expenseRating=$('#activityTypeExpenseRating').val();
	feeling.timeConsumptionRating=$('#activityTypeTimeConsumptionRating').val();
	activityType.feeling=feeling;
	return activityType;
}

function submitActivityType(event) {
	// it should already be valid since html 5 attributes validate it.
	// no need to validate it here unless worried about IE 8.
	
	// let's prevent a double click mistake:
	// page 270 of javascript: the missing manual
	$("#activityTypeSubmit").attr('disabled',true);
	
	// I want the activity type as a json object
	// I can't use the serialize method since that produces 
	// a query parameter string, and I need a json object. 
	var activityType = getCurrentActivityType();
	
	// can't just use a json object, even if processData is false,
	// it just doesn't work.  You must convert the object to a 
	// JSON string before it's sent.
	var activityTypeJSON = JSON.stringify(activityType);
	
	// is the post url relative to the js file?  or the page?
	// page 352 of javascript: The missing manual: it's all relative
	// to the current page.
	// but my current page re-defines the base to the web app base.
	// the resource is in resource/activityType
	// unfortunately the $.post method posts using content type of form data,
	// I am trying to post a json object.
	$.ajax({
		type:"POST",
		url:"resource/activityType",
		data:activityTypeJSON,
		statusCode: {
			201:handleCreated,
			500:handleErrorResponse,
			// can be not modified if the name is already taken.
			304:handleNotModified,
			// can be 400 if we make a bad request.
			400:handleBadRequest
		},
		contentType: "application/json; charset=utf-8",
		// processData is true by default, and it f*s with the 
		// json by converting it into a query string.
		// I want this to be sent AS JSON!  So don't process it.
		processData:false
	});
	
	// don't want the page to refresh
	// I prefer to use preventDefault instead of return false
	// so maybe this can be used from other methods that don't provide
	// an event.  Returning false insinuates that the form
	// was invalid.
	event.preventDefault();
	
	// TODO we should really show them something so they know it's in progress.
}

function handleNotModified(data,textStatus,jqXHR) {
	alert("The object was not modified since it already exists.");
	
	// when a failure happens, we still need to let them try again.
	$('#activityTypeSubmit').attr("disabled",false);
}

function handleBadRequest(data,textStatus,jqXHR) {
	alert("Seems like we made a bad request :(");
	
	// when a failure happens, we still need to let them try again.
	$('#activityTypeSubmit').attr("disabled",false);
}

/**
 * 
 * @param jqXHR a jquery jqXHR object
 * @param textStatus a string text status
 * @param errorThrown a string of the error thrown
 */
function handleErrorResponse(jqXHR,textStatus,errorThrown) {
	alert("The request failed!");
	
	// when a failure happens, we still need to let them try again.
	$('#activityTypeSubmit').attr("disabled",false);
}

function handleCreated(data,textStatus,jqXHR) {
	// on success, I expect a 201: created,
	// and that response will tell me where it was created.
	// erring on the side of caution, I will retrieve that object
	// and reset the form, instead of adding a row to the table
	// because I want to be sure that the entity is really persistent
	// and retrievable.
	
	// I know it's a 201 because I assigned this function to that status code.
	
	// so how do I get the location data?
	// it should be assigned to the response header named 'location'
	var savedLocation = jqXHR.getResponseHeader('location');
	
	// now load it.
	loadActivityTypeFromUrl(savedLocation);
	
	// let them continue adding data.
	$('#activityTypeSubmit').attr("disabled",false);
}

function loadActivityTypeFromUrl(url) {
	// getJSON always does get requests, data is 
	// appended to the url, so I use an empty string.
	$.getJSON(url,'',insertActivityType).error(function(){
		alert("We failed to get the activity type from url: "+url);
	});
}

/**
 * This will insert the activity type into the table,
 * it's meant to be mapped as the 'success' method from 
 * loadActivityTypeFromUrl. 
 * @param activityType
 * @param status
 * @param jqXHR
 */
function insertActivityType(activityType) {//,status,jqXHR) {
	// it might already be in the table...
	$('#activityTypeTable tbody tr').each(function(index,row){
		// check the name.
		var name = $(row).children()[1].innerHTML;
		if (activityType.name == name) {
			$(row).remove();
		}
	});
	
	// the data is already a JSON object since I used
	// getJSON to retrieve it.  So I can form a new row
	// and insert it straight into the table.
	var newRow = convertToRow(activityType);
	
	if (typeof newRow !== "undefined") {
		// append should put the row inside the body, after all of its
		// existing children.
		$('#activityTypeTable tbody').append(newRow);
	}
}

function convertToRow(activityType) {
	if (activityType == undefined || !(activityType instanceof Object)) {
		return undefined;
	}
	var feeling = activityType.feeling;
	if (typeof feeling === "undefined") {
		feeling=new Object();
		feeling.outdoorsy="false";
		feeling.recreationRating="5";
		feeling.riskRating="5";
		feeling.socialRating="5";
		feeling.timeConsumptionRating="5";
		feeling.expenseRating="5";
	}
	var newRow = "<tr>" +
			"<td>" +
			"<img src=\"icons/gif/brown/16x16/edit.gif\" class=\"editIcon\" id=\""+activityType.name+"EditIcon\" onclick=\"editActivityType(this)\"/>" +
			"<img src=\"icons/gif/red/16x16/close.gif\" class=\"deleteIcon\" id=\""+activityType.name+"DeleteIcon\" onclick=\"deleteActivityType(this)\"/>" +
					"</td>"
		+ "<td>"+activityType.name + "</td><td>" + feeling.outdoorsy
		+ "</td><td>" + feeling.recreationRating + "</td><td>"
		+ feeling.expenseRating + "</td><td>"
		+ feeling.riskRating + "</td><td>"
		+ feeling.socialRating + "</td><td>"
		+ feeling.timeConsumptionRating + "</td></tr>";
	return newRow;
}

function editActivityType(icon) {
	
	// up once for the td, again for the tr
	var row=icon.parentNode.parentNode;
	
	// with the row, I can update the form
	$(row).children().each(function(index,td){
		// index 0 is the control icons, so I skip it.
		if (index === 1) {
			$('#activityTypeName').val($(td).text());
		} else if (index === 2) {
			$('input[name="outdoorsy"]').each(function(index,radio){
				if ($(radio).val()==$(td).text()) {
					radio.checked=true;
				}
			});
		} else if (index === 3) {
			$('#activityTypeRecreationRating').val($(td).text());
			$('#activityTypeRecreationRatingValue').text($(td).text());
		} else if (index === 4) {
			$('#activityTypeExpenseRating').val($(td).text());
			$('#activityTypeExpenseRatingValue').text($(td).text());
		} else if (index === 5) {
			$('#activityTypeRiskRating').val($(td).val());
			$('#activityTypeRiskRatingValue').text($(td).text());
		} else if (index === 6) {
			$('#activityTypeSocialRating').val($(td).text());
			$('#activityTypeSocialRatingValue').text($(td).text());
		} else if (index === 7) {
			$('#activityTypeTimeConsumptionRating').val($(td).text());
			$('#activityTypeTimeConsumptionRatingValue').text($(td).text());
		}
	});
}

function deleteActivityType(icon) {
	if (!deletingRow) {
		var activityTypeName = icon.id.replace("DeleteIcon","");
		deletingRow = icon.parentNode.parentNode;
		$.ajax({
			type:"DELETE",
			url:"resource/activityType/"+activityTypeName,
			data:"",
			statusCode: {
				204:handleSuccessfulDelete,
				409:handleFailedDelete,
				400:handleFailedDelete
			}
		});
	}
}

function handleSuccessfulDelete(data,status,jqXHR) {
	// 204 means no content.
	$(deletingRow).remove();
	deletingRow=undefined;
}

function handleFailedDelete(data) {
	alert("The deletion failed!");
	deletingRow=undefined;
}

function resetSliders() {
	$('input[type="range"]').each(function (){
		$(this).val("5");
	});
	$('.sliderValue').each(function() {
		$(this).text("5");
	});
}

/**
 * If the top of the page is far up, have a button to scroll to the top.
 */
function manageScrollToTop(event) {
	// check the scroll position
	if ($(window).scrollTop()>100) {
		if (!scrollButtonVisible) {
			$('.scrollToTopButton').show();
			scrollButtonVisible=true;
		}
	} else {
		if (scrollButtonVisible) {
			$('.scrollToTopButton').hide();
			scrollButtonVisible=false;
		}
	}	
}

function jumpToTop() {
	window.scroll(0, 0);
	$('.scrollToTopButton').hide();
	scrollButtonVisible=false;
}
function refreshActivityTypes() {
	// first remove all the rows from the table.
	$('#activityTypeTable tbody').html("");
	
	// now load everything
	$.getJSON("resource/activityType/all",'',insertAll).error(onRefreshFailure);
}

function insertAll(activityTypes,status) {
	$.each(activityTypes,function(index,activityType){
		insertActivityType(activityType);
	});
}

function onRefreshFailure() {
	alert("We failed to refresh!");
}

function sort(icon) {
	var by = $(icon).parent().text();
	// naturally it will be ascending
	// if the arrow already says it's ascending, then we go descending
	var ascending=$(icon).attr("src")=="icons/gif/black/16x16/arrow2_w.gif";
	
	$("#activityTypeTable thead img").each(function(index,img) {
		$(img).attr("src","icons/gif/black/16x16/arrow2_w.gif");
	});
	// the sorted column points up or down.
	if (ascending) {
		$(icon).attr("src","icons/gif/black/16x16/arrow2_s.gif");
	} else {
		$(icon).attr("src","icons/gif/black/16x16/arrow2_n.gif");
	}
}