User Stories
============

User comes to the website for first time, and starts a profile.  Tells the site
what things they enjoy doing: outdoors, camping, kayaking, bowling, tennis, 
bars, travel, etc.  They also tell the website where they live or frequent to.

After that, they log in and tell the web app what mood they are in: to do one 
activity or another.  They tell the web app if they are looking for something 
fun to do right now or at a specific time in the future.

The web app then searches for fun things they could do, and returns a list of  
suggestions.  The user reviews their results and decides where to go.  They may
provide feedback to the web app, saying suggestions are good or bad.  The 
app will consider that in future suggestions.

Case 2
------

The user returns after having done an event.  They can report to the site whether
they liked it or not.  They can also add other activities to the site that it does 
not know about.  They can report how they enjoyed other activities they 
did in the past.  

Over time, the web site will use a collaborative filter to decide what recommendations
to make for people.

Case 3
------

The user has already picked out an activity to do, and returns to the site to see 
that activity again.  They can view it on a list, or on their personal calendar.

They can interact with other users, planning events to attend, etc.

How Is this Different
=====================

From reddit
-----------

reddit lets you review websites.  fungle is activity and event oriented.
reddit has no calendar, and no event planning.  

From pinterest
--------------

pinterest let's you bookmark websites, it makes no recommendations though,
about other sites you might like.  It is not event or activity oriented.

From yelp
---------

Yelp is all about reviews for a place/business.  This website will have reviews 
as well, but will use collaborative filtering to help make suggestions on 
what you might want to do.  Fungle will not be limited to just businesses,
but it will be limited to fun things to do.  So you could list a bar, bowling
alley, etc.  but not a tax office, law firm, laundromat, etc. on fungle.

Access Patterns:
================

Upon reaching the home page, it show the user:
* their recommendations
* their calendar of upcoming events
* possibly a list of what their friends are doing (future work)

The user does a search for events.  The design of the events table depends heavily on how they will search.
Users will specify a location and maximum distance, a time frame, as well as their mood vector.  The search 
will also take into account their personal preferences for certain activity types.  