package fungle.funfinder.data.geo;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.Validate;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.util.Bytes;

import fungle.common.core.util.Reflect;
import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.ByteRange;
import fungle.common.hdao.model.Index;
import fungle.common.hdao.model.IndexField;
import fungle.common.hdao.util.HBaseByteConverter;

public class GeoIndex<T extends ByteIdentifiable> implements Index<T> {
	private final HTable referencedTable, indexTable;
	private final String coordinateFieldName;
	private final String timeFieldName;
	private final List<IndexField> fields;
	private final GeoFinder finder;

	private GeoIndex(GeoFinder finder, HTable indexTable,
			HTable referencedTable, String coordinateFieldName,
			String timeFieldName, Class<T> entityClass) {
		this.finder = finder;
		this.referencedTable = referencedTable;
		this.indexTable = indexTable;
		this.coordinateFieldName = coordinateFieldName;
		this.timeFieldName = timeFieldName;
		IndexField f1 = new IndexField(coordinateFieldName, false, true);
		if (timeFieldName != null) {
			IndexField f2 = new IndexField(timeFieldName, true, false);
			fields = Arrays.asList(f1, f2);
		} else {
			fields = Arrays.asList(f1);
		}
		if (!Reflect.fieldExistsLike(entityClass, coordinateFieldName,
				GeoCoordinate.class)) {
			throw new IllegalArgumentException(
					"The field named "
							+ coordinateFieldName
							+ " either does not exist or is not a coordinate on the class "
							+ entityClass.getSimpleName());
		}
		if (timeFieldName != null) {
			Reflect.verifyHasField(entityClass, timeFieldName);
		}
		Validate.notNull(finder);
	}

	@Override
	public List<IndexField> getFieldsInOrder() {
		return fields;
	}

	@Override
	public ByteRange getKey(T entity) {
		GeoCoordinate coordinate = Reflect.getFieldValue(coordinateFieldName,
				entity, GeoCoordinate.class);
		if (coordinate != null) {
			long time = timeFieldName == null ? 0 : Long.MAX_VALUE
					- getTime(entity);
			int geoIndex = finder.getIndex(coordinate);
			byte[] gi = Bytes.toBytes(geoIndex);
			if (time == 0 || time == Long.MAX_VALUE) {
				// the entity does not have an associated time, 
				// either the time field name is null or the entity 
				// has no schedule.  
				// A range scan is necessary in this case.
				int geoIndexEnd = geoIndex+1;
				byte[] gie = Bytes.toBytes(geoIndexEnd);
				return new ByteRange(gi,gie);
			} else {
				byte[] ti = Bytes.toBytes(time);
				byte[] b = HBaseByteConverter.merge(gi, ti);
				ByteId id = new ByteId(b);
				return new ByteRange(id);
			}
		}
		return null;
	}

	private long getTime(T entity) {
		Object o = Reflect.getFieldValue(timeFieldName, entity, Object.class);
		if (o instanceof Calendar) {
			return ((Calendar) o).getTimeInMillis();
		} else if (o instanceof Date) {
			return ((Date) o).getTime();
		} else if (o instanceof Number) {
			return ((Number) o).longValue();
		}
		return Long.MAX_VALUE;
	}

	@Override
	public boolean isUnique() {
		return false;
	}

	@Override
	public HTable getReferencedTable() {
		return referencedTable;
	}

	@Override
	public String getReferenceTableName() {
		return Bytes.toString(referencedTable.getTableName());
	}

	@Override
	public String getIndexTableName() {
		return Bytes.toString(indexTable.getTableName());
	}

	@Override
	public HTable getIndexTable() {
		return indexTable;
	}

	public static GeoIndexBuilder forTables(HTable indexTable,
			HTable referencedTable) {
		return new GeoIndexBuilder().indexTable(indexTable).referencedTable(
				referencedTable);
	}

	public static class GeoIndexBuilder {
		private HTable referencedTable, indexTable;
		private String coordinateFieldName = "coordinate";
		private String timeFieldName;
		private GeoFinder finder;

		public GeoIndexBuilder referencedTable(HTable referencedTable) {
			this.referencedTable = referencedTable;
			return this;
		}

		public GeoIndexBuilder indexTable(HTable indexTable) {
			this.indexTable = indexTable;
			return this;
		}

		public GeoIndexBuilder timeFieldName(String timeFieldName) {
			this.timeFieldName = timeFieldName;
			return this;
		}

		public GeoIndexBuilder finder(GeoFinder finder) {
			this.finder = finder;
			return this;
		}

		public GeoIndexBuilder coordinateFieldName(String coordinateFieldName) {
			this.coordinateFieldName = coordinateFieldName;
			return this;
		}

		public <T extends ByteIdentifiable> GeoIndex<T> build(
				Class<T> entityClass) {
			return new GeoIndex<T>(finder, indexTable, referencedTable,
					coordinateFieldName, timeFieldName, entityClass);
		}
	}
}