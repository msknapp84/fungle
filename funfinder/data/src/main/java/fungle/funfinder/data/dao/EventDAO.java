package fungle.funfinder.data.dao;

import java.util.List;

import fungle.common.hdao.ByteIdentifiableDAO;
import fungle.common.hdao.Fetch;
import fungle.common.hdao.model.Cascade;
import fungle.funfinder.data.GeoSearch;
import fungle.funfinder.data.TimeRange;
import fungle.funfinder.data.entity.Activity;
import fungle.funfinder.data.entity.ActivityType;
import fungle.funfinder.data.entity.Event;
import fungle.funfinder.data.entity.Place;

public interface EventDAO extends ByteIdentifiableDAO<Event> {
	List<Event> getByActivityTypeNear(GeoSearch search,ActivityType activityType,TimeRange timeRange, Fetch fetch);
	List<Event> getByActivity(Activity activity,TimeRange timeRange, Fetch fetch);
	List<Event> getByActivityTypeNear(GeoSearch search,ActivityType activityType,Fetch fetch);
	List<Event> getByActivity(Activity activity,Fetch fetch);
	List<Event> getByPlace(Place place,Fetch fetch);

	void deleteByActivity(Activity activity,Cascade cascade);
	void deleteByPlace(Place place,Cascade cascade);
}