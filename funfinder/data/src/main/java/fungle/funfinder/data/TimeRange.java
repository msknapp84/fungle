package fungle.funfinder.data;

import java.util.Calendar;

import org.apache.avro.reflect.Nullable;

public class TimeRange {
	
	@Nullable
	private Calendar start,stop;
	
	public TimeRange() {
		
	}
	
	public TimeRange(Calendar start,Calendar stop) {
		this.start = start;
		this.stop = stop;
	}

	public Calendar getStart() {
		return start;
	}

	public void setStart(Calendar start) {
		this.start = start;
	}

	public Calendar getStop() {
		return stop;
	}

	public void setStop(Calendar stop) {
		this.stop = stop;
	}
	
	public boolean contains(Calendar calendar) {
		return !start.after(calendar) && !stop.before(calendar);
	}
}