package fungle.funfinder.data.dao.hbase;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fungle.common.hdao.HBaseDAOBase;
import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.Cascade;
import fungle.common.hdao.model.Index;
import fungle.common.hdao.model.OnError;
import fungle.common.hdao.util.FetchUtils;
import fungle.funfinder.data.dao.ActivityTypeDAO;
import fungle.funfinder.data.entity.ActivityType;
import fungle.funfinder.data.tabledef.ActivityTypeDAODef;

@Component("activityTypeDAO")
public class ActivityTypeHBaseDAO extends HBaseDAOBase<ActivityType>
		implements ActivityTypeDAO {

	public ActivityTypeHBaseDAO() {
	}

	@Autowired
	public void setDefinition(ActivityTypeDAODef def) {
		this.daoDefinition=def;
		validate();
	}

	@Override
	public ActivityType getByName(String name) {
		Index<ActivityType> nameIndex = daoDefinition.getIndexByFieldName("name");
		ActivityType template = new ActivityType();
		template.setName(name);
		Map<ByteId,ActivityType> places = getUsingIndex(nameIndex, FetchUtils.never(), OnError.CONTINUE, template);
		Set<ActivityType> placesSet = new HashSet<>(places.values());
		placesSet.remove(null);
		return placesSet.isEmpty()?null:placesSet.iterator().next();
	}
	
	@Override
	public void deleteByName(String name,Cascade cascade) {
		Index<ActivityType> nameIndex = daoDefinition.getIndexByFieldName("name");
		ActivityType template = new ActivityType();
		template.setName(name);
		deleteUsingIndex(nameIndex, cascade, OnError.CONTINUE, template);
	}

//	@Override
//	public Set<ActivityType> getAll(Fetch fetch) {
//		Transaction transaction = new Transaction(OnError.CONTINUE);
//		Map<ByteId,ActivityType> types = new HashMap<>();
//		Scan scan = new Scan();
//		Map<ByteId,Scan> scans = new HashMap<>();
//		ByteId byteId = new ByteId(Bytes.toBytes("a"));
//		scans.put(byteId,scan);
//		
//		return null;
//	}
}