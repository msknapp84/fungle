package fungle.funfinder.data.dao;

import fungle.common.hdao.ByteIdentifiableDAO;
import fungle.common.hdao.model.Cascade;
import fungle.funfinder.data.entity.ActivityType;

public interface ActivityTypeDAO extends ByteIdentifiableDAO<ActivityType> {
	ActivityType getByName(String name);
	void deleteByName(String name,Cascade cascade);
}