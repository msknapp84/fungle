package fungle.funfinder.data.geo;

public enum GeoUnits {
	MILES,
	METERS,
	DEGREES,
	KILOMETERS;
}
