package fungle.funfinder.data.geo;

import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.CRS;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.TransformException;

import com.vividsolutions.jts.geom.Coordinate;

public class GeoToolsRuler extends GeoRulerBase implements GeoRuler {
	// http://docs.geotools.org/stable/userguide/library/referencing/calculator.html
	// http://docs.geotools.org/latest/javadocs/org/opengis/referencing/crs/CoordinateReferenceSystem.html
	// http://stackoverflow.com/questions/24006631/geotools-jts-orthodromicdistance-throws-exception-for-latitude-being-out-of-rang/24011038#24011038
	
	private CoordinateReferenceSystem crs = null;
	
	public GeoToolsRuler() {
		try {
			crs = CRS.decode("epsg:4326");
		} catch (FactoryException e) {
			e.printStackTrace();
		}
	}
	
	// JTS is not thread safe, we must synchronize here.
	
	public synchronized double measureDistanceInMeters(double[] coord1, double[] coord2) {
//		System.out.println("latitude: "+coord1[0]+", longitude: "+coord1[1]);
//		System.out.println("latitude: "+coord2[0]+", longitude: "+coord2[1]);
		// latitude first, longitude second.
		Coordinate start = new Coordinate(coord1[0],coord1[1]);
		Coordinate end = new Coordinate(coord2[0],coord2[1]);
	    double distance = -1;
		try {
			distance = JTS.orthodromicDistance(start, end, crs);
		} catch (TransformException e1) {
			e1.printStackTrace();
		}
	    return distance;
	}
	
}