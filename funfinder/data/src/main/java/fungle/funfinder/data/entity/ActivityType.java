package fungle.funfinder.data.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.SimpleIDGenerator;
import fungle.funfinder.data.FeelingVector;

/**
 * FunCategory are timeless, they are not a specific event. For example, baking,
 * running, watching movies, visiting bars, and snorkeling are all FunCategory.
 * 
 * @author cloudera
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(propOrder={"byteId","name","feeling"})
public class ActivityType implements ByteIdentifiable,Comparable<ActivityType> {
	private ByteId byteId;
	private User creator;
	private String name;
	private FeelingVector feeling;

	public ActivityType() {
	}

	public ActivityType(String name) {
		this.name = name;
	}

	public ActivityType(ActivityType other) {
		if (other!=null) {
			this.name = other.name;
			this.feeling = other.getFeeling()==null? null : new FeelingVector(other.getFeeling());
			this.byteId = other.byteId;
		}
	}

	public ActivityType(String name, FeelingVector feelingVector) {
		this.name = name;
		this.feeling = feelingVector;
	}
	
	public String toString() {
		return name==null?"":name;
	}

	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof ActivityType)) {
			return false;
		}
		ActivityType that = (ActivityType) o;
		return new EqualsBuilder().append(this.name, that.name)
				.append(this.feeling, that.feeling).isEquals();
	}

	public int hashCode() {
		return new HashCodeBuilder().append(this.name).append(feeling)
				.toHashCode();
	}
	
	public ActivityType defaultId() {
		this.byteId = SimpleIDGenerator.getInstance(ActivityType.class).nextBytes();
		return this;
	}

	public ActivityType id(ByteId id) {
		this.byteId = id;
		return this;
	}

	public ActivityType feeling(FeelingVector feeling) {
		this.feeling = feeling;
		return this;
	}

	@Override
	public ByteId getByteId() {
		return byteId;
	}

	@Override
	public void setByteId(ByteId id) {
		this.byteId = id;
	}
	
	public ActivityType name(String name) {
		this.name = name;
		return this;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public FeelingVector getFeeling() {
		return feeling;
	}

	public void setFeeling(FeelingVector feeling) {
		this.feeling = feeling;
	}

	@Override
	public int compareTo(ActivityType o) {
		if (o == null) {
			return -1;
		}
		if (this.name==null) {
			return (o.getName() == null) ? 0 : 1; 
		} else if (o.name == null) {
			return -1;
		}
		return this.name.compareTo(o.getName());
	}

	@Override
	public void setByteId(byte[] id) {
		this.byteId=new ByteId(id);
	}

	@XmlTransient
	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}
}