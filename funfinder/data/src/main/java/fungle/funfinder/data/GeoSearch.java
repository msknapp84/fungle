package fungle.funfinder.data;

import java.math.BigDecimal;
import java.util.List;

import fungle.funfinder.data.geo.GeoCoordinate;
import fungle.funfinder.data.geo.GeoFinder;
import fungle.funfinder.data.geo.GeoUnits;

public class GeoSearch {
	private final GeoCoordinate coordinate;
	private final BigDecimal distance;
	private final GeoUnits units;
	
	public GeoSearch(GeoCoordinate coordinate,BigDecimal distance,GeoUnits units) {
		this.coordinate = coordinate;
		this.distance = distance;
		this.units = units;
	}

	public GeoCoordinate getCoordinate() {
		return coordinate;
	}

	public BigDecimal getDistance() {
		return distance;
	}

	public GeoUnits getUnits() {
		return units;
	}
	
//	public List<Integer> getMapKeys(GeoFinder finder) {
//		return finder.getRectangleIndexesNear(coordinate, distance, units);
//	}
}