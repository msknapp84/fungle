package fungle.funfinder.data.dao;

import fungle.common.hdao.ByteIdentifiableDAO;
import fungle.funfinder.data.entity.UserEvent;

public interface UserEventDAO extends ByteIdentifiableDAO<UserEvent> {

}
