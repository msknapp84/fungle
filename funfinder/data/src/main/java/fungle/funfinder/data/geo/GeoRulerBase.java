package fungle.funfinder.data.geo;

import java.math.BigDecimal;
import java.math.RoundingMode;

public abstract class GeoRulerBase implements GeoRuler {
	private final BigDecimal METERS_PER_MILE = new BigDecimal(1609.344);
	
	@Override
	public BigDecimal getDistance(GeoCoordinate first, GeoCoordinate second,
			GeoUnits units) {
		double[] coord1 = new double[] { first.getLatitude().doubleValue(),
				first.getLongitude().doubleValue() };
		double[] coord2 = new double[] { second.getLatitude().doubleValue(),
				second.getLongitude().doubleValue() };
		BigDecimal dist = new BigDecimal(measureDistanceInMeters(coord1, coord2));
		if (units==GeoUnits.METERS) {
			return dist;
		} else if (units == GeoUnits.KILOMETERS) {
			return dist.divide(new BigDecimal(1000),RoundingMode.HALF_UP);
		} else if (units == GeoUnits.MILES) {
			return dist.divide(METERS_PER_MILE,RoundingMode.HALF_UP);
		} else {
			throw new IllegalArgumentException("No support for degrees or other units at this time.");
		}
		
	}

	public abstract double measureDistanceInMeters(double[] coord1, double[] coord2);

	@Override
	public GeoCoordinate getCoordinate(GeoCoordinate base, BigDecimal distance,
			GeoUnits units, GeoDirection direction) {
		// I don't know of any function in geotools for this, so 
		// we are going to make an estimate.  This is sort of 
		// a hacky mathematical way to do things.
		BigDecimal deltaDegrees = new BigDecimal("0.001");
		GeoCoordinate other = shift(base,deltaDegrees,direction);
		BigDecimal deltaInUnits = getDistance(base, other, units);
		BigDecimal shiftedDegrees = distance.multiply(deltaDegrees).divide(deltaInUnits,RoundingMode.HALF_DOWN);
		GeoCoordinate coordinate = shift(base,shiftedDegrees,direction);
		return coordinate;
	}
	
	protected GeoCoordinate shift(GeoCoordinate base,BigDecimal shiftedDegrees,GeoDirection direction) {
		GeoCoordinate coordinate = new GeoCoordinate(base);
		if (GeoDirection.WEST == direction) {
			coordinate.setLongitude(coordinate.getLongitude().subtract(shiftedDegrees));
		} else if (GeoDirection.EAST == direction) {
			coordinate.setLongitude(coordinate.getLongitude().add(shiftedDegrees));
		} else if (GeoDirection.NORTH == direction) {
			coordinate.setLatitude(coordinate.getLatitude().add(shiftedDegrees));
		} else if (GeoDirection.SOUTH == direction) {
			coordinate.setLatitude(coordinate.getLatitude().subtract(shiftedDegrees));
		}
		return coordinate;
	}
}