package fungle.funfinder.data.geo;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.HashCodeBuilder;

import fungle.common.hdao.util.HBaseByteConverter;
import fungle.funfinder.data.util.GeoCoordinateByteConverter;

@XmlType(name="coordinate")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class GeoCoordinate {
	// here to make test writing easier.
	public static final GeoCoordinate DC = new GeoCoordinate(38.8951, -77.0367);
	public static final GeoCoordinate NEWYORK = new GeoCoordinate(40.7127,
			-74.0059);

	private static final double threshold = 1E-6;//0.000001;
	
	static {
		HBaseByteConverter.addConverter(GeoCoordinate.class, new GeoCoordinateByteConverter());
	}
	
	private BigDecimal latitude;
	
	private BigDecimal longitude;


	public GeoCoordinate() {
		latitude = new BigDecimal(0);
		longitude = new BigDecimal(0);
	}

	public GeoCoordinate(double latitude, double longitude) {
		this.latitude = new BigDecimal(latitude);
		this.longitude = new BigDecimal(longitude);
		validate();
	}

	public GeoCoordinate(BigDecimal latitude, BigDecimal longitude) {
		// BigDecimal instances are immutable, no need to make a defensive copy.
		this.latitude = latitude;
		this.longitude = longitude;
		validate();
	}

	public GeoCoordinate(GeoCoordinate base) {
		this.latitude = new BigDecimal(base.getLatitude().toString());
		this.longitude = new BigDecimal(base.getLongitude().toString());
		validate();
	}
	
	private void validate() {
		if (-180>this.longitude.doubleValue() || 180<this.longitude.doubleValue()) {
			throw new IllegalArgumentException("longitude is out of range: "+this.longitude.doubleValue());
		}
		if (-90>this.latitude.doubleValue() || 90<this.latitude.doubleValue()) {
			throw new IllegalArgumentException("latitude is out of range: "+this.latitude.doubleValue());
		}
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
		validate();
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
		validate();
	}

	public int hashCode() {
		return new HashCodeBuilder().append(this.latitude)
				.append(this.longitude).toHashCode();
	}
	
	public String toString() {
		return latitude+", "+longitude;
	}

	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof GeoCoordinate)) {
			return false;
		}
		GeoCoordinate that = (GeoCoordinate) o;
		return doubleEquals(this.latitude, that.latitude)
				&& doubleEquals(this.longitude, that.longitude);
	}

	private boolean doubleEquals(BigDecimal v1, BigDecimal v2) {
		return doubleEquals(v1.doubleValue(), v2.doubleValue());
	}

	private boolean doubleEquals(double v1, double v2) {
		return v1 - threshold < v2 && v1 + threshold > v2;
	}
}