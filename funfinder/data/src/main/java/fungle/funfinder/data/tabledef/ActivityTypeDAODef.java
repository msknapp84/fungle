package fungle.funfinder.data.tabledef;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.hadoop.hbase.client.HTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fungle.common.hdao.BaseDAODefinition;
import fungle.common.hdao.ByteIdentifiableDAO;
import fungle.common.hdao.model.BasicIndex;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.Column;
import fungle.common.hdao.model.EntityReference;
import fungle.common.hdao.model.Index;
import fungle.common.hdao.util.TableNameService;
import fungle.funfinder.data.dao.ActivityDAO;
import fungle.funfinder.data.dao.UserDAO;
import fungle.funfinder.data.def.ActivityTypeDef;
import fungle.funfinder.data.entity.ActivityType;

@Component("activityTypeDAODef")
public class ActivityTypeDAODef extends BaseDAODefinition<ActivityType> {

	private UserDAO userDAO;
	private ActivityDAO activityDAO;

	private Index<ActivityType> nameIndex;
	private Index<ActivityType> userIndex;
	
	@Autowired
	public ActivityTypeDAODef(TableNameService tableNameService) {
		super(new ActivityTypeDef(), tableNameService);
	}

	@Override
	public List<Index<ActivityType>> buildIndexes() {
		HTable nameIndexTable = tableNameService.get(this)
				.ensureIndexTable("name");
		HTable userIndexTable = tableNameService.get(this)
				.ensureIndexTable("creator");
		nameIndex = BasicIndex.builder().ascending("name")
				.referenceTable(getTable()).indexTable(nameIndexTable).build();
		userIndex = BasicIndex.builder().ascending("creator")
				.referenceTable(getTable()).indexTable(userIndexTable).build();
		return Arrays.asList(nameIndex,userIndex);
	}

	@Override
	public BidiMap<String, Column> buildMapping() {
		BidiMap<String, Column> mapping = new DualHashBidiMap<String, Column>();
		mapping = new DualHashBidiMap<String, Column>();
		mapping.put("name", Column.from("f", "n", false));
		mapping.put("feeling", Column.from("f", "f", false));
		mapping.put("creator", Column.from("f", "u", false));
		return mapping;
	}

	@Override
	public <X extends ByteIdentifiable> ByteIdentifiableDAO<X> getIncomingDAO(
			EntityReference<X, ActivityType> reference) {
		// only one thing refers to activity types.
		return (ByteIdentifiableDAO<X>) activityDAO;
	}

	@Override
	public Index<ActivityType> getIndexForReference(
			EntityReference<ActivityType, ?> reference) {
		return userIndex;
	}

	public ActivityDAO getActivityDAO() {
		return activityDAO;
	}

	@Autowired
	public void setActivityDAO(ActivityDAO activityDAO) {
		this.activityDAO = activityDAO;
	}

	@Override
	public <X extends ByteIdentifiable> ByteIdentifiableDAO<X> getOutgoingDAO(
			EntityReference<ActivityType, X> reference) {
		// only thing this refers to is the user
		return (ByteIdentifiableDAO<X>) userDAO;
	}

	public UserDAO getUserDAO() {
		return userDAO;
	}

	@Autowired
	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@Override
	public Index<ActivityType> getIndexByFieldName(String... fieldNames) {
		getIndexes();
		String f = fieldNames[0];
		if ("name".equals(f)) {
			return nameIndex;
		} else if ("user".equals(f) || "creator".equals(f)) {
			return userIndex;
		}
		return null;
	}
}