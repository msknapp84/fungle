package fungle.funfinder.data.entity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.SimpleIDGenerator;
import fungle.common.hdao.util.HBaseByteConverter;
import fungle.funfinder.data.util.AddressConverter;

@XmlType(name="address",propOrder={"byteId","number","street1","street2","city","state","zip","subZip","country"})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Address implements ByteIdentifiable {
	private static final String FIRST_LINE_PATTERN = "(\\d+)\\s+(.+)";
	private static final String LAST_LINE_PATTERN = "([^,]+),\\s+(\\w+)\\s+([0-9\\-]+)";
	private ByteId byteId;
	private int number;
	private String street1, street2;
	private String city;
	private String state;
	private String country;
	private int zip;
	private int subZip;
	
	static {
		HBaseByteConverter.addConverter(Address.class, new AddressConverter());
	}

	public Address() {

	}

	public Address(Address other) {
		this.number = other.number;
		this.street1 = other.street1;
		this.street2 = other.street2;
		this.city = other.city;
		this.state = other.state;
		this.zip = other.zip;
		this.subZip = other.subZip;
		this.country = other.country;
	}

	public Address(int number, String street, String city, String state, int zip) {
		this.number = number;
		this.street1 = street;
		this.street2 = "";
		this.city = city;
		this.state = state;
		this.zip = zip;
	}
	
	public static Address US(String street1Line,String cityStateLine) {
		return US(street1Line,null,cityStateLine);
	}

	public static Address US(String street1Line,String street2Line,String cityStateLine) {
		return newInstance(street1Line,street2Line,cityStateLine,"United States of America");
	}

	public static Address foreign(String street1Line,String cityStateLine,String countryLine) {
		return newInstance(street1Line,null,cityStateLine,countryLine);
	}

	public static Address newInstance(String street1Line,String street2Line,String cityStateLine,String countryLine) {
		Address address = new Address();
		if (!street1Line.matches(FIRST_LINE_PATTERN)) {
			throw new IllegalArgumentException("Invalid address format");
		}
		Pattern p = Pattern.compile(FIRST_LINE_PATTERN);
		Matcher m = p.matcher(street1Line);
		if (m.matches()) {
			address.number = Integer.parseInt(m.group(1));
			address.street1 = m.group(2);
		}
		address.street2 = street2Line;
		Pattern lp = Pattern.compile(LAST_LINE_PATTERN);
		Matcher lm = lp.matcher(cityStateLine);
		if (lm.matches()) {
			address.city = lm.group(1);
			address.state = lm.group(2);
			String z = lm.group(3);
			if (z.contains("-")) {
				String[] ps = z.split("-");
				address.zip = Integer.parseInt(ps[0]);
				address.setSubZip(Integer.parseInt(ps[1]));
			} else {
				address.zip = Integer.parseInt(z);
			}
		} else {
			throw new IllegalArgumentException("Invalid address format");
		}
		address.country = countryLine;
		return address;
	}
	
	public Address defaultId() {
		this.byteId = SimpleIDGenerator.getInstance(Address.class).nextBytes();
		return this;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getStreet1() {
		return street1;
	}

	public void setStreet1(String street1) {
		this.street1 = street1;
	}

	public String getStreet2() {
		return street2;
	}

	public void setStreet2(String street2) {
		this.street2 = street2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getZip() {
		return zip;
	}

	public void setZip(int zip) {
		this.zip = zip;
	}

	@Override
	public ByteId getByteId() {
		return byteId;
	}

	@Override
	public void setByteId(ByteId id) {
		this.byteId = id;
	}

	public int getSubZip() {
		return subZip;
	}

	public void setSubZip(int subZip) {
		this.subZip = subZip;
	}

	public Address subZip(int subZip) {
		this.subZip = subZip;
		return this;
	}

	public Address zip(int zip) {
		this.zip = zip;
		return this;
	}

	public Address id(ByteId id) {
		this.byteId =id;
		return this;
	}

	public Address street1(String street1) {
		this.street1 = street1;
		return this;
	}

	public Address street2(String street2) {
		this.street2 = street2;
		return this;
	}

	public Address city(String city) {
		this.city = city;
		return this;
	}

	public Address country(String country) {
		this.country = country;
		return this;
	}

	public Address state(String state) {
		this.state = state;
		return this;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof Address)) {
			return false;
		}
		Address that = (Address) o;
		EqualsBuilder eb = new EqualsBuilder();
		// let null and an empty string be equivalent
		String thisS2 = "".equals(this.street2)? null:this.street2;
		String thatS2 = "".equals(that.street2)? null:that.street2;
		eb.append(this.number, that.number).append(this.street1, that.street1)
				.append(thisS2,thatS2)
				.append(this.city, that.city).append(this.state, that.state)
				.append(this.country, that.country)
				.append(this.zip, that.zip).append(this.subZip, that.subZip);

		return eb.isEquals();
	}
	
	public boolean equalId(Address o) {
		return o.getByteId().equals(byteId);
	}

	public boolean completelyEqual(Address o) {
		return o.getByteId().equals(byteId) && equals(o);
	}
	
	public int hashCode() {
		HashCodeBuilder hcb = new HashCodeBuilder();
		hcb.append(number).append(street1).append(street2).append(city).append(state).append(zip).append(subZip);
		return hcb.toHashCode();
	}

	public String toString() {
		return String.format("%d %s%n%s, %s %d", number, street1, city, state,
				zip);
	}

	@Override
	public void setByteId(byte[] id) {
		this.byteId=new ByteId(id);
	}
}
