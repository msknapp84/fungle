package fungle.funfinder.data.def;

import java.util.Collections;
import java.util.Set;

import fungle.common.hdao.BaseEntityDefinition;
import fungle.common.hdao.model.BasicEntityReference;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.EntityDefinition;
import fungle.common.hdao.model.EntityReference;
import fungle.common.hdao.model.EntityReferenceConverter;
import fungle.funfinder.data.entity.Place;
import fungle.funfinder.data.entity.User;

public class PlaceDef extends BaseEntityDefinition<Place> implements EntityDefinition<Place> {
	// outgoing ref to user.
	public static final EntityReference<Place, User> PLACEUSERREF= BasicEntityReference.<Place,User>builder()
			.from(Place.class).to(User.class).manyToOne().notRequired().converter(new PlaceUserConverter()).build();
	
	public PlaceDef() {
		super(Place.class);
	}

	@Override
	public Set<EntityReference<?, Place>> getIncomingReferences() {
		return Collections.singleton(ActivityDef.PLACEREF);
	}

	@Override
	public Set<EntityReference<Place, ?>> getOutgoingReferences() {
		return Collections.singleton(PLACEUSERREF);
	}
	
	private static final class PlaceUserConverter implements EntityReferenceConverter<Place, User> {

		@Override
		public User getForeignEntity(Place f) {
			return f.getCreator();
		}

		@Override
		public void setForeignEntity(Place f, ByteIdentifiable t) {
			f.setCreator((User) t);
		}

		@Override
		public User newForeignInstance() {
			return new User();
		}

		@Override
		public Place newLocalInstance() {
			return new Place();
		}
	}
}