package fungle.funfinder.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import fungle.common.hdao.util.HBaseByteConverter;
import fungle.funfinder.data.util.FeelingVectorConverter;

@XmlType(name="feeling",propOrder={"outdoorsy","expenseRating","riskRating","recreationRating","socialRating","timeConsumptionRating"})
@XmlAccessorType(XmlAccessType.PROPERTY)
public class FeelingVector {
	private static final byte MINVALUE = 1;
	private static final byte MAXVALUE = 10;

	static {
		HBaseByteConverter.addConverter(FeelingVector.class,
				new FeelingVectorConverter());
	}

	/**
	 * True if it is typically done outdoors, more than half the time.
	 */
	private boolean outdoorsy;

	/**
	 * A rating of how expensive this tends to be.
	 */
	private byte expenseRating=5;

	/**
	 * A rating of how risky this tends to be.
	 */
	private byte riskRating=5;

	/**
	 * A rating of how recreational this activity is.
	 */
	private byte recreationRating=5;

	/**
	 * Records a rating of how social this tends to be.
	 */
	private byte socialRating=5;

	/**
	 * Records a rating of how time consuming this tends to be.
	 */
	private byte timeConsumptionRating=5;

	public FeelingVector() {

	}

	public FeelingVector(FeelingVector other) {
		this.expenseRating = other.expenseRating;
		this.recreationRating = other.recreationRating;
		this.outdoorsy = other.outdoorsy;
		this.riskRating = other.riskRating;
		this.socialRating = other.socialRating;
		this.timeConsumptionRating = other.timeConsumptionRating;
		validate();
	}

	private void validate() {
		validate(expenseRating);
		validate(recreationRating);
		validate(riskRating);
		validate(socialRating);
		validate(timeConsumptionRating);
	}

	private void validate(int b) {
		if (b < MINVALUE || b > MAXVALUE) {
			throw new IllegalArgumentException(String.format(
					"The value %d is out of range.  Min=%d, Max=%d", b,
					MINVALUE, MAXVALUE));
		}
	}

	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof FeelingVector)) {
			return false;
		}
		FeelingVector that = (FeelingVector) o;
		EqualsBuilder eb = new EqualsBuilder();
		eb.append(this.expenseRating, that.expenseRating)
				.append(this.recreationRating, that.recreationRating)
				.append(this.outdoorsy, that.outdoorsy)
				.append(this.riskRating, that.riskRating)
				.append(this.socialRating, that.socialRating)
				.append(this.timeConsumptionRating, that.timeConsumptionRating);
		return eb.isEquals();
	}

	public int hashCode() {
		return new HashCodeBuilder().append(this.expenseRating)
				.append(recreationRating).append(outdoorsy).append(riskRating)
				.append(socialRating).append(timeConsumptionRating)
				.toHashCode();
	}

	public FeelingVector timeConsumptionRating(int timeConsumptionRating) {
		validate(timeConsumptionRating);
		this.timeConsumptionRating = (byte)timeConsumptionRating;
		return this;
	}

	public FeelingVector recreationRating(int recreationRating) {
		validate(recreationRating);
		this.recreationRating =(byte) recreationRating;
		return this;
	}

	public FeelingVector outdoorsy(boolean outdoorsy) {
		this.outdoorsy = outdoorsy;
		return this;
	}

	public FeelingVector socialRating(int socialRating) {
		validate((byte)socialRating);
		this.socialRating = (byte)socialRating;
		return this;
	}

	public FeelingVector riskRating(int riskRating) {
		validate((byte)riskRating);
		this.riskRating = (byte)riskRating;
		return this;
	}

	public FeelingVector expenseRating(int expenseRating) {
		validate((byte)expenseRating);
		this.expenseRating = (byte)expenseRating;
		return this;
	}

	public boolean isOutdoorsy() {
		return outdoorsy;
	}

	public void setOutdoorsy(boolean outdoorsy) {
		this.outdoorsy = outdoorsy;
	}

	public int getExpenseRating() {
		return expenseRating;
	}

	public void setExpenseRating(int expenseRating) {
		validate((byte)expenseRating);
		this.expenseRating = (byte)expenseRating;
	}

	public int getRiskRating() {
		return riskRating;
	}

	public void setRiskRating(int riskRating) {
		validate((byte)riskRating);
		this.riskRating = (byte)riskRating;
	}

	public int getRecreationRating() {
		return recreationRating;
	}

	public void setRecreationRating(int recreationRating) {
		validate((byte)recreationRating);
		this.recreationRating = (byte)recreationRating;
	}

	public int getSocialRating() {
		return socialRating;
	}

	public void setSocialRating(int socialRating) {
		validate((byte)socialRating);
		this.socialRating = (byte)socialRating;
	}

	public int getTimeConsumptionRating() {
		return timeConsumptionRating;
	}

	public void setTimeConsumptionRating(int timeConsumptionRating) {
		validate((byte) timeConsumptionRating);
		this.timeConsumptionRating = (byte)timeConsumptionRating;
	}
}