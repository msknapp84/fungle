package fungle.funfinder.data.dao;

import java.util.Calendar;
import java.util.Set;

import fungle.common.hdao.ByteIdentifiableDAO;
import fungle.funfinder.data.entity.Address;
import fungle.funfinder.data.entity.User;

public interface UserDAO extends ByteIdentifiableDAO<User> {
	User getByUserName(String userName);
	User getByEmail(String email);
	Set<User> getByName(String lastName,String firstName);
	User getByNameAndDOB(String lastName,String firstName,Calendar dob);
	User getByNameAndAddress(String lastName,String firstName,Address address);
}