package fungle.funfinder.data.entity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class CalendarAdapter extends XmlAdapter<String, Calendar> {
	private static final String FORMAT = "yyyy-MM-dd HH:mm";

	@Override
	public Calendar unmarshal(String v) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat(FORMAT);
		Date d = sdf.parse(v);
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		return c;
	}

	@Override
	public String marshal(Calendar v) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat(FORMAT);
		return v==null?null:sdf.format(v.getTime());
	}

}
