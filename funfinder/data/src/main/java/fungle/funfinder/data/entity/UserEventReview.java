package fungle.funfinder.data.entity;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.funfinder.data.FeelingVector;

public class UserEventReview implements ByteIdentifiable {
	private ByteId byteId;
	private UserEvent userEvent;
	private byte overallRating;
	private FeelingVector instilledFeelings;
	private String review;

	public UserEventReview() {
	}

	public UserEventReview(UserEvent event) {
		this.userEvent = event;
	}

	public UserEventReview(UserEvent event, FeelingVector feeling) {
		this.userEvent = event;
		this.instilledFeelings = feeling;
	}

	public UserEventReview(UserEvent event, FeelingVector feeling,
			byte overallRating) {
		this.userEvent = event;
		this.instilledFeelings = feeling;
		this.overallRating = overallRating;
	}

	public UserEventReview(UserEventReview other) {
		this.userEvent = new UserEvent(other.getUserEvent());
		this.instilledFeelings = new FeelingVector(other.getInstilledFeelings());
		this.overallRating = other.getOverallRating();
		this.review = other.getReview();
	}

	@Override
	public void setByteId(byte[] id) {
		this.byteId=new ByteId(id);
	}

	public UserEventReview userEvent(UserEvent userEvent) {
		this.userEvent = userEvent;
		return this;
	}

	public UserEventReview overallRating(byte overallRating) {
		this.overallRating = overallRating;
		return this;
	}

	public UserEventReview instilledFeelings(FeelingVector instilledFeelings) {
		this.instilledFeelings = instilledFeelings;
		return this;
	}

	public UserEventReview id(ByteId id) {
		this.byteId =id;
		return this;
	}

	public UserEventReview review(String review) {
		this.review = review;
		return this;
	}
	
	public String toString() {
		return "Rating: "+overallRating+"\n"+userEvent.toString();
	}

	public int hashCode() {
		return new HashCodeBuilder().append(userEvent).append(overallRating)
				.append(instilledFeelings).append(review).toHashCode();
	}

	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof UserEventReview)) {
			return false;
		}
		UserEventReview that = (UserEventReview) o;
		return new EqualsBuilder().append(this.userEvent, that.userEvent)
				.append(this.overallRating, that.overallRating)
				.append(this.instilledFeelings, that.instilledFeelings)
				.append(this.review, that.review).isEquals();
	}

	public UserEvent getUserEvent() {
		return userEvent;
	}

	public void setUserEvent(UserEvent userEvent) {
		this.userEvent = userEvent;
	}

	public byte getOverallRating() {
		return overallRating;
	}

	public void setOverallRating(byte overallRating) {
		this.overallRating = overallRating;
	}

	public FeelingVector getInstilledFeelings() {
		return instilledFeelings;
	}

	public void setInstilledFeelings(FeelingVector instilledFeelings) {
		this.instilledFeelings = instilledFeelings;
	}

	public String getReview() {
		return review;
	}

	public void setReview(String review) {
		this.review = review;
	}

	@Override
	public ByteId getByteId() {
		return byteId;
	}

	@Override
	public void setByteId(ByteId id) {
		this.byteId =id;
	}
}