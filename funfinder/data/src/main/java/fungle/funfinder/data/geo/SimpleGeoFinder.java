package fungle.funfinder.data.geo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SimpleGeoFinder extends SimpleGeoGrid implements GeoFinder {
	private final GeoRuler ruler;
	
	public SimpleGeoFinder(GeoRuler ruler) {
		this.ruler = ruler;
	}
	
	public SimpleGeoFinder(GeoRuler ruler,int longitudeDivisor,int latitudeDivisor) {
		super(longitudeDivisor,latitudeDivisor);
		this.ruler = ruler;
	}
	
	@Override
	public Set<GeoRectangle> getRectanglesNear(GeoCoordinate coordinate,
			BigDecimal distance, GeoUnits units) {
		// we keep things simple and model it as a rectangle on the ground.
		Set<GeoRectangle> rectangles = new HashSet<>();
		Set<Integer> indexes = getRectangleIndexesNear(coordinate, distance, units);
		for (Integer index : indexes) {
			rectangles.add(getRectangle(index));
		}
		return rectangles;
	}

	@Override
	public Set<Integer> getRectangleIndexesNear(GeoCoordinate coordinate,
			BigDecimal distance, GeoUnits units) {
		// we keep things simple and model it as a rectangle on the ground.
		GeoCoordinate west = ruler.getCoordinate(coordinate, distance, units, GeoDirection.WEST);
		GeoCoordinate east = ruler.getCoordinate(coordinate, distance, units, GeoDirection.EAST);
		GeoCoordinate north = ruler.getCoordinate(coordinate, distance, units, GeoDirection.NORTH);
		GeoCoordinate south= ruler.getCoordinate(coordinate, distance, units, GeoDirection.SOUTH);
		int minLatIndex = this.getLatitudeIndex(south.getLatitude());
		int maxLatIndex = this.getLatitudeIndex(north.getLatitude());
		int minLongIndex = this.getLongitudeIndex(west.getLongitude());
		int maxLongIndex = this.getLongitudeIndex(east.getLongitude());
		Set<Integer> indexes = new HashSet<>();
		for (int lat = minLatIndex;lat<= maxLatIndex;lat++) {
			for (int lng = minLongIndex;lng<=maxLongIndex;lng++) {
				int i = this.getIndex(lat,lng);
				indexes.add(i);
			}
		}
		return indexes;
	}

	@Override
	public int getIndex(GeoCoordinate coordinate) {
		if (coordinate == null) {
			return -1;
		}
		int[] indexes = getIndexes(coordinate);
		return this.getIndex(indexes[0],indexes[1]);
	}
}