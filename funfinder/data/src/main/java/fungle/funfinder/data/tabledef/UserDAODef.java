package fungle.funfinder.data.tabledef;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fungle.common.hdao.BaseDAODefinition;
import fungle.common.hdao.ByteIdentifiableDAO;
import fungle.common.hdao.model.BasicIndex;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.Column;
import fungle.common.hdao.model.EntityReference;
import fungle.common.hdao.model.Index;
import fungle.common.hdao.util.TableNameService;
import fungle.funfinder.data.dao.ActivityTypeDAO;
import fungle.funfinder.data.dao.EventDAO;
import fungle.funfinder.data.dao.PlaceDAO;
import fungle.funfinder.data.def.ActivityTypeDef;
import fungle.funfinder.data.def.EventDef;
import fungle.funfinder.data.def.PlaceDef;
import fungle.funfinder.data.def.UserDef;
import fungle.funfinder.data.entity.User;
import fungle.funfinder.data.geo.GeoFinder;
import fungle.funfinder.data.geo.GeoIndex;

@Component("userDAODef")
public class UserDAODef extends BaseDAODefinition<User> {
	private Index<User> nameDOBIndex;
	private Index<User> nameAddressIndex;
	private Index<User> userNameIndex;
	private Index<User> emailIndex;
	private GeoIndex<User> coordinateIndex;
	private GeoFinder finder;

	private PlaceDAO placeDAO;
	private ActivityTypeDAO activityTypeDAO;
	private EventDAO eventDAO;

	@Autowired
	public UserDAODef(TableNameService tableNameService) {
		super(new UserDef(), tableNameService);
	}

	@Override
	public <X extends ByteIdentifiable> ByteIdentifiableDAO<X> getOutgoingDAO(
			EntityReference<User, X> reference) {
		return null;
	}

	@Override
	public <X extends ByteIdentifiable> ByteIdentifiableDAO<X> getIncomingDAO(
			EntityReference<X, User> reference) {
		if (reference == PlaceDef.PLACEUSERREF) {
			return (ByteIdentifiableDAO<X>) placeDAO;
		} else if (reference == ActivityTypeDef.USERREF) {
			return (ByteIdentifiableDAO<X>) activityTypeDAO;
		} else if (reference == EventDef.CREATORREF) {
			return (ByteIdentifiableDAO<X>) eventDAO;
		}
		return null;
	}

	@Override
	public Index<User> getIndexForReference(EntityReference<User, ?> reference) {
		// not reachable at the moment.
		throw new UnsupportedOperationException("user knows of no outgoing references.");
	}

	@Override
	public List<Index<User>> buildIndexes() {
		// index on last name, first name, date of birth, address
		nameDOBIndex = BasicIndex
				.builder()
				.indexTable(
						tableNameService.get(this).ensureIndexTable("name.dob"))
				.referenceTable(getTable()).ascending("lastName")
				.ascending("firstName").ascending("dateOfBirth").build();

		// index on last name, first name, address, date of birth
		nameAddressIndex = BasicIndex
				.builder()
				.indexTable(
						tableNameService.get(this).ensureIndexTable(
								"name.address")).referenceTable(getTable())
				.ascending("lastName").ascending("firstName")
				.ascending("address").build();
		
		userNameIndex = BasicIndex
				.builder()
				.indexTable(
						tableNameService.get(this).ensureIndexTable(
								"userName")).referenceTable(getTable())
				.ascending("userName").unique(true).build();
		
		emailIndex = BasicIndex
				.builder()
				.indexTable(
						tableNameService.get(this).ensureIndexTable(
								"email")).referenceTable(getTable())
				.ascending("email").unique(true).build();

		// index on coordinate
		coordinateIndex = GeoIndex
				.forTables(
						tableNameService.get(this).ensureIndexTable(
								"coordinate"),
						tableNameService.get(this).getTable())
				.coordinateFieldName("coordinate").finder(getFinder())
				.timeFieldName(null).build(User.class);

		return Arrays.asList(nameDOBIndex, nameAddressIndex, userNameIndex,emailIndex,coordinateIndex);
	}

	@Override
	public BidiMap<String, Column> buildMapping() {
		BidiMap<String, Column> mapping = new DualHashBidiMap<String, Column>();
		mapping.put("firstName", Column.from("f", "f"));
		mapping.put("lastName", Column.from("f", "l"));
		mapping.put("male", Column.from("f", "m"));
		mapping.put("dateOfBirth", Column.from("f", "b"));
		mapping.put("encryptedPassword", Column.from("f", "p"));
		mapping.put("address", Column.from("f", "a"));
		mapping.put("coordinate", Column.from("f", "c"));
		mapping.put("schedule", Column.from("f", "s"));
		mapping.put("reviews", Column.from("f", "r"));
		mapping.put("friends", Column.from("f", "x"));
		mapping.put("privacySettings", Column.from("f", "z"));
		mapping.put("email", Column.from("f", "e"));
		mapping.put("userName", Column.from("f", "u"));
		return mapping;
	}

	public GeoFinder getFinder() {
		return finder;
	}

	@Autowired
	public void setFinder(GeoFinder finder) {
		this.finder = finder;
	}

	public PlaceDAO getPlaceDAO() {
		return placeDAO;
	}

	@Autowired
	public void setPlaceDAO(PlaceDAO placeDAO) {
		this.placeDAO = placeDAO;
	}

	public ActivityTypeDAO getActivityTypeDAO() {
		return activityTypeDAO;
	}

	@Autowired
	public void setActivityTypeDAO(ActivityTypeDAO activityTypeDAO) {
		this.activityTypeDAO = activityTypeDAO;
	}

	public EventDAO getEventDAO() {
		return eventDAO;
	}

	@Autowired
	public void setEventDAO(EventDAO eventDAO) {
		this.eventDAO = eventDAO;
	}

	@Override
	public Index<User> getIndexByFieldName(String... fieldNames) {
		getIndexes();
		String f = fieldNames[0];
		if ("name".equals(f)) {
			if (fieldNames.length>1) {
				String f2 = fieldNames[1];
				if ("dob".equals(f2)) {
					return nameDOBIndex;
				} else {
					return nameAddressIndex;
				}
			}
			return nameDOBIndex;
		} else if ("userName".equals(f)) {
			return userNameIndex;
		} else if ("email".equals(f)) {
			return emailIndex;
		} else if ("coordinate".equals(f)) {
			return coordinateIndex;
		}
		return null;
	}
}