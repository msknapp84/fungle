package fungle.funfinder.data.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.SimpleIDGenerator;
import fungle.funfinder.data.geo.GeoCoordinate;

@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(propOrder={"byteId","lastName","firstName","email","userName","male","dateOfBirth","coordinate","address","privacySettings"})
public class User implements ByteIdentifiable,Comparable<User> {
	private ByteId byteId;
	private String firstName;
	private String lastName;
	private String email;
	private String userName;
	private boolean male;
	private Date dateOfBirth;
	private String encryptedPassword;
	private Address address;
	private GeoCoordinate coordinate;
	private UserSchedule schedule;
	private List<UserEventReview> reviews;
	private List<User> friends;
	private PrivacySettings privacySettings;

	public User(String firstName, String lastName, boolean male,
			Date dateOfBirth, String encryptedPassword) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.encryptedPassword = encryptedPassword;
		this.male = male;
		this.dateOfBirth = dateOfBirth;
	}

	public User(String firstName, String lastName, boolean male,
			Date dateOfBirth, String encryptedPassword, Address address) {
		this(firstName, lastName, male, dateOfBirth, encryptedPassword);
		this.address = address;
	}

	public User(User user) {
		this.byteId =user.getByteId();
		this.firstName = user.firstName;
		this.lastName = user.lastName;
		this.encryptedPassword = user.encryptedPassword;
		this.address = new Address(user.getAddress());
		this.schedule = new UserSchedule(user.getSchedule());
		this.male = user.male;
		this.dateOfBirth = new Date(user.getDateOfBirth().getTime());
		this.privacySettings = new PrivacySettings(user.getPrivacySettings());
		this.friends = new ArrayList<>(user.getFriends());
		this.reviews = new ArrayList<>(user.getReviews());
		this.email = user.email;
		this.userName=user.userName;
	}
	
	public User() {
		
	}
	
	public User defaultId() {
		this.byteId = SimpleIDGenerator.getInstance(User.class).nextBytes();
		return this;
	}

	@Override
	public void setByteId(byte[] id) {
		this.byteId=new ByteId(id);
	}

	@Override
	public ByteId getByteId() {
		return byteId;
	}

	@Override
	public void setByteId(ByteId id) {
		this.byteId =id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@XmlTransient
	public String getEncryptedPassword() {
		return encryptedPassword;
	}

	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@XmlTransient
	public UserSchedule getSchedule() {
		return schedule;
	}

	public void setSchedule(UserSchedule schedule) {
		this.schedule = schedule;
	}

	@XmlTransient
	public List<UserEventReview> getReviews() {
		return reviews;
	}

	public void setReviews(List<UserEventReview> reviews) {
		this.reviews = reviews;
	}

	public String toString() {
		return firstName + " " + lastName;
	}

	@XmlTransient
	public List<User> getFriends() {
		return friends;
	}

	public void setFriends(List<User> friends) {
		this.friends = friends;
	}

	public PrivacySettings getPrivacySettings() {
		return privacySettings;
	}

	public void setPrivacySettings(PrivacySettings privacySettings) {
		this.privacySettings = privacySettings;
	}

	public boolean isMale() {
		return male;
	}

	public void setMale(boolean male) {
		this.male = male;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public User firstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	public User encryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
		return this;
	}

	public User male(boolean male) {
		this.male = male;
		return this;
	}

	public User addReview(UserEventReview review) {
		if (this.reviews == null) {
			this.reviews = new ArrayList<>();
		}
		this.reviews.add(review);
		return this;
	}

	public User addFriend(User friend) {
		if (this.friends == null) {
			this.friends = new ArrayList<>();
		}
		this.friends.add(friend);
		return this;
	}

	public User privacySettings(PrivacySettings privacySettings) {
		this.privacySettings = privacySettings;
		return this;
	}

	public User dateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
		return this;
	}

	public User schedule(UserSchedule schedule) {
		this.schedule = schedule;
		return this;
	}

	public User address(Address address) {
		this.address = address;
		return this;
	}

	public User lastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	public int hashCode() {
		return new HashCodeBuilder().append(firstName).append(lastName)
				.append(male).append(dateOfBirth)
				.append(encryptedPassword).append(address).append(reviews)
				.append(friends).append(privacySettings)
				.append(userName).append(email)
				.append(schedule).toHashCode();
	}

	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (o == null || !(o instanceof User)) {
			return false;
		}
		User u = (User) o;
		return new EqualsBuilder().append(this.firstName, u.firstName)
				.append(this.lastName, u.lastName)
				.append(this.encryptedPassword, u.encryptedPassword)
				.append(this.address, u.address)
				.append(this.reviews, u.reviews)
				.append(this.male,u.male)
				.append(this.dateOfBirth,u.dateOfBirth)
				.append(this.friends, u.friends)
				.append(this.privacySettings,u.privacySettings)
				.append(this.email, u.email)
				.append(this.userName,u.userName)
				.append(this.schedule, u.schedule).isEquals();
	}

	@Override
	public int compareTo(User o) {
		if (this.lastName==null) {
			return o.lastName==null?0:1;
		} else if (o.lastName==null) {
			return -1;
		}
		return this.lastName.compareTo(o.lastName);
	}

	public GeoCoordinate getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(GeoCoordinate coordinate) {
		this.coordinate = coordinate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
