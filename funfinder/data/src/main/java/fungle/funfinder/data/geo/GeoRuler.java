package fungle.funfinder.data.geo;

import java.math.BigDecimal;

/**
 * Responsible for the difficult geographic math,
 * like measuring distance between two points, etc.
 * 
 * Probably implemented by GeoTools only.
 * @author cloudera
 *
 */
public interface GeoRuler {
	BigDecimal getDistance(GeoCoordinate first,GeoCoordinate second,GeoUnits units);
	GeoCoordinate getCoordinate(GeoCoordinate base,BigDecimal distance,GeoUnits units,GeoDirection direction);
}