package fungle.funfinder.data.dao.hbase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fungle.common.hdao.Fetch;
import fungle.common.hdao.HBaseDAOBase;
import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.Cascade;
import fungle.common.hdao.model.Index;
import fungle.common.hdao.model.OnError;
import fungle.common.hdao.util.FetchUtils;
import fungle.funfinder.data.GeoSearch;
import fungle.funfinder.data.TimeRange;
import fungle.funfinder.data.dao.EventDAO;
import fungle.funfinder.data.entity.Activity;
import fungle.funfinder.data.entity.ActivityType;
import fungle.funfinder.data.entity.Event;
import fungle.funfinder.data.entity.Place;
import fungle.funfinder.data.geo.GeoFinder;
import fungle.funfinder.data.geo.GeoRectangle;
import fungle.funfinder.data.tabledef.EventDAODef;

@Component("eventDAO")
public class EventHBaseDAO extends HBaseDAOBase<Event> implements EventDAO {
	private static final Logger logger = LoggerFactory.getLogger(EventHBaseDAO.class);	
	
	private GeoFinder finder;

	public EventHBaseDAO() {
	}

	@Autowired
	public void setDefinition(EventDAODef def) {
		this.daoDefinition=def;
		validate();
	}

	@Override
	public List<Event> getByActivityTypeNear(GeoSearch search,
			ActivityType activityType, TimeRange timeRange, Fetch fetch) {
		List<Event> events = getByActivityTypeNear(search, activityType, fetch);
		List<Event> accepted = new ArrayList<>();
		for (Event event : events) {
			if (timeRange.contains(event.getDateTime())) {
				accepted.add(event);
			}
		}
		return accepted;
	}

	@Override
	public List<Event> getByActivity(Activity activity, TimeRange timeRange,
			Fetch fetch) {
		List<Event> events = getByActivity(activity, fetch);
		List<Event> accepted = new ArrayList<>();
		for (Event event : events) {
			if (timeRange.contains(event.getDateTime())) {
				accepted.add(event);
			}
		}
		return accepted;
	}

	@Override
	public List<Event> getByActivityTypeNear(GeoSearch search,
			ActivityType activityType, Fetch fetch) {
		Index<Event> geoIndex = daoDefinition.getIndexByFieldName("coordinate");
		Set<GeoRectangle> rectangles = getFinder().getRectanglesNear(search.getCoordinate(), search.getDistance(), search.getUnits());
		List<Event> templates = new ArrayList<>();
		for (GeoRectangle rectangle : rectangles) {
			logger.debug(rectangle.toString());
			Place place = new Place().coordinate(rectangle.getCenter());
			Activity a = new Activity().place(place);
			Event template = new Event().activity(a);
			templates.add(template);
			if (rectangle.contains(search.getCoordinate())) {
				logger.debug("This guy contains your coordinate.  His index is {}",getFinder().getIndex(rectangle));
			}
		}
		logger.debug("Coordinate: "+search.getCoordinate());
		Set<ByteId> ids = getIdsUsingIndex(geoIndex, OnError.CONTINUE, templates);
		if (ids.isEmpty()) {
			return Collections.EMPTY_LIST;
		}
		
		Index<Event> typeIndex = daoDefinition.getIndexByFieldName("category");
		Activity a = new Activity().category(activityType);
		Event template = new Event().activity(a);
		Set<ByteId> ids2 = getIdsUsingIndex(typeIndex, OnError.CONTINUE, template);
		
		// index merging.
		ids.retainAll(ids2);
		
		Map<ByteId,Event> eventMap = get(ids, fetch);
		List<Event> events = new ArrayList<>(eventMap.values());
		return events;
	}

	@Override
	public List<Event> getByActivity(Activity activity, Fetch fetch) {
		Index<Event> typeIndex = daoDefinition.getIndexByFieldName("activity");
		Event template = new Event().activity(activity);
		Map<ByteId,Event> eventMap = getUsingIndex(typeIndex, fetch,OnError.CONTINUE, template);
		List<Event> events = new ArrayList<>(eventMap.values());
		events.removeAll(null);
		return events;
	}

	@Override
	public List<Event> getByPlace(Place place, Fetch fetch) {
		Index<Event> typeIndex = null;
		if (place.getByteId()==null) {
			// use the name.
			typeIndex = daoDefinition.getIndexByFieldName("placename");
		} else {
			typeIndex = daoDefinition.getIndexByFieldName("place");
		}
		Event template = new Event().activity(new Activity().place(place));
		Map<ByteId,Event> eventMap = getUsingIndex(typeIndex, fetch,OnError.CONTINUE, template);
		List<Event> events = new ArrayList<>(eventMap.values());
		return events;
	}

	@Override
	public void deleteByActivity(Activity activity, Cascade cascade) {
		// TODO rewrite
		List<Event> events = getByActivity(activity, FetchUtils.never());
		delete(events,cascade,OnError.CONTINUE);
	}

	@Override
	public void deleteByPlace(Place place, Cascade cascade) {
		// TODO rewrite
		List<Event> events = getByPlace(place, FetchUtils.never());
		delete(events,cascade,OnError.CONTINUE);
	}

	public GeoFinder getFinder() {
		return finder;
	}

	@Autowired
	public void setFinder(GeoFinder finder) {
		this.finder = finder;
	}
}