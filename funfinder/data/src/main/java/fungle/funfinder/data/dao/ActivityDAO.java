package fungle.funfinder.data.dao;

import java.util.Set;

import fungle.common.hdao.ByteIdentifiableDAO;
import fungle.common.hdao.Fetch;
import fungle.common.hdao.model.Cascade;
import fungle.funfinder.data.GeoSearch;
import fungle.funfinder.data.entity.Activity;
import fungle.funfinder.data.entity.ActivityType;
import fungle.funfinder.data.entity.Place;

public interface ActivityDAO extends ByteIdentifiableDAO<Activity> {
	Set<Activity> getByName(String name,Fetch fetch);
	void deleteByName(String name,Cascade cascade);
	Set<Activity> getByCategory(ActivityType category,Fetch fetch);
	void deleteByCategory(ActivityType category,Cascade cascade);
	Set<Activity> getByPlace(Place place,Fetch fetch);
	void deleteByPlace(Place place,Cascade cascade);
	Set<Activity> getByPlaceAndCategory(Place place,ActivityType category,Fetch fetch);
	void deleteByPlaceAndCategory(Place place,ActivityType category,Cascade cascade);
	Set<Activity> getInProximity(GeoSearch search,Fetch fetch);
	Set<Activity> getInProximity(GeoSearch search,ActivityType category,Fetch fetch);
}