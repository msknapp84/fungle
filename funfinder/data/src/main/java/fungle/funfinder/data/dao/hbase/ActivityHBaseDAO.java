package fungle.funfinder.data.dao.hbase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fungle.common.hdao.Fetch;
import fungle.common.hdao.HBaseDAOBase;
import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.Cascade;
import fungle.common.hdao.model.Index;
import fungle.common.hdao.model.OnError;
import fungle.common.hdao.util.FetchUtils;
import fungle.funfinder.data.GeoSearch;
import fungle.funfinder.data.dao.ActivityDAO;
import fungle.funfinder.data.entity.Activity;
import fungle.funfinder.data.entity.ActivityType;
import fungle.funfinder.data.entity.Place;
import fungle.funfinder.data.geo.GeoFinder;
import fungle.funfinder.data.geo.GeoRectangle;
import fungle.funfinder.data.tabledef.ActivityDAODef;

@Component("activityDAO")
public class ActivityHBaseDAO extends HBaseDAOBase<Activity> implements
		ActivityDAO {
	private static final Logger logger = LoggerFactory.getLogger(ActivityHBaseDAO.class);
	
	private GeoFinder finder;

	public ActivityHBaseDAO() {
	}

	@Override
	public Set<Activity> getByName(String name,Fetch fetch) {
		Activity template = new Activity();
		template.setName(name);
		Index<Activity> index = ((ActivityDAODef)getDefinition()).getNameIndex();
		Map<ByteId,Activity> activities = getUsingIndex(index,fetch, OnError.CONTINUE, template);
		if (CollectionUtils.isEmpty(activities.values())) {
			return null;
		}
		Set<Activity> res = new HashSet<>(activities.values());
		res.remove(null);
		return res;
	}
	
	@Autowired
	public void setDefinition(ActivityDAODef def) {
		this.daoDefinition=def;
		validate();
	}

	@Override
	public void deleteByName(String name, Cascade cascade) {
		Index<Activity> nameIndex = daoDefinition.getIndexByFieldName("name");
		Activity template = new Activity();
		template.setName(name);
		deleteUsingIndex(nameIndex, cascade, OnError.CONTINUE, template);
	}

	@Override
	public Set<Activity> getByCategory(ActivityType category, Fetch fetch) {
		Index<Activity> categoryIndex = daoDefinition.getIndexByFieldName("category");
		Activity template = new Activity();
		template.setCategory(category);
		Map<ByteId,Activity> activityMap = getUsingIndex(categoryIndex, fetch, OnError.CONTINUE, template);
		Set<Activity> activities = new HashSet<>(activityMap.values());
		activities.remove(null);
		return activities;
	}

	@Override
	public void deleteByCategory(ActivityType category, Cascade cascade) {
		// TODO rewrite to be optimized
		Set<Activity> activities = getByCategory(category, FetchUtils.never());
		delete(activities, cascade, OnError.CONTINUE);
	}

	@Override
	public Set<Activity> getByPlace(Place place, Fetch fetch) {
		Index<Activity> categoryIndex = daoDefinition.getIndexByFieldName("place");
		Activity template = new Activity();
		template.setPlace(place);
		Map<ByteId,Activity> activityMap = getUsingIndex(categoryIndex, fetch, OnError.CONTINUE, template);
		Set<Activity> activities = new HashSet<>(activityMap.values());
		activities.remove(null);
		return activities;
	}

	@Override
	public void deleteByPlace(Place place, Cascade cascade) {
		// TODO rewrite to be optimized
		Set<Activity> activities = getByPlace(place, FetchUtils.never());
		delete(activities, cascade, OnError.CONTINUE);
	}

	@Override
	public Set<Activity> getInProximity(GeoSearch search, Fetch fetch) {
		Index<Activity> geoIndex = daoDefinition.getIndexByFieldName("coordinate");
		if (geoIndex==null) {
			logger.warn("Found no coordinate index for the activity HBase DAO.");
			return Collections.EMPTY_SET;
		}
		Set<GeoRectangle> rectangles = getFinder().getRectanglesNear(search.getCoordinate(), search.getDistance(), search.getUnits());
		List<Activity> templates = new ArrayList<>();
		for (GeoRectangle rectangle : rectangles) {
			Place place = new Place().coordinate(rectangle.getCenter());
			Activity template = new Activity().place(place);
			templates.add(template);
		}
		Map<ByteId,Activity> activities = getUsingIndex(geoIndex, fetch, OnError.CONTINUE, templates);
		Set<Activity> acts = new HashSet<>(activities.values());
		acts.remove(null);
		return acts;
	}

	@Override
	public Set<Activity> getInProximity(GeoSearch search,
			ActivityType category, Fetch fetch) {
		Index<Activity> geoIndex = daoDefinition.getIndexByFieldName("coordinate");
		if (geoIndex==null) {
			logger.warn("Found no coordinate index for the activity HBase DAO.");
			return Collections.EMPTY_SET;
		}
		Set<GeoRectangle> rectangles = getFinder().getRectanglesNear(search.getCoordinate(), search.getDistance(), search.getUnits());
		List<Activity> templates = new ArrayList<>();
		for (GeoRectangle rectangle : rectangles) {
			Place place = new Place().coordinate(rectangle.getCenter());
			Activity template = new Activity().place(place);
			templates.add(template);
		}
		Set<ByteId> ids = getIdsUsingIndex(geoIndex, OnError.CONTINUE, templates);
		ids.remove(null);
		
		if (ids.isEmpty()) {
			return Collections.EMPTY_SET;
		}
		
		// now get the ids by category
		Index<Activity> categoryIndex = daoDefinition.getIndexByFieldName("category");
		Activity catTemplate = new Activity().category(category);
		Set<ByteId> catIds = getIdsUsingIndex(categoryIndex, OnError.CONTINUE, catTemplate);
		
		// this is index merging, we only keep ids that are found in both indexes.
		ids.retainAll(catIds);
		
		if (ids.isEmpty()) {
			return Collections.EMPTY_SET;
		}
		Map<ByteId,Activity> activities =  get(ids, fetch);
		Set<Activity> acts = new HashSet<>(activities.values());
		acts.remove(null);
		return acts;
	}

	@Override
	public Set<Activity> getByPlaceAndCategory(Place place,
			ActivityType category, Fetch fetch) {
		// get the ids by place
		Index<Activity> placeIndex = daoDefinition.getIndexByFieldName("place");
		Activity placeTemplate = new Activity().place(place);
		Set<ByteId> ids = getIdsUsingIndex(placeIndex, OnError.CONTINUE, placeTemplate);

		if (ids.isEmpty()) {
			return Collections.EMPTY_SET;
		}

		// now get the ids by category
		Index<Activity> categoryIndex = daoDefinition.getIndexByFieldName("category");
		Activity catTemplate = new Activity().category(category);
		Set<ByteId> catIds = getIdsUsingIndex(categoryIndex, OnError.CONTINUE, catTemplate);
		
		ids.retainAll(catIds);

		if (ids.isEmpty()) {
			return Collections.EMPTY_SET;
		}
		Map<ByteId,Activity> activities =  get(ids, fetch);
		Set<Activity> acts = new HashSet<>(activities.values());
		acts.remove(null);
		return acts;
	}

	@Override
	public void deleteByPlaceAndCategory(Place place, ActivityType category,
			Cascade cascade) {
		// TODO rewrite to be optimized
		Set<Activity> activities = getByPlaceAndCategory(place, category,FetchUtils.never());
		delete(activities, cascade, OnError.CONTINUE);
	}

	public GeoFinder getFinder() {
		return finder;
	}

	@Autowired
	public void setFinder(GeoFinder finder) {
		this.finder = finder;
	}
}