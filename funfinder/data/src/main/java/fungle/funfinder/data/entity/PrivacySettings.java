package fungle.funfinder.data.entity;

import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.ByteIdentifiable;

public class PrivacySettings  implements ByteIdentifiable {
	private ByteId byteId;
	
	public PrivacySettings() {
		
	}

	public PrivacySettings(PrivacySettings privacySettings) {
		this.byteId=privacySettings.getByteId();
	}

	public ByteId getByteId() {
		return byteId;
	}

	public void setByteId(ByteId id) {
		this.byteId =id;
	}
	
	public int hashCode() {
		return 0;
	}

	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!( o instanceof PrivacySettings)) {
			return false;
		}
		return true;
	}

	@Override
	public void setByteId(byte[] id) {
		this.byteId=new ByteId(id);
	}
}
