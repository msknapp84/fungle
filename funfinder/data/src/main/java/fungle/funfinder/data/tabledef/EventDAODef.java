package fungle.funfinder.data.tabledef;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.hadoop.hbase.client.HTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fungle.common.hdao.BaseDAODefinition;
import fungle.common.hdao.ByteIdentifiableDAO;
import fungle.common.hdao.model.BasicIndex;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.Column;
import fungle.common.hdao.model.EntityReference;
import fungle.common.hdao.model.Index;
import fungle.common.hdao.util.TableNameService;
import fungle.funfinder.data.dao.ActivityDAO;
import fungle.funfinder.data.dao.UserDAO;
import fungle.funfinder.data.dao.UserEventDAO;
import fungle.funfinder.data.def.EventDef;
import fungle.funfinder.data.entity.Event;
import fungle.funfinder.data.geo.GeoFinder;
import fungle.funfinder.data.geo.GeoIndex;

@Component("eventDAODef")
public class EventDAODef extends BaseDAODefinition<Event> {
	private UserEventDAO userEventDAO;
	private UserDAO userDAO;
	private ActivityDAO activityDAO;

	private Index<Event> creatorIndex;
	private Index<Event> activityIndex;
	private Index<Event> categoryIndex;
	private Index<Event> placeIndex;
	private Index<Event> placeNameIndex;
	private GeoIndex<Event> coordinateIndex;
	
	private GeoFinder finder;

	@Autowired
	public EventDAODef(TableNameService tableNameService) {
		super(new EventDef(), tableNameService);
	}

	@Override
	public <X extends ByteIdentifiable> ByteIdentifiableDAO<X> getOutgoingDAO(
			EntityReference<Event, X> reference) {
		if (reference == EventDef.ACTIVITYREF) {
			return (ByteIdentifiableDAO<X>) activityDAO;
		} else if (reference == EventDef.CREATORREF) {
			return (ByteIdentifiableDAO<X>) userDAO;
		}
		return null;
	}

	@Override
	public <X extends ByteIdentifiable> ByteIdentifiableDAO<X> getIncomingDAO(
			EntityReference<X, Event> reference) {
		// not possible at the moment.
		throw new UnsupportedOperationException();
	}

	@Override
	public Index<Event> getIndexForReference(EntityReference<Event, ?> reference) {
		if (reference == EventDef.ACTIVITYREF) {
			return activityIndex;
		} else if (reference == EventDef.CREATORREF) {
			return creatorIndex;
		}
		return null;
	}

	@Override
	public List<Index<Event>> buildIndexes() {
		// index on last name, first name, date of birth, address
		HTable myTable = tableNameService.get(this).ensureTable();
		creatorIndex = BasicIndex
				.builder()
				.indexTable(
						tableNameService.get(this).ensureIndexTable("creator"))
				.referenceTable(myTable).ascending("creator").build();

		// index on last name, first name, address, date of birth
		activityIndex = BasicIndex
				.builder()
				.indexTable(
						tableNameService.get(this).ensureIndexTable("activity"))
				.referenceTable(myTable).ascending("activity").build();

		// index on last name, first name, address, date of birth
		placeIndex = BasicIndex
				.builder()
				.indexTable(
						tableNameService.get(this).ensureIndexTable("place"))
				.referenceTable(myTable).ascending("activity.place").build();

		// index on last name, first name, address, date of birth
		placeNameIndex = BasicIndex
				.builder()
				.indexTable(
						tableNameService.get(this).ensureIndexTable("place"))
				.referenceTable(myTable).ascending("activity.place.name").build();

		// index on last name, first name, address, date of birth
		categoryIndex = BasicIndex
				.builder()
				.indexTable(
						tableNameService.get(this).ensureIndexTable("activity.type"))
				.referenceTable(myTable).ascending("activity.category").build();

		// index on coordinate
		coordinateIndex = GeoIndex
				.forTables(
						tableNameService.get(this).ensureIndexTable(
								"coordinate"), myTable).finder(getFinder())
				.coordinateFieldName("activity.place.coordinate")
				.timeFieldName("startDateTime").build(Event.class);

		return Arrays.asList(creatorIndex, activityIndex, coordinateIndex,placeIndex,placeNameIndex,categoryIndex);
	}

	@Override
	public BidiMap<String, Column> buildMapping() {
		BidiMap<String, Column> mapping = new DualHashBidiMap<String, Column>();
		mapping.put("creator", Column.from("f", "u"));
		mapping.put("activity", Column.from("f", "a"));
		mapping.put("startDateTime", Column.from("f", "s"));
		mapping.put("endDateTime", Column.from("f", "e"));
		mapping.put("description", Column.from("f", "d"));
		mapping.put("website", Column.from("f", "w"));
		mapping.put("price", Column.from("f", "p"));
		mapping.put("rsvpDeadline", Column.from("f", "r"));
		mapping.put("maxPeople", Column.from("f", "m"));
		return mapping;
	}

	public UserEventDAO getUserEventDAO() {
		return userEventDAO;
	}

//	@Autowired
	public void setUserEventDAO(UserEventDAO userEventDAO) {
		this.userEventDAO = userEventDAO;
	}

	public UserDAO getUserDAO() {
		return userDAO;
	}

	@Autowired
	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	public ActivityDAO getActivityDAO() {
		return activityDAO;
	}

	@Autowired
	public void setActivityDAO(ActivityDAO activityDAO) {
		this.activityDAO = activityDAO;
	}

	@Override
	public Index<Event> getIndexByFieldName(String... fieldNames) {
		getIndexes();
		String f = fieldNames[0].toLowerCase();
		if ("creator".equals(f) || "user".equals(f)) {
			return creatorIndex;
		} else if ("activity".equals(f) || "activity.name".equals(f)) {
			return activityIndex;
		} else if ("activitytype".equals(f) || "category".equals(f) || "type".equals(f) || "activity.category".equals(f)) {
			return categoryIndex;
		} else if ("place".equals(f) || "activity.place".equals(f)) {
			return placeIndex;
		} else if ("placename".equals(f) || "activity.place.name".equals(f)) {
			return placeNameIndex;
		} else if ("coordinate".equals(f)) {
			return coordinateIndex;
		}
		return null;		
	}

	public GeoFinder getFinder() {
		return finder;
	}

	@Autowired
	public void setFinder(GeoFinder finder) {
		this.finder = finder;
	}
}