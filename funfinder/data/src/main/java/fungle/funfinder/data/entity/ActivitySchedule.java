package fungle.funfinder.data.entity;

import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.SimpleIDGenerator;

public class ActivitySchedule implements ByteIdentifiable {
	
	private ByteId byteId;
	
	public ActivitySchedule() {
		
	}
	
	public ActivitySchedule(ActivitySchedule schedule) {
		// TODO Auto-generated constructor stub
	}

	@Override
	public ByteId getByteId() {
		return byteId;
	}

	@Override
	public void setByteId(ByteId id) {
		this.byteId = id;
	}

	@Override
	public void setByteId(byte[] id) {
		this.byteId=new ByteId(id);
	}
	
	public ActivitySchedule defaultId() {
		this.byteId = SimpleIDGenerator.getInstance(ActivitySchedule.class).nextBytes();
		return this;
	}

}