package fungle.funfinder.data.geo;

public enum GeoDirection {
	NORTH,
	SOUTH,
	EAST,
	WEST;
}