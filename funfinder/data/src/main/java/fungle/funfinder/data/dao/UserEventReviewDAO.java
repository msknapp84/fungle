package fungle.funfinder.data.dao;

import fungle.common.hdao.ByteIdentifiableDAO;
import fungle.funfinder.data.entity.UserEventReview;

public interface UserEventReviewDAO extends ByteIdentifiableDAO<UserEventReview> {

}