//package fungle.funfinder.data.dao.hbase;
//
//import org.apache.commons.collections4.BidiMap;
//import org.apache.commons.collections4.bidimap.DualHashBidiMap;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import fungle.common.hdao.SimpleHBaseDAO;
//import fungle.common.hdao.model.Column;
//import fungle.common.hdao.util.TableNameService;
//import fungle.funfinder.data.dao.UserEventReviewDAO;
//import fungle.funfinder.data.entity.UserEventReview;
//
//@Component("userEventReviewDAO")
//public class UserEventReviewHBaseDAO extends SimpleHBaseDAO<UserEventReview> implements UserEventReviewDAO {
//
//	private TableNameService tableNameService;
//	private static BidiMap<String, Column> mapping=buildMapping();
//
//	private static BidiMap<String, Column> buildMapping() {
//		mapping = new DualHashBidiMap<>();
//		return mapping;
//	}
//
//	@Autowired
//	public UserEventReviewHBaseDAO(TableNameService tableNameService) {
//		super(UserEventReview.class, tableNameService.get(UserEventReview.class).ensureTable(), mapping);
//		this.tableNameService=tableNameService;
//	}
//	
//
//
//}