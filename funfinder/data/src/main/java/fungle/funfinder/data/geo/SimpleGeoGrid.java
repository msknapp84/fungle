package fungle.funfinder.data.geo;

import java.math.BigDecimal;

/**
 * Splits the world into a grid where each rectangle has equal dimensions in
 * degrees, not miles.
 * 
 * @author cloudera
 *
 */
public class SimpleGeoGrid implements GeoGrid {
	private static final BigDecimal POSITIVE360 = new BigDecimal(360);
	private static final BigDecimal NEGATIVE180 = new BigDecimal(-180);
	private static final BigDecimal POSITIVE180 = new BigDecimal(180);
	private static final BigDecimal NEGATIVE90 = new BigDecimal(-90);

	private final int longitudeDivisor;
	private final int latitudeDivisor;

	public SimpleGeoGrid() {
		// default values give you rectangles that
		// are .001 degrees in each dimension. To give you
		// an idea for this size, a large city could easily
		// be .2 degrees wide, meaning this grid would have
		// 200*200=40000 rectangles for that city alone.
		this.longitudeDivisor = 36000;
		this.latitudeDivisor = 18000;
	}

	public SimpleGeoGrid(int longitudeDivisor, int latitudeDivisor) {
		this.longitudeDivisor = longitudeDivisor;
		this.latitudeDivisor = latitudeDivisor;
	}

	@Override
	public GeoRectangle getRectangle(GeoCoordinate coordinate) {
		// determine the longitude index.
		int[] latlong = getIndexes(coordinate);
		return getRectangle(latlong[0], latlong[1]);
	}

	@Override
	public int getLongitudeIndex(BigDecimal longitudeDegrees) {
		if (longitudeDegrees.doubleValue()>180 || longitudeDegrees.doubleValue()<-180) {
			throw new IllegalArgumentException("Invalid longitude degrees "+longitudeDegrees);
		}
		if (POSITIVE180.equals(longitudeDegrees)) {
			return 0;
		}
		BigDecimal bd = longitudeDegrees.subtract(NEGATIVE180)
				.multiply(new BigDecimal(longitudeDivisor)).divide(POSITIVE360);
		return (int) Math.floor(bd.doubleValue());
		// return (int)
		// Math.floor(longitudeDivisor*(longitudeDegrees-NEGATIVE180)/(POSITIVE360));
	}

	@Override
	public int getLatitudeIndex(BigDecimal latitudeDegrees) {
		BigDecimal bd = latitudeDegrees.subtract(NEGATIVE90)
				.multiply(new BigDecimal(latitudeDivisor)).divide(POSITIVE180);
		return (int) Math.min(latitudeDivisor-1, Math.floor(bd.doubleValue()));
	}

	@Override
	public int getIndex(GeoRectangle rectangle) {
		GeoCoordinate southwest = rectangle.getSouthWest();
		int[] latlong = getIndexes(southwest);
		return getIndex(latlong[0], latlong[1]);
	}

	public int[] getIndexes(GeoCoordinate coordinate) {
		if (coordinate == null) {
			return null;
		}
		return new int[] { getLatitudeIndex(coordinate.getLatitude()),
				getLongitudeIndex(coordinate.getLongitude()) };
	}

	@Override
	public int getMaximumRectangles() {
		return longitudeDivisor * latitudeDivisor;
	}

	@Override
	public GeoRectangle getRectangle(int latitudeIndex, int longitudeIndex) {
		validateIndexes(latitudeIndex,longitudeIndex);
		BigDecimal latitudeRectangleHeight = POSITIVE180.divide(new BigDecimal(
				latitudeDivisor));
		BigDecimal longitudeRectangleWidth = POSITIVE360.divide(new BigDecimal(
				longitudeDivisor));
		BigDecimal west = NEGATIVE180.add(
				longitudeRectangleWidth.multiply(new BigDecimal(longitudeIndex)));
		BigDecimal east = west.add(longitudeRectangleWidth);
		BigDecimal south = new BigDecimal(latitudeIndex)
				.multiply(latitudeRectangleHeight).add(NEGATIVE90);
		BigDecimal north = south.add(
				latitudeRectangleHeight);
		return new GeoRectangle(north, south, east, west);
	}

	private void validateIndexes(int latitudeIndex, int longitudeIndex) {
		if (latitudeIndex<0) {
			throw new IllegalArgumentException("latitude index must be greater than 0");
		}
		if (latitudeIndex>=latitudeDivisor) {
			throw new IllegalArgumentException("latitude index must be less than "+latitudeDivisor);
		}
		if (longitudeIndex<0) {
			throw new IllegalArgumentException("longitude index must be greater than 0");
		}
		if (longitudeIndex>=longitudeDivisor) {
			throw new IllegalArgumentException("longitude index must be less than "+longitudeDivisor);
		}
	}

	@Override
	public int getIndex(int latitudeIndex, int longitudeIndex) {
		// this is designed to read like a book, the first index is in the
		// north west corner at lat=90,long=-180, then it increases going east,
		// until it reaches long=180. Then the index proceeds with latitude
		// decreased by the height. So it basically reads like a book.
		int latitudeFactor = (latitudeDivisor - latitudeIndex-1);
		int product = (latitudeFactor * longitudeDivisor);
		int sum = longitudeIndex + product;
		return sum;
//		return Math.round(sum);
	}

	@Override
	public GeoRectangle getRectangle(int index) {
		int latitudeIndex = latitudeDivisor
				- ((int) Math.floor(index / longitudeDivisor))-1;
		int longitudeIndex = index % longitudeDivisor;
		return getRectangle(latitudeIndex, longitudeIndex);
	}
}