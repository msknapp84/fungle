package fungle.funfinder.data.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.SimpleIDGenerator;

/**
 * Activities are a combination of a place and a category of fun. For example,
 * snorkeling at cinnamon bay.
 * 
 * @author cloudera
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(propOrder={"byteId","name","description","website","category","place","schedule"})
public class Activity implements ByteIdentifiable {
	private ByteId byteId;
	private User creator;
	private ActivityType category;
	private String name;
	private Place place;
	private String description;
	private String website;
	private ActivitySchedule schedule;

	public Activity(ActivityType category, Place place) {
		this.category = category;
		this.place = place;
	}

	public Activity(ActivityType category, Place place, String name,
			String description, String website) {
		this(category, place);
		this.name = name;
		this.description = description;
		this.website = website;
	}

	public Activity(String name) {
		this.name = name;
	}

	public Activity(Activity activity) {
		this.byteId = activity.getByteId();
		this.name = activity.name;
		this.place = new Place(activity.getPlace());
		this.description = activity.description;
		this.website = activity.website;
		this.schedule = new ActivitySchedule(activity.schedule);
		this.category = new ActivityType(activity.category);
	}

	public Activity() {
		
	}
	
	public Activity defaultId() {
		this.byteId = SimpleIDGenerator.getInstance(Activity.class).nextBytes();
		return this;
	}

	@Override
	public ByteId getByteId() {
		return byteId;
	}

	@Override
	public void setByteId(ByteId byteId) {
		this.byteId = byteId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ActivityType getCategory() {
		return category;
	}

	public void setCategory(ActivityType category) {
		this.category = category;
	}

	public Place getPlace() {
		return place;
	}

	public void setPlace(Place place) {
		this.place = place;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public ActivitySchedule getSchedule() {
		return schedule;
	}

	public void setSchedule(ActivitySchedule schedule) {
		this.schedule = schedule;
	}

	public Activity name(String name) {
		this.name = name;
		return this;
	}

	public Activity place(Place place) {
		this.place = place;
		return this;
	}

	public Activity schedule(ActivitySchedule schedule) {
		this.schedule = schedule;
		return this;
	}

	public Activity website(String website) {
		this.website = website;
		return this;
	}

	public Activity description(String description) {
		this.description = description;
		return this;
	}

	public Activity category(ActivityType category) {
		this.category = category;
		return this;
	}

	public Activity byteId(ByteId byteId) {
		this.byteId = byteId;
		return this;
	}
	
	public String toString() {
		return String.format("%s: %s",name,description);
	}

	public boolean refEquals(Activity that) {
		if (that == null) {
			return false;
		}
		if (that == this) {
			return true;
		}
		ByteId thisCatId = this.category == null ? null : this.category.getByteId();
		ByteId thatCatId = that.schedule == null ? null : that.schedule.getByteId();
		ByteId thisSchId = this.schedule == null ? null : this.schedule.getByteId();
		ByteId thatSchId = that.schedule == null ? null : that.schedule.getByteId();
		ByteId thisPlaceId = this.place == null ? null : this.place.getByteId();
		ByteId thatPlaceId = that.place == null ? null : that.place.getByteId();
		EqualsBuilder eb = new EqualsBuilder();
		return eb.append(this.name, that.name)
				.append(this.description, that.description)
				.append(this.website, that.website)
				.append(thisCatId, thatCatId).append(thisSchId, thatSchId)
				.append(thisPlaceId, thatPlaceId) .isEquals();
	}

	public int hashCode() {
		HashCodeBuilder eb = new HashCodeBuilder();
		return eb.append(this.name).append(this.place)
				.append(this.description)
				.append(this.website)
				.append(this.category)
				.append(this.schedule).toHashCode();
	}

	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof Activity)) {
			return false;
		}
		Activity that = (Activity) o;
		EqualsBuilder eb = new EqualsBuilder();
		return eb.append(this.name, that.name).append(this.place, that.place)
				.append(this.description, that.description)
				.append(this.website, that.website)
				.append(this.category, that.category)
				.append(this.schedule, that.schedule).isEquals();
	}

	@Override
	public void setByteId(byte[] id) {
		this.byteId=new ByteId(id);
	}

	@XmlTransient
	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}

}
