package fungle.funfinder.data.entity;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.ByteIdentifiable;

/**
 * Records a user's preference for certain activities or activity types.
 * 
 * @author cloudera
 *
 */
public class UserActivityPreferences implements ByteIdentifiable {
	private ByteId byteId;

	/**
	 * A map from activity type and the user's preferences.
	 */
	private Map<ActivityType, Byte> activityTypeRatings;

	/**
	 * A map from specific activities and the user's rating of that.
	 */
	private Map<Activity, Byte> activityRatings;

	public UserActivityPreferences() {

	}

	public UserActivityPreferences(UserActivityPreferences other) {
		this.byteId =other.getByteId();
		this.activityRatings = new HashMap<>(other.activityRatings);
		this.activityTypeRatings = new HashMap<>(other.activityTypeRatings);
	}

	public UserActivityPreferences id(ByteId id) {
		this.byteId =id;
		return this;
	}

	public UserActivityPreferences addRating(ActivityType activity, byte rating) {
		if (this.activityTypeRatings == null) {
			this.activityTypeRatings = new HashMap<>();
		}
		this.activityTypeRatings.put(activity, rating);
		return this;
	}

	public UserActivityPreferences addRating(Activity activity, byte rating) {
		if (this.activityRatings == null) {
			this.activityRatings = new HashMap<>();
		}
		this.activityRatings.put(activity, rating);
		return this;
	}

	@Override
	public void setByteId(byte[] id) {
		this.byteId=new ByteId(id);
	}

	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof UserActivityPreferences)) {
			return false;
		}
		UserActivityPreferences that = (UserActivityPreferences) o;
		return new EqualsBuilder()
				.append(this.activityRatings, that.activityRatings)
				.append(this.activityTypeRatings, that.activityTypeRatings)
				.isEquals();
	}

	public int hashCode() {
		return new HashCodeBuilder().append(this.activityRatings)
				.append(this.activityTypeRatings).toHashCode();
	}

	public Map<ActivityType, Byte> getActivityTypeRatings() {
		return activityTypeRatings;
	}

	public void setActivityTypeRatings(
			Map<ActivityType, Byte> activityTypeRatings) {
		this.activityTypeRatings = activityTypeRatings;
	}

	public Map<Activity, Byte> getActivityRatings() {
		return activityRatings;
	}

	public void setActivityRatings(Map<Activity, Byte> activityRatings) {
		this.activityRatings = activityRatings;
	}

	public ByteId getByteId() {
		return byteId;
	}

	public void setByteId(ByteId id) {
		this.byteId =id;
	}
}
