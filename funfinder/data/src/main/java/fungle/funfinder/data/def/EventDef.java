package fungle.funfinder.data.def;

import java.util.Collections;
import java.util.Set;

import com.google.common.collect.Sets;

import fungle.common.hdao.BaseEntityDefinition;
import fungle.common.hdao.model.BasicEntityReference;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.EntityDefinition;
import fungle.common.hdao.model.EntityReference;
import fungle.common.hdao.model.EntityReferenceConverter;
import fungle.funfinder.data.entity.Activity;
import fungle.funfinder.data.entity.Event;
import fungle.funfinder.data.entity.User;

public class EventDef extends BaseEntityDefinition<Event> implements EntityDefinition<Event> {
	public static final EntityReference<Event, User> CREATORREF = BasicEntityReference.<Event, User>builder()
			.from(Event.class).to(User.class).converter(new EventUserConverter())
			.manyToOne().notRequired()
			.build();
	public static final EntityReference<Event, Activity> ACTIVITYREF = BasicEntityReference.<Event, Activity>builder()
			.from(Event.class).to(Activity.class).converter(new EventActivityConverter())
			.manyToOne().required()
			.build();

	public EventDef() {
		super(Event.class);
	}
	
	@Override
	public Set<EntityReference<?, Event>> getIncomingReferences() {
		return Collections.EMPTY_SET;
	}

	@Override
	public Set<EntityReference<Event, ?>> getOutgoingReferences() {
		return Sets.newHashSet(CREATORREF,ACTIVITYREF);
	}
	
	public static final class EventActivityConverter implements EntityReferenceConverter<Event, Activity> {

		@Override
		public Activity getForeignEntity(Event f) {
			return f.getActivity();
		}

		@Override
		public void setForeignEntity(Event f, ByteIdentifiable t) {
			f.setActivity((Activity) t);
		}

		@Override
		public Activity newForeignInstance() {
			return new Activity();
		}

		@Override
		public Event newLocalInstance() {
			return new Event();
		}
		
	}
	
	public static final class EventUserConverter implements EntityReferenceConverter<Event,User> {

		@Override
		public User getForeignEntity(Event f) {
			return f.getCreator();
		}

		@Override
		public void setForeignEntity(Event f, ByteIdentifiable t) {
			f.setCreator((User) t);
		}

		@Override
		public User newForeignInstance() {
			return new User();
		}

		@Override
		public Event newLocalInstance() {
			return new Event();
		}
	}
}
