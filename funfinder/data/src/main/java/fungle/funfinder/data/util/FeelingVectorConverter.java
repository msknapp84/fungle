package fungle.funfinder.data.util;

import fungle.common.hdao.ByteConverter;
import fungle.funfinder.data.FeelingVector;

public final class FeelingVectorConverter implements ByteConverter<FeelingVector> {
	private static final byte version = 1;
	
	public FeelingVectorConverter(){}
	
	public FeelingVector fromBytes(byte[] bytes) {
		if (bytes[0]!=version) {
			throw new IllegalArgumentException("This converter does not know about version "+bytes[0]);
		}
		FeelingVector fv = new FeelingVector(); 
		int[] vs = splitFromByte(bytes[1]);
		fv.setOutdoorsy(1==vs[0]);
		fv.setExpenseRating((byte) vs[1]);
		vs = splitFromByte(bytes[2]);
		fv.setRecreationRating((byte) vs[0]);
		fv.riskRating((byte)vs[1]);
		vs = splitFromByte(bytes[3]);
		fv.setSocialRating((byte) vs[0]);
		fv.setTimeConsumptionRating((byte) vs[1]);
		return fv;
	}
	
	public byte[] toBytes(FeelingVector feelingVector) {
		if (feelingVector==null) {
			return null;
		}
		byte[] bs = new byte[4];
		bs[0]=version;
		bs[1] = mergeToByte(feelingVector.isOutdoorsy()?1:0,feelingVector.getExpenseRating());
		bs[2] = mergeToByte(feelingVector.getRecreationRating(),feelingVector.getRiskRating());
		bs[3] = mergeToByte(feelingVector.getSocialRating(),feelingVector.getTimeConsumptionRating());
		return bs;
	}
	
	public static byte mergeToByte(int a,int b) {
		int m = mergeTwo(a,b);
		return (byte)(Byte.MIN_VALUE+m);
	}
	
	public static int mergeTwo(int a,int b) {
		return ((a<<4) | b);
	}
	
	public static int[] splitFromByte(byte a) {
		int m = a-Byte.MIN_VALUE;
		return splitTwo(m);
	}
	
	public static int[] splitTwo(int a) {
		int[] bs = new int[2];
		bs[0]= (a>>4);
		bs[1]=(a-(bs[0]<<4));
		return bs;
	}
}