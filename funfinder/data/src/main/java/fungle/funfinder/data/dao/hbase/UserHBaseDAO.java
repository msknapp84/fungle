package fungle.funfinder.data.dao.hbase;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fungle.common.hdao.HBaseDAOBase;
import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.Index;
import fungle.common.hdao.model.OnError;
import fungle.common.hdao.util.FetchUtils;
import fungle.funfinder.data.dao.UserDAO;
import fungle.funfinder.data.entity.Address;
import fungle.funfinder.data.entity.User;
import fungle.funfinder.data.tabledef.UserDAODef;

@Component("userDAO")
public class UserHBaseDAO extends HBaseDAOBase<User> implements UserDAO {
	private static final Logger logger = LoggerFactory.getLogger(UserHBaseDAO.class);

	public UserHBaseDAO() {
	}

	@Autowired
	public void setDefinition(UserDAODef def) {
		this.daoDefinition = def;
		validate();
	}

	@Override
	public User getByUserName(String userName) {
		User template = new User();
		template.setUserName(userName);
		Index<User> index = daoDefinition.getIndexByFieldName("userName");
		if (index == null) {
			logger.warn("There is no index for the userName field name.");
			return null;
		}
		Map<ByteId,User> activities = getUsingIndex(index,FetchUtils.never(), OnError.CONTINUE, template);
		if (CollectionUtils.isEmpty(activities.values())) {
			return null;
		}
		Set<User> res = new HashSet<>(activities.values());
		res.remove(null);
		return res.isEmpty()? null : res.iterator().next();
	}
	

	@Override
	public User getByEmail(String email) {
		User template = new User();
		template.setEmail(email);
		Index<User> index = daoDefinition.getIndexByFieldName("email");
		if (index == null) {
			logger.warn("There is no index for the email field name.");
			return null;
		}
		Map<ByteId,User> activities = getUsingIndex(index,FetchUtils.never(), OnError.CONTINUE, template);
		if (CollectionUtils.isEmpty(activities.values())) {
			return null;
		}
		Set<User> res = new HashSet<>(activities.values());
		res.remove(null);
		return res.isEmpty()? null : res.iterator().next();
	}

	@Override
	public Set<User> getByName(String lastName, String firstName) {
		User template = new User();
		template.setLastName(lastName);
		template.setFirstName(firstName);
		Index<User> index = daoDefinition.getIndexByFieldName("lastName","firstName","dob");
		Map<ByteId,User> activities = getUsingIndex(index,FetchUtils.never(), OnError.CONTINUE, template);
		if (CollectionUtils.isEmpty(activities.values())) {
			return null;
		}
		Set<User> res = new HashSet<>(activities.values());
		res.remove(null);
		return res;
	}

	@Override
	public User getByNameAndDOB(String lastName, String firstName, Calendar dob) {
		User template = new User();
		template.setLastName(lastName);
		template.setFirstName(firstName);
		template.setDateOfBirth(dob.getTime());
		Index<User> index = daoDefinition.getIndexByFieldName("lastName","firstName","dob");
		Map<ByteId,User> activities = getUsingIndex(index,FetchUtils.never(), OnError.CONTINUE, template);
		if (CollectionUtils.isEmpty(activities.values())) {
			return null;
		}
		Set<User> res = new HashSet<>(activities.values());
		res.remove(null);
		return res.isEmpty()? null : res.iterator().next();
	}

	@Override
	public User getByNameAndAddress(String lastName, String firstName,
			Address address) {
		User template = new User();
		template.setLastName(lastName);
		template.setFirstName(firstName);
		template.setAddress(address);
		Index<User> index = daoDefinition.getIndexByFieldName("lastName","firstName","address");
		Map<ByteId,User> activities = getUsingIndex(index,FetchUtils.never(), OnError.CONTINUE, template);
		if (CollectionUtils.isEmpty(activities.values())) {
			return null;
		}
		Set<User> res = new HashSet<>(activities.values());
		res.remove(null);
		return res.isEmpty()? null : res.iterator().next();
	}
}