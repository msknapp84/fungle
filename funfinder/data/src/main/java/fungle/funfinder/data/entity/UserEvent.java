package fungle.funfinder.data.entity;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.ByteIdentifiable;

/**
 * A suggestion is a combination of a user and event. In some cases you would
 * rather pair a user with an activity. When that happens just make a default
 * event for the activity.
 * 
 * @author cloudera
 */
public class UserEvent implements ByteIdentifiable {
	private User user;
	private Event event;
	private ByteId byteId;

	public UserEvent() {

	}

	public UserEvent(User user, Event event) {
		this.user = user;
		this.event = event;
	}

	public UserEvent(UserEvent other) {
		this.user = new User(other.getUser());
		this.event = new Event(other.getEvent());
	}

	public UserEvent id(ByteId id) {
		this.byteId =id;
		return this;
	}

	@Override
	public void setByteId(byte[] id) {
		this.byteId=new ByteId(id);
	}

	public UserEvent event(Event event) {
		this.event = event;
		return this;
	}

	public UserEvent user(User user) {
		this.user = user;
		return this;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public ByteId getByteId() {
		return byteId;
	}

	public void setByteId(ByteId id) {
		this.byteId =id;
	}

	public String toString() {
		return user.toString() + "\n\n" + event.toString();
	}

	public boolean refEquals(UserEvent that) {
		ByteId thisuid = this.user == null ? null : this.user.getByteId();
		ByteId thatuid = that.user == null ? null : that.user.getByteId();
		ByteId thiseid = this.event == null ? null : this.event.getByteId();
		ByteId thateid = that.event == null ? null : that.event.getByteId();
		return new EqualsBuilder().append(thisuid, thatuid)
				.append(thiseid, thateid).isEquals();
	}

	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof UserEvent)) {
			return false;
		}
		UserEvent that = (UserEvent) o;
		return new EqualsBuilder().append(this.user, that.user)
				.append(this.event, that.event).isEquals();
	}

	public int hashCode() {
		return new HashCodeBuilder().append(this.user).append(event)
				.toHashCode();
	}
}