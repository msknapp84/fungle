package fungle.funfinder.data.util;

import java.math.BigDecimal;

import org.apache.hadoop.hbase.util.Bytes;

import fungle.common.hdao.ByteConverter;
import fungle.common.hdao.util.HBaseByteConverter;
import fungle.funfinder.data.geo.GeoCoordinate;

public class GeoCoordinateByteConverter implements ByteConverter<GeoCoordinate> {

	@Override
	public GeoCoordinate fromBytes(byte[] bytes) {
		int len = bytes[0];
		byte[] lat = new byte[len];
		System.arraycopy(bytes, 1, lat, 0, len);
		byte[] lng = new byte[bytes.length-len-1];
		System.arraycopy(bytes, 1+len, lng, 0,bytes.length- len-1);
		BigDecimal latbd = Bytes.toBigDecimal(lat);
		BigDecimal lngbd = Bytes.toBigDecimal(lng);
		return new GeoCoordinate(latbd, lngbd);
	}

	@Override
	public byte[] toBytes(GeoCoordinate entity) {
		byte[] bs = Bytes.toBytes(entity.getLatitude());
		byte[] bs2 = Bytes.toBytes(entity.getLongitude());
		return HBaseByteConverter.merge(new byte[]{(byte) bs.length},bs,bs2);
	}
}