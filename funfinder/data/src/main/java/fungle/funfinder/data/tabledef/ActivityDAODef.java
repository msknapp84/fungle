package fungle.funfinder.data.tabledef;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.hadoop.hbase.client.HTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fungle.common.hdao.BaseDAODefinition;
import fungle.common.hdao.ByteIdentifiableDAO;
import fungle.common.hdao.model.BasicIndex;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.Column;
import fungle.common.hdao.model.EntityReference;
import fungle.common.hdao.model.Index;
import fungle.common.hdao.util.TableNameService;
import fungle.funfinder.data.dao.ActivityTypeDAO;
import fungle.funfinder.data.dao.EventDAO;
import fungle.funfinder.data.dao.PlaceDAO;
import fungle.funfinder.data.def.ActivityDef;
import fungle.funfinder.data.entity.Activity;
import fungle.funfinder.data.geo.GeoFinder;
import fungle.funfinder.data.geo.GeoIndex;

@Component("activityDAODef")
public class ActivityDAODef extends BaseDAODefinition<Activity> {

	private ActivityTypeDAO activityTypeDAO;
	private PlaceDAO placeDAO;
	private EventDAO eventDAO;
	
	private HTable geoIndexTable;
	private HTable categoryIndexTable;
	private HTable nameIndexTable;
	private HTable placeIndexTable;

	private GeoIndex<Activity> geoIndex;
	private Index<Activity> categoryIndex;
	private Index<Activity> nameIndex;
	private Index<Activity> placeIndex;
	
	private GeoFinder finder;
	
	@Autowired
	public ActivityDAODef(TableNameService tableNameService) {
		super(new ActivityDef(), tableNameService);
	}

	@Override
	public <X extends ByteIdentifiable> ByteIdentifiableDAO<X> getOutgoingDAO(
			EntityReference<Activity, X> reference) {
		if (reference == ActivityDef.PLACEREF) {
			return (ByteIdentifiableDAO<X>) placeDAO;
		} else if (reference == ActivityDef.TYPEREF) {
			return (ByteIdentifiableDAO<X>) activityTypeDAO;
		}
		return null;
	}

	@Override
	public <X extends ByteIdentifiable> ByteIdentifiableDAO<X> getIncomingDAO(
			EntityReference<X, Activity> reference) {
		return (ByteIdentifiableDAO<X>) eventDAO;
	}

	@Override
	public Index<Activity> getIndexForReference(
			EntityReference<Activity, ?> reference) {
		if (reference == ActivityDef.PLACEREF) {
			return placeIndex;
		} else if (reference == ActivityDef.TYPEREF) {
			return categoryIndex;
		}
		return null;
	}
	
	private void lazyInitIndexes() {
		if (this.indexes==null || this.indexes.isEmpty()) {
			this.indexes = buildIndexes();
		}
	}

	@Override
	public List<Index<Activity>> buildIndexes() {
		geoIndexTable = tableNameService.get(this).ensureIndexTable(
				"coordinate");
		categoryIndexTable = tableNameService.get(this).ensureIndexTable(
				"category");
		nameIndexTable = tableNameService.get(this).ensureIndexTable("name");
		placeIndexTable = tableNameService.get(this).ensureIndexTable("place");
		geoIndex = GeoIndex.forTables(geoIndexTable, getTable()).finder(getFinder())
				.coordinateFieldName("place.coordinate").build(Activity.class);
		categoryIndex = BasicIndex.builder().indexTable(categoryIndexTable)
				.referenceTable(getTable()).ascending("category.name").build();
		nameIndex = BasicIndex.builder().indexTable(nameIndexTable)
				.referenceTable(getTable()).ascending("name").build();
		placeIndex = BasicIndex.builder().indexTable(placeIndexTable)
				.referenceTable(getTable()).ascending("place").build();
		return Arrays.asList(geoIndex, categoryIndex, nameIndex,placeIndex);
	}

	@Override
	public BidiMap<String, Column> buildMapping() {
		BidiMap<String, Column> mapping = new DualHashBidiMap<String, Column>();
		mapping.put("category", Column.from("f", "c", true));
		mapping.put("name", Column.from("f", "n", false));
		mapping.put("place", Column.from("f", "p", true));
		mapping.put("description", Column.from("f", "d", false));
		mapping.put("website", Column.from("f", "w", false));
		mapping.put("schedule", Column.from("f", "s", false));
		mapping.put("creator", Column.from("f", "u", false));
		return mapping;
	}

	public ActivityTypeDAO getActivityTypeDAO() {
		return activityTypeDAO;
	}

	@Autowired
	public void setActivityTypeDAO(ActivityTypeDAO activityTypeDAO) {
		this.activityTypeDAO = activityTypeDAO;
	}

	public PlaceDAO getPlaceDAO() {
		return placeDAO;
	}

	@Autowired
	public void setPlaceDAO(PlaceDAO placeDAO) {
		this.placeDAO = placeDAO;
	}

	public EventDAO getEventDAO() {
		return eventDAO;
	}

	@Autowired
	public void setEventDAO(EventDAO eventDAO) {
		this.eventDAO = eventDAO;
	}

	public HTable getGeoIndexTable() {
		return geoIndexTable;
	}

	public HTable getCategoryIndexTable() {
		return categoryIndexTable;
	}

	public HTable getNameIndexTable() {
		return nameIndexTable;
	}

	public HTable getPlaceIndexTable() {
		return placeIndexTable;
	}

	public GeoIndex<Activity> getGeoIndex() {
		lazyInitIndexes();
		return geoIndex;
	}

	public Index<Activity> getCategoryIndex() {
		lazyInitIndexes();
		return categoryIndex;
	}

	public Index<Activity> getNameIndex() {
		lazyInitIndexes();
		return nameIndex;
	}

	public Index<Activity> getPlaceIndex() {
		lazyInitIndexes();
		return placeIndex;
	}

	@Override
	public Index<Activity> getIndexByFieldName(String... fieldNames) {
		getIndexes();
		String f = fieldNames[0].toLowerCase().trim();
		if ("name".equals(f)) {
			return nameIndex;
		} else if ("coordinate".equals(f) || "geocoordinate".equals(f)) {
			return geoIndex;
		} else if ("category".equals(f) || "type".equals(f) || "activitytype".equals(f)) {
			return categoryIndex;
		} else if ("place".equals(f)) {
			return placeIndex;
		}
		return null;
	}

	public GeoFinder getFinder() {
		return finder;
	}

	@Autowired
	public void setFinder(GeoFinder finder) {
		this.finder = finder;
	}

}
