package fungle.funfinder.data.def;

import java.util.Set;

import org.springframework.stereotype.Component;

import com.google.common.collect.Sets;

import fungle.common.hdao.BaseEntityDefinition;
import fungle.common.hdao.model.BasicEntityReference;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.EntityDefinition;
import fungle.common.hdao.model.EntityReference;
import fungle.common.hdao.model.EntityReferenceConverter;
import fungle.funfinder.data.entity.Activity;
import fungle.funfinder.data.entity.ActivityType;
import fungle.funfinder.data.entity.Place;

public class ActivityDef extends BaseEntityDefinition<Activity> implements
		EntityDefinition<Activity> {
	public static final EntityReference<Activity, ActivityType> TYPEREF = BasicEntityReference.<Activity, ActivityType>builder()
			.from(Activity.class).to(ActivityType.class).manyToOne().required().converter(new ActivityToTypeConverter())
			.build();
	public static final EntityReference<Activity, Place> PLACEREF = BasicEntityReference.<Activity, Place>builder()
			.from(Activity.class).to(Place.class).converter(new ActivityToPlaceConverter())
			.manyToOne().required()
			.build();
	
	public ActivityDef() {
		super(Activity.class);
	}

	@Override
	public Set<EntityReference<?, Activity>> getIncomingReferences() {
		return Sets.newHashSet(EventDef.ACTIVITYREF);
	}

	@Override
	public Set<EntityReference<Activity, ?>> getOutgoingReferences() {
		return Sets.newHashSet(TYPEREF,PLACEREF);
	}
	
	public static final class ActivityToPlaceConverter implements EntityReferenceConverter<Activity, Place> {

		@Override
		public Place getForeignEntity(Activity f) {
			return f.getPlace();
		}

		@Override
		public void setForeignEntity(Activity f, ByteIdentifiable t) {
			f.setPlace((Place)t);
		}

		@Override
		public Place newForeignInstance() {
			return new Place();
		}

		@Override
		public Activity newLocalInstance() {
			return new Activity();
		}
	}
	
	public static final class ActivityToTypeConverter implements EntityReferenceConverter<Activity, ActivityType> {

		@Override
		public ActivityType getForeignEntity(Activity f) {
			return f.getCategory();
		}

		@Override
		public void setForeignEntity(Activity f, ByteIdentifiable t) {
			f.setCategory((ActivityType) t);
		}

		@Override
		public ActivityType newForeignInstance() {
			return new ActivityType();
		}

		@Override
		public Activity newLocalInstance() {
			return new Activity();
		}
	}
}