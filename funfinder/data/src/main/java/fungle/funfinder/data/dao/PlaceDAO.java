package fungle.funfinder.data.dao;

import java.util.Set;

import fungle.common.hdao.ByteIdentifiableDAO;
import fungle.funfinder.data.GeoSearch;
import fungle.funfinder.data.entity.Place;

public interface PlaceDAO extends ByteIdentifiableDAO<Place>{
	Set<Place> getByName(String name);
	void deleteByName(String name);
	Set<Place> getNear(GeoSearch search);
}
