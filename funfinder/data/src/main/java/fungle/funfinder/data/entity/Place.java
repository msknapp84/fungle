package fungle.funfinder.data.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.SimpleIDGenerator;
import fungle.funfinder.data.geo.GeoCoordinate;

@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(propOrder={"byteId","name","type","businessType","description","coordinate","address"})
public class Place implements ByteIdentifiable {

	private ByteId byteId;
	private String name;
	private String type;
	private String businessType;
	private String description;
	private GeoCoordinate coordinate;
	private Address address;
	private User creator;
	
	public Place (Place other) {
		this.address = new Address(other.address);
		this.coordinate = new GeoCoordinate(other.coordinate);
		this.name = other.name;
		this.type = other.type;
		this.businessType = other.businessType;
		this.description = other.description;
	}
	
	public String toString() {
		return String.format("%s (%s)%n%s%n%s", name,type,description,address==null?"":address.toString());
	}

	public int hashCode() {
		HashCodeBuilder eb = new HashCodeBuilder();
		eb.append(this.address).append(this.name)
				.append(this.type)
				.append(this.businessType)
				.append(this.description)
				.append(this.coordinate);
		return eb.toHashCode();
	}

	public boolean refEquals(Place that) {
		EqualsBuilder eb = new EqualsBuilder();
		ByteId thisAdd = this.address==null ? null : this.address.getByteId();
		ByteId thatAdd = that.address==null ? null : that.address.getByteId();
		eb.append(this.name, that.name)
				.append(thisAdd, thatAdd)
				.append(this.type, that.type)
				.append(this.businessType, that.businessType)
				.append(this.description, that.description)
				.append(this.coordinate, that.coordinate);
		return eb.isEquals();
	}

	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (!(o instanceof Place)) {
			return false;
		}
		Place that = (Place) o;
		EqualsBuilder eb = new EqualsBuilder();
		eb.append(this.address, that.address).append(this.name, that.name)
				.append(this.type, that.type)
				.append(this.businessType, that.businessType)
				.append(this.description, that.description)
				.append(this.coordinate, that.coordinate);
		return eb.isEquals();
	}

	public Place(Address address) {
		this.address = address;
	}

	public Place(Address address, String name, String description) {
		this.address = address;
		this.name = name;
		this.description = description;
	}

	public Place() {
		
	}

	public Place address(Address address) {
		this.address = address;
		return this;
	}
	
	public Place id(ByteId id) {
		this.byteId = id;
		return this;
	}
	
	public Place defaultId() {
		this.byteId = SimpleIDGenerator.getInstance(Place.class).nextBytes();
		return this;
	}

	public Place name(String name) {
		this.name = name;
		return this;
	}

	public Place coordinate(GeoCoordinate coordinate) {
		this.coordinate = coordinate;
		return this;
	}

	public Place businessType(String businessType) {
		this.businessType = businessType;
		return this;
	}

	public Place type(String type) {
		this.type = type;
		return this;
	}

	public Place description(String description) {
		this.description = description;
		return this;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public ByteId getByteId() {
		return byteId;
	}

	@Override
	public void setByteId(ByteId id) {
		this.byteId =id;
	}

	public GeoCoordinate getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(GeoCoordinate coordinate) {
		this.coordinate = coordinate;
	}

	@Override
	public void setByteId(byte[] id) {
		this.byteId=new ByteId(id);
	}

	@XmlTransient
	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}

}
