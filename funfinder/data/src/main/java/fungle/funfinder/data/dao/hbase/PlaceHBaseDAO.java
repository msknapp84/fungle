package fungle.funfinder.data.dao.hbase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fungle.common.hdao.HBaseDAOBase;
import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.Index;
import fungle.common.hdao.model.OnError;
import fungle.common.hdao.util.CascadeUtils;
import fungle.common.hdao.util.FetchUtils;
import fungle.funfinder.data.GeoSearch;
import fungle.funfinder.data.dao.PlaceDAO;
import fungle.funfinder.data.entity.Place;
import fungle.funfinder.data.geo.GeoFinder;
import fungle.funfinder.data.geo.GeoRectangle;
import fungle.funfinder.data.tabledef.PlaceDAODef;

@Component("placeDAO")
public class PlaceHBaseDAO extends HBaseDAOBase<Place> implements PlaceDAO {
	private static final Logger logger = LoggerFactory
			.getLogger(PlaceHBaseDAO.class);

	private GeoFinder finder;

	public PlaceHBaseDAO() {
	}

	@Autowired
	public void setDefinition(PlaceDAODef def) {
		this.daoDefinition = def;
		validate();
	}

	@Override
	public Set<Place> getByName(String name) {
		Index<Place> nameIndex = daoDefinition.getIndexByFieldName("name");
		Place template = new Place();
		template.setName(name);
		Map<ByteId, Place> places = getUsingIndex(nameIndex,
				FetchUtils.never(), OnError.CONTINUE, template);
		Set<Place> placesSet = new HashSet<>(places.values());
		placesSet.remove(null);
		return placesSet;
	}

	@Override
	public void deleteByName(String name) {
		Index<Place> nameIndex = daoDefinition.getIndexByFieldName("name");
		Place template = new Place();
		template.setName(name);
		deleteUsingIndex(nameIndex, CascadeUtils.always(), OnError.CONTINUE,
				template);
	}

	@Override
	public Set<Place> getNear(GeoSearch search) {
		Index<Place> geoIndex = daoDefinition.getIndexByFieldName("coordinate");
		if (geoIndex == null) {
			logger.warn("Found no coordinate index for the place HBase DAO.");
			return Collections.EMPTY_SET;
		}
		Set<GeoRectangle> rectangles = getFinder()
				.getRectanglesNear(search.getCoordinate(),
						search.getDistance(), search.getUnits());
		List<Place> templates = new ArrayList<>();
		for (GeoRectangle rectangle : rectangles) {
			Place template = new Place().coordinate(rectangle.getCenter());
			templates.add(template);
		}
		Map<ByteId, Place> placesMap = getUsingIndex(geoIndex, FetchUtils.never(),
				OnError.CONTINUE, templates);
		Set<Place> places = new HashSet<>(placesMap.values());
		places.remove(null);
		return places;
	}

	public GeoFinder getFinder() {
		return finder;
	}

	@Autowired
	public void setFinder(GeoFinder finder) {
		this.finder = finder;
	}
}