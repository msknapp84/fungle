package fungle.funfinder.data.dao;

import fungle.common.hdao.ByteIdentifiableDAO;
import fungle.funfinder.data.entity.UserSchedule;

public interface UserScheduleDAO extends ByteIdentifiableDAO<UserSchedule>{

}
