package fungle.funfinder.data.util;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import com.sun.xml.bind.v2.runtime.IllegalAnnotationsException;

import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.ByteIdAdapter;
import fungle.funfinder.data.entity.*;
import fungle.funfinder.data.geo.GeoCoordinate;

public final class JAXBUtil {
	private static final Logger logger = LoggerFactory
			.getLogger(JAXBUtil.class);
	private static final JAXBContext context;
	private static final Marshaller marshaller;
	private static final Unmarshaller unmarshaller;

	static {
		JAXBContext temp = null;
		Marshaller tm = null;
		Unmarshaller tum = null;
		try {
			// temp = JAXBContext.newInstance(Event.class);
			temp = JAXBContext.newInstance(Activity.class,
					ActivitySchedule.class, ActivityType.class, Address.class,
					Event.class, Place.class, PrivacySettings.class,User.class, GeoCoordinate.class);
			// temp = JAXBContext.newInstance(Activity.class,
			// ActivitySchedule.class, ActivityType.class, Address.class,
			// Event.class, Place.class, PrivacySettings.class,
			// User.class, GeoCoordinate.class, ByteId.class);
			tm = temp.createMarshaller();
			tum = temp.createUnmarshaller();

//		} catch (IllegalAnnotationsException e) {
//			logger.error(e.getMessage() + "\n" + e.getErrors());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		context = temp;
		marshaller = tm;
		unmarshaller = tum;
	}

	private JAXBUtil() {

	}

	public static String marshal(Object o) {
		String s = null;
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			marshaller.marshal(o, os);
			s = new String(os.toByteArray());
		} catch (JAXBException e) {
			logger.error(e.getMessage(), e);
		} finally {
			IOUtils.closeQuietly(os);
		}
		return s;
	}

	public static <T> T unmarshal(String text) {
		T t = null;
		InputStream is = IOUtils.toInputStream(text);
		try {
			t = (T) unmarshaller.unmarshal(is);
		} catch (JAXBException e) {
			logger.error(e.getMessage(), e);
		} finally {
			IOUtils.closeQuietly(is);
		}
		return t;
	}

}
