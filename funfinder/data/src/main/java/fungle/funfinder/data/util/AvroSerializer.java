package fungle.funfinder.data.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.avro.Schema;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.file.SeekableByteArrayInput;
import org.apache.avro.file.SeekableFileInput;
import org.apache.avro.file.SeekableInput;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;

import fungle.common.hdao.model.ByteId;
import fungle.funfinder.data.FeelingVector;
import fungle.funfinder.data.TimeRange;
import fungle.funfinder.data.entity.Activity;
import fungle.funfinder.data.entity.ActivitySchedule;
import fungle.funfinder.data.entity.ActivityType;
import fungle.funfinder.data.entity.Address;
import fungle.funfinder.data.entity.Place;
import fungle.funfinder.data.entity.PrivacySettings;
import fungle.funfinder.data.entity.User;
import fungle.funfinder.data.entity.UserActivityPreferences;
import fungle.funfinder.data.entity.UserEvent;
import fungle.funfinder.data.entity.UserEventReview;
import fungle.funfinder.data.entity.UserSchedule;

public final class AvroSerializer {

	private static final Map<Class<?>, Schema> schemas = new HashMap<>();

	private AvroSerializer() {
	}

	public static void main(String[] args) {
		createAllSchemas();
	}

	public static void createAllSchemas() {
		try {
			createSchemaText(TimeRange.class);
			createSchemaText(Activity.class);
			createSchemaText(ActivitySchedule.class);
			createSchemaText(ActivityType.class);
			createSchemaText(Address.class);
			createSchemaText(Place.class);
			createSchemaText(PrivacySettings.class);
			createSchemaText(User.class);
			createSchemaText(UserActivityPreferences.class);
			createSchemaText(UserEvent.class);
			createSchemaText(UserEventReview.class);
			createSchemaText(UserSchedule.class);
		} catch (Exception e) {

		}
	}

	public static void createSchemaText(Class<?> clazz) throws IOException {
		createSchemaText(clazz,
				"src/main/resources/avro/" + clazz.getSimpleName() + ".avsc");
	}

	public static void createSchemaText(Class<?> clazz, String classPathOut)
			throws IOException {
		createSchemaText(clazz, new FileWriter(new File(classPathOut)));
	}

	public static void createSchemaText(Class<?> clazz, Writer writer) {
		try {
			writer.write("{\"namespace\": \"");
			writer.write(clazz.getPackage().getName());
			writer.write("\",\n\"type\": \"record\",\n\"name\": \"");
			writer.write(clazz.getSimpleName());
			writer.write("\",\n\"fields\": [\n");
			for (Field field : clazz.getDeclaredFields()) {
				Class<?> c = field.getType();
				String tp = determineAvroTypeFor(c);
				if (tp != null) {
					writer.write("\t{\"name\":\"");
					writer.write(field.getName());
					writer.write("\", \"type\":[\"");
					writer.write(tp);
					// by default we assume anything can be null.
					writer.write("\",\"null\"]}\n");
				}
			}
			writer.write("\n]\n}");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static String determineAvroTypeFor(Class<?> c) {
		if (c == null) {
			return "null";
		} else if (c == boolean.class || c == Boolean.class) {
			return "boolean";
		} else if (c == int.class || c == Integer.class || c == byte.class
				|| c == Byte.class || c == short.class || c == Short.class) {
			return "int";
		} else if (c == long.class || c == Long.class || c == Calendar.class
				|| c == Date.class) {
			return "long";
		} else if (c == float.class || c == Float.class) {
			return "float";
		} else if (c == double.class || c == Double.class) {
			return "double";
		} else if (c == ByteId.class) {
			return "bytes";
		} else if (c == String.class) {
			return "string";
		} else if (c.getPackage().getName().startsWith("fungle")) {
			return "record";
		}
		return null;
	}

	public static void serializeFeelingVector(
			Collection<FeelingVector> feelingVectors, OutputStream outputStream) {
		try {
			Schema schema = new Schema.Parser().parse(AvroSerializer.class
					.getResourceAsStream("/avro/FeelingVector.avsc"));

			DatumWriter<GenericRecord> datumWriter = new GenericDatumWriter<GenericRecord>(
					schema);
			DataFileWriter<GenericRecord> dataFileWriter = new DataFileWriter<GenericRecord>(
					datumWriter);

			dataFileWriter.create(schema, outputStream);

			feelingVectors
					.forEach(fv -> {
						try {
							dataFileWriter.append(buildFeelingVectorRecord(fv,
									schema));
						} catch (Exception e) {
							e.printStackTrace();
						}
					});
			dataFileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void serialize(Collection<Object> objects,
			OutputStream outputStream) {
		if (objects.isEmpty()) {
			try {
				outputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return;
		}
		Object first = objects.iterator().next();
		Schema schema = getSchema(first.getClass());
		serialize(objects, schema, outputStream);
	}

	public static void serialize(Collection<Object> objects, Schema schema,
			OutputStream outputStream) {
		DatumWriter<GenericRecord> datumWriter = new GenericDatumWriter<GenericRecord>(
				schema);
		DataFileWriter<GenericRecord> dataFileWriter = new DataFileWriter<GenericRecord>(
				datumWriter);
		try {
			dataFileWriter.create(schema, outputStream);
			objects.forEach(fv -> {
				try {
					dataFileWriter.append(buildRecordReflectively(fv, schema));
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				dataFileWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static GenericRecord buildFeelingVectorRecord(
			FeelingVector feelingVector, Schema schema) {
		GenericRecord fv = new GenericData.Record(schema);
		fv.put("outdoorsy", feelingVector.isOutdoorsy());
		fv.put("expenseRating", (int) feelingVector.getExpenseRating());
		fv.put("riskRating", (int) feelingVector.getRiskRating());
		fv.put("recreationRating", (int) feelingVector.getRecreationRating());
		fv.put("socialRating", (int) feelingVector.getSocialRating());
		fv.put("timeConsumptionRating",
				(int) feelingVector.getTimeConsumptionRating());
		return fv;
	}

	private static GenericRecord buildRecordReflectively(Object o, Schema schema) {
		GenericRecord fv = new GenericData.Record(schema);
		for (Field f : o.getClass().getDeclaredFields()) {
			try {
				fv.put(f.getName(), f.get(o));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return fv;
	}

	public static <T> List<T> deserializeFromBytes(byte[] bytes, Class<T> clazz) {
		return deserializeFromStream(new SeekableByteArrayInput(bytes), clazz);
	}

	public static <T> List<T> deserializeFromFile(File file, Class<T> clazz) {
		try {
			return deserializeFromStream(new SeekableFileInput(file), clazz);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static <T> List<T> deserializeFromString(String serialized, Class<T> clazz) {
		return deserializeFromStream(
				new SeekableByteArrayInput(serialized.getBytes()), clazz);
	}

	public static <T> List<T> deserializeFromStream(SeekableInput seekableInput,
			Class<T> clazz) {
		Schema schema = getSchema(clazz);
		DatumReader<GenericRecord> datumReader = new GenericDatumReader<GenericRecord>(
				schema);
		DataFileReader<GenericRecord> dataFileReader=null;
		List<T> entities = new ArrayList<>();
		try {
			dataFileReader = new DataFileReader<GenericRecord>(seekableInput,
					datumReader);
			dataFileReader.forEach(record -> {entities.add(buildFromRecord(record,clazz));});
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			justFuckingClose(dataFileReader);
		}
		return entities;
	}
	
	public static void justFuckingClose(Object o) {
		if (o == null) {
			return;
		}
		Method m;
		try {
			m = o.getClass().getDeclaredMethod("close", new Class<?>[]{});
			if (m!=null) {
				m.invoke(o, new Object[]{});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static <T> T buildFromRecord(GenericRecord record,Class<T> clazz) {
		T instance = null;
		try {
			instance = clazz.newInstance();
			List<Schema.Field> fields = record.getSchema().getFields();
			for (Schema.Field field : fields) {
				String fieldName = field.getProp("name");
				Object o = record.get(fieldName);
				java.lang.reflect.Field classField = clazz.getDeclaredField(fieldName);
				classField.set(instance, o);
			}
		} catch (InstantiationException e) {
			
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return instance;
	}

	public static Schema getSchema(Class<?> clazz) {
		Schema schema = schemas.get(clazz);
		if (schema == null) {
			try {
				schema = new Schema.Parser().parse(AvroSerializer.class
						.getResourceAsStream("/avro/" + clazz.getSimpleName()));
			} catch (IOException e) {
				e.printStackTrace();
			}
			schemas.put(clazz, schema);
		}
		return schema;
	}

	public static String serializeToString(Collection<Object> objects) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		serialize(objects, baos);
		return new String(baos.toByteArray());
	}
}