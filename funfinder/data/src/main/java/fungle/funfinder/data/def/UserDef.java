package fungle.funfinder.data.def;

import java.util.Collections;
import java.util.Set;

import com.google.common.collect.Sets;

import fungle.common.hdao.BaseEntityDefinition;
import fungle.common.hdao.model.EntityDefinition;
import fungle.common.hdao.model.EntityReference;
import fungle.funfinder.data.entity.User;

public class UserDef extends BaseEntityDefinition<User> implements EntityDefinition<User> {
//	public static final EntityReference<User, User> CREATORREF = BasicEntityReference.<User, User>builder()
//			.from(User.class).to(User.class).converter(new UserUserConverter())
//			.manyToOne().notRequired()
//			.build();
//	public static final EntityReference<User, Activity> ACTIVITYREF = BasicEntityReference.<User, Activity>builder()
//			.from(User.class).to(Activity.class).converter(new UserActivityConverter())
//			.manyToOne().required()
//			.build();

	public UserDef() {
		super(User.class);
	}
	
	@Override
	public Set<EntityReference<?, User>> getIncomingReferences() {
		return Sets.newHashSet(EventDef.CREATORREF,ActivityTypeDef.USERREF);
	}

	@Override
	public Set<EntityReference<User, ?>> getOutgoingReferences() {
		return Collections.EMPTY_SET;
	}
	
//	public static final class UserActivityConverter implements EntityReferenceConverter<User, Activity> {
//
//		@Override
//		public Activity getForeignEntity(User f) {
//			return f.getActivity();
//		}
//
//		@Override
//		public void setForeignEntity(User f, ByteIdentifiable t) {
//			f.setActivity((Activity) t);
//		}
//
//		@Override
//		public Activity newForeignInstance() {
//			return new Activity();
//		}
//
//		@Override
//		public User newLocalInstance() {
//			return new User();
//		}
//		
//	}

}
