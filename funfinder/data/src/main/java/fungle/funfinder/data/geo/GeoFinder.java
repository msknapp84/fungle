package fungle.funfinder.data.geo;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

public interface GeoFinder extends GeoGrid {
	Set<GeoRectangle> getRectanglesNear(GeoCoordinate coordinate,BigDecimal distance,GeoUnits units);
	Set<Integer> getRectangleIndexesNear(GeoCoordinate coordinate,BigDecimal distance,GeoUnits units);
	int getIndex(GeoCoordinate coordinate);
}