package fungle.funfinder.data.dao;

import fungle.common.hdao.ByteIdentifiableDAO;
import fungle.funfinder.data.entity.ActivitySchedule;

public interface ActivityScheduleDAO extends ByteIdentifiableDAO<ActivitySchedule> {

}
