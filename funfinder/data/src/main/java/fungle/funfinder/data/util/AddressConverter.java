package fungle.funfinder.data.util;

import org.apache.hadoop.hbase.util.Bytes;

import fungle.common.hdao.ByteConverter;
import fungle.funfinder.data.entity.Address;

public final class AddressConverter implements ByteConverter<Address> {
	private static final String delimiter = "::";

	public AddressConverter() {

	}

	public Address fromBytes(byte[] bytes) {
		String s = Bytes.toString(bytes);
		String[] parts = s.split(delimiter);
		Address address = new Address();
		address.setNumber(Integer.parseInt(parts[0]));
		if (parts[1].length()>0) {
			address.setStreet1(parts[1]);
		}
		if (parts[2].length()>0) {
			address.setStreet2(parts[2]);
		}
		if (parts[3].length()>0) {
			address.setCity(parts[3]);
		}
		if (parts[4].length()>0) {
			address.setState(parts[4]);
		}
		address.setZip(Integer.parseInt(parts[5]));
		address.setSubZip(Integer.parseInt(parts[6]));
		if (parts[7].length()>0) {
			address.setCountry(parts[7]);
		}
		return address;
	}

	public byte[] toBytes(Address address) {
		StringBuilder sb = new StringBuilder();

		sb.append(address.getNumber()).append(delimiter);
		if (address.getStreet1() != null) {
			sb.append(address.getStreet1());
		}
		sb.append(delimiter);
		if (address.getStreet2() != null) {
			sb.append(address.getStreet2());
		}
		sb.append(delimiter);
		if (address.getCity()!=null) {
			sb.append(address.getCity());
		}
		sb.append(delimiter);
		if (address.getState()!=null) {
			sb.append(address.getState());
		}
		sb.append(delimiter);
		sb.append(address.getZip()).append(delimiter)
				.append(address.getSubZip()).append(delimiter);
		if (address.getCountry()!=null) {
			sb.append(address.getCountry());
		}
		return Bytes.toBytes(sb.toString());
	}
}
