package fungle.funfinder.data.geo;

import java.math.BigDecimal;

public interface GeoGrid {
	GeoRectangle getRectangle(GeoCoordinate coordinate);
	GeoRectangle getRectangle(int index);
	GeoRectangle getRectangle(int latitudeIndex,int longitudeIndex);
	int getLongitudeIndex(BigDecimal longitudeDegrees);
	int getLatitudeIndex(BigDecimal latitudeDegrees);
	int getIndex(GeoRectangle rectangle);
	int getIndex(int latitudeIndex,int longitudeIndex);
	int getMaximumRectangles();
}