package fungle.funfinder.data.entity;

import java.math.BigDecimal;
import java.util.Calendar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang3.time.DateUtils;

import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.SimpleIDGenerator;

@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(propOrder = { "byteId", "description", "website", "activity", "price",
		"startDateTime", "endDateTime", "rsvpDeadline", "maxPeople" })
public class Event implements ByteIdentifiable {
	private ByteId byteId;
	private String description;
	private String website;
	private Activity activity;
	private BigDecimal price;
	private Calendar startDateTime;
	private Calendar endDateTime;
	private Calendar rsvpDeadline;
	private int maxPeople;

	private User creator;

	public Event() {

	}

	public Event(Activity activity) {
		this.activity = activity;
	}

	public Event(Activity activity, String description, String website) {
		this.activity = activity;
		this.description = description;
		this.website = website;
	}

	public Event(Event event) {
		this.byteId = event.byteId;
		this.activity = new Activity(event.getActivity());
		this.setStartDateTime(event.getDateTime());
		this.endDateTime = event.getEndDateTime();
		this.description = event.getDescription();
		this.website = event.getWebsite();
		this.price = event.getPrice();
		this.rsvpDeadline = event.getRsvpDeadline();
		this.maxPeople = event.getMaxPeople();
	}

	public Event defaultId() {
		this.byteId = SimpleIDGenerator.getInstance(Event.class).nextBytes();
		return this;
	}

	@Override
	public ByteId getByteId() {
		return byteId;
	}

	@Override
	public void setByteId(ByteId id) {
		this.byteId = id;
	}

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	@XmlTransient
	public Calendar getDateTime() {
		return getStartDateTime();
	}

	public void setDateTime(Calendar dateTime) {
		this.setStartDateTime(dateTime);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@XmlJavaTypeAdapter(CalendarAdapter.class)
	public Calendar getRsvpDeadline() {
		return rsvpDeadline;
	}

	public void setRsvpDeadline(Calendar rsvpDeadline) {
		this.rsvpDeadline = rsvpDeadline;
	}

	public int getMaxPeople() {
		return maxPeople;
	}

	public void setMaxPeople(int maxPeople) {
		this.maxPeople = maxPeople;
	}

	@XmlJavaTypeAdapter(CalendarAdapter.class)
	public Calendar getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(Calendar endDateTime) {
		this.endDateTime = endDateTime;
	}

	public Event maxPeople(int maxPeople) {
		this.maxPeople = maxPeople;
		return this;
	}

	public Event price(BigDecimal price) {
		this.price = price;
		return this;
	}

	public Event price(double price) {
		this.price = new BigDecimal(price);
		return this;
	}

	public Event rsvpDeadline(Calendar rsvpDeadline) {
		this.rsvpDeadline = DateUtils.truncate(rsvpDeadline, Calendar.MINUTE);
		return this;
	}

	public Event startDateTime(Calendar startDateTime) {
		this.setStartDateTime(startDateTime);
		return this;
	}

	public Event endDateTime(Calendar endDateTime) {
		this.endDateTime = DateUtils.truncate(endDateTime, Calendar.MINUTE);
		return this;
	}

	public Event website(String website) {
		this.website = website;
		return this;
	}

	public Event description(String description) {
		this.description = description;
		return this;
	}

	public Event activity(Activity activity) {
		this.activity = activity;
		return this;
	}

	public Event id(ByteId id) {
		this.byteId = id;
		return this;
	}

	public int hashCode() {
		return new HashCodeBuilder().append(activity)
				.append(getStartDateTime()).append(endDateTime)
				.append(description).append(website).append(price)
				.append(rsvpDeadline).append(maxPeople).toHashCode();
	}

	public String toString() {
		return activity.toString() + "\n\n" + description;
	}

	public boolean refEquals(Event that) {
		ByteId thisaid = this.activity == null ? null : this.activity
				.getByteId();
		ByteId thataid = that.activity == null ? null : that.activity
				.getByteId();
		if (new EqualsBuilder().append(thisaid, thataid).isEquals()) {
			return restEquals(that);
		}
		return false;
	}

	private boolean restEquals(Event that) {
		EqualsBuilder eb = new EqualsBuilder();
		eb.append(this.getStartDateTime(), that.getStartDateTime())
				.append(this.endDateTime, that.endDateTime)
				.append(this.description, that.description)
				.append(this.website, that.website)
				.append(this.price, that.price)
				.append(this.rsvpDeadline, that.rsvpDeadline)
				.append(this.maxPeople, that.maxPeople);
		return eb.isEquals();
	}

	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof Event)) {
			return false;
		}
		Event that = (Event) o;
		if (new EqualsBuilder().append(this.activity, that.activity).isEquals()) {
			return restEquals(that);
		}
		return false;
	}

	@Override
	public void setByteId(byte[] id) {
		this.byteId = new ByteId(id);
	}

	@XmlTransient
	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}

	@XmlJavaTypeAdapter(CalendarAdapter.class)
	public Calendar getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(Calendar startDateTime) {
		this.startDateTime = DateUtils.truncate(startDateTime, Calendar.MINUTE);
	}
}