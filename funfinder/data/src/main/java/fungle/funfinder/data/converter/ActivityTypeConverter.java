package fungle.funfinder.data.converter;

import org.apache.commons.collections4.BidiMap;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;

import fungle.common.hdao.ResultConverter;
import fungle.common.hdao.model.Column;
import fungle.funfinder.data.entity.ActivityType;
import fungle.funfinder.data.util.FeelingVectorConverter;

public class ActivityTypeConverter implements ResultConverter<ActivityType> {
	private static final FeelingVectorConverter feelingVectorConverter = new FeelingVectorConverter();

	@Override
	public ActivityType toEntity(Result result, BidiMap<String, Column> mapping) {
		ActivityType t = new ActivityType();
		return mergeInto(t,result,mapping);
	}

	@Override
	public ActivityType mergeInto(ActivityType entity, Result result,
			BidiMap<String, Column> mapping) {
		entity.setByteId(result.getRow());
		Column c = mapping.get("name");
		entity.setName(Bytes.toString(result.getValue(c.getColumnFamily(), c.getColumnQualifier())));
		c = mapping.get("feelingVector");
		entity.setFeeling(feelingVectorConverter.fromBytes(result.getValue(c.getColumnFamily(), c.getColumnQualifier())));
		return entity;
	}

	@Override
	public Put toPut(ActivityType entity, BidiMap<String, Column> mapping) {
		Put put = new Put(entity.getByteId().getByteId());
		Column c = mapping.get("name");
		put.add(c.getColumnFamily(), c.getColumnQualifier(), Bytes.toBytes(entity.getName()));
		c = mapping.get("feelingVector");
		put.add(c.getColumnFamily(), c.getColumnQualifier(), feelingVectorConverter.toBytes(entity.getFeeling()));
		return put;
	}

}
