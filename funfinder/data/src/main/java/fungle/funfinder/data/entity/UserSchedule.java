package fungle.funfinder.data.entity;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.ByteIdentifiable;

public class UserSchedule implements ByteIdentifiable {

	private ByteId byteId;
	private List<UserEvent> events;

	public UserSchedule() {
		this.events = new ArrayList<UserEvent>();
	}

	public UserSchedule(List<UserEvent> events) {
		this.events = events;
	}

	public UserSchedule(UserSchedule schedule) {
		this.byteId =schedule.getByteId();
		this.events = new ArrayList<>(schedule.events);
	}

	@Override
	public ByteId getByteId() {
		return byteId;
	}

	@Override
	public void setByteId(byte[] id) {
		this.byteId=new ByteId(id);
	}

	@Override
	public void setByteId(ByteId id) {
		this.byteId =id;
	}

	public List<UserEvent> getEvents() {
		return events;
	}

	public void setEvents(List<UserEvent> events) {
		this.events = events;
	}

	public UserSchedule id(ByteId id) {
		this.byteId =id;
		return this;
	}

	public UserSchedule userEvent(UserEvent event) {
		if (this.events == null) {
			this.events = new ArrayList<>();
		}
		this.events.add(event);
		return this;
	}

	public int hashCode() {
		return new HashCodeBuilder().append(events).toHashCode();
	}

	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof UserSchedule)) {
			return false;
		}
		UserSchedule that = (UserSchedule) o;
		return new EqualsBuilder().append(this.events, that.events).isEquals();
	}
}