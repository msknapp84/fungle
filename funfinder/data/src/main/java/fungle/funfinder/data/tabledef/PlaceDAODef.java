package fungle.funfinder.data.tabledef;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;
import org.apache.hadoop.hbase.client.HTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fungle.common.hdao.BaseDAODefinition;
import fungle.common.hdao.ByteIdentifiableDAO;
import fungle.common.hdao.model.BasicIndex;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.Column;
import fungle.common.hdao.model.EntityReference;
import fungle.common.hdao.model.Index;
import fungle.common.hdao.util.TableNameService;
import fungle.funfinder.data.dao.ActivityDAO;
import fungle.funfinder.data.dao.UserDAO;
import fungle.funfinder.data.def.PlaceDef;
import fungle.funfinder.data.entity.Place;
import fungle.funfinder.data.geo.GeoFinder;
import fungle.funfinder.data.geo.GeoIndex;

@Component("placeDAODef")
public class PlaceDAODef extends BaseDAODefinition<Place> {
	private GeoFinder finder;

	private ActivityDAO activityDAO;
	private UserDAO userDAO;

	private Index<Place> creatorIndex;
	private Index<Place> nameIndex;
	private Index<Place> coordinateIndex;
	private Index<Place> typeIndex;
	private Index<Place> businessTypeIndex;

	@Autowired
	public PlaceDAODef(TableNameService tableNameService, GeoFinder finder) {
		super(new PlaceDef(), tableNameService);
		this.finder = finder;
		tableNameService.get(this).ensureTable();
	}

	private void lazyBuildIndexes() {
		if (this.indexes == null || this.indexes.isEmpty()) {
			getIndexes();
		}
	}

	@Override
	public List<Index<Place>> buildIndexes() {
		// index on name, coordinate, type, businessType
		HTable nameIndexTable = tableNameService.get(this).ensureIndexTable(
				"name");
		HTable typeIndexTable = tableNameService.get(this).ensureIndexTable(
				"type");
		HTable businessTypeIndexTable = tableNameService.get(this)
				.ensureIndexTable("business_type");
		HTable coordinateIndexTable = tableNameService.get(this)
				.ensureIndexTable("coordinate");
		HTable creatorIndexTable = tableNameService.get(this).ensureIndexTable(
				"creator");
		nameIndex = BasicIndex.builder().ascending("name")
				.indexTable(nameIndexTable).referenceTable(getTable())
				.unique(false).build();
		typeIndex = BasicIndex.builder().ascending("type")
				.indexTable(typeIndexTable).referenceTable(getTable())
				.unique(false).build();
		businessTypeIndex = BasicIndex.builder().ascending("businessType")
				.indexTable(businessTypeIndexTable).referenceTable(getTable())
				.unique(false).build();
		coordinateIndex = GeoIndex.forTables(coordinateIndexTable, getTable())
				.finder(getFinder()).coordinateFieldName("coordinate")
				.build(Place.class);
		creatorIndex = BasicIndex.<Place> builder().referenceTable(getTable())
				.indexTable(creatorIndexTable).ascending("creator").build();
		return Arrays.asList(nameIndex, typeIndex, businessTypeIndex,
				coordinateIndex, creatorIndex);
	}

	@Override
	public BidiMap<String, Column> buildMapping() {
		BidiMap<String, Column> mapping = new DualHashBidiMap<String, Column>();
		mapping.put("name", Column.from("f", "n", false));
		mapping.put("type", Column.from("f", "t", false));
		mapping.put("businessType", Column.from("f", "b", false));
		mapping.put("description", Column.from("f", "d", false));
		mapping.put("coordinate", Column.from("f", "c", false));
		mapping.put("address", Column.from("f", "a", false));
		mapping.put("creator", Column.from("f", "u", false));
		return mapping;
	}

	public GeoFinder getFinder() {
		return finder;
	}

	@Autowired
	public void setFinder(GeoFinder finder) {
		this.finder = finder;
	}

	@Override
	public <X extends ByteIdentifiable> ByteIdentifiableDAO<X> getIncomingDAO(
			EntityReference<X, Place> reference) {
		// activity is the only thing that refers to a place.
		return (ByteIdentifiableDAO<X>) getActivityDAO();
	}

	@Override
	public <X extends ByteIdentifiable> ByteIdentifiableDAO<X> getOutgoingDAO(
			EntityReference<Place, X> reference) {
		// user is the only dao that place refers to.
		return (ByteIdentifiableDAO<X>) userDAO;
	}

	@Override
	public Index<Place> getIndexForReference(EntityReference<Place, ?> reference) {
		lazyBuildIndexes();
		// the only reference possible is the creator.
		return creatorIndex;
	}

	public ActivityDAO getActivityDAO() {
		return activityDAO;
	}

	@Autowired
	public void setActivityDAO(ActivityDAO activityDAO) {
		this.activityDAO = activityDAO;
	}

	public UserDAO getUserDAO() {
		return userDAO;
	}

	@Autowired
	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@Override
	public Index<Place> getIndexByFieldName(String... fieldNames) {
		getIndexes();
		if ("name".equals(fieldNames[0])) {
			return nameIndex;
		} else if ("type".equals(fieldNames[0])) {
			return typeIndex;
		} else if ("businessType".equals(fieldNames[0])) {
			return businessTypeIndex;
		} else if ("coordinate".equals(fieldNames[0])) {
			return coordinateIndex;
		} else if ("creator".equals(fieldNames[0])) {
			return creatorIndex;
		}
		return null;
	}
}