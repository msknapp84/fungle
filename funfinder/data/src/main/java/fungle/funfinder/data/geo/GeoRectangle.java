package fungle.funfinder.data.geo;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class GeoRectangle {
	private BigDecimal eastLongitude;
	private BigDecimal westLongitude;
	private BigDecimal southLatitude;
	private BigDecimal northLatitude;

	public GeoRectangle() {

	}

	public GeoRectangle(GeoRectangle other) {
		this.eastLongitude = other.eastLongitude;
		this.westLongitude = other.westLongitude;
		this.southLatitude = other.southLatitude;
		this.northLatitude = other.northLatitude;
		validate();
	}

	public GeoRectangle(BigDecimal north, BigDecimal south, BigDecimal east,
			BigDecimal west) {
		this.eastLongitude = east;
		this.westLongitude = west;
		this.northLatitude = north;
		this.southLatitude = south;
		validate();
	}
	
	private void validate() {
		if (eastLongitude.doubleValue()<=westLongitude.doubleValue()) {
			throw new IllegalArgumentException("longitude for west must be less than east.");
		}
		if (southLatitude.doubleValue()>=northLatitude.doubleValue()) {
			throw new IllegalArgumentException("latitude for south must be less than north.");
		}
		if (eastLongitude.doubleValue()>180) {
			throw new IllegalArgumentException("longitude must be less than 180");
		}
		if (westLongitude.doubleValue()<-180) {
			throw new IllegalArgumentException("longitude must be greater than -180");
		}
		if (northLatitude.doubleValue()>90) {
			throw new IllegalArgumentException("latitude must be less than 90");
		}
		if (southLatitude.doubleValue()<-90) {
			throw new IllegalArgumentException("latitude must be greater than -90");
		}
	}

	public BigDecimal getEastLongitude() {
		return eastLongitude;
	}

	public void setEastLongitude(BigDecimal eastLongitude) {
		this.eastLongitude = eastLongitude;
	}

	public BigDecimal getWestLongitude() {
		return westLongitude;
	}

	public void setWestLongitude(BigDecimal westLongitude) {
		this.westLongitude = westLongitude;
	}

	public BigDecimal getSouthLatitude() {
		return southLatitude;
	}

	public void setSouthLatitude(BigDecimal southLatitude) {
		this.southLatitude = southLatitude;
	}

	public BigDecimal getNorthLatitude() {
		return northLatitude;
	}

	public void setNorthLatitude(BigDecimal northLatitude) {
		this.northLatitude = northLatitude;
	}

	public GeoCoordinate getSouthEast() {
		return new GeoCoordinate(this.southLatitude, this.eastLongitude);
	}

	public GeoCoordinate getSouthWest() {
		return new GeoCoordinate(this.southLatitude, this.westLongitude);
	}

	public GeoCoordinate getNorthWest() {
		return new GeoCoordinate(this.northLatitude, this.westLongitude);
	}

	public GeoCoordinate getNorthEast() {
		return new GeoCoordinate(this.northLatitude, this.eastLongitude);
	}

	public GeoCoordinate getCenter() {
		BigDecimal lat = northLatitude.add(southLatitude).divide(new BigDecimal(2),RoundingMode.HALF_UP);
		BigDecimal lng = eastLongitude.add(westLongitude).divide(new BigDecimal(2),RoundingMode.HALF_UP);
		return new GeoCoordinate(lat,lng);
	}

	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof GeoRectangle)) {
			return false;
		}
		GeoRectangle that = (GeoRectangle) o;
		return new EqualsBuilder()
				.append(this.eastLongitude, that.eastLongitude)
				.append(this.westLongitude, that.westLongitude)
				.append(this.northLatitude, that.northLatitude)
				.append(this.southLatitude, that.southLatitude).isEquals();
	}

	public int hashCode() {
		return new HashCodeBuilder().append(this.eastLongitude)
				.append(westLongitude).append(northLatitude)
				.append(southLatitude).toHashCode();
	}

	public String toString() {
		return String.format(
				"From %f west to %f east, from %f south to %f north",
				this.westLongitude.doubleValue(),
				this.eastLongitude.doubleValue(),
				this.southLatitude.doubleValue(),
				this.northLatitude.doubleValue());
	}

	public boolean contains(GeoCoordinate coordinate) {
		return (this.southLatitude.doubleValue()<=coordinate.getLatitude().doubleValue() && 
				this.northLatitude.doubleValue()>coordinate.getLatitude().doubleValue() && 
				this.westLongitude.doubleValue()<=coordinate.getLongitude().doubleValue() && 
				this.eastLongitude.doubleValue()>coordinate.getLongitude().doubleValue());
	}
}