package fungle.funfinder.data.def;

import java.util.Collections;
import java.util.Set;

import fungle.common.hdao.BaseEntityDefinition;
import fungle.common.hdao.model.BasicEntityReference;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.EntityDefinition;
import fungle.common.hdao.model.EntityReference;
import fungle.common.hdao.model.EntityReferenceConverter;
import fungle.funfinder.data.entity.ActivityType;
import fungle.funfinder.data.entity.User;

public class ActivityTypeDef extends BaseEntityDefinition<ActivityType>
		implements EntityDefinition<ActivityType> {
	public static final EntityReference<ActivityType, User> USERREF = BasicEntityReference
			.<ActivityType, User> builder().from(ActivityType.class)
			.to(User.class).manyToOne().notRequired()
			.converter(new ActivityTypeCreatorConverter()).build();

	public ActivityTypeDef() {
		super(ActivityType.class);
	}

	@Override
	public Set<EntityReference<?, ActivityType>> getIncomingReferences() {
		return Collections.singleton(ActivityDef.TYPEREF);
	}

	@Override
	public Set<EntityReference<ActivityType, ?>> getOutgoingReferences() {
		return Collections.singleton(USERREF);
	}

	public static class ActivityTypeCreatorConverter implements
			EntityReferenceConverter<ActivityType, User> {

		@Override
		public User getForeignEntity(ActivityType f) {
			return f==null?null:f.getCreator();
		}

		@Override
		public void setForeignEntity(ActivityType f, ByteIdentifiable t) {
			if (f!=null) {
				f.setCreator((User) t);
			}
		}

		@Override
		public User newForeignInstance() {
			return new User();
		}

		@Override
		public ActivityType newLocalInstance() {
			return new ActivityType();
		}
	}
}