package fungle.funfinder.data.dao;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogTest {
	public static final Logger logger = LoggerFactory.getLogger(LogTest.class);
	
	@Test
	public void test() {
		logger.warn("hello");
	}
}
