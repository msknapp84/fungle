package fungle.funfinder.data.dao.hbase;

import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.OnError;
import fungle.common.hdao.util.FetchUtils;
import fungle.funfinder.data.dao.DataTestBase;
import fungle.funfinder.data.entity.ActivityType;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:funfinder.data-appContext.xml"})
@ActiveProfiles("test")
public class ActivityTypeHBaseDAOTest {
	
	@Autowired
	private ActivityTypeHBaseDAO dao;

	@Test
	public void test() {
		System.out.println("hello");
	}
	
	@Test
	public void getAll() {
		Map<ByteId, ActivityType> activityTypes = dao.getAll(FetchUtils.always(), OnError.ATTEMPT_UNDO, null, null);
		Assert.assertNotNull(activityTypes);
	}
}
