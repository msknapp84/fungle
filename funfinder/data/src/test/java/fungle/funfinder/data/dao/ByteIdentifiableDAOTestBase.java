package fungle.funfinder.data.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;

import fungle.common.hdao.ByteIdentifiableDAO;
import fungle.common.hdao.model.ByteId;
import fungle.common.hdao.model.ByteIdentifiable;
import fungle.common.hdao.model.OnError;
import fungle.common.hdao.util.CascadeUtils;
import fungle.common.hdao.util.FetchUtils;

public abstract class ByteIdentifiableDAOTestBase<T extends ByteIdentifiable> extends DataTestBase {
	
	@Test
	public void crud() {
		List<T> entities = getNonPersistentEntities();
		List<ByteId> ids = getByteIds(entities);
		try {
			Map<ByteId,T> initially = getDAO().get(ids,FetchUtils.always());
			Assert.assertTrue(areEntitiesEmpty(initially));
			getDAO().delete(entities,CascadeUtils.never(),OnError.CONTINUE);
			getDAO().save(entities,CascadeUtils.never(),OnError.CONTINUE);
			Map<ByteId,T> found = getDAO().get(ids,FetchUtils.always());
			Assert.assertTrue(CollectionUtils.isEqualCollection(found.values(),entities));
		} finally {
			getDAO().delete(entities,CascadeUtils.never(),OnError.CONTINUE);
			Map<ByteId,T> initially = getDAO().get(ids,FetchUtils.always());
			Assert.assertTrue(areEntitiesEmpty(initially));
		}
	}
	
	public boolean areEntitiesEmpty(Map<ByteId,T> initially) {
		Set<T> ents = new HashSet<>(initially.values());
		ents.remove(null);
		return ents.isEmpty();
	}
	
	public List<ByteId> getByteIds(Collection<T> entities) {
		List<ByteId> ids = new ArrayList<>();
		entities.forEach(e -> ids.add(e.getByteId()));
		return ids;
	}
	
	public abstract ByteIdentifiableDAO<T> getDAO();
	public abstract List<T> getNonPersistentEntities();
}
