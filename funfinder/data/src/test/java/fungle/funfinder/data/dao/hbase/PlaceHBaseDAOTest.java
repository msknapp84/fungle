package fungle.funfinder.data.dao.hbase;

import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fungle.common.hdao.model.OnError;
import fungle.common.hdao.util.CascadeUtils;
import fungle.common.hdao.util.FetchUtils;
import fungle.funfinder.data.dao.PlaceDAO;
import fungle.funfinder.data.entity.Address;
import fungle.funfinder.data.entity.Place;
import fungle.funfinder.data.geo.GeoCoordinate;

@ActiveProfiles("integration")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:appContext.xml" })
public class PlaceHBaseDAOTest {

	@Autowired
	private PlaceDAO placeDAO;
	
	@Test
	public void saveGet() {
		GeoCoordinate coordinate = GeoCoordinate.NEWYORK;
		Address address = Address.US("8390 HBase Street", "Shadow, Kentucky 12345");
		Place place = new Place().address(address).businessType("cloud computing")
				.type("information technology").description("does dumb cloud stuff.")
				.coordinate(coordinate).name("Cloud Building");
		Assert.assertNull(place.getByteId());
		placeDAO.deleteByName(place.getName());
		placeDAO.save(place, CascadeUtils.always(), OnError.CONTINUE);
		Assert.assertNotNull(place.getByteId());
		Place recovered = placeDAO.get(place.getByteId(), FetchUtils.always());
		Place recoveredByName = placeDAO.getByName(place.getName()).iterator().next();
		Assert.assertEquals(place,recovered);
		Assert.assertEquals(place,recoveredByName);
		
		placeDAO.deleteById(place.getByteId(), CascadeUtils.always(), OnError.CONTINUE);
		Place remaining = placeDAO.get(place.getByteId(), FetchUtils.always());
		Assert.assertNull(remaining);
		Set<Place> remainingByName = placeDAO.getByName(place.getName());
		Assert.assertTrue(CollectionUtils.isEmpty(remainingByName));
	}
}