package fungle.funfinder.data.geo;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class SimpleGeoFinderTest {
	private static GeoToolsRuler ruler = new GeoToolsRuler();
	private static SimpleGeoFinder finder = new SimpleGeoFinder(
			ruler,360,180);

	@Test
	public void getRectanglesNear() {
		Set<GeoRectangle> rs = finder.getRectanglesNear(GeoCoordinate.DC, new BigDecimal(20), GeoUnits.MILES);
		for (GeoRectangle r : rs) {
			double distMiles = ruler.getDistance(r.getCenter(), GeoCoordinate.DC, GeoUnits.MILES).doubleValue();
			// TODO check the distance to the closest point, not the center.
			Assert.assertTrue("real: "+distMiles,100>distMiles);
		}
	}

	@Test
	public void getRectangleIndexesNear() {
		Set<Integer> rs = finder.getRectangleIndexesNear(GeoCoordinate.DC, new BigDecimal(20), GeoUnits.MILES);
		for (Integer r : rs) {
			Assert.assertTrue(r>=0 && r < 360*180);
		}
	}

	@Test
	public void getRectangle() {
		GeoRectangle expected = new GeoRectangle(new BigDecimal(90), new BigDecimal(89), new BigDecimal(-179), new BigDecimal(-180));
		GeoRectangle actual = finder.getRectangle(new GeoCoordinate(new BigDecimal(89.5), new BigDecimal(-179.5)));
		Assert.assertEquals(expected,actual);
		
		expected = new GeoRectangle(new BigDecimal(90), new BigDecimal(89), new BigDecimal(-178), new BigDecimal(-179));
		actual = finder.getRectangle(new GeoCoordinate(new BigDecimal(89.5), new BigDecimal(-178.5)));
		Assert.assertEquals(expected,actual);
		
		expected = new GeoRectangle(new BigDecimal(-89), new BigDecimal(-90), new BigDecimal(180), new BigDecimal(179));
		actual = finder.getRectangle(new GeoCoordinate(new BigDecimal(-89.5), new BigDecimal(179.5)));
		Assert.assertEquals(expected,actual);
	}
	
	@Test
	public void getRectangleByIndex() {
		GeoRectangle expected = new GeoRectangle(new BigDecimal(90), new BigDecimal(89), new BigDecimal(-179), new BigDecimal(-180));
		Assert.assertEquals(expected, finder.getRectangle(0));
		
		expected = new GeoRectangle(new BigDecimal(90), new BigDecimal(89), new BigDecimal(-178), new BigDecimal(-179));
		Assert.assertEquals(expected, finder.getRectangle(1));

		expected = new GeoRectangle(new BigDecimal(90), new BigDecimal(89), new BigDecimal(180), new BigDecimal(179));
		Assert.assertEquals(expected, finder.getRectangle(359));
		
		expected = new GeoRectangle(new BigDecimal(89), new BigDecimal(88), new BigDecimal(-179), new BigDecimal(-180));
		Assert.assertEquals(expected, finder.getRectangle(360));
		
		expected = new GeoRectangle(new BigDecimal(-89), new BigDecimal(-90), new BigDecimal(180), new BigDecimal(179));
		Assert.assertEquals(expected, finder.getRectangle(360*180-1));
	}

	@Test
	public void getLongitudeIndex() {
		Assert.assertEquals(0,finder.getLongitudeIndex(new BigDecimal(-180)));
		Assert.assertEquals(0,finder.getLongitudeIndex(new BigDecimal(-179.999)));
		Assert.assertEquals(0,finder.getLongitudeIndex(new BigDecimal(-179.0001)));
		Assert.assertEquals(1,finder.getLongitudeIndex(new BigDecimal(-179)));
		Assert.assertEquals(1,finder.getLongitudeIndex(new BigDecimal(-178.0001)));
		Assert.assertEquals(2,finder.getLongitudeIndex(new BigDecimal(-178)));
		Assert.assertEquals(358,finder.getLongitudeIndex(new BigDecimal(178.999)));
		Assert.assertEquals(359,finder.getLongitudeIndex(new BigDecimal(179)));
		Assert.assertEquals(359,finder.getLongitudeIndex(new BigDecimal(179.999)));
		Assert.assertEquals(0,finder.getLongitudeIndex(new BigDecimal(180)));
	}

	@Test
	public void getLatitudeIndex() {
		Assert.assertEquals(179,finder.getLatitudeIndex(new BigDecimal(90)));
		Assert.assertEquals(179,finder.getLatitudeIndex(new BigDecimal(89)));
		Assert.assertEquals(178,finder.getLatitudeIndex(new BigDecimal(88.999)));
		Assert.assertEquals(0,finder.getLatitudeIndex(new BigDecimal(-90)));
		Assert.assertEquals(0,finder.getLatitudeIndex(new BigDecimal(-89.5)));
		Assert.assertEquals(0,finder.getLatitudeIndex(new BigDecimal(-89.0001)));
		Assert.assertEquals(1,finder.getLatitudeIndex(new BigDecimal(-89)));
	}

	@Test
	public void getIndex() {
		GeoRectangle rectangle = new GeoRectangle(new BigDecimal(90), new BigDecimal(89), new BigDecimal(-179), new BigDecimal(-180));
		Assert.assertEquals(0,finder.getIndex(rectangle));
		rectangle = new GeoRectangle(new BigDecimal(90), new BigDecimal(89), new BigDecimal(-178), new BigDecimal(-179));
		Assert.assertEquals(1,finder.getIndex(rectangle));
		rectangle = new GeoRectangle(new BigDecimal(90), new BigDecimal(89), new BigDecimal(180), new BigDecimal(179));
		Assert.assertEquals(359,finder.getIndex(rectangle));
		rectangle = new GeoRectangle(new BigDecimal(89), new BigDecimal(88), new BigDecimal(-179), new BigDecimal(-180));
		Assert.assertEquals(360,finder.getIndex(rectangle));
		rectangle = new GeoRectangle(new BigDecimal(-89), new BigDecimal(-90), new BigDecimal(180), new BigDecimal(179));
		Assert.assertEquals(360*180-1,finder.getIndex(rectangle));
	}

	@Test
	public void getMaximumRectangles() {
		Assert.assertEquals(360*180,finder.getMaximumRectangles());
	}
}