package fungle.funfinder.data.geo;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;

public class GeoToolsRulerTest {
	private static GeoToolsRuler ruler = new GeoToolsRuler();
	private static BigDecimal TEN = new BigDecimal(10);
	
	@Test
	public void test() {
		BigDecimal d = ruler.getDistance(GeoCoordinate.DC, GeoCoordinate.NEWYORK, GeoUnits.MILES);
		Assert.assertTrue(d.doubleValue()>0);
	}
	
	@Test
	public void testAgreement() {
		GeoCoordinate c = ruler.getCoordinate(GeoCoordinate.DC, TEN, GeoUnits.MILES, GeoDirection.EAST);
		BigDecimal d = ruler.getDistance(GeoCoordinate.DC, c, GeoUnits.MILES);
		// unfortunately the getCoordinate function is not very accurate.
		Assert.assertTrue(d.toString(),9.9<d.doubleValue() && d.doubleValue()<10.1);
	}
	
	@Test
	public void getCoordinate() {
		GeoCoordinate c = ruler.getCoordinate(GeoCoordinate.DC, TEN, GeoUnits.MILES, GeoDirection.NORTH);
		Assert.assertEquals(GeoCoordinate.DC.getLongitude(), c.getLongitude());
		Assert.assertTrue(c.getLatitude()+" vs. "+GeoCoordinate.DC.getLatitude(),c.getLatitude().doubleValue()>GeoCoordinate.DC.getLatitude().doubleValue());
		
		c = ruler.getCoordinate(GeoCoordinate.DC, TEN, GeoUnits.MILES, GeoDirection.SOUTH);
		Assert.assertEquals(GeoCoordinate.DC.getLongitude(), c.getLongitude());
		Assert.assertTrue(c.getLatitude().doubleValue()<GeoCoordinate.DC.getLatitude().doubleValue());
		
		c = ruler.getCoordinate(GeoCoordinate.DC, TEN, GeoUnits.MILES, GeoDirection.EAST);
		Assert.assertEquals(GeoCoordinate.DC.getLatitude(), c.getLatitude());
		Assert.assertTrue(c.getLongitude().doubleValue()>GeoCoordinate.DC.getLongitude().doubleValue());
		
		c = ruler.getCoordinate(GeoCoordinate.DC, TEN, GeoUnits.MILES, GeoDirection.WEST);
		Assert.assertEquals(GeoCoordinate.DC.getLatitude(), c.getLatitude());
		Assert.assertTrue(c.getLongitude().doubleValue()<GeoCoordinate.DC.getLongitude().doubleValue());
	}
	
	@Test
	public void getCoordinateMiles() {
		GeoCoordinate c = ruler.getCoordinate(GeoCoordinate.DC, TEN, GeoUnits.MILES, GeoDirection.NORTH);
		GeoCoordinate c2 = ruler.getCoordinate(GeoCoordinate.DC, TEN, GeoUnits.KILOMETERS, GeoDirection.NORTH);
		Assert.assertEquals(c.getLongitude(),c2.getLongitude());
		Assert.assertTrue(c.getLatitude().doubleValue()>c2.getLatitude().doubleValue());
	}
	
	@Test
	public void getCoordinateKilometers() {
		GeoCoordinate c = ruler.getCoordinate(GeoCoordinate.DC, new BigDecimal(10000), GeoUnits.METERS, GeoDirection.EAST);
		GeoCoordinate c2 = ruler.getCoordinate(GeoCoordinate.DC, TEN, GeoUnits.KILOMETERS, GeoDirection.EAST);
		Assert.assertEquals(c,c2);

		c = ruler.getCoordinate(GeoCoordinate.DC, new BigDecimal(10001), GeoUnits.METERS, GeoDirection.EAST);
		c2 = ruler.getCoordinate(GeoCoordinate.DC, TEN, GeoUnits.KILOMETERS, GeoDirection.EAST);
		Assert.assertNotSame(c,c2);
	}
}
