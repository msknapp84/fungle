//package fungle.funfinder.data.dao.mem;
//
//import java.util.Arrays;
//import java.util.List;
//
//import fungle.common.hdao.model.SimpleIDGenerator;
//import fungle.funfinder.data.dao.ByteIdentifiableDAOTestBase;
//import fungle.funfinder.data.entity.User;
//
//public class UserMemDAOTest extends ByteIdentifiableDAOTestBase<User> {
//
//	private UserMemDAO dao = new UserMemDAO();
//	
//	@Override
//	public UserMemDAO getDAO() {
//		return dao;
//	}
//
//	@Override
//	public List<User> getNonPersistentEntities() {
//		SimpleIDGenerator gen = SimpleIDGenerator.getInstance(User.class);
//		User u1 = new User();
//		u1.setFirstName("Mark");
//		u1.setLastName("Hammel");
//		u1.setByteId(gen.nextBytes());
//		User u2 = new User();
//		u2.setFirstName("Jim");
//		u2.setLastName("Lovell");
//		u2.setByteId(gen.nextBytes());
//		User u3 = new User();
//		u3.setFirstName("Bob");
//		u3.setLastName("Dole");
//		u3.setByteId(gen.nextBytes());
//		return Arrays.asList(u1,u2,u3);
//	}
//}