package fungle.funfinder.data.util;

import org.junit.Assert;
import org.junit.Test;

import fungle.funfinder.data.FeelingVector;

public class FeelingVectorConverterTest {

	@Test
	public void test1() {
		int b = FeelingVectorConverter.mergeTwo(15,3);
		int[] bs = FeelingVectorConverter.splitTwo(b);
		Assert.assertEquals(15,bs[0]);
		Assert.assertEquals(3,bs[1]);
	}
	@Test
	public void test2() {
		byte b = FeelingVectorConverter.mergeToByte(15,3);
		int[] bs = FeelingVectorConverter.splitFromByte(b);
		Assert.assertEquals(15,bs[0]);
		Assert.assertEquals(3,bs[1]);
	}

	@Test
	public void test() {
		FeelingVector fv = new FeelingVector();
		fv.setExpenseRating((byte)8);
		fv.setOutdoorsy(true);
		fv.setSocialRating((byte)15);
		fv.setRecreationRating((byte)2);
		fv.setRiskRating((byte) 5);
		fv.setTimeConsumptionRating((byte) 9);
		FeelingVectorConverter feelingVectorConverter = new FeelingVectorConverter();
		byte[] bs = feelingVectorConverter.toBytes(fv);
		FeelingVector rec = feelingVectorConverter.fromBytes(bs);
		Assert.assertEquals(fv,rec);
	}
}