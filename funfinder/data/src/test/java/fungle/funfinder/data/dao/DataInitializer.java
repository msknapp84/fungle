package fungle.funfinder.data.dao;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.ApplicationContext;

import fungle.common.hdao.model.Cascade;
import fungle.common.hdao.model.OnError;
import fungle.common.hdao.util.CascadeUtils;
import fungle.funfinder.data.FeelingVector;
import fungle.funfinder.data.entity.Activity;
import fungle.funfinder.data.entity.ActivityType;
import fungle.funfinder.data.entity.Address;
import fungle.funfinder.data.entity.Event;
import fungle.funfinder.data.entity.Place;
import fungle.funfinder.data.entity.User;
import fungle.funfinder.data.geo.GeoCoordinate;

public class DataInitializer {
	
	private static final Map<ApplicationContext,Boolean> initialized = new HashMap<>();
	
	public static void lazyInit(ApplicationContext appContext) {
		Boolean init = initialized.get(appContext);
		if (init!=null && init) {
			return;
		}
		initialized.put(appContext, true);
		
		Map<String,Address> addresses=DataLoader.buildAddresses();
		Map<String,GeoCoordinate> coordinates = DataLoader.buildCoordinates();
		Map<String,FeelingVector> feelings= DataLoader.buildFeelings();
		Map<String,User> users = DataLoader.buildUsers(addresses, coordinates);
		Map<String,ActivityType> activityTypes = DataLoader.buildActivityTypes(users, feelings);
		Map<String,Place> places = DataLoader.buildPlaces(users, addresses, coordinates);
		Map<String,Activity> activities = DataLoader.buildActivities(users, activityTypes,places);
		Map<String,Event> events = DataLoader.buildEvents(users, activities);
		
		appContext.getBean(UserDAO.class).save(users.values(), CascadeUtils.never(), OnError.CONTINUE);
		appContext.getBean(ActivityTypeDAO.class).save(activityTypes.values(), CascadeUtils.never(), OnError.CONTINUE);
		appContext.getBean(PlaceDAO.class).save(places.values(), CascadeUtils.never(), OnError.CONTINUE);
		appContext.getBean(ActivityDAO.class).save(activities.values(), CascadeUtils.never(), OnError.CONTINUE);
		appContext.getBean(EventDAO.class).save(events.values(), CascadeUtils.never(), OnError.CONTINUE);
	}
}
