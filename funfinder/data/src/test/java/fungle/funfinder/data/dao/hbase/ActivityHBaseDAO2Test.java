package fungle.funfinder.data.dao.hbase;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fungle.common.hdao.model.OnError;
import fungle.common.hdao.util.CascadeUtils;
import fungle.common.hdao.util.FetchUtils;
import fungle.funfinder.data.FeelingVector;
import fungle.funfinder.data.GeoSearch;
import fungle.funfinder.data.dao.ActivityDAO;
import fungle.funfinder.data.dao.ActivityTypeDAO;
import fungle.funfinder.data.dao.EventDAO;
import fungle.funfinder.data.dao.PlaceDAO;
import fungle.funfinder.data.entity.Activity;
import fungle.funfinder.data.entity.ActivityType;
import fungle.funfinder.data.entity.Address;
import fungle.funfinder.data.entity.Event;
import fungle.funfinder.data.entity.Place;
import fungle.funfinder.data.geo.GeoCoordinate;
import fungle.funfinder.data.geo.GeoUnits;

@ActiveProfiles("integration")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:appContext.xml" })
public class ActivityHBaseDAO2Test {
	private static final Logger logger = LoggerFactory.getLogger(ActivityHBaseDAO2Test.class);

	@Autowired
	private PlaceDAO placeDAO;
	
	@Autowired
	private ActivityDAO activityDAO;
	
	@Autowired
	private ActivityTypeDAO activityTypeDAO;
	
	@Autowired
	private EventDAO eventDAO;
	
	@Test
	public void saveGet() {
		GeoCoordinate coordinate = GeoCoordinate.NEWYORK;
		Address address = Address.US("8390 HBase Street", "Shadow, Kentucky 12345");
		Place place = new Place().address(address).businessType("cloud computing")
				.type("information technology").description("does dumb cloud stuff.")
				.coordinate(coordinate).name("Cloud Building");
		placeDAO.deleteByName(place.getName());
		Assert.assertTrue(placeDAO.getByName(place.getName()).isEmpty());
		
		FeelingVector feeling = new FeelingVector().expenseRating(4).riskRating(8)
				.outdoorsy(true).socialRating(6);
		ActivityType type = new ActivityType();
		type.setName("Test activity 89");
		type.setFeeling(feeling);
		
		activityTypeDAO.deleteByName(type.getName(), CascadeUtils.always());
		Assert.assertNull(activityTypeDAO.getByName(type.getName()));
		
		Activity activity = new Activity().name("Testing HBase").category(type).description("testing it.")
				.place(place).website("www.fubar.com");
		activityDAO.deleteByName(activity.getName(), CascadeUtils.always());
		Assert.assertNull(activityDAO.getByName(activity.getName(),FetchUtils.always()));
		
		Date sd = DateUtils.add(new Date(), Calendar.DAY_OF_MONTH, 5);
		Calendar sc= Calendar.getInstance();
		sc.setTime(sd);
		Event event = new Event().activity(activity).description("my testing event").startDateTime(sc);
		eventDAO.deleteByPlace(place, CascadeUtils.always());
		Assert.assertTrue(eventDAO.getByPlace(place, FetchUtils.never()).isEmpty());
		
		
		// now we do a cascading save
		eventDAO.save(event, CascadeUtils.always(), OnError.ATTEMPT_UNDO);
		
		
		// should have added the place
		Assert.assertNotNull(placeDAO.getByName(place.getName()));
		
		// should have added the type
		Assert.assertNotNull(activityTypeDAO.getByName(type.getName()));
		
		// should have added the activity
		Assert.assertNotNull(activityDAO.getByName(activity.getName(),FetchUtils.always()));
		
		// should have added the event.
		logger.warn("============================================================================");
		List<Event> found = eventDAO.getByPlace(place, FetchUtils.always());
		logger.warn("============================================================================");
		Assert.assertFalse(found.isEmpty());
		Event recovered = found.get(0);
		Assert.assertEquals(event, recovered);
		
		// verify that getting entities by geo coordinate works.
		GeoSearch search = new GeoSearch(GeoCoordinate.NEWYORK, new BigDecimal(10), GeoUnits.MILES);
		List<Event> es1 = eventDAO.getByActivityTypeNear(search, type, FetchUtils.always());
		Assert.assertTrue(es1.contains(event));
		
		// now we want to check that cascading deletes work
		activityDAO.delete(activity, CascadeUtils.always(), OnError.ATTEMPT_UNDO);

		// should have retained the place
		Assert.assertNotNull(placeDAO.getByName(place.getName()));
		
		// should have retained the type
		Assert.assertNotNull(activityTypeDAO.getByName(type.getName()));
		
		// should have deleted the activity
		Assert.assertNull(activityDAO.getByName(activity.getName(),FetchUtils.always()));
		
		// the event should have been deleted as well
		found = eventDAO.getByPlace(place, FetchUtils.never());
		Assert.assertTrue(found.isEmpty());
	}
}
