package fungle.funfinder.data.entity;

import org.junit.Assert;
import org.junit.Test;

public class AddressTest extends EntityTest<Address> {
	
	@Test
	public void construct() {
		Address a = Address.US("123 Emerald Drive","Boston, MA 12345");
		Assert.assertEquals(123,a.getNumber());
		Assert.assertEquals("Emerald Drive",a.getStreet1());
		Assert.assertEquals(null,a.getStreet2());
		Assert.assertEquals("Boston",a.getCity());
		Assert.assertEquals("MA",a.getState());
		Assert.assertEquals(12345,a.getZip());
		Assert.assertEquals(0,a.getSubZip());
		Assert.assertEquals("United States of America",a.getCountry());
		
		Address a2 = Address.US("123 Emerald Drive","Suite 100","Boston, MA 12345-678");
		Assert.assertEquals(123,a2.getNumber());
		Assert.assertEquals("Emerald Drive",a2.getStreet1());
		Assert.assertEquals("Suite 100",a2.getStreet2());
		Assert.assertEquals("Boston",a2.getCity());
		Assert.assertEquals("MA",a2.getState());
		Assert.assertEquals(12345,a2.getZip());
		Assert.assertEquals(678,a2.getSubZip());
		Assert.assertEquals("United States of America",a2.getCountry());
	}
	
	@Test
	public void equals() {
		Address a = Address.US("123 Emerald Drive","Boston, MA 12345");
		Address clone = new Address(a);
		Assert.assertEquals(a,clone);
		Assert.assertEquals(a.hashCode(),clone.hashCode());
		clone.city("Fubar");
		Assert.assertNotSame(a,clone);
		Assert.assertNotSame(a.hashCode(),clone.hashCode());
	}

	@Override
	public Address getMinimalEntity() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Address getFullEntity() {
		// TODO Auto-generated method stub
		return null;
	}

}
