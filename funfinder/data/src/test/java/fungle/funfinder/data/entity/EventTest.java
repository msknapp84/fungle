package fungle.funfinder.data.entity;

import java.util.Calendar;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Assert;
import org.junit.Test;

import fungle.funfinder.data.FeelingVector;
import fungle.funfinder.data.geo.GeoCoordinate;
import fungle.funfinder.data.util.JAXBUtil;

public class EventTest extends EntityTest<Event> {

	@Override
	public Event getMinimalEntity() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Event getFullEntity() {
		// TODO Auto-generated method stub
		return null;
	}

	@Test
	public void jaxb() {
		Address address=Address.US("523 Lovers lane","Elkmont, Radbury 82563");
		Place place = new Place().address(address).businessType("fubar").type("snafu").name("lol").coordinate(GeoCoordinate.DC)
				.description("blah blah").defaultId();
		FeelingVector feeling = new FeelingVector().expenseRating(4).outdoorsy(false).timeConsumptionRating(8);
		ActivityType cat = new ActivityType().feeling(feeling).name("my category");
		Activity activity =new Activity().category(cat).name("playing").place(place).description("playing").website("www.snafu.com")
				.defaultId();
		Calendar rsvpDeadline = Calendar.getInstance();
		rsvpDeadline.setTime(DateUtils.addDays(rsvpDeadline.getTime(), 8));
		Event event = new Event().activity(activity).defaultId().description("blah").maxPeople(8).rsvpDeadline(rsvpDeadline)
				.price(3);
		String text = JAXBUtil.marshal(event);
		Event recovered = JAXBUtil.unmarshal(text);
		Assert.assertEquals(event, recovered);
	}
	
}
