package fungle.funfinder.data.dao.hbase;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import fungle.common.hdao.util.FetchUtils;
import fungle.funfinder.data.FeelingVector;
import fungle.funfinder.data.dao.DataTestBase;
import fungle.funfinder.data.entity.Activity;
import fungle.funfinder.data.entity.ActivityType;
import fungle.funfinder.data.entity.Address;
import fungle.funfinder.data.entity.Place;

public class ActivityHBaseDAOTest extends DataTestBase {

	@Autowired
	private ActivityHBaseDAO activityDAO;
	
	@Autowired
	private ActivityTypeHBaseDAO activityTypeDAO;

	@Autowired
	private PlaceHBaseDAO placeDAO;
	
	
//	creator=creator
//			category=Clubbing
//			name=cyan
//			place=jibberish
//			description=
//			website=www.lwsejr.com
	@Test
	public void getUsingIndex(){
		String activityName = "cyan";
		Set<Activity> activities = activityDAO.getByName(activityName,FetchUtils.always());
		Activity activity = activities.iterator().next();
		Assert.assertEquals("cyan", activity.getName());
		Assert.assertEquals("www.lwsejr.com", activity.getWebsite());
		
		ActivityType type = activity.getCategory();
		Assert.assertEquals("Clubbing",type.getName());
	}
	
	@Test
	public void cascadingDeletes() {
		
	}

//	public boolean doInit() {
//		return false;
//	}
	
	@Test
	public void cascadingPersistence() {
		
	}
	
	

	public List<Activity> getNonPersistentEntities() {
		Activity activity = new Activity();
		ActivityType category = new ActivityType();
//		category.setByteId(g.nextBytes());
		category.setName("running");
		FeelingVector fv = new FeelingVector();
		fv.setExpenseRating((byte) 0);
		fv.setOutdoorsy(true);
		fv.setRecreationRating((byte) 12);
		fv.setRiskRating((byte) 3);
		fv.setSocialRating((byte) 2);
		fv.setTimeConsumptionRating((byte) 4);
		category.setFeeling(fv);
		activity.setCategory(category);
//		activity.setByteId(g.nextBytes());
		activity.setName("Running at Spring Park");
		Place place = new Place();
		place.setName("Spring Park");
//		place.setByteId(g.nextBytes());
		place.setBusinessType("park");
		place.setType("park");
		Address address = new Address();
		address.setNumber(1432);
		address.setCity("Emerald City");
		address.setState("Wyoming");
		address.setStreet1("crystal place");
		address.setZip(12345);
		place.setAddress(address);
		activity.setPlace(place);
		activity.setWebsite("http://fubar.com");
		activity.setDescription("let's go running!");
		return Collections.singletonList(activity);
	}

}
