package fungle.funfinder.data.entity;

import org.junit.Assert;
import org.junit.Test;

import fungle.common.hdao.model.SimpleIDGenerator;
import fungle.funfinder.data.geo.GeoCoordinate;
import fungle.funfinder.data.util.JAXBUtil;

public class PlaceTest extends EntityTest<Place> {
	
	@Test
	public void jaxb2() {
		Address address = Address.US("5289 Palm Lane", "Wok, Illinois 85235");
		Place place = new Place().coordinate(GeoCoordinate.NEWYORK).description("a place").name("somewhere")
				.type("mytype").businessType("business").address(address);
		String text = JAXBUtil.marshal(place);
		Place recovered = JAXBUtil.<Place>unmarshal(text);
		Assert.assertEquals(place, recovered);
		place.setByteId(SimpleIDGenerator.getInstance(Place.class).nextBytes());
		text = JAXBUtil.marshal(place);
		recovered = JAXBUtil.<Place>unmarshal(text);
		Assert.assertEquals(place, recovered);
		Assert.assertEquals(place.getByteId(), recovered.getByteId());
	}
	
	@Test
	public void construct() {
		Place p = new Place(Address.US("432 Elm Ln", "Duke, NY 52538"),"my place","it rocks!");
		p.businessType("web building").setCoordinate(GeoCoordinate.NEWYORK);
		Place clone = new Place(p);
		Assert.assertEquals(p,clone);
		Assert.assertEquals(p.hashCode(),clone.hashCode());
		clone.coordinate(GeoCoordinate.DC);
		Assert.assertNotSame(p,clone);
		Assert.assertNotSame(p.hashCode(),clone.hashCode());
		
	}

	@Override
	public Place getMinimalEntity() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Place getFullEntity() {
		// TODO Auto-generated method stub
		return null;
	}

}
