package fungle.funfinder.data.geo;

import java.math.BigDecimal;

import org.junit.Assert;

public class GeoCoordinateTest {
	
	
	
	@org.junit.Test
	public void construct() {
		GeoCoordinate c = new GeoCoordinate(30,59);
		GeoCoordinate c2 = new GeoCoordinate(c.getLatitude(),c.getLongitude());
		GeoCoordinate c3 = new GeoCoordinate(c);
		Assert.assertEquals(c,c2);
		Assert.assertEquals(c,c3);
		c2.setLatitude(c2.getLatitude().add(new BigDecimal(10)));
		Assert.assertNotSame(c,c2);
		Assert.assertEquals(c,c3);
		Assert.assertEquals(c.getLatitude(),new BigDecimal(30));
		Assert.assertEquals(new BigDecimal(40),c2.getLatitude());
	}
	
	@org.junit.Test
	public void bigDecTest() {
		BigDecimal b = new BigDecimal(10);
		BigDecimal c = b.add(new BigDecimal(0));
		Assert.assertEquals(b,c);
		// we want them to be separate instances though.
		Assert.assertFalse(b==c);
	}
}
