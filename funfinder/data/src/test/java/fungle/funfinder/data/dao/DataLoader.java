package fungle.funfinder.data.dao;

import java.util.Map;

import fungle.common.core.util.FlatFileORM;
import fungle.funfinder.data.FeelingVector;
import fungle.funfinder.data.entity.Activity;
import fungle.funfinder.data.entity.ActivityType;
import fungle.funfinder.data.entity.Address;
import fungle.funfinder.data.entity.Event;
import fungle.funfinder.data.entity.Place;
import fungle.funfinder.data.entity.User;
import fungle.funfinder.data.geo.GeoCoordinate;

public final class DataLoader {
	private DataLoader() {

	}

	public static Map<String, Address> buildAddresses() {
		return FlatFileORM.newInstanceWithoutReferences().loadFromClasspath(
				"addresses.txt", Address.class);
	}

	public static Map<String, FeelingVector> buildFeelings() {
		return FlatFileORM.newInstanceWithoutReferences().loadFromClasspath(
				"feelings.txt", FeelingVector.class);
	}

	public static Map<String, GeoCoordinate> buildCoordinates() {
		return FlatFileORM.newInstanceWithoutReferences().loadFromClasspath(
				"coordinates.txt", GeoCoordinate.class);
	}

	public static Map<String, User> buildUsers(Map<String, Address> addresses,Map<String,GeoCoordinate> coordinates) {
		Map<String, User> users = FlatFileORM.builder()
				.addMapping(Address.class, addresses)
				.addMapping(GeoCoordinate.class, coordinates).build()
				.loadFromClasspath("users.txt", User.class);
		return users;
	}

	public static Map<String, Place> buildPlaces(Map<String,User> users,
			Map<String, Address> addresses,Map<String,GeoCoordinate> coordinates) {
		Map<String, Place> places = FlatFileORM.builder()
				.addMapping(User.class, users)
				.addMapping(Address.class, addresses)
				.addMapping(GeoCoordinate.class, coordinates).build()
				.loadFromClasspath("places.txt", Place.class);
		return places;
	}

	public static Map<String, ActivityType> buildActivityTypes(Map<String,User> users,Map<String, FeelingVector> feelings) {
		Map<String, ActivityType> places = FlatFileORM.builder()
				.addMapping(User.class, users)
				.addMapping(FeelingVector.class, feelings)
				.build()
				.loadFromClasspath("activityTypes.txt", ActivityType.class);
		return places;
	}

	public static Map<String, Activity> buildActivities(Map<String,User> users,
			Map<String, ActivityType> types, Map<String, Place> places) {
		Map<String, Activity> activities = FlatFileORM.builder()
				.addMapping(ActivityType.class, types).build()
				.loadFromClasspath("activities.txt", Activity.class);
		return activities;
	}

	public static Map<String, Event> buildEvents(Map<String,User> users,Map<String, Activity> activities) {
		Map<String, Event> events = FlatFileORM.builder()
				.addMapping(User.class, users)
				.addMapping(Activity.class, activities)
				.build()
				.loadFromClasspath("events.txt", Event.class);
		return events;
	}

	public static void main(String[] args) {
		Map<String, GeoCoordinate> adds = buildCoordinates();
		for (GeoCoordinate a : adds.values()) {
			System.out.println(a);
		}
	}
}
