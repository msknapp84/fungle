package fungle.funfinder.data.entity;

import org.junit.Assert;
import org.junit.Test;

import fungle.common.hdao.model.SimpleIDGenerator;
import fungle.funfinder.data.FeelingVector;
import fungle.funfinder.data.util.JAXBUtil;

public class ActivityTypeTest extends EntityTest<ActivityType> {

	@Test
	public void jaxb() {
		FeelingVector feeling = new FeelingVector().expenseRating(2)
				.outdoorsy(true).socialRating(9).recreationRating(7);
		ActivityType type = new ActivityType("fubar").feeling(feeling).id(SimpleIDGenerator.getInstance(ActivityType.class)
				.nextBytes());
		String text = JAXBUtil.marshal(type);
		ActivityType recovered = JAXBUtil.unmarshal(text);
		Assert.assertEquals(type,recovered);
	}

	@Test
	public void test() {
		ActivityType at = new ActivityType("foo", new FeelingVector());
		ActivityType t2 = new ActivityType(at);
		Assert.assertEquals(at, t2);
		Assert.assertEquals(at.hashCode(), t2.hashCode());
		t2.setName("bar");
		Assert.assertNotSame(at, t2);
		Assert.assertNotSame(at.hashCode(), t2.hashCode());
	}

	@Override
	public ActivityType getMinimalEntity() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ActivityType getFullEntity() {
		// TODO Auto-generated method stub
		return null;
	}

}
