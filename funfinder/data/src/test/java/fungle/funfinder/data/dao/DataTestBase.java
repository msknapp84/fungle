package fungle.funfinder.data.dao;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fungle.common.hdao.util.TableNameService;

@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:funfinder.data-appContext.xml" })
public class DataTestBase {
	private static boolean initialized = false;

	@Autowired
	protected ApplicationContext appContext;

	@Autowired
	protected TableNameService tableNameService;

	@Before
	public void lazyInit() {
		if (doInit()) {
			if (!initialized) {

				DataInitializer.lazyInit(appContext);
				initialized = true;
			}
		}
	}

	public boolean doInit() {
		return true;
	}
}