package fungle.funfinder.data.entity;

import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import fungle.funfinder.data.util.AvroSerializer;

public abstract class EntityTest<T> {

	@Test
	@Ignore("worry about this when I actually start using avro.")
	public void avroMarshalling() {
		avroMarshalling(getMinimalEntity());
		avroMarshalling(getFullEntity());
	}
	
	public void avroMarshalling(T entity) {
		String serialized = AvroSerializer.serializeToString(Collections.singleton(entity));
		@SuppressWarnings("unchecked")
		List<T> recovered = (List<T>) AvroSerializer.deserializeFromString(serialized,entity.getClass());
		Assert.assertEquals(entity,recovered.iterator().next());
	}
	
	public abstract T getMinimalEntity();
	public abstract T getFullEntity();
}
