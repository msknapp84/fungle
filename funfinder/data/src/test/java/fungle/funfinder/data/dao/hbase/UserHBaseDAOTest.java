package fungle.funfinder.data.dao.hbase;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fungle.common.hdao.model.OnError;
import fungle.common.hdao.util.CascadeUtils;
import fungle.funfinder.data.dao.UserDAO;
import fungle.funfinder.data.entity.User;

@ActiveProfiles("integration")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:funfinder.data-appContext.xml" })
public class UserHBaseDAOTest {

	@Autowired
	public UserDAO userDAO;

	@Test
	public void test() {
		User user = new User();
		user.setEmail("dude@email.com");
		user.setUserName("dude");
		try {
			userDAO.deleteUsingIndex(userDAO.getDefinition()
					.getIndexByFieldName("email"), CascadeUtils.never(),
					OnError.CONTINUE, user);
			userDAO.save(user, CascadeUtils.never(), OnError.ATTEMPT_UNDO);

			User t = userDAO.getByEmail("foo@email.com");
			Assert.assertNull(t);
			t = userDAO.getByUserName("bogus");
			Assert.assertNull(t);

			User rec = userDAO.getByEmail(user.getEmail());
			Assert.assertEquals(user, rec);
			rec = userDAO.getByUserName("dude");
			Assert.assertEquals(user, rec);
		} finally {
			userDAO.deleteUsingIndex(userDAO.getDefinition()
					.getIndexByFieldName("email"), CascadeUtils.never(),
					OnError.CONTINUE, user);
		}
	}
}