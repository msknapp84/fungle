package fungle.funfinder.data.entity;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import fungle.funfinder.data.util.JAXBUtil;

public class UserTest extends EntityTest<User> {

	@Override
	public User getMinimalEntity() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getFullEntity() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Test
	public void jaxb() {
		Address address= Address.US("253 Blah ln", "langley, Shmo 25321");
		User user = new User().lastName("Potter").firstName("Harry").dateOfBirth(new Date()).defaultId()
				.male(true).encryptedPassword("fubar").address(address);
		String text = JAXBUtil.marshal(user);
		User recovered = JAXBUtil.unmarshal(text);
		recovered.encryptedPassword(user.getEncryptedPassword());
		Assert.assertEquals(user,recovered);
	}

}
