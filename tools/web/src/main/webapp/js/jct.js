fungle = {
	tools: {
		toggle: function(image) {
			var img = $(image);
			if (img.attr('src') == "icons/arrow2_s.gif") {
				img.attr('src','icons/arrow2_e.gif');
			} else {
				img.attr('src','icons/arrow2_s.gif');
			}
			var trgt = img.attr('id').replace('Img','');
			$("#"+trgt).slideToggle();
		},
		submit: function(event) {
			// they are submitting a new form.
			
			
			return false;
		},
		retrieve: function(event) {
			// they want to retrieve a previous submission.
			
			
			return false;
		}
	}
};

$(function() {
	// hide warning to enable javascript
	$('.warning').hide();
	
	// add expand collapse buttons
	$('#descriptionHeader').each(function(index,item){
		var img = "<img onclick='fungle.tools.toggle(this)' src='icons/arrow2_s.gif' id='descriptionImg' class='arrowToggle'/>";
		$(item).prepend(img);
	});
	$('#typesHeader').each(function(index,item){
		var img = "<img onclick='fungle.tools.toggle(this)' src='icons/arrow2_s.gif' id='typesImg' class='arrowToggle' />";
		$(item).prepend(img);
	});
	
	// when somebody submits the form, let's intercept and use ajax.
	$('#submitForm').submit(fungle.tools.submit);
	// when somebody wants to see a past submission, let's intercept.
	$('#retrieveForm').submit(fungle.tools.retrieve);
});