package fungle.tools.resource;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fungle.tools.dao.TransformRequestDAO;
import fungle.tools.schema.ConversionRequests;

@Service
@Path("tool")
public class ToolResource {

	private TransformRequestDAO transformRequestDAO;
	
	@Autowired
	public ToolResource(TransformRequestDAO transformRequestDAO) {
		this.transformRequestDAO=transformRequestDAO;
	}
	
	@POST
	@Path(value = "request")
	public Response createRequest(ConversionRequests requests) {
		return null;
	}
	
	@GET
	@Path(value="request/{requestKey}")
	public Response getRequest(@PathParam(value = "requestKey") byte[] requestKey, byte[] password) {
		return null;
	}
	
	@DELETE
	@Path(value="request/{requestKey}")
	public Response deleteRequest(@PathParam(value = "requestKey") byte[] requestKey, byte[] password) {
		return null;
	}
}
