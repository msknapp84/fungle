package fungle.tools.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fungle.tools.entity.TransformRequest;

/**
 * Hibernate implementation
 * @author cloudera
 */
@Component("transformRequestDAO")
public class TransformRequestHDAO implements TransformRequestDAO {

	private SessionFactory sessionFactory;
	
	@Autowired
	public TransformRequestHDAO(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	@Transactional
	public byte[] create(TransformRequest request) {
		sessionFactory.getCurrentSession().save(request);
		return request.getRequestKey();
	}

	@Override
	@Transactional
	public TransformRequest read(byte[] requestKey) {
		TransformRequest req = (TransformRequest) sessionFactory.getCurrentSession().get(TransformRequest.class, requestKey);
		return req;
	}

	@Override
	@Transactional
	public TransformRequest update(TransformRequest request) {
		sessionFactory.getCurrentSession().update(request);
		return request;
	}

	@Override
	@Transactional
	public boolean delete(byte[] requestKey) {
		sessionFactory.getCurrentSession().delete(requestKey);
		return true;
	}
}