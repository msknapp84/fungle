package fungle.tools.dao;

import fungle.tools.entity.TransformRequest;

public interface TransformRequestDAO {
	// CRUD operations
	byte[] create(TransformRequest request);
	TransformRequest read(byte[] requestKey);
	TransformRequest update(TransformRequest request);
	boolean delete(byte[] requestKey);
}