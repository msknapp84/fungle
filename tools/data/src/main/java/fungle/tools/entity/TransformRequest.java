package fungle.tools.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity()
@Table(name = "request")
public class TransformRequest {
	private byte[] requestKey;
	private String fromLanguage;
	private String toLanguage;
	private byte[] password;
	private String salt;
	private boolean ready;
	private String filepath;
	private String packageName;
	private Date requestTime;
	private Set<TransformRequestField> fields = new HashSet<>();

	public TransformRequest() {

	}

	@Column(name = "requestKey")
	public byte[] getRequestKey() {
		return requestKey;
	}

	public void setRequestKey(byte[] requestKey) {
		this.requestKey = requestKey;
	}

	@Column(name = "fromLanguage")
	public String getFromLanguage() {
		return fromLanguage;
	}

	public void setFromLanguage(String fromLanguage) {
		this.fromLanguage = fromLanguage;
	}

	@Column(name = "toLanguage")
	public String getToLanguage() {
		return toLanguage;
	}

	public void setToLanguage(String toLanguage) {
		this.toLanguage = toLanguage;
	}

	@Column(name = "password")
	public byte[] getPassword() {
		return password;
	}

	public void setPassword(byte[] password) {
		this.password = password;
	}

	@Column(name = "salt")
	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	@Column(name = "ready")
	public boolean isReady() {
		return ready;
	}

	public void setReady(boolean ready) {
		this.ready = ready;
	}

	@Column(name = "filepath")
	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	@Column(name = "package")
	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	@Column(name = "requesttime")
	public Date getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(Date requestTime) {
		this.requestTime = requestTime;
	}

	@OneToMany()
	@JoinColumn(name = "requestKey")
	public Set<TransformRequestField> getFields() {
		return fields;
	}

	public void setFields(Set<TransformRequestField> fields) {
		this.fields = fields;
	}

}
