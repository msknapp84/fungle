create table request (requestKey BINARY(32) PRIMARY KEY, 
fromLanguage ENUM('java','python','sql','javascript','avro','xsd'),
toLanguage ENUM('java','python','sql','javascript','avro','xsd'), 
targetStyle ENUM('POJO','immutable','factory','builder','hibernateEntity','jaxb2POJO','wrapper','interface','hadoopPOJO'),
password BINARY(64),
salt CHAR(4),
ready BOOLEAN,
filepath VARCHAR(255) UNIQUE,
finishedFilepath VARCHAR(255) UNIQUE,
package VARCHAR(255), 
requesttime TIMESTAMP, 
KEY (filepath)) ENGINE InnoDB;

create table field (requestKey BINARY(32),name VARCHAR(64), mutable BOOLEAN,identifier BOOLEAN, 
encapsulation ENUM('private','protected','public','packageprivate'),targetType VARCHAR(255),
PRIMARY KEY (`requestKey`, `name`), FOREIGN KEY (`requestKey`) REFERENCES `request` (`requestKey`) on delete cascade) ENGINE InnoDB;

create view request_fields as 
select r.fromLanguage as `from`, r.toLanguage as `to`, r.ready as `ready`, f.name as `name`,f.mutable as `mutable`, 
f.identifier as `identifier`, f.encapsulation as `encapsulation`, f.targetType as `targetType`
from request r 
join field f
using (`requestKey`)
order by r.requesttime desc,`name` asc;

delimiter //

create procedure getRequest(IN requestKey BINARY(32))
BEGIN
	select * from request_fields rf where requestKey = rf.requestKey;
END//

create procedure deleteOld()
BEGIN
	delete from request_fields where requesttime < NOW() - INTERVAL 24 HOUR;
END//

delimiter ;

create event hourlyDeletion on SCHEDULE EVERY 1 HOUR STARTS '2010-01-01 00:00:00'  DO call deleteOld(); 